<?php

namespace App;

use App\Models\Role;
use App\Models\Post;
use App\Models\Group;
use App\Models\Pay;
use App\Models\Allpost;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'login', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role(){
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    public function post(){
        return $this->hasMany(Post::class, 'id', 'user_id');
    }

    public function group(){
        return $this->hasMany(Group::class, 'id', 'user_id');
    }

    public function pays(){
        return $this->hasMany(Pay::class, 'id', 'user_id');
    }

    public function allpost(){
        return $this->hasMany(AllPost::class, 'user_id', 'id');
    }

    public function allpostpublish(){
        return $this->allpost()->whereNotNull('vk_link');
    }

    public function allpoststriked(){
        return $this->allpost()->where('strike',1);
    }

    public function allpostcosts(){
        return $this->allpost()->where('strike',0);
    }
}
