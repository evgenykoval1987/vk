<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Tag;

class Taggroup extends Model
{
	public function tags(){
        return $this->hasMany(Tag::class, 'tag_group_id', 'id');
    }
}
