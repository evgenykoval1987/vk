<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CompanyPostToGroup;
use App\Models\CompanyPostAttachment;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use App\Models\Company;

class CompanyPost extends Model
{
    protected $table = 'company_posts';

    protected static function boot(){
	  	parent::boot();
	  	if (Auth::user()) {
	      	static::addGlobalScope('user_', function (Builder $builder) {
	          	$builder->orderBy('id', 'DESC');
	      	});
	    }
	}

    public function post_groups(){
        return $this->hasMany(CompanyPostToGroup::class, 'company_post_id', 'id');
    }

    public function attachments(){
    	return $this->hasMany(CompanyPostAttachment::class, 'company_post_id', 'id');
  	}

    public function company(){
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
}
