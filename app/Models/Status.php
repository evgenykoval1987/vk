<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public function post(){
        return $this->hasMany(Post::class, 'id', 'group_id');
    }
}
