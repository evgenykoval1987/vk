<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CompanyPost;
use App\Models\Group;

class CompanyPostToGroup extends Model
{
    protected $table = 'company_posts_to_groups';
    protected $fillable = [
        'vk_id', 'vk_link', 'status'
    ];
    //protected $primaryKey = ['company_post_id', 'group_id'];
	public $incrementing = false;

    public function post(){
    	return $this->belongsTo(CompanyPost::class, 'company_post_id', 'id');
  	}

  	public function group(){
  		return $this->belongsTo(Group::class, 'group_id', 'id');
  	}
}
