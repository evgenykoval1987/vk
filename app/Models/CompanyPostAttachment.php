<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CompanyPost;
use Illuminate\Database\Eloquent\Builder;

class CompanyPostAttachment extends Model
{
    protected $table = 'company_post_attachments';
    public $timestamps = false;

    protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('company_post_attachments', function (Builder $builder) {
      		$builder->orderBy('sort_order', 'ASC');
    	});
	  }

    public function post(){
    	return $this->belongsTo(CompanyPost::class, 'company_post_id');
  	}
}
