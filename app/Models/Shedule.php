<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Group;

class Shedule extends Model
{
	protected $table = 'shedule';
	public $timestamps = false;

    public function group(){
		return $this->belongsTo(Group::class, 'group_id', 'id');
	}

	public function scopeOrdered($query){
	    return $query->orderBy('value', 'asc');
	}

	public function scopeType($query, $type){
	    return $query->where('type', $type);
	}

	public function scopeOverNow($query, $time){
	    return $query->where('value', '>', $time)->orderBy('value', 'asc');
	}

	public function getValueAttribute($value){
	    return date('H:i', strtotime($value));
	}
}
