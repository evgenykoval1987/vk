<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Client;
use Illuminate\Database\Eloquent\Builder;
use App\Models\CompanyPost;
use App\Models\CompanyToTag;

class Company extends Model
{
    protected $table = 'company';

    protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('company', function (Builder $builder) {
      		$builder->orderBy('created_at', 'DESC');
    	});
	}

  public function client(){
   	return $this->hasOne(Client::class, 'id', 'client_id');
  }

  public function companyposts(){
    return $this->hasMany(CompanyPost::class, 'company_id', 'id');
  }

  public function tags(){
    return $this->hasMany(CompanyToTag::class, 'company_id', 'id');
  }
}
