<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use App\Models\PostToPay;
use App\User;

class Pay extends Model
{
    protected static function boot(){
	  	parent::boot();
	  	if (Auth::user()) {
	  		if (Auth::user()->role->slug != 'admin'){
		    	static::addGlobalScope('user', function (Builder $builder) {
		      		$builder->where('user_id', '=', Auth::user()->id);
		    	});
		    }
	     
	      	static::addGlobalScope('user_', function (Builder $builder) {
	          	$builder->orderBy('id', 'DESC');
	      	});
	    }
	}

	public function posts(){
		return $this->hasMany(PostToPay::class, 'pay_id', 'id');
	}

	public function user(){
    	return $this->hasOne(User::class, 'id', 'user_id');
  	}
}
