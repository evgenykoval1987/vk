<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use App\Models\Group;

class PackageGroup extends Model
{
    protected $table = 'packages_groups';
    public $timestamps = false;

    public function group(){
        return $this->belongsTo(Group::class, 'group_id', 'id');
    }
}
