<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Client;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Manager;

class Reminder extends Model
{

	protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('message_orders', function (Builder $builder) {
      		$builder->orderBy('dt', 'DESC');
    	});
	  }

    public function client(){
    	return $this->belongsTo(Client::class, 'client_id', 'id');
  	}

  	public function manager(){
	    return $this->belongsTo(Manager::class, 'manager_id', 'id');
	}
}
