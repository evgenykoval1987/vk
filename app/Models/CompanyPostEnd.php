<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CompanyPost;
use App\Models\Group;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class CompanyPostEnd extends Model
{
    protected $table = 'company_post_ends';

    protected static function boot(){
	  	parent::boot();
	  	
    	static::addGlobalScope('dt', function (Builder $builder) {
      		$builder->where('dt', '<=', Carbon::now()->subDays(1));
    	});
		  
	}
    
    /*public function post(){
    	return $this->belongsTo(CompanyPost::class, 'id', 'company_post_id');
  	}

  	public function group(){
  		return $this->belongsTo(Group::class, 'group_id', 'id');
  	}*/
}
