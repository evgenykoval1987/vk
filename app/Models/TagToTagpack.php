<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tag;
use App\Models\Tagpack;

class TagToTagpack extends Model
{
    protected $table = 'tag_to_tagpack';

    public function tag(){
    	return $this->belongsTo(Tag::class, 'tag_id', 'id');
  	}

  	/*public function post(){
    	return $this->hasOne(Post::class, 'id', 'post_id');
  	}*/
}
