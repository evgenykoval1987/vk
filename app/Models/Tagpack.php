<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\TagToTagpack;

class Tagpack extends Model
{
	public function tags(){
        return $this->hasMany(TagToTagpack::class, 'tagpack_id', 'id');
    }
}
