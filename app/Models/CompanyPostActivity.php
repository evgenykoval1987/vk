<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CompanyPost;
use Illuminate\Database\Eloquent\Builder;

class CompanyPostActivity extends Model
{
    protected $table = 'company_posts_activity';

    public function post(){
    	return $this->belongsTo(CompanyPost::class, 'company_post_id');
  	}
}
