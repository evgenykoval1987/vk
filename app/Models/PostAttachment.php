<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Post;
use Illuminate\Database\Eloquent\Builder;

class PostAttachment extends Model
{
    protected $table = 'post_attachments';
    public $timestamps = false;

    protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('post_attachments', function (Builder $builder) {
      		$builder->orderBy('sort_order', 'ASC');
    	});
	  }

    public function post(){
    	return $this->belongsTo(Post::class, 'post_id');
  	}
}
