<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Manager;
use App\Models\Company;
use App\Models\CompanyReorder;

class Client extends Model
{
  protected static function boot() {
    parent::boot();
    static::addGlobalScope('client', function (Builder $builder) {
    		$builder->orderBy('created_at', 'DESC');
  	});
	}

	public function manager(){
    return $this->belongsTo(Manager::class, 'manager_id', 'id');
  }

  public function company(){
    return $this->hasMany(Company::class, 'client_id', 'id');
  }

  public function companyr(){
    return $this->hasMany(CompanyReorder::class, 'client_id', 'id');
  }
}
