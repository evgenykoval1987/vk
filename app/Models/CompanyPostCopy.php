<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CompanyPostToGroup;
use App\Models\CompanyPostAttachment;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use App\Models\Company;

class CompanyPostCopy extends Model
{
    protected $table = 'company_posts_copy';

    public function company(){
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
}
