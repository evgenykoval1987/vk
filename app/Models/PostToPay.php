<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pay;
use App\Models\Post;

class PostToPay extends Model
{
    protected $table = 'post_to_pays';
    public $timestamps = false;

    public function pay(){
    	return $this->belongsTo(Pay::class, 'id', 'pay_id');
  	}

  	public function post(){
    	return $this->hasOne(Post::class, 'id', 'post_id');
  	}
}
