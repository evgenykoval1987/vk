<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Group;
use App\Models\Status;
use App\Models\PostToPay;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class Post extends Model
{
	protected static function boot(){
  	parent::boot();
  	if (Auth::user()) {
  		if (Auth::user()->role->slug != 'admin'){
	    	static::addGlobalScope('user', function (Builder $builder) {
	      		$builder->where('user_id', '=', Auth::user()->id);
	    	});
	    }
      
      static::addGlobalScope('user_', function (Builder $builder) {
          $builder->orderBy('id', 'DESC');
      });
    }
	}

  public function user(){
    return $this->hasOne(User::class, 'id', 'user_id');
  }

	public function group(){
    return $this->belongsTo(Group::class, 'group_id', 'id');
  }

  public function status(){
    return $this->hasOne(Status::class, 'id', 'status_id');
  }

  public function scopeTerm($query){
  	return $query->where('status_id', 2);
	}

	public function scopeNoterm($query){
  	return $query->where('status_id', 1);
	}

  public function scopeLastEnded($query){
    return $query->whereIn('status_id', [3])->orderBy('publish_date', 'DESC');
  }

  public function scopeError($query){
    return $query->where('status_id', 4);
  }

  public function scopeEarlier($query){
    return $query->where('publish_date', '<=', date('Y-m-d H:i:s'));
  }

  /*public function getPublishDateAttribute($value){
    return date('d.m.Y H:i', strtotime($value));
  }*/

  public function attachments(){
    return $this->hasMany(PostAttachment::class, 'post_id', 'id');
  }

  public function posttopay(){
    return $this->hasOne(PostToPay::class, 'post_id', 'id');
  }
}
