<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Group;
use Illuminate\Database\Eloquent\Builder;

class Message extends Model
{

	protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('message_orders', function (Builder $builder) {
      		$builder->orderBy('vk_dt', 'DESC');
    	});
	  }

    public function group(){
    	return $this->belongsTo(Group::class, 'group_id', 'id');
  	}
}
