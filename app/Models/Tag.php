<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Taggroup;

class Tag extends Model
{
    protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('client', function (Builder $builder) {
      		$builder->orderBy('name', 'ASC');
    	});
	}

	public function taggroup(){
    	return $this->belongsTo(Taggroup::class, 'tag_group_id', 'id');
  	}
}
