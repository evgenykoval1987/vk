<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Video extends Model
{
    protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('video', function (Builder $builder) {
      		$builder->orderBy('created_at', 'DESC');
    	});
	}

	
}
