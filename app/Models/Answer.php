<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Answer extends Model
{
    protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('answer', function (Builder $builder) {
      		$builder->orderBy('created_at', 'DESC');
    	});
	}

	
}
