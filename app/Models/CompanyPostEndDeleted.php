<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CompanyPost;
use App\Models\Group;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class CompanyPostEndDeleted extends Model
{
    protected $table = 'company_post_end_deleted';

}
