<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class UserShedule extends Model
{
	protected $table = 'users_shedules';
	public $timestamps = false;

    protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('chat', function (Builder $builder) {
      		$builder->orderBy('value', 'ASC');
    	});
	}

	
}
