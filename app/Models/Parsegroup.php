<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Parseresult;

class Parsegroup extends Model
{
    public $timestamps = false;

    public function parseresult(){
        return $this->hasMany(Parseresult::class, 'parsegroup_id', 'id');
    }
}
