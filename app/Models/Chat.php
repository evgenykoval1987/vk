<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Chat extends Model
{
	protected $table = 'chat';

    protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('chat', function (Builder $builder) {
      		$builder->orderBy('created_at', 'ASC');
    	});
	}

	
}
