<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class News extends Model
{
	protected $table = 'news';

    protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('news', function (Builder $builder) {
      		$builder->orderBy('created_at', 'DESC');
    	});
	}

	
}
