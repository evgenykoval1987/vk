<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use App\Models\PackageGroup;

class Package extends Model
{
	public function packagegroups(){
		return $this->hasMany(PackageGroup::class, 'package_id', 'id');
	}
}
