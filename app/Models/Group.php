<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Post;
use App\Models\Shedule;
use App\Models\Message;
use App\Models\Sentence;
use App\Models\CompanyPostToGroup;
use App\User;
use App\Models\PackageGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use App\Models\UserShedule;

class Group extends Model
{
	/*protected static function boot(){
	  	parent::boot();
	  	if (Auth::user()) {
	  		if (Auth::user()->role->slug == 'admin' && Auth::user()->id != 3){
		    	static::addGlobalScope('user', function (Builder $builder) {
		      		$builder->where('user_id', '=', Auth::user()->id);
		    	});
		    }
	    }
	}*/

    public function post(){
        return $this->hasMany(Post::class, 'group_id', 'id');
    }

    public function user(){
    	return $this->hasOne(User::class, 'id', 'user_id');
  	}

  	public function shedule(){
        return $this->hasMany(Shedule::class, 'group_id', 'id');
    }

    public function company_group_post(){
        return $this->hasMany(CompanyPostToGroup::class, 'group_id', 'id');
    }

    public function messages(){
        return $this->hasMany(Message::class, 'group_id', 'id');
    }

    public function sentences(){
        return $this->hasMany(Sentence::class, 'group_id', 'id');
    }

    public function packages_groups(){
        return $this->hasMany(PackageGroup::class, 'group_id', 'id');
    }

    public function usershedule(){
        return $this->hasMany(UserShedule::class, 'group_id', 'id');
    }
}
