<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Logger extends Model
{

	protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('logger_order', function (Builder $builder) {
      		$builder->orderBy('created_at', 'DESC');
    	});
	}
}
