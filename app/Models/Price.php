<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Price extends Model
{

	public $timestamps = false;
	
    protected static function boot(){
	  	parent::boot();
      	static::addGlobalScope('price', function (Builder $builder) {
          	$builder->orderBy('value', 'ASC');
      	});
	}
}
