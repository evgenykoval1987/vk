<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Group;
use Illuminate\Database\Eloquent\Builder;

class Sentence extends Model
{
	protected $fillable = ['status', 'status_dt'];

    protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('sentence_orders', function (Builder $builder) {
      		$builder->orderBy('created_at', 'DESC');
    	});
	  }

    public function group(){
    	return $this->belongsTo(Group::class, 'group_id', 'id');
  	}
}
