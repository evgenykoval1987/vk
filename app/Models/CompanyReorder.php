<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Client;
use Illuminate\Database\Eloquent\Builder;

class CompanyReorder extends Model
{
    protected $table = 'company';

    protected static function boot() {
      parent::boot();
      static::addGlobalScope('client', function (Builder $builder) {
          $builder->orderBy('created_at', 'ASC');
      });
    }

    public function client(){
    	return $this->hasOne(Client::class, 'id', 'client_id');
  	}
}
