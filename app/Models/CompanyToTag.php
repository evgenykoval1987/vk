<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tag;
use App\Models\Company;

class CompanyToTag extends Model
{
    protected $table = 'company_to_tag';

    public function tag(){
    	return $this->belongsTo(Tag::class, 'tag_id', 'id');
  	}

  	public function company(){
    	return $this->belongsTo(Company::class, 'company_id', 'id');
  	}

  	/*public function post(){
    	return $this->hasOne(Post::class, 'id', 'post_id');
  	}*/
}
