<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Manager extends Model
{
    protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('client', function (Builder $builder) {
      		$builder->orderBy('created_at', 'DESC');
    	});
	}

	
}
