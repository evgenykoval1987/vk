<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Manager;
use App\Models\Company;
use App\Models\Сдшуте;

class ClientLast extends Model{
  protected static function boot() {
    parent::boot();
    static::addGlobalScope('client', function (Builder $builder) {
    		$builder->orderBy('created_at', 'DESC');
  	});
	}

	public function client(){
    return $this->belongsTo(Client::class, 'client_id', 'id');
  }

  	/*public function company(){
    	return $this->hasMany(Company::class, 'client_id', 'id');
  	}*/
}
