<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Monitoring extends Model
{
    protected $table = 'monitoring';

    protected static function boot() {
	    parent::boot();
	    static::addGlobalScope('monitoring', function (Builder $builder) {
      		$builder->orderBy('vk_dt', 'DESC');
    	});
	}
}
