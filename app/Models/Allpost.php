<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Group;
use App\Models\Status;
use App\Models\PostToPay;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class Allpost extends Model
{
	 protected $table = 'posts';

   public function user(){
    return $this->hasOne(User::class, 'user_id', 'id');
  }
}
