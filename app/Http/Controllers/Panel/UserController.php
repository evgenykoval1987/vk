<?php
namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use App\Models\Group;
use App\Models\UserShedule;
use App\Models\Shedule;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('users'),
    		'title' => 'Пользователи'
    	];

    	$data['success'] = false;
    	if ($request->session()->get('user_added')){
    		$data['success'] = $request->session()->pull('user_added');
    	}

        return view('panel.user.list')->with('data',$data);
    }

    public function ajax(Request $request, User $user){
    	$users = [];
    	$results = User::all();
    	
    	foreach ($results as $key => $result) {
    		$users[] = [
    			$result->id,
    			$result->name,
    			$result->login,
    			$result->role->name,
    			date('d.m.Y', $result->created_at->timestamp),
    			'<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a><a href="'.route('users-edit',$result->id).'" class="on-default edit-row" style="margin-left: 10px"><i class="fa fa-pencil"></i></a>'
    			/*'<a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a>'*/
    		];
    	}

    	echo json_encode(['aaData' => $users]);
    }

    public function addForm(Role $role){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('users'),
    		'title' => 'Пользователи'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('users-add'),
    		'title' => 'Добавить'
    	];

    	$data['roles'] = $role->all();

        $data['title'] = 'VK.POSTER - Добавить пользователя';
        $data['h1'] = 'Добавить пользователя';

        $data['name'] = '';
        $data['login'] = '';
        $data['role_id'] = '';
        $data['wallet'] = '';
        $data['vk_id'] = '';
        $data['vk_account'] = '';
        $data['id'] = 0;
        $data['action'] = route('users-save');

    	return view('panel.user.add')->with('data',$data);
    }

    public function editForm(Request $request, $user_id){
        $data = [];

        $data['breadcrumbs'][] = [
            'link' => route('users'),
            'title' => 'Пользователи'
        ];

        $data['breadcrumbs'][] = [
            'link' => route('users-edit',$user_id),
            'title' => 'Редактировать'
        ];

        $data['roles'] = Role::all();

        $user_info = User::find($user_id);

        $data['title'] = 'VK.POSTER - Редактировать пользователя';
        $data['h1'] = 'Редактировать пользователя '.$user_info->name;

        $data['name'] = $user_info->name;
        $data['login'] = $user_info->login;
        $data['role_id'] = $user_info->role_id;
        $data['wallet'] = $user_info->wallet;
        $data['vk_id'] = $user_info->vk_id;
        $data['id'] = $user_id;
        $data['vk_account'] = "https://vk.com/id".$user_info->vk_id;
        $data['action'] = route('users-save-edit',$user_id);

        return view('panel.user.add')->with('data',$data);
    }

    public function addUser(Request $request){
    	$user = new User;

	    $user->name = $request->name;
	    $user->login = $request->login;
	    $user->password = Hash::make($request->password);
	    $user->role_id = $request->role_id;
        $user->vk_id = $request->vk_id;
        $user->wallet = $request->wallet;

        $audio_token_user = User::where('role_id','1')->first();
        $user->vk_audio_token = $audio_token_user->vk_audio_token;
        $user->vk_audio_device_id = $audio_token_user->vk_audio_device_id;
        $user->vk_audio_secret = $audio_token_user->vk_audio_secret;

	    $user->save();

	    $request->session()->put('user_added', 'Пользователь <strong>'.$request->name.'</strong> добавлен');

	    return redirect(route('users'));
    }

    public function editUser(Request $request, $user_id){
        $user = User::find($user_id);

        $user->name = $request->name;
        $user->login = $request->login;
        if ($request->password)
            $user->password = Hash::make($request->password);
        $user->role_id = $request->role_id;
        $user->vk_id = $request->vk_id;
        $user->wallet = $request->wallet;

        $audio_token_user = User::where('role_id','1')->first();
        $user->vk_audio_token = $audio_token_user->vk_audio_token;
        $user->vk_audio_device_id = $audio_token_user->vk_audio_device_id;
        $user->vk_audio_secret = $audio_token_user->vk_audio_secret;

        $user->save();

        $request->session()->put('user_added', 'Пользователь <strong>'.$request->name.'</strong> изминен');

        return redirect(route('users'));
    }

    public function checkLogin(Request $request){
    	$flag = 'true';
    	$login = $request->post('login');

    	$user = User::where('login', $login)->first();
    	if ($user)
    		$flag = 'false';

    	return response($flag, 200)->header('Content-Type', 'application/json');
    }

    public function dropUser(Request $request){
    	$id = $request->post('id');

    	User::destroy($id);

    	return response('1')->header('Content-Type', 'text/html');
    }

    public function groups(Request $request, $user_id){
        $groups = [];

        $results = Group::all();

        foreach ($results as $key => $result) {
            $shedule = [];

            foreach ($result->shedule()->where('user_id', $user_id)->get() as $k => $shed) {
                $item = [
                    'id' => $shed->id,
                    'value' => date('H:i',strtotime($shed->value))
                ];

                
                $shedule[] = $item;
               
            }

            if (!$shedule)
                continue;

            $groups[$result->id] = [
                'id' => $result->id,
                'name' => $result->name,
                'shedule' => $shedule
            ];
        }

        return response()->json(['groups' => $groups]);
    }

    public function removeShedule(Request $request){
        $shedule = Shedule::find($request->id);
        $shedule->user_id = null;
        $shedule->save();

        return response()->json(['success' => true]);
    }

    public function addShedule(Request $request){
        $shedule = Shedule::find($request->shedule_id);
        $shedule->user_id = $request->user_id;
        $shedule->save();

        return response()->json(['success' => true]);
    }

    public function groupList(Request $request){
        $groups = [];

        $results = Group::all();

        foreach ($results as $key => $result) {
            $shedule = [];

            foreach ($result->shedule()->where('user_id', null)->where('type','usual')->get() as $k => $shed) {
                $item = [
                    'id' => $shed->id,
                    'value' => date('H:i',strtotime($shed->value))
                ];

                
                $shedule[] = $item;
               
            }

            if (!$shedule)
                continue;

            $groups[$result->id] = [
                'id' => $result->id,
                'name' => $result->name,
                'shedule' => $shedule
            ];
        }

        return response()->json(['groups' => $groups]);
    }
}