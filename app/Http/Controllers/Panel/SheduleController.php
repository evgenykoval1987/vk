<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Shedule;
use App\Models\Post;

class SheduleController extends Controller
{
    public function index(){
    	$data = [];

        $data['breadcrumbs'][] = [
            'link' => route('shedule'),
            'title' => 'Рассписание'
        ];

    	return view('panel.shedule.form')->with('data',$data);
    }

    public function groups(){
    	$groups = [];

    	$results = Group::all();

    	foreach ($results as $key => $result) {
    		$shedule = [];
    		$shedulea = [];
            $shedulen = [];
            $shedules = [];
    		foreach ($result->shedule()->ordered()->get() as $k => $shed) {
    			$item = [
    				'id' => $shed->id,
    				'value' => date('H:i',strtotime($shed->value))
    			];

    			if ($shed->type == 'usual')
    				$shedule[] = $item;
    			elseif ($shed->type == 'adv')
    				$shedulea[] = $item;
                elseif ($shed->type == 'native')
                    $shedulen[] = $item;
                else
                    $shedules[] = $item;
    		}
    		$groups[$result->id] = [
    			'id' => $result->id,
    			'name' => $result->name,
    			'shedule' => $shedule,
    			'shedulea' => $shedulea,
                'shedulen' => $shedulen,
                'shedules' => $shedules
    		];
    	}

    	return response()->json(['groups' => $groups]);
    }

    public function add(Request $request){
    	$shedule = new Shedule;

    	$shedule->group_id = $request->group_id;
    	$shedule->value = $request->value;
    	$shedule->type = $request->type;

    	$shedule->save();

    	return response()->json(['success' => true]);
    }

    public function remove(Request $request){
    	Shedule::destroy($request->id);

    	return response()->json(['success' => true]);
    }

    public function getByGroup(Request $request){
        $group_id = $request->group_id;

        $response = false;
        if ($request->user_shedule == "true"){
           $shedules = Group::find($group_id)->shedule()->type('usual')->where('user_id', $request->user_id)->overnow(date('H:i'))->get(); 
        }
        else{
            $shedules = Group::find($group_id)->shedule()->type('usual')->overnow(date('H:i'))->get(); 
        }
        //$shedules = Group::find($group_id)->shedule()->type('usual')->overnow(date('H:i'))->get(); 

        if ($shedules->count() > 0){
            foreach ($shedules as $key => $shedule) {
                if ($response) continue;
                $posts = Group::find($group_id)->post()->where('publish_date', date('Y-m-d').' '.$shedule->value)->get();

                if ($posts->count() == 0){
                    $response = date('d.m.Y').' '.$shedule->value;
                }

            }
        }

        if (!$response){
            if ($request->user_shedule == "true"){
                $shedules = Group::find($group_id)->shedule()->type('usual')->where('user_id', $request->user_id)->ordered()->get();
            }
            else{
                $shedules = Group::find($group_id)->shedule()->type('usual')->ordered()->get();
            }
            if ($shedules->count() > 0){
                $i = 1;
                while(!$response){
                    foreach ($shedules as $key => $shedule) {
                        if ($response) continue;
                        $posts = Group::find($group_id)->post()->where('publish_date', date('Y-m-d',strtotime('+'.$i.' day')).' '.$shedule->value)->get();

                        if ($posts->count() == 0){
                            $response = date('d.m.Y',strtotime('+'.$i.' day')).' '.$shedule->value;
                        }
                    }
                    $i++;
                }
            }
        }


        if ($response){
            return response()->json(['response' => $response, 'success' => true]);
        }
        else{
            return response()->json(['response' => 'Отсутствует расписания', 'success' => false]);
        }
    }
}
