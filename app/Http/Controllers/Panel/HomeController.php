<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Vodka\VKAudioToken\TokenReceiverOfficial;
use App\Vodka\VKAudioToken\CommonParams;
use App\Vodka\VKAudioToken\SupportedClients;
use App\Models\Pay;
use App\Models\PostToPay;
use App\Models\Post;
use App\Models\Client;
use App\Models\CompanyPost;
use App\Models\Company;
use App\Models\Monitoring;
use App\Models\Sentence;
use App\Models\Message;
use App\Models\Shedule;
use App\Models\Video;
use App\Models\News;
use App\Models\CompanyPostEndDeleted;
use App\Models\Stat;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$data = [];

        $ru_month = array( 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' );

        $data['items'] = [];
        $results = Stat::orderBy('sort', 'asc')->get();
        foreach ($results as $key => $result) {
           $data['items'][] = [
                'month' => $result->month,
                'p1' => $result->p1,
                'p2' => $result->p2,
                'p3' => $result->p3,
                'p4' => $result->p4,
                'p5' => $result->p5,
                'p6' => $result->p6,
                'p7' => $result->p7,
                'p8' => $result->p8,
                'p9' => $result->p9,
                'p10' => $result->p10,
           ];
        }
        /*$data['items'] = [];
        $months = $this->getMonthsInRange('01.06.2019', date('d.m.Y'));
        $months = array_reverse($months);
        foreach ($months as $key => $month) {
            $last_day = '';
            $date = new \DateTime($month['year'].'-'.$month['month'].'-01');
            $date->modify('last day of this month');
            $last_day = $date->format('d'); 
            

            $date_from = $month['year'].'-'.$month['month'].'-01';
            $date_to = $month['year'].'-'.$month['month'].'-'.$last_day;
            $json = [];
            $json['month'] = $month['year'].'-'.$month['month'];
            $json['month'] = $ru_month[(int)$month['month']-1];

            $clients_ids = [];
            $results = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get();
            foreach ($results as $key => $value) {
                if(!in_array($value->client_id, $clients_ids))
                    $clients_ids[] = $value->client_id;
            }

            $clients = Client::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->whereIn('id',$clients_ids)->get()->count();
            $sum = 0;
            $cls = Client::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->whereIn('id',$clients_ids)->get();
            foreach ($cls as $key => $cl) {
                
                    $sum += $cl->company->sum('total');
            }
            $json['p1'] = $clients." ( $sum )";


            $clients = Client::where('created_at','<', $date_from)->whereIn('id',$clients_ids)->get()->count();
            $sum = 0;
            $cls = Client::where('created_at','<', $date_from)->whereIn('id',$clients_ids)->get();
            foreach ($cls as $key => $cl) {
                $sum += $cl->company->where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->sum('total');
            }
            $json['p2'] = $clients." ( $sum )";

            $crm = 0;
            $results = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get();
            foreach ($results as $key => $result) {
                $count = 0;
                $posts = CompanyPost::where('company_id', '=', $result['id'])->where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get();
                foreach ($posts as $ke => $post) {
                    foreach ($post->post_groups as $k => $post_group) {
                        $count += $post_group->viewed;
                    }
                }
                if ($count != 0)
                    $crm += ($result->total/$count)*1000;
            }
            $company_count = $results = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get()->count();
            if ($company_count != 0){
                $crm = $crm/$company_count;
            }
            else
                $crm = 0;
            $json['p7'] = number_format($crm,2,'.',' ');

            $clients = [];
            $total = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->sum('total');
            $results = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get();
            foreach ($results as $key => $result) {
                if(!in_array($result->client_id, $clients) && $result->total != 0)
                    $clients[] = $result->client_id;
            }
            if (count($clients) != 0)
                $json['p8'] = number_format($total/count($clients),2,'.',' ');
            else
                $json['p8'] = 0;



            $total = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get()->count();
            $json['p9'] = $total;

            $json['p5'] = Monitoring::where('readed_dt','>=', $date_from)->where('readed_dt','<=', $date_to)->get()->count();
            $json['p4'] = Sentence::where('status_dt','>=', $date_from)->where('status_dt','<=', $date_to)->get()->count();
            $json['p3'] = Message::where('updated_at','>=', $date_from)->where('updated_at','<=', $date_to)->whereNotNull('answer')->get()->count();
            $json['p6'] = CompanyPostEndDeleted::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get()->count();
            $json['p10'] = Company::where('readed', 1)->where('readed_dt','>=', $date_from)->where('readed_dt','<=', $date_to)->get()->count();

            $data['items'][] = $json;
        }*/

        $data['leftposts'] = false;
        if (Auth::user()->role->slug == 'client')
            $data['items'] = [];
        if (Auth::user()->role->slug != 'admin' && Auth::user()->role->slug != 'client') {
            $data['items'] = [];

            $shedules = Shedule::where('type','usual')->get()->count();

            $last_day = '';
            $date = new \DateTime(date('Y-m-d'));
            $date->modify('last day of this month');
            $last_day = $date->format('d'); 
            $date_from = date('Y-m').'-01';
            $date_to = date('Y-m').'-'.$last_day;

            $shedules = $shedules * ($last_day - (date('d')-1));

            $posts = Post::whereNotNull('shedule_id')->where('publish_date','>=', $date_from)->where('publish_date','<=', $date_to)->get()->count();
            //dd($posts);
            if ($shedules > $posts){
                $month = (int)date('n');
                $month = $month-1;
                $count_left = $shedules-$posts;
                $data['leftposts'] = 'На '.$ru_month[$month].' '.date('Y').' года, необходимо '.$count_left.' постов.';
            }
            else{
                $date = date('Y-m-d', strtotime('+1 month'));

                $date = new \DateTime(date('Y-m-d', strtotime('+1 month')));
                $date->modify('last day of this month');
                $last_day = $date->format('d'); 
                $date_from = date('Y-m', strtotime('+1 month')).'-01';
                $date_to = date('Y-m', strtotime('+1 month')).'-'.$last_day;

                $shedules = $shedules * $last_day;
                
                $posts = Post::whereNotNull('shedule_id')->where('publish_date','>=', $date_from)->where('publish_date','<=', $date_to)->get()->count();
                if ($shedules > $posts){
                    $month = (int)date('n', strtotime('+1 month'));
                    $month = $month-1;
                    $count_left = $shedules-$posts;
                    $data['leftposts'] = 'На '.$ru_month[$month].' '.date('Y', strtotime('+1 month')).' года, необходимо '.$count_left.' постов.';
                }
            }
        }
        $data['videos'] = false;
        if (Auth::user()->role->slug != 'admin' && Auth::user()->role->slug != 'client') {
            $data['videos'] = Video::all();
        }

        $data['news'] = false;
        if (Auth::user()->role->slug != 'admin' && Auth::user()->role->slug != 'client') {
            $data['news'] = News::all();
        }

        return view('panel.home')->with($data);
    }

    public function getMonthsInRange($startDate, $endDate) {
        $months = array();
        while (strtotime($startDate) <= strtotime($endDate)) {
            $months[] = array('year' => date('Y', strtotime($startDate)), 'month' => date('m', strtotime($startDate)), );
            $startDate = date('d M Y', strtotime($startDate.
                '+ 1 month'));
        }

        return $months;
    }

    public function addUserToken(Request $request){
        $token_url = $request->user_token;
        $token_url = str_replace('https://oauth.vk.com/blank.html#', '', $token_url);

        $access_token = false;
        $params = explode('&', $token_url);
        foreach ($params as $key => $param) {
            $item = explode('=', $param);
            if ($item[0] == 'access_token' && isset($item[1]))
                $access_token = $item[1];
        }
        
        if ($access_token){
            $user = User::find(Auth::user()->id);
            $user->vk_token = $access_token;
            $user->save();

            return response()->json(['success' => true]);
        }
        else{
            return response()->json(['error' => true]);
        }
    }

    public function addUserAudioToken(Request $request){
        $login = $request->login;
        $password = $request->password;

        $params = new CommonParams(SupportedClients::VkOfficial()->getUserAgent());

        $receiver = new TokenReceiverOfficial($login, $password, $params);

        $info = $receiver->getToken();
        file_put_contents('token', json_encode($info));
        //print_r($info); die();
        $token = $info[0];
        $device_id = $info[2];
        $secret = $info[1];

        if ($token){
            /*$user = User::find(Auth::user()->id);
            $user->vk_audio_token = $token;
            $user->vk_audio_device_id = $device_id;
            $user->vk_audio_secret = $secret;
            $user->save();*/
            $users = User::all();
            foreach ($users as $key => $user) {
                $user->vk_audio_token = $token;
                $user->vk_audio_device_id = $device_id;
                $user->vk_audio_secret = $secret;
                $user->save();
            }

            return response()->json(['success' => true]);
        }
        else{
            return response()->json(['error' => true]);
        }
    }
}
