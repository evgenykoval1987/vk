<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Client;
use App\Models\Video;

class VideoController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('video'),
    		'title' => 'Видеоинструкции'
    	];

    	$data['success'] = false;
    	if ($request->session()->get('video_added')){
    		$data['success'] = $request->session()->pull('video_added');
    	}

        return view('panel.video.list')->with($data);
    }

    public function addForm(){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('video'),
    		'title' => 'Видеоинструкции'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('video-add'),
    		'title' => 'Добавить'
    	];

        $data['action'] = route('video-save');

		$data['title'] = "";
		$data['href'] = "";
		$data['video_id'] = false;

    	return view('panel.video.add')->with($data);
    }

    public function editForm(Request $request, $video_id){
    	$data = [];
    	$data['breadcrumbs'][] = [
            'link' => route('video'),
            'title' => 'Видеоинструкции'
        ];

    	$data['breadcrumbs'][] = [
    		'link' => route('video-edit', $video_id),
    		'title' => 'Редактировать'
    	];
    	
    	if ($video_id && Video::find($video_id)){ 
    		$video_info = Video::find($video_id);
    	}

    	if ($video_info){
    		$data['title'] = $video_info->title;
    		$data['href'] = $video_info->href;
    		$data['video_id'] = $video_id;
            $data['action'] = route('video-esave');
    	}

    	return view('panel.video.add')->with($data);
    }

    public function addVideo(Request $request){
    	$video = new Video;
	    $video->title = $request->title;
	    $video->href = $request->href;
	    $video->save();

	    $request->session()->put('video_added', 'Видеоинструкция добавлена');

	    return redirect(route('video'));
    }

    public function ajax(Request $request){
    	$videos = [];
    	$results = Video::all();
    	
    	foreach ($results as $key => $result) {
    		$videos[] = [
    			$result->id,
    			$result->title,
    			$result->href,
    			'<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>&nbsp;<a href="'.route('video-edit',$result->id).'" class="on-default edit-row" style="margin-left: 10px"><i class="fa fa-pencil"></i></a>'
    		];
    	}

    	echo json_encode(['aaData' => $videos]);
    }

    public function dropVideo(Request $request){
    	$id = $request->post('id');

    	Video::destroy($id);

    	return response('1')->header('Content-Type', 'text/html');
    }

    public function editVideo(Request $request){
    	$video = Video::find($request->video_id);

    	$video->title = $request->title;
    	$video->href = $request->href;
	    $video->save();

	    $request->session()->put('video_added', 'Видеоинструкция изминена');

	    return redirect(route('video'));
    }
}