<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Client;
use App\Models\News;

class NewController extends Controller
{
    public function index(Request $request){
    	$data = []; 

    	$data['breadcrumbs'][] = [
    		'link' => route('news'),
    		'title' => 'Новости'
    	];

    	$data['success'] = false;
    	if ($request->session()->get('new_added')){
    		$data['success'] = $request->session()->pull('new_added');
    	}

        return view('panel.news.list')->with($data);
    }

    public function addForm(){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('news'),
    		'title' => 'Новости'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('new-add'),
    		'title' => 'Добавить'
    	];

        $data['action'] = route('new-save');

		$data['title'] = "";
		$data['description'] = "";
		$data['new_id'] = false;

    	return view('panel.news.add')->with($data);
    }

    public function editForm(Request $request, $new_id){
    	$data = [];
    	$data['breadcrumbs'][] = [
            'link' => route('news'),
            'title' => 'Новости'
        ];

    	$data['breadcrumbs'][] = [
    		'link' => route('new-edit', $new_id),
    		'title' => 'Редактировать'
    	];
    	
    	if ($new_id && News::find($new_id)){ 
    		$new_info = News::find($new_id);
    	}

    	if ($new_info){
    		$data['title'] = $new_info->title;
    		$data['description'] = $new_info->description;
    		$data['new_id'] = $new_id;
            $data['action'] = route('new-esave');
    	}

    	return view('panel.news.add')->with($data);
    }

    public function addNew(Request $request){
    	$new = new News;
	    $new->title = $request->title;
	    $new->description = $request->description;
	    $new->save();

	    $request->session()->put('new_added', 'Новость добавлена');

	    return redirect(route('news'));
    }

    public function ajax(Request $request){
    	$news = [];
    	$results = News::all();
    	
    	foreach ($results as $key => $result) {
    		$news[] = [
    			$result->id,
    			$result->title,
    			$result->description,
    			'<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>&nbsp;<a href="'.route('new-edit',$result->id).'" class="on-default edit-row" style="margin-left: 10px"><i class="fa fa-pencil"></i></a>'
    		];
    	}

    	echo json_encode(['aaData' => $news]);
    }

    public function dropNew(Request $request){
    	$id = $request->post('id');

    	News::destroy($id);

    	return response('1')->header('Content-Type', 'text/html');
    }

    public function editNew(Request $request){
    	$new = News::find($request->new_id);

    	$new->title = $request->title;
    	$new->description = $request->description;
	    $new->save();

	    $request->session()->put('new_added', 'Новость изминена');

	    return redirect(route('news'));
    }
}