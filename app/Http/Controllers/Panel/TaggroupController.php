<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Taggroup;
use App\Models\Tag;

class TaggroupController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('taggroup'),
    		'title' => 'Группы меток'
    	];

    	$data['success'] = false;
    	if ($request->session()->get('taggroup_added')){
    		$data['success'] = $request->session()->pull('taggroup_added');
    	}

        return view('panel.taggroup.list')->with('data',$data);
    }

    public function addForm(){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('taggroup'),
    		'title' => 'Группы меток'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('taggroup-add'),
    		'title' => 'Добавить'
    	];

        $data['action'] = route('taggroup-save');

		$data['name'] = "";
        $data['taggroup_id'] = false;

    	return view('panel.taggroup.add')->with('data',$data);
    }

    public function editForm(Request $request, $taggroup_id){
    	$data = [];
    	$data['breadcrumbs'][] = [
            'link' => route('taggroup'),
            'title' => 'Метки'
        ];

    	$data['breadcrumbs'][] = [
    		'link' => route('taggroup-edit', $taggroup_id),
    		'title' => 'Редактировать'
    	];
    	
    	if ($taggroup_id && Taggroup::find($taggroup_id)){ 
    		$taggroup_info = Taggroup::find($taggroup_id);
    	}

        $data['tags'] = Tag::all();

    	if ($taggroup_info){
    		$data['name'] = $taggroup_info->name;
            $data['taggroup_id'] = $taggroup_id;
            $data['action'] = route('taggroup-esave');
    	}

    	return view('panel.taggroup.add')->with('data',$data);
    }

    public function addTaggroup(Request $request){
    	$taggroup = new Taggroup;

	    $taggroup->name = $request->name;

	    $taggroup->save();

	    $request->session()->put('taggroup_added', 'Группа меток <strong>'.$request->name.'</strong> добавлена');

	    return redirect(route('taggroup'));
    }

    public function ajax(Request $request){
    	$taggroups = [];
    	$results = Taggroup::all();
    	
    	foreach ($results as $key => $result) {
    		$taggroups[] = [
    			$result->id,
    			$result->name,
    			'<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>&nbsp;<a href="'.route('taggroup-edit',$result->id).'" class="on-default edit-row" style="margin-left: 10px"><i class="fa fa-pencil"></i></a>'
    		];
    	}

    	echo json_encode(['aaData' => $taggroups]);
    }

    public function dropTaggroup(Request $request){
    	$id = $request->post('id');

    	Taggroup::destroy($id);

    	return response('1')->header('Content-Type', 'text/html');
    }

    public function editTaggroup(Request $request){
    	$taggroup = Taggroup::find($request->taggroup_id);

    	$taggroup->name = $request->name;
	    $taggroup->save();

	    $request->session()->put('taggroup_added', 'Группа меток изминена');

	    return redirect(route('taggroup'));
    }
}
