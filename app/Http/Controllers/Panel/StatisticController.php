<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pay;
use App\Models\PostToPay;
use App\Models\Client;
use App\Models\CompanyPost;
use App\Models\Company;
use App\Models\Monitoring;
use App\Models\Sentence;
use App\Models\Message;
use App\Models\CompanyPostEndDeleted;

class StatisticController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('statistic'),
    		'title' => 'Статистика'
    	];

        $data['date_from'] = date('Y-m-d', strtotime(date('Y-m')." -1 month"));
        $data['date_to'] = date('Y-m-d', strtotime('last day of previous month'));

        return view('panel.statistic.info')->with($data);
    }

    public function ajax(Request $request){
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $json = [];

        $clients_ids = [];
        $results = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get();
        foreach ($results as $key => $value) {
            if(!in_array($value->client_id, $clients_ids))
                $clients_ids[] = $value->client_id;
        }

        $clients = Client::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->whereIn('id',$clients_ids)->get()->count();
        $sum = 0;
        $cls = Client::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->whereIn('id',$clients_ids)->get();
        foreach ($cls as $key => $cl) {
            /*$orders = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->where('client_id', $cl->client_id)->get()->count();
            echo $orders."<br>";
            if ($orders)*/
                $sum += $cl->company->sum('total');
        }
        $json['p1'] = $clients." ( $sum )";


        $clients = Client::where('created_at','<', $date_from)->whereIn('id',$clients_ids)->get()->count();
        $sum = 0;
        $cls = Client::where('created_at','<', $date_from)->whereIn('id',$clients_ids)->get();
        foreach ($cls as $key => $cl) {
            $sum += $cl->company->where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->sum('total');
        }
        $json['p2'] = $clients." ( $sum )";

        $crm = 0;
        $results = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get();
        foreach ($results as $key => $result) {
            $count = 0;
            $posts = CompanyPost::where('company_id', '=', $result['id'])->where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get();
            foreach ($posts as $ke => $post) {
                foreach ($post->post_groups as $k => $post_group) {
                    $count += $post_group->viewed;
                }
            }
            if ($count != 0)
                $crm += ($result->total/$count)*1000;
        }
        $company_count = $results = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get()->count();
        if ($company_count != 0){
            $crm = $crm/$company_count;
        }
        else
            $crm = 0;
        $json['p7'] = number_format($crm,2,'.',' ');

        $clients = [];
        $total = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->sum('total');
        $results = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get();
        foreach ($results as $key => $result) {
            if(!in_array($result->client_id, $clients) && $result->total != 0)
                $clients[] = $result->client_id;
        }
        if (count($clients) != 0)
            $json['p8'] = number_format($total/count($clients),2,'.',' ');
        else
            $json['p8'] = 0;



        $total = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get()->count();
        $json['p9'] = $total;

        $json['p5'] = Monitoring::where('readed_dt','>=', $date_from)->where('readed_dt','<=', $date_to)->get()->count();
        $json['p4'] = Sentence::where('status_dt','>=', $date_from)->where('status_dt','<=', $date_to)->get()->count();
        $json['p3'] = Message::where('updated_at','>=', $date_from)->where('updated_at','<=', $date_to)->whereNotNull('answer')->get()->count();
        $json['p6'] = CompanyPostEndDeleted::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get()->count();

        echo json_encode(['data' => $json]);
    }
}
