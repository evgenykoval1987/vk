<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Client;
use App\Models\Company;
use App\Models\Group;
use App\Models\Shedule;
use App\Models\Logger;
use App\Models\CompanyPost;
use App\Models\CompanyPostToGroup;
use App\Models\CompanyPostAttachment;
use App\Models\CompanyPostEndDeleted;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Models\Log;
use App\Models\Package;
use App\Models\CompanyPostEnd;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Models\Setting;
use App\Models\CompanyPostActivity;

use App\Models\Tag;
use App\Models\Tagpack;
use App\Models\CompanyToTag;

use App\Models\CompanyPostCopy;

class CompanyController extends Controller
{
	 public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('companys'),
    		'title' => 'Рекламные компании'
    	];

    	$data['success'] = false;
    	if ($request->session()->get('company_added')){
    		$data['success'] = $request->session()->pull('company_added');
    	}

        return view('panel.company.list')->with('data',$data);
    }

    public function addForm(){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('companys'),
    		'title' => 'Рекламные компании'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('company-add'),
    		'title' => 'Добавить'
    	];

    	$data['title'] = 'VK.POSTER - Добавить рекламную компанию';
    	$data['h1'] = 'Добавить рекламную компанию';
    	$data['company_id'] = false;

    	$data['clients'] = Client::all();

    	$data['action'] = route('company-save');

    	$data['ctitle'] = '';
		$data['client_id'] = 0;
        $data['total'] = '';
        $data['dt'] = '';
        $data['dt_end'] = '';

        $data['quantity'] = 0;
        $data['views'] = 0;
        $data['tagpack_id'] = 0;
        $data['selected'] = [];

        $data['tags'] = Tag::all();
        $data['tagpacks'] = Tagpack::all();

    	return view('panel.company.add')->with('data',$data);
    }

    public function editForm(Request $request, $company_id){
    	$data = [];
    	$data['breadcrumbs'][] = [
    		'link' => route('companys'),
    		'title' => 'Рекламные компании'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('company-edit', $company_id),
    		'title' => 'Редактировать'
    	];

    	$data['title'] = 'VK.POSTER - Редактировать рекламную компанию';
    	$data['h1'] = 'Редактировать рекламную компанию';
    	
    	if ($company_id && Company::find($company_id)){ 
    		$company_info = Company::find($company_id);
    	}

    	if ($company_info){
    		$data['ctitle'] = $company_info->title;
    		$data['client_id'] = $company_info->client_id;
    		$data['action'] = route('company-esave');
    		$data['company_id'] = $company_id;
            $data['total'] = $company_info->total;
            $data['dt'] = $company_info->dt;
            $data['dt_end'] = $company_info->dt_end;

            $data['quantity'] =  $company_info->quantity;
            $data['views'] =  $company_info->views;
            $data['tagpack_id'] =  $company_info->tagpack_id;
            $data['selected'] = $company_info->tags;
    	}

    	$data['clients'] = Client::all();
        $data['tags'] = Tag::all();
        $data['tagpacks'] = Tagpack::all();

    	return view('panel.company.add')->with('data',$data);
    }

    public function addCompany(Request $request){
    	$company = new Company;

	    $company->title = $request->title;
	    $company->client_id = $request->client_id;
	    $company->user_id = Auth::user()->id;
        $company->total = $request->total;
        $company->dt = $request->dt;
        $company->dt_end = $request->dt_end;

        $company->quantity = $request->quantity;
        $company->views = $request->views;
        $company->tagpack_id = $request->tagpack_id;

	    $company->save();

        if ($request->tags){
            foreach ($request->tags as $key => $tag) {
                $ttt = new CompanyToTag();
                $ttt->tag_id = $tag;
                $ttt->company_id = $company->id;
                $ttt->save();
            }
        }

	    $request->session()->put('company_added', 'Рекламная компания <strong>'.$request->title.'</strong> добавлена');

	    return redirect(route('companys'));
    }

    public function ajax(Request $request){
    	$companies = [];
    	$results = Company::all();

        $search = false;
        if (isset($request->search['value'])){
            if ($request->search['value'] != ''){
                $search = $request->search['value'];
            }
        }
    	
        if ($search){
            $results = DB::table('company')->join('clients', 'company.client_id', '=', 'clients.id')->where('clients.name','like',"%$search%")->orWhere('company.title','like',"%$search%")->offset($request->start)->limit($request->length)->orderBy('company.id','DESC')->select('company.*')->get();
            $total = DB::table('company')->join('clients', 'company.client_id', '=', 'clients.id')->where('clients.name','like',"%$search%")->orWhere('company.title','like',"%$search%")->get()->count();
            /*$results = Company::with(['Client' => function($q) use($search) {
                $q->where('name', 'like', "%$search%");
            }])->orWhere('title','like',"%$search%")->offset($request->start)->limit($request->length)->get();
            $total = Company::where('title','like',"%$search%")->get()->count();*/

        }
        else{
            $results = Company::offset($request->start)->limit($request->length)->get();
            $total = Company::get()->count();
        }

    	foreach ($results as $key => $result) {  
            $result = Company::find($result->id);

            $count = 0;
            $totals = 0;
            $posts = CompanyPost::where('company_id', '=', $result['id'])->get();
            foreach ($posts as $ke => $post) {
                foreach ($post->post_groups as $k => $post_group) {
                    $count += $post_group->viewed;
                    
                }
                $totals += $post->tag_quantity + $post->ttag_quantity;
            }
    		$companies[] = [
    			$result->id,
    			'<a href="'.route('company-posts',$result->id).'">'.$result->title.'</a>',
                
    			'<a href="'.$result->client->vk_profile.'" target="_blank">'.$result->client->name.'</a>',
                $result->total,
                number_format((($count != 0) ? ($result->total/$count)*1000 : 0),2),
                $count,
                $result->views,
                $result->quantity,
                $totals,
    			date('d.m.Y', strtotime($result->created_at)),
    			'<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>&nbsp;<a href="'.route('company-edit',$result->id).'" class="on-default edit-row" style="margin-left: 10px"><i class="fa fa-pencil"></i></a>'
    		];
    	}

    	//echo json_encode(['aaData' => $companies]);
        echo json_encode(['draw' => $request->sEcho, 'recordsTotal' => $total, 'recordsFiltered' => $total, 'data' => $companies]);
    }

    public function dropCompany(Request $request){
    	$id = $request->post('id');

    	Company::destroy($id);
        CompanyToTag::where('company_id', $id)->delete();

    	return response('1')->header('Content-Type', 'text/html');
    }

    public function editCompany(Request $request){
    	$company = Company::find($request->company_id);

    	$company->title = $request->title;
    	$company->client_id = $request->client_id;
        $company->total = $request->total;
        $company->dt = $request->dt;
        $company->report = 0;
        $company->readed = 0;
        $company->dt_end = $request->dt_end;

        $company->quantity = $request->quantity;
        $company->views = $request->views;
        $company->tagpack_id = $request->tagpack_id;

        CompanyToTag::where('company_id', $request->company_id)->delete();

        if ($request->tags){
            foreach ($request->tags as $key => $tag) {
                $ttt = new CompanyToTag();
                $ttt->tag_id = $tag;
                $ttt->company_id = $request->company_id;
                $ttt->save();
            }
        }

	    $company->save();

	    $request->session()->put('company_added', 'Рекламная компания изминена');

	    return redirect(route('companys'));
    }

    public function getShowTags($company_info){
        $company_tags = [];
        $tags = $company_info->tags;
        foreach ($tags as $key => $tag) {
            $company_tags[] = $tag->tag;
        }

        if ($company_info->tagpack_id){
            $tagpack_info = Tagpack::find($company_info->tagpack_id);
            foreach ($tagpack_info->tags as $key => $tag) {
                $company_tags[] = $tag->tag;
            }
        }
        
        $company_tags_total = [];
        foreach ($company_tags as $key => $company_tag) {
            if (!isset($company_tags_total[$company_tag->id]))
                $company_tags_total[$company_tag->id] = 0;
            $company_tags_total[$company_tag->id] += $company_tag->quantity;
        }
        

        $data['show_tags'] = [];
        foreach ($company_info->companyposts as $ke => $post) {
            if ($post->tag_id && isset($company_tags_total[$post->tag_id])){
                $company_tags_total[$post->tag_id] -= $post->tag_quantity;
            }
            if ($post->ttag_id && isset($company_tags_total[$post->ttag_id])){
                $company_tags_total[$post->ttag_id] -= $post->ttag_quantity;
            }
        }

        foreach ($company_tags_total as $key => $item) {
            if ($item <= 0)
                continue;
            $ta = Tag::find($key);
            if ($ta){
                if ($ta->tag_group_id){
                    $data['show_tags'][$ta->tag_group_id]['items'][] = $ta->name.'('.$item.')';
                    $data['show_tags'][$ta->tag_group_id]['name'] = $ta->taggroup->name;
                }
                else{
                    $data['show_tags'][0]['name'] = 'Без группы';
                    $data['show_tags'][0]['items'][] = $ta->name.'('.$item.')';
                }
            }
        }

        return $data['show_tags'];
    }

    public function posts(Request $request, $company_id){ 
    	$data = [];

    	$company_info = Company::find($company_id);
        $data['report'] = $company_info->report;

    	$data['company_id'] = $company_id;

    	$data['breadcrumbs'][] = [
    		'link' => route('companys'),
    		'title' => 'Рекламные компании'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('company-edit', $company_id),
    		'title' => $company_info['title']
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('company-posts', $company_id),
    		'title' => 'Посты'
    	];

    	$data['h1'] = 'Управление компанией '.$company_info['title'];

        $data['show_tags'] = $this->getShowTags($company_info);
        
        //dd($company_tags_total);
        /*
        $data['tags'] = [];

        $tags = $company_info->tags;
        foreach ($tags as $key => $tag) {
            $data['tags'][] = $tag->tag_id;
        }

        if ($company_info->tagpack_id){
            $tagpack_info = Tagpack::find($company_info->tagpack_id);
            foreach ($tagpack_info->tags as $key => $tag) {
                $data['tags'][] = $tag->tag_id;
            }
        }
       
        $company_tags = [];
        $posts = $company_info->companyposts;
        foreach ($posts as $key => $post) {
            if ($post->tag_id)
                $company_tags[] = $post->tag_id;
            if ($post->ttag_id)
                $company_tags[] = $post->ttag_id;
        }

        foreach ($company_tags as $key => $company_tag) {
            $removed = false;
            foreach ($data['tags'] as $ke => $tag) {
                if ($tag == $company_tag && !$removed){
                    unset($data['tags'][$ke]);
                    $removed = true;
                }
            }
        }
        
        $data['show_tags'] = [];
        foreach ($data['tags'] as $key => $tag) {
            $ta = Tag::find($tag);
            if ($ta){
                if ($ta->tag_group_id){
                    $data['show_tags'][$ta->tag_group_id]['items'][] = $ta->name.'('.$ta->quantity.')';
                    $data['show_tags'][$ta->tag_group_id]['name'] = $ta->taggroup->name;
                }
                else{
                    $data['show_tags'][0]['name'] = 'Без группы';
                    $data['show_tags'][0]['items'][] = $ta->name.'('.$ta->quantity.')';
                }
                
            }
        }*/
        //dd($data['show_tags']);
        //sort($data['show_tags']);

    	$data['success'] = false;
    	if ($request->session()->get('company_post_added')){
    		$data['success'] = $request->session()->pull('company_post_added');
    	}

    	return view('panel.company.posts')->with('data',$data);
    }

    public function addPostForm($company_id){
    	$data = [];

    	$company_info = Company::find($company_id);

    	$data['company_id'] = $company_id;

    	$data['breadcrumbs'][] = [
    		'link' => route('companys'),
    		'title' => 'Рекламные компании'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('company-edit', $company_id),
    		'title' => $company_info['title']
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('company-posts', $company_id),
    		'title' => 'Посты'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('company-post-add', $company_id),
    		'title' => 'Добавить'
    	];

    	$data['title'] = 'VK.POSTER - Добавить пост в рекламную компанию';
    	$data['h1'] = 'Добавить пост в рекламную компанию';

    	$data['description'] = '';
        $data['link'] = '';
        $data['copyright'] = '';
        $data['ttitle'] = '';
    	$data['post_id'] = false;
        $data['comments'] = '';

        $data['tags'] = Tag::all();

        /*$tags = [];
        $items = $company_info->tags;
        foreach ($items as $key => $tag) {
            $tags[] = $tag->tag_id;
        }

        if ($company_info->tagpack_id){
            $tagpack_info = Tagpack::find($company_info->tagpack_id);
            foreach ($tagpack_info->tags as $key => $tag) {
                $tags[] = $tag->tag_id;
            }
        }
       
        $company_tags = [];
        foreach ($company_info->companyposts as $key => $post) {
            if ($post->tag_id)
                $company_tags[] = $post->tag_id;
            if ($post->ttag_id)
                $company_tags[] = $post->ttag_id;
        }

        foreach ($company_tags as $key => $company_tag) {
            $removed = false;
            foreach ($tags as $ke => $tag) {
                if ($tag == $company_tag && !$removed){
                    unset($tags[$ke]);
                    $removed = true;
                }
            }
        }*/
        $company_tags = [];
        $tags = $company_info->tags;
        foreach ($tags as $key => $tag) {
            $company_tags[] = $tag->tag;
        }

        if ($company_info->tagpack_id){
            $tagpack_info = Tagpack::find($company_info->tagpack_id);
            foreach ($tagpack_info->tags as $key => $tag) {
                $company_tags[] = $tag->tag;
            }
        }
        
        $company_tags_total = [];
        foreach ($company_tags as $key => $company_tag) {
            if (!isset($company_tags_total[$company_tag->id]))
                $company_tags_total[$company_tag->id] = 0;
            $company_tags_total[$company_tag->id] += $company_tag->quantity;
        }
        
        foreach ($company_info->companyposts as $ke => $post) {
            if ($post->tag_id){
                $company_tags_total[$post->tag_id] -= $post->tag_quantity;
            }
            if ($post->ttag_id){
                $company_tags_total[$post->ttag_id] -= $post->ttag_quantity;
            }
        }
        
        $data['show_tags'] = [];
        foreach ($company_tags_total as $key => $item) {
            if ($item <= 0)
                continue;
            $ta = Tag::find($key);
            if ($ta){
                $data['show_tags'][] = $ta->id;
            }
        }

        if ($company_info->client->system_id){
            $data['comments'] = $company_info->client->comments;
        }

    	$groups = Group::all();
    	$data['groups'] = [];
    	foreach ($groups as $key => $group) {
    		$response = false;
    		$shedule_id = 0;
	        /*$shedules =  $group->shedule()->type('adv')->overnow(date('H:i:s'))->get();
	        if ($shedules->count() > 0){
	            foreach ($shedules as $key => $shedule) {
	                if ($response) continue;
	                $posts = $group->company_group_post()->where('publish_date', date('Y-m-d').' '.$shedule->value)->get();

	                if ($posts->count() == 0){
	                    $response = date('d.m.Y').' '.$shedule->value;
	                    $shedule_id = $shedule->id;
	                }

	            }
	        }

	        if (!$response){
	            $shedules = $group->shedule()->type('adv')->ordered()->get();
	            if ($shedules->count() > 0){
	                $i = 1;
	                while(!$response){
	                    foreach ($shedules as $key => $shedule) {
	                        if ($response) continue;

	                        $posts = $group->company_group_post()->where('publish_date', date('Y-m-d', strtotime('+'.$i.' day')).' '.$shedule->value)->get();

	                        if ($posts->count() == 0){
	                            $response = date('d.m.Y',strtotime('+'.$i.' day')).' '.$shedule->value;
	                            $shedule_id = $shedule->id;
	                        }
	                    }
	                    $i++;
	                }
	            }
	        }*/
	       
	        if (!$response){
	        	//$response = 'Поиск...Нету рассписания';
                $response = 'Поиск...';
	        }

    		$data['groups'][] = [
    			'id' => $group->id,
    			'url' => $group->url,
    			'name' => $group->name,
    			'shedule' => $response,
    			'data-shedule' => $shedule_id
    		];

    	}

        $data['packages'] = Package::all();

    	return view('panel.company.post-add')->with('data',$data);
    }

    public function addPost(Request $request, $company_id){

    	$company_info = Company::find($company_id);
    	$description = $request->description;
        $title = $request->title;
        $link = $request->link;
        $copyright = $request->copyright;
    	$groups = $request->group; 
    	$is_adv = $request->advertasing;
        $not_count = (isset($request->not_count)) ? 1 : 0;
    	//dd($groups);
    	$post = new CompanyPost;
    	$post->description = json_encode($description);
        $post->title = $title;
        $post->link = $link;
        $post->copyright = $copyright;
    	$post->user_id = Auth::user()->id;
    	$post->company_id = $company_id;
        $post->comments = $request->comments;
        $post->tag_id = $request->tag_id;
        $post->ttag_id = $request->ttag_id;
        if ($post->tag_id)
            $post->tag_quantity = $request->tag_quantity;
        if ($post->ttag_id)
            $post->ttag_quantity = $request->ttag_quantity;

        $post->type = 'usual';
        if (isset($request->post_type))
            $post->type = $request->post_type;

        if ($post->type == 'native' || $post->type == 'story')
            $not_count = 1;

    	$post->save();
        
        $show_tags = $this->getShowTags($company_info);
    
        $repost = false;
        if(isset($request->repost)){
            $repost = true;
        }

    	$company_post_id = $post->id;

        if ($request->attachments){
            foreach ($request->attachments as $key => $attachment) {
                $arr = explode(':::', $attachment);
                $attachment = $arr[0];
                $content = $arr[1];
                $type = $arr[2];
                $title = $arr[3];
                if ($type == 'video-local'){
                    $arr2 = explode('.', $title);
                    unset($arr2[count($arr2)-1]);
                    $title = implode('.', $arr2);
                }

                $post_attachment = new CompanyPostAttachment;
                $post_attachment->attachment = $arr[0];
                $post_attachment->content = $arr[1];
                $post_attachment->type = $arr[2];
                $post_attachment->sort_order = $key;
                $post_attachment->title = $title;
                $post_attachment->company_post_id = $company_post_id;
                $post_attachment->save();
            }
        }

        $finish_dt = strtotime('+1 day', strtotime(date('Y-m-d')));
        $description_key = 0;
        $count_descriptions = count($description);
        //dd($groups);
    	foreach ($groups as $key => $group) {
            if (!isset($group['name'])) continue;
    		$publish_date = date('Y-m-d H:i:s', strtotime($group['dt']));

            if (strtotime('+1 day', strtotime($group['dt'])) > $finish_dt)
                $finish_dt = strtotime('+1 day', strtotime($group['dt']));

    		$company_post_to_group = new CompanyPostToGroup;
    		$company_post_to_group->company_post_id = $company_post_id;
    		$company_post_to_group->group_id = $group['name'];
    		$company_post_to_group->publish_date = $publish_date;
            $company_post_to_group->not_count = $not_count;
    		$company_post_to_group->save();
    		//continue;
    		$group_info = Group::find($group['name']);
    		//$access_token = Auth::user()->vk_token;
            $access_token =  User::find($group_info->user_id)->vk_token;

            if (isset($description[$description_key])){
                $cut_description = $description[$description_key];
                $description_key++;
            }
            else{
                $cut_description = $description[0];
                $description_key = 1;
            }

    		$data = [
                'owner_id' => '-'.$group_info->idd,
                'access_token' => $access_token,
                'from_group' => '1',
                'message' => $cut_description,
                'publish_date' => strtotime($group['dt']),
                'v' => '5.131',
            ];

            if ($is_adv != '')
                $data['mark_as_ads'] = 1;

            $atta = array();

            $post_info = CompanyPost::find($company_post_id);
            $attachments = $post_info->attachments()->get();

            foreach ($attachments as $key => $attachment) { 
                /*if (isset($request->this_is_story))
                    $attachment->type = str_replace('video-local', 'story-local', $attachment->type);*/
                if ($attachment->attachment == '0' && $attachment->type == 'image-local'){ 
                   /*if ($post->type == 'story'){
                        $atta[] = $this->transformPostAtachmentImageStory($attachment, $group_info->idd, $access_token);
                    }
                    else{*/
                        $atta[] = $this->transformPostAtachmentImage($attachment, $group_info->idd, $access_token);
                   /* }*/
                    sleep(1);
                }
                if ($attachment->attachment == '0' && $attachment->type == 'video-local'){ 
                   /* if ($post->type == 'story'){
                        $atta[] = $this->transformPostAtachmentVideoStory($attachment, $group_info->idd, $access_token);
                    }
                    else{*/
                        $atta[] = $this->transformPostAtachmentVideo($attachment, $group_info->idd, $access_token);
                    /*}*/
                    sleep(1);
                }
                if ($attachment->attachment == '0' && $attachment->type == 'video-youtube'){ 
                    $atta[] = $this->transformPostAtachmentVideoYoutube($attachment, $group_info->idd, $access_token);
                    sleep(1);
                }
                if ($attachment->attachment == '0' && $attachment->type == 'audio-local'){ 
                    $atta[] = $this->transformPostAtachmentAudio($attachment, $group_info->idd, $access_token);
                    sleep(1);
                }
                if ($attachment->attachment == '0' && $attachment->type == 'playlist-vk'){ 
                    $atta[] = $this->transformPostAtachmentPlaylist($attachment);
                    sleep(1);
                }
                if ($attachment->attachment == '0' && $attachment->type == 'image-url'){ 
                    $atta[] = $this->transformPostAtachmentImageUrl($attachment, $group_info->idd, $access_token);
                    sleep(1);
                }
            }
            
            $attachments = $post_info->attachments()->get();
            foreach ($attachments as $key => $attachment) { 
                if ($attachment->attachment == '0' && ($attachment->type == 'image-local' || $attachment->type == 'video-local' || $attachment->type == 'video-local' || $attachment->type == 'audio-local' || $attachment->type == 'playlist-vk'))
                    continue;
                //if ($attachment->attachment != '0'){ 
                    $atta[] = $attachment->attachment;
                //}
            }

            $data['attachments'] = '';

            if ($atta){
                $data['attachments'] = implode(',', $atta);
            }
            if ($link != ''){
                $data['attachments'] .= ','.$link;
            }
            if ($copyright != ''){
                $data['copyright'] = $copyright;
            }

            if ($post->type == 'story'){
                continue;
            }
            else{
                if ($repost){
                    if (isset($data['attachments']))
                        unset($data['attachments']);
                    $repost_link = $request->repost_link;
                    $arr = explode('/', $repost_link);
                    $data['object'] = $arr[count($arr)-1];
                    $data['group_id'] = $group_info->idd;
                    unset($data['owner_id']);
                    $url = "https://api.vk.com/method/wall.repost"/*.http_build_query($data)*/; 
                }
                else{
                    $url = "https://api.vk.com/method/wall.post"/*.http_build_query($data)*/; 
                }
            }
            
            //$response = file_get_contents($url);

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);

            $response = curl_exec($ch);
            curl_close ($ch);
            
            $log = new Log();
            $log->text = $company_post_id.' --- '.$group['name'].' --- '.$response;
            $log->save();

            $vk_info = json_decode($response);
           
            if (isset($vk_info->response->post_id)){
                $vk_id = $vk_info->response->post_id;

                $update = [
                	'vk_id' => $vk_id,
                	'vk_link' => 'https://vk.com/'.$group_info->code.'?w=wall-'.$group_info->idd.'_'.$vk_id,
                	'status' => 1
                ];

                DB::table('company_posts_to_groups')->where([
                	'company_post_id' => $company_post_id,
                	'group_id' => $group['name']
                ])->update($update);
            }

            sleep(1);
    	}

        /*if (!$show_tags && $finish_dt){
            $company_info->dt_end = date('Y-m-d', $finish_dt);
            $company_info->save();
        }*/

    	$request->session()->put('company_post_added', 'Пост в рекламную компанию <strong>'.$company_info->name.'</strong> добавлен');

	    return redirect(route('company-posts', $company_id));
    }

    public function ajaxPosts(Request $request, $company_id){
        $company_info = Company::find($company_id);
        $tags = [];
        $items = $company_info->tags;
        foreach ($items as $key => $tag) {
            $tags[$tag->tag_id] = $tag->tag;
        }

        if ($company_info->tagpack_id){
            $tagpack_info = Tagpack::find($company_info->tagpack_id);
            foreach ($tagpack_info->tags as $key => $tag) {
                $tags[$tag->tag_id] = $tag->tag;
            }
        }

    	$company_posts = [];
    	$results = CompanyPost::where('company_id', '=', $company_id)->get();
    	foreach ($results as $key => $result) {

            $select = '<select class="form-control post-tag" data-post-id="'.$result->id.'"><option value="0"></option>';
            foreach ($tags as $k => $show_tag) {
                if ($show_tag->id == $result->tag_id)
                    $select .= '<option value="'.$show_tag->id.'" selected>'.$show_tag->name.'</option>';
                else
                    $select .= '<option value="'.$show_tag->id.'">'.$show_tag->name.'</option>';
            }
            $select .= '</select>';

            $select2 = '<select class="form-control post-ttag" data-post-id="'.$result->id.'"><option value="0"></option>';
            foreach ($tags as $k => $show_tag) {
                if ($show_tag->id == $result->ttag_id)
                    $select2 .= '<option value="'.$show_tag->id.'" selected>'.$show_tag->name.'</option>';
                else
                    $select2 .= '<option value="'.$show_tag->id.'">'.$show_tag->name.'</option>';
            }
            $select2 .= '</select>';

            $quantity = '<input type="text" class="form-control post-tag-quantity" value="'.$result->tag_quantity.'" data-post-id="'.$result->id.'">';
            $quantity2 = '<input type="text" class="form-control post-ttag-quantity" value="'.$result->ttag_quantity.'" data-post-id="'.$result->id.'">';

    		$groups = [];
    		$groups_for_posts = $result->post_groups()->get();
    		foreach ($groups_for_posts as $k => $groups_for_post) {
    			$group = Group::find($groups_for_post->group_id);
                if($group)
    			  $groups[] = $group->url.' '.date('d.m.Y H:i', strtotime($groups_for_post->publish_date));
    		}
    		$company_posts[] = [
                $result->id,
    			$result->id.'<br>'.$result->title,
                $select."<br>".$select2,
                $quantity.'<br>'.$quantity2,
    			implode('<br>', $groups),
    			date('d.m.Y', strtotime($result->created_at)),
    			'<a href="#" class="company-post-report"><i class="fa fa-file-text-o" aria-hidden="true"></i></a>',
                '<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>',
                '<a href="#" class="on-default change-company-row"><i class="fa fa-exchange"></i></a>',
                '<a href="#" class="on-default copy-company-row"><i class="fa fa-copy"></i></a>'
    		];
    	}

    	echo json_encode(['aaData' => $company_posts]);
    }

    public function checkGroupDate(Request $request){
    	$json = ['success' => false];
    	$group_id = $request->group_id;
    	$date = $request->date;
    	$time = $request->time;

    	$posts = Group::find($group_id)->company_group_post()->where('publish_date', $date.' '.$time)->get();
    	if ($posts->count() != 0){
    		$json = ['success' => true];
    	}

    	return response(json_encode($json))->header('Content-Type', 'text/json');
    }

    public function checkGroupDateNative(Request $request){
        $json = ['success' => false];
        $group_id = $request->group_id;
        $date = $request->date;
        $time = $request->time;

        $posts = Group::find($group_id)->company_group_post()->where('publish_date', $date.' '.$time)->get();
        if ($posts->count() != 0){
            $json = ['success' => true];
        }

        return response(json_encode($json))->header('Content-Type', 'text/json');
    }

    public function checkGroupShedule(Request $request){
    	$group_id = $request->group_id;
    	$shedule = $request->shedule;

    	$date = date('Y-m-d', strtotime($shedule));
    	$time = date('H:i', strtotime($shedule));
    	$publish_date = date('Y-m-d H:i:s', strtotime($shedule));

    	$response = false;
        $shedules = Group::find($group_id)->shedule()->where('value','!=',$time)->type('adv')->overnow($time)->get();

        if ($shedules->count() > 0){
            foreach ($shedules as $key => $shedule) {
                if ($response) continue;
                $posts = Group::find($group_id)->company_group_post()->where('publish_date', $date.' '.$shedule->value)->get();

                if ($posts->count() == 0){
                    $response = date('d.m.Y', strtotime($date)).' '.$shedule->value;
                }

            }
        }

        if (!$response){
            $shedules = Group::find($group_id)->shedule()->type('adv')->ordered()->get();
            if ($shedules->count() > 0){
                $i = 1;
                while(!$response){
                    foreach ($shedules as $key => $shedule) {
                        if ($response) continue;
                        $posts = Group::find($group_id)->company_group_post()->where('publish_date', date('Y-m-d',strtotime($date.'+'.$i.' day')).' '.$shedule->value)->get();

                        if ($posts->count() == 0){
                            $response = date('d.m.Y',strtotime($date.'+'.$i.' day')).' '.$shedule->value;
                        }
                    }
                    $i++;
                }
            }
        }

        echo $response;
    }

    public function getPostReport(Request $request){
    	$post_id = $request->id;



    	$data['posts'] = [];
        $data['total'] = 0;
        $data['likes'] = 0;
        $data['reposts'] = 0;
        $data['comments'] = 0;
        

        $flag = false;
        $flag = CompanyPost::find($post_id)->where('id', $post_id)->where('report_dt', '<', DB::raw('NOW() - INTERVAL 2 MONTH')/*'(NOW()-INTERVAL 2 DAY)'*/)->first();
        
        if ($flag){
            $post = CompanyPost::find($post_id);
            $post->report = null;
            $post->save();
        }



        $data['post_info'] = CompanyPost::find($post_id);

        if ($data['post_info']->type == 'story'){
            $posts = CompanyPost::find($post_id)->post_groups()->get();

            foreach ($posts as $key => $post) {
                $data['total'] += ($post->story_views == '') ? 0 : $post->story_views;

                $data['likes'] += ($post->like == '') ? 0 : $post->like;
                $data['reposts'] += ($post->repost == '') ? 0 : $post->repost;
                $data['comments'] += ($post->comment == '') ? 0 : $post->comment;

                $data['posts'][] = [
                    'company_post_id' => $post->company_post_id,
                    'group_id' => $post->group_id,
                    'group' => $post->group->url,
                    'url' => $post->vk_link,
                    'showlink' => (strtotime(date('d.m.Y H:i:s')) > strtotime($post->publish_date)) ? true : false,
                    'viewed' => ($post->viewed == '') ? 0 : $post->viewed,

                    'like' => ($post->like == '') ? 0 : $post->like,
                    'repost' => ($post->repost == '') ? 0 : $post->repost,
                    'comment' => ($post->comment == '') ? 0 : $post->comment,

                    'publish_date' => date('d.m.Y H:i', strtotime($post->publish_date)),
                    'status' => ($post->status == 1) ? 'Опубликован' : 'Не опубликован',
                    'story_shares' => $post->story_shares,
                    'story_subscribers' => $post->story_subscribers,
                    'story_bans' => $post->story_bans,
                    'story_opens' => $post->story_opens,
                    'story_views' => $post->story_views,
                    'story_likes' => $post->story_likes,
                    'sstatus' => $post->status
                ];


            }

            return view('panel.company.story-stat')->with('data',$data);
        }
        else{
            $data['str'] = [];
            $posts = CompanyPost::find($post_id)->post_groups()->get();

            foreach ($posts as $key => $post) {
                $data['total'] += ($post->viewed == '') ? 0 : $post->viewed;
                $data['posts'][] = [
                    'company_post_id' => $post->company_post_id,
                    'group_id' => $post->group_id,
                    'group' => $post->group->url,
                    'url' => $post->vk_link,
                    'showlink' => (strtotime(date('d.m.Y H:i:s')) > strtotime($post->publish_date)) ? true : false,
                    'viewed' => ($post->viewed == '') ? 0 : $post->viewed,

                    'like' => ($post->like == '') ? 0 : $post->like,
                    'repost' => ($post->repost == '') ? 0 : $post->repost,
                    'comment' => ($post->comment == '') ? 0 : $post->comment,

                    'publish_date' => date('d.m.Y H:i', strtotime($post->publish_date)),
                    'status' => ($post->status == 1) ? 'Опубликован' : 'Не опубликован',
                    'sstatus' => $post->status
                ];

                $data['likes'] += ($post->like == '') ? 0 : $post->like;
                $data['reposts'] += ($post->repost == '') ? 0 : $post->repost;
                $data['comments'] += ($post->comment == '') ? 0 : $post->comment;
                $data['str'][] = $post->vk_link;
            }

            $data['str'] = implode('\r\n', $data['str']);

            return view('panel.company.stat')->with('data',$data);
        }
    	
    }

    public function dropPost(Request $request){
        $id = $request->post('id');

        CompanyPost::destroy($id);

        CompanyPostToGroup::where('company_post_id',$id)->delete();

        return response('1')->header('Content-Type', 'text/html');
    }

    public function transformPostAtachmentImage($attachment, $group_id, $access_token){
        $da = [
            'group_id' => $group_id,
            'access_token' => $access_token,
            'v' => '5.131',
        ];

        $url = "https://api.vk.com/method/photos.getWallUploadServer?".http_build_query($da);
        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $info = $response;

        $logger = new Logger;
        $logger->type = 'reklama';
        $logger->key = 'GROUP_ID';
        $logger->value = $group_id;
        $logger->save();

        $logger = new Logger;
        $logger->type = 'reklama';
        $logger->key = 'photos.getWallUploadServer';
        $logger->value = json_encode($response);
        $logger->save();

        $return = false;

        if (isset($info->response->upload_url)){
            $url = $info->response->upload_url;
            $arr = explode('/', $attachment->content);
            $name = $arr[count($arr)-1];
            $response = $this->transformImageToVkImage($url, $group_id, $access_token, $name);
            
            if (isset($response->response)){
                $response = $response->response[0];

                $code = "photo".$response->owner_id.'_'.$response->id;
                $content = $response->sizes[count($response->sizes)-1]->url;
                $type = 'image-vk';
                
                //unlink(storage_path('app/public').'/'.$name);

                /*$atach = CompanyPostAttachment::find($attachment->id);
                $atach->attachment = $code;
                $atach->content = $content;
                $atach->type = $type;
                $atach->save();*/

                $return = $code;
            }
        }

        return $return;
    }

    public function transformImageToVkImage($vk_url, $group_id, $access_token, $content){
        $filename = storage_path('app/public').'/'.$content;
        $cfile = curl_file_create($filename,mime_content_type($filename),$content);
        $response = $this->makeRequest($vk_url, ['photo' => $cfile]);

        $logger = new Logger;
        $logger->type = 'reklama';
        $logger->key = 'sendPhotoToServerCurl';
        $logger->value = json_encode($response);
        $logger->save();

        $json = [];
        if (isset($response->photo)){
            $da = [
                'group_id' => $group_id,
                'access_token' => $access_token,
                'photo' => $response->photo,
                'server' => $response->server,
                'hash' => $response->hash,
                'v' => '5.131',
            ];

            $url = "https://api.vk.com/method/photos.saveWallPhoto?".http_build_query($da);
            //$response = file_get_contents($url);
            $response = $this->makeRequestGet($url);
            $json = $response;

            $logger = new Logger;
            $logger->type = 'reklama';
            $logger->key = 'photos.saveWallPhoto';
            $logger->value = json_encode($response);
            $logger->save();
        }

        return $json;
    }

    /**Картинка с УРЛА*/

    public function transformPostAtachmentImageUrl($attachment, $group_id, $access_token){
        $da = [
            'group_id' => $group_id,
            'access_token' => $access_token,
            'v' => '5.131',
        ];

        $url = "https://api.vk.com/method/photos.getWallUploadServer?".http_build_query($da);

        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $info = $response;

        $return = false;

        if (isset($info->response->upload_url)){
            $url = $info->response->upload_url;
            $arr = explode('/', $attachment->content);

            $url2 = $attachment->content;
            $contents = file_get_contents($url2);
            $name = substr($url2, strrpos($url2, '/') + 1);
            Storage::put('public/'.$name, $contents);
            
            //$name = $arr[count($arr)-1];
            $response = $this->transformImageToVkImage($url, $group_id, $access_token, $name);
            
            if (isset($response->response)){
                $response = $response->response[0];

                $code = "photo".$response->owner_id.'_'.$response->id;
                $content = $response->sizes[count($response->sizes)-1]->url;
                $type = 'image-vk';
                
                //unlink(storage_path('app/public').'/'.$name);

                /*$atach = CompanyPostAttachment::find($attachment->id);
                $atach->attachment = $code;
                $atach->content = $content;
                $atach->type = $type;
                $atach->save();*/

                $return = $code;
            }
        }

        return $return;
    }

    /**Картинка с УРЛА*/

    public function transformPostAtachmentImageStory($attachment, $group_id, $access_token){
        $da = [
            'group_id' => $group_id,
            'access_token' => $access_token,
            //'link_text' => 'more',
            'add_to_news' => 1,
            'v' => '5.130',
        ];

        $url = "https://api.vk.com/method/stories.getPhotoUploadServer?".http_build_query($da);
        $response = $this->makeRequestGetNotDecode($url);
        $info2 = $response; 
        $info = json_decode($response);
        //dd($info);
        if (isset($info->response->upload_url)){
            $url = $info->response->upload_url;
            
            $url2 = $attachment->content;
            $contents = file_get_contents($url2);
            $name = substr($url2, strrpos($url2, '/') + 1);
            Storage::put('public/'.$name, $contents);

            $filename = storage_path('app/public').'/'.$name;
            
            $cfile = curl_file_create($filename,mime_content_type($filename),$name);
            $response = $this->makeRequest($url, ['photo' => $cfile]);
            $json = $response;
            dd($json);
            $da = [
                'upload_results' => $json->response->upload_result,
                'access_token' => $access_token,
                'v' => '5.130',
            ];
            $url = "https://api.vk.com/method/stories.save?".http_build_query($da);
            $response = $this->makeRequestGet($url);
            $info = $response;
        }

        return true;
    }

    public function transformPostAtachmentVideoStory($attachment, $group_id, $access_token){
        $da = [
            'group_id' => $group_id,
            'access_token' => $access_token,
            //'link_text' => 'more',
            'add_to_news' => 1,
            'v' => '5.130',
        ];

        $url = "https://api.vk.com/method/stories.getVideoUploadServer?".http_build_query($da);
        $response = $this->makeRequestGetNotDecode($url);
        $info2 = $response; 
        $info = json_decode($response);
        
        if (isset($info->response->upload_url)){
            $url = $info->response->upload_url;
            
            $url2 = $attachment->content;
            $contents = file_get_contents($url2);
            $name = substr($url2, strrpos($url2, '/') + 1);
            Storage::put('public/'.$name, $contents);

            $filename = storage_path('app/public').'/'.$name;

            $cfile = curl_file_create($filename,mime_content_type($filename),$name);
            $response = $this->makeRequest($url, ['file' => $cfile]);
            $json = $response;

            $da = [
                'upload_results' => $json->response->upload_result,
                'access_token' => $access_token,
                'v' => '5.130',
            ];
            $url = "https://api.vk.com/method/stories.save?".http_build_query($da);
            $response = $this->makeRequestGet($url);
            $info = $response;
        }

        return true;
    }

    public function transformPostAtachmentVideo($attachment, $group_id, $access_token){
        $arr = explode('/', $attachment->content);
        $name = $arr[count($arr)-1];
        
        $da = [
            'name' => $attachment->title,
            'wallpost' => 0,
            'group_id' => $group_id,
            'access_token' => $access_token,
            'v' => '5.131',
        ];
        
        $url = "https://api.vk.com/method/video.save?".http_build_query($da);
        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $info = $response;
        
        $log = new Log();
        $log->text = json_encode($response);
        $log->save();

        $return = false;
        if (isset($info->response->upload_url)){
            $url = $info->response->upload_url;
           
            $response = $this->transformVideoToVkVideo($url, $group_id, $access_token, $name);
            
            if (isset($response->size)){
                $code = "video".$response->owner_id.'_'.$response->video_id;
                $content = '';
                $type = 'video-vk';


                
                //unlink(storage_path('app/public').'/'.$name);

                /*$atach = CompanyPostAttachment::find($attachment->id);
                $atach->attachment = $code;
                $atach->content = $content;
                $atach->type = $type;
                $atach->save();*/

                $return = $code;
            }
        }

        return $return;
    }

    public function transformVideoToVkVideo($vk_url, $group_id, $access_token, $content){
        $filename = storage_path('app/public').'/'.$content;
        $log = new Log();
        $log->text = $filename;
        $log->save();

        $cfile = curl_file_create($filename,mime_content_type($filename),$content);
        $response = $this->makeRequest($vk_url, ['photo' => $cfile]);
        $json = $response;

        return $json;
    }

    public function transformPostAtachmentVideoYoutube($attachment, $group_id, $access_token){        
        $da = [
            'name' => '',
            'wallpost' => 0,
            'link' => $attachment->content,
            'group_id' => $group_id,
            'access_token' => $access_token,
            'v' => '5.131',
        ];

        $url = "https://api.vk.com/method/video.save?".http_build_query($da);
        //$response = file_get_contents($url);
        $response = $this->makeRequest($url);
        $info = $response;

        $return = false;
        
        if (isset($info->response->upload_url)){
            $url = $info->response->upload_url;
            file_get_contents($url);
            
            $code = "video".$info->response->owner_id.'_'.$info->response->video_id;

            $content = '';
            $type = 'video-vk';

            /*$atach = CompanyPostAttachment::find($attachment->id);
            $atach->attachment = $code;
            $atach->content = $content;
            $atach->type = $type;
            $atach->save();*/
            
            $return = $code;
        }

        return $return;
    }

    public function transformPostAtachmentPlaylist($attachment){
        $link = $attachment->content;
        $arr = explode("z=", $link);
        $return = false;

        if(isset($arr[1])){
            $url = $arr[1];
            $url = str_replace(['/','%2F'], '_', $url);

            /*$atach = CompanyPostAttachment::find($attachment->id);
            $atach->attachment = $url;
            $atach->save();*/

            $return = $url;
        }

        return $return;
    }

    public function transformPostAtachmentAudio($attachment, $group_id, $access_token){
        $da = [
            'access_token' => $access_token,
            'v' => '5.131',
        ];

        $url = "https://api.vk.com/method/audio.getUploadServer?".http_build_query($da);
        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $info = $response;

        $return = false;
        
        if (isset($info->response->upload_url)){
            $url = $info->response->upload_url;
            $arr = explode('/', $attachment->content);
            $name = $arr[count($arr)-1];
            $response = $this->transformAudioToVkImage($url, $group_id, $access_token, $name);
            
            if (isset($response->response)){
                $response = $response->response;

                $code = "audio".$response->owner_id.'_'.$response->id;
                $content = $response->url;
                $type = 'audio-vk';
                
                //unlink(storage_path('app/public').'/'.$name);

                /*$atach = PostAttachment::find($attachment->id);
                $atach->attachment = $code;
                $atach->content = $content;
                $atach->type = $type;
                $atach->save();*/

                $return = $code;
            }
        }

        return $return;
    }

    public function transformAudioToVkImage($vk_url, $group_id, $access_token, $content){
        $filename = storage_path('app/public').'/'.$content;

        $cfile = curl_file_create($filename,'audio/mp3',$content);
        $response = $this->makeRequest($vk_url, ['file' => $cfile]);

        $json = [];
        if (isset($response->audio)){
            $da = [
                'group_id' => $group_id,
                'access_token' => $access_token,
                'audio' => $response->audio,
                'server' => $response->server,
                'hash' => $response->hash,
                'v' => '5.131',
            ];

            $url = "https://api.vk.com/method/audio.save?".http_build_query($da);
            //$response = file_get_contents($url);
            $response = $this->makeRequestGet($url);
            $json = $response;
        }

        return $json;
    }

   public function makeRequestWithoutFile($url, $data = []){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $server_output = curl_exec($ch);
        curl_close ($ch);
        
        return json_decode($server_output);
    }

    public function makeRequest($url, $data = []){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data; charset=UTF-8'));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $server_output = curl_exec($ch);
        curl_close ($ch);
        
        return json_decode($server_output);
    }

    public function makeRequestGet($url, $data = []){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $server_output = curl_exec($ch);
        curl_close ($ch);
        
        return json_decode($server_output);
    }

    public function makeRequestGetNotDecode($url, $data = []){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $server_output = curl_exec($ch);
        curl_close ($ch);
        
        return $server_output;
    }

    public function checkGroupDateCount(Request $request){
        if (!isset($request->not_count)){
            if($request->group){
                $json = array();
                foreach ($request->group as $key => $group) {
                    $dt = explode(' ', $group['dt']);
                    $dt = $dt[0];
                    $dt = date('Y-m-d',strtotime($dt));

                    $posts = CompanyPostToGroup::where('group_id',$key)->where('publish_date', 'like', $dt.'%')->where('not_count',0)->get();
                    
                    if ($posts->count() >= 3){
                        $group_info = Group::find($key);
                        $json[] = $group_info->url." исчерпан лимит рекламных постов на день.";
                    }
                }
                if ($json){
                    return json_encode(['success' => false, 'error' => $json]);
                }
                else{
                    return json_encode(['success' => true]);
                }
            }
            else{
                return json_encode(['success' => true]);
            }
        }
        else{
            return json_encode(['success' => true]);
        }
    }

    public function checkGroupDateCountNative(Request $request){
        return json_encode(['success' => true]);
    }

    public function generatePostReport(Request $request){
        $post_id = $request->id;

        $key = 0;

        $flag = false;
        while (!$flag) {
            $key =  rand(10000000000,99999999999);

            $post = CompanyPost::where('report', $key)->get();

            if ($post->count() <= 0){
                $post = CompanyPost::find($post_id);
                $post->report = $key.'';
                $post->report_dt = now();
                $post->save();
                $flag = true;
            }
        }

        echo $key;
    }

    public function companyEditPost(Request $request){
        $company_post_id = $request->company_post_id;
        $group_id = $request->company_post_group_id;
        $vk_link = $request->vk_link;
        $viewed = $request->viewed;

        $update = [
            'viewed' => $viewed
        ];

        $cp = CompanyPostToGroup::where('company_post_id', $company_post_id)->where('group_id',$group_id)->first();

        if ($vk_link != '' && $vk_link != $cp->vk_link){
            $update['vk_link'] = $vk_link;
            $arr = explode('_', $vk_link);

            $vk_id = $arr[count($arr)-1];
            $update['vk_id'] = $vk_id;
            $update['publish_date'] = now();
            $update['status'] = 1;
        }
       
        DB::table('company_posts_to_groups')->where([
            'company_post_id' => $company_post_id,
            'group_id' => $group_id
        ])->update($update);

        echo 1;
    }

    public function ended(){
        $data = [];

        $data['breadcrumbs'][] = [
            'link' => route('companys-end'),
            'title' => 'Завершенные посты в рекламных компаниях'
        ];

        return view('panel.company.ended')->with('data',$data);
    }

    public function endedAjax(){
        $company_posts = [];
        $results = CompanyPost::whereIn('id', function($query){
            $query->select('company_post_id')
            ->from(with(new CompanyPostEnd)->getTable())->where('dt', '<=', Carbon::now()->subDays(2));
        })->get();
        foreach ($results as $key => $result) {
            $groups = [];
            $groups_for_posts = $result->post_groups()->get();
            foreach ($groups_for_posts as $k => $groups_for_post) {
                $group = Group::find($groups_for_post->group_id);
                if($group)
                  $groups[] = $group->url.' '.date('d.m.Y H:i', strtotime($groups_for_post->publish_date));
            }
            $company_posts[] = [
                $result->id,
                '<a href="'.$result->company->client->vk_profile.'" target="_blank">'.$result->company->client->name.'</a>',
                ($result->company->client->manager_id != '') ? $result->company->client->manager->name : '',
                '<a href="#" class="company-post-report"><i class="fa fa-file-text-o" aria-hidden="true"></i></a>',
                '<a class="btn btn-danger btn-sm remove-ended" href="'.route('companys-ended-remove-one',$result->id).'"><i class="fa fa-pencil"></i>&nbsp;Отписал</a>'
            ];
        }

        echo json_encode(['aaData' => $company_posts]);
    }

    public function endedDropOne(Request $request, $id){
        $cpe = CompanyPostEnd::where('company_post_id', $id)->delete();

        $item = new CompanyPostEndDeleted();
        $item->company_post_id = $id;
        $item->save();

        echo 1;
    }

    public function addPostFormNative($company_id){
        $data = [];

        $company_info = Company::find($company_id);

        $data['company_id'] = $company_id;

        $data['breadcrumbs'][] = [
            'link' => route('companys'),
            'title' => 'Рекламные компании'
        ];

        $data['breadcrumbs'][] = [
            'link' => route('company-edit', $company_id),
            'title' => $company_info['title']
        ];

        $data['breadcrumbs'][] = [
            'link' => route('company-posts', $company_id),
            'title' => 'Посты'
        ];

        $data['breadcrumbs'][] = [
            'link' => route('company-post-add', $company_id),
            'title' => 'Добавить'
        ];

        $data['title'] = 'VK.POSTER - Добавить нативный пост в рекламную компанию';
        $data['h1'] = 'Добавить нативный пост в рекламную компанию';

        $data['description'] = '';
        $data['link'] = '';
        $data['comments'] = '';
        $data['copyright'] = '';
        $data['ttitle'] = '';
        $data['post_id'] = false;



        $groups = Group::all();
        $data['groups'] = [];
        foreach ($groups as $key => $group) {
            $response = false;
            $shedule_id = 0;
            /*$shedules =  $group->shedule()->type('native')->overnow(date('H:i:s'))->get();
            if ($shedules->count() > 0){
                foreach ($shedules as $key => $shedule) {
                    if ($response) continue;
                    $posts = $group->company_group_post()->where('publish_date', date('Y-m-d').' '.$shedule->value)->get();

                    if ($posts->count() == 0){
                        $response = date('d.m.Y').' '.$shedule->value;
                        $shedule_id = $shedule->id;
                    }

                }
            }

            if (!$response){
                $shedules = $group->shedule()->type('native')->ordered()->get();
                if ($shedules->count() > 0){
                    $i = 1;
                    while(!$response){
                        foreach ($shedules as $key => $shedule) {
                            if ($response) continue;
                            $posts = $group->company_group_post()->where('publish_date', date('Y-m-d', strtotime('+'.$i.' day')).' '.$shedule->value)->get();
                            //echo date('Y-m-d', strtotime('+'.$i.' day')).' '.$shedule->value." --- ".$posts->count()." --- ".$group->id."<br>";
                            if ($posts->count() == 0){
                                $response = date('d.m.Y',strtotime('+'.$i.' day')).' '.$shedule->value;
                                $shedule_id = $shedule->id;
                            }
                        }
                        $i++;
                    }
                }
            }*/
           
            if (!$response){
                //$response = 'Нету рассписания';
                $response = 'Поиск...';
            }

            $data['groups'][] = [
                'id' => $group->id,
                'url' => $group->url,
                'name' => $group->name,
                'shedule' => $response,
                'data-shedule' => $shedule_id
            ];

            
        }

        $data['tags'] = Tag::all();
            
        $company_tags = [];
        $tags = $company_info->tags;
        foreach ($tags as $key => $tag) {
            $company_tags[] = $tag->tag;
        }

        if ($company_info->tagpack_id){
            $tagpack_info = Tagpack::find($company_info->tagpack_id);
            foreach ($tagpack_info->tags as $key => $tag) {
                $company_tags[] = $tag->tag;
            }
        }
        
        $company_tags_total = [];
        foreach ($company_tags as $key => $company_tag) {
            if (!isset($company_tags_total[$company_tag->id]))
                $company_tags_total[$company_tag->id] = 0;
            $company_tags_total[$company_tag->id] += $company_tag->quantity;
        }
        
        foreach ($company_info->companyposts as $ke => $post) {
            if ($post->tag_id){
                $company_tags_total[$post->tag_id] -= $post->tag_quantity;
            }
            if ($post->ttag_id){
                $company_tags_total[$post->ttag_id] -= $post->ttag_quantity;
            }
        }
        
        $data['show_tags'] = [];
        foreach ($company_tags_total as $key => $item) {
            if ($item <= 0)
                continue;
            $ta = Tag::find($key);
            if ($ta){
                $data['show_tags'][] = $ta->id;
            }
        }

        $data['packages'] = Package::all();

        return view('panel.company.post-add-native')->with('data',$data);
    }

    public function checkGroupSheduleNative(Request $request){
        $group_id = $request->group_id;
        $shedule = $request->shedule;

        $date = date('Y-m-d', strtotime($shedule));
        $time = date('H:i', strtotime($shedule));
        $publish_date = date('Y-m-d H:i:s', strtotime($shedule));

        $response = false;
        $shedules = Group::find($group_id)->shedule()->where('value','!=',$time)->type('native')->overnow($time)->get();

        if ($shedules->count() > 0){
            foreach ($shedules as $key => $shedule) {
                if ($response) continue;
                $posts = Group::find($group_id)->company_group_post()->where('publish_date', $date.' '.$shedule->value)->get();

                if ($posts->count() == 0){
                    $response = date('d.m.Y', strtotime($date)).' '.$shedule->value;
                }

            }
        }

        if (!$response){
            $shedules = Group::find($group_id)->shedule()->type('native')->ordered()->get();
            if ($shedules->count() > 0){
                $i = 1;
                while(!$response){
                    foreach ($shedules as $key => $shedule) {
                        if ($response) continue;
                        $posts = Group::find($group_id)->company_group_post()->where('publish_date', date('Y-m-d',strtotime($date.'+'.$i.' day')).' '.$shedule->value)->get();

                        if ($posts->count() == 0){
                            $response = date('d.m.Y',strtotime($date.'+'.$i.' day')).' '.$shedule->value;
                        }
                    }
                    $i++;
                }
            }
        }

        echo $response;
    }

    public function pin(Request $request){
        $data = [];

        $data['breadcrumbs'][] = [
            'link' => route('company-pin'),
            'title' => 'Закрепленная запись'
        ];

        $data['success'] = false;
        $data['error'] = false;
        if ($request->session()->get('company_pinned')){
            $data['success'] = $request->session()->pull('company_pinned');
        }
        if ($request->session()->get('company_pinned_error')){
            $data['error'] = $request->session()->pull('company_pinned_error');
        }

        $data['pinned'] = [];
        $company_post = CompanyPost::where('pin', 1)->first();
        if ($company_post){
            $data['pinned'] = $company_post;
        }

        return view('panel.company.pin')->with('data',$data);
    }

    public function pinAdd(Request $request){

        $company_post_id = $request->company_post_id;
        $date = $request->pin_date;
        $time = $request->pin_time;

        $company_post = CompanyPost::find($company_post_id);
        if ($company_post){
            if (isset($request->unpin)){
                $posts = CompanyPostToGroup::where('company_post_id', $company_post_id)->get();
                foreach ($posts as $key => $post) {
                    $group_info = Group::find($post->group_id);
                    $access_token =  User::find($group_info->user_id)->vk_token;
                    $data = [
                        'owner_id' => '-'.$group_info->idd,
                        'access_token' => $access_token,
                        'post_id' => $post->vk_id,
                        'v' => '5.131',
                    ];

                    $url = "https://api.vk.com/method/wall.unpin?".http_build_query($data);
                    $response = $this->makeRequestGet($url);
                    $response = json_encode($response);

                    CompanyPostToGroup::where('company_post_id', $company_post_id)->where('group_id', $post->group_id)->update(['pin' => 0, 'pin_message' => null]);
                }

                CompanyPost::where('pin', 1)->update(['pin' => 0, 'pin_to' => null]);
                CompanyPostToGroup::where('pin', 1)->update(['pin' => 0, 'pin_message' => null]);
                $request->session()->put('company_pinned', 'Пост откреплен.');
            }
            else{
                CompanyPost::where('pin', 1)->update(['pin' => 0, 'pin_to' => null]);
                CompanyPostToGroup::where('pin', 1)->update(['pin' => 0, 'pin_message' => null]);
                $company_post->pin = 1;
                $company_post->pin_to = $date.' '.$time;
                $company_post->save();

                $posts = CompanyPostToGroup::where('company_post_id', $company_post_id)->get();
                foreach ($posts as $key => $post) {
                    $group_info = Group::find($post->group_id);
                    $access_token =  User::find($group_info->user_id)->vk_token;
                    $data = [
                        'owner_id' => '-'.$group_info->idd,
                        'access_token' => $access_token,
                        'post_id' => $post->vk_id,
                        'v' => '5.131',
                    ];

                    $url = "https://api.vk.com/method/wall.pin?".http_build_query($data);
                    $response = $this->makeRequestGet($url);

                    $status = 0;
                    if (isset($response->response)){
                        if ($response->response == 1)
                            $status = 1;
                    }

                    $response = json_encode($response);

                    CompanyPostToGroup::where('company_post_id', $company_post_id)->where('group_id', $post->group_id)->update(['pin' => $status, 'pin_message' => $response]);
                }
                $request->session()->put('company_pinned', 'Пост закреплен.');
            }
        }
        else{
            $request->session()->put('company_pinned_error', '<strong>Ошибка!</strong> Пост с данным ID '.$company_post_id.' не найден!');
        }

        return redirect(route('company-pin'));
    }

    public function unpin(){
        $company_post = CompanyPost::where('pin', 1)->where('pin_to','<=',now())->first();
        if ($company_post){
            $posts = CompanyPostToGroup::where('company_post_id', $company_post->id)->get();
            foreach ($posts as $key => $post) {
                $group_info = Group::find($post->group_id);
                $access_token =  User::find($group_info->user_id)->vk_token;
                $data = [
                    'owner_id' => '-'.$group_info->idd,
                    'access_token' => $access_token,
                    'post_id' => $post->vk_id,
                    'v' => '5.131',
                ];

                $url = "https://api.vk.com/method/wall.unpin?".http_build_query($data);
                $response = $this->makeRequestGet($url);
                $response = json_encode($response);

                CompanyPostToGroup::where('company_post_id', $company_post->id)->where('group_id', $post->group_id)->update(['pin' => 0, 'pin_message' => null]);
            }

            CompanyPost::where('pin', 1)->update(['pin' => 0, 'pin_to' => null]);
            CompanyPostToGroup::where('pin', 1)->update(['pin' => 0, 'pin_message' => null]);
        }
        else{
            echo "NOTHING TO DO!!!!";
        }
        /*$posts = CompanyPostToGroup::where('company_post_id', $company_post_id)->get();
        foreach ($posts as $key => $post) {
            $group_info = Group::find($post->group_id);
            $access_token =  User::find($group_info->user_id)->vk_token;
            $data = [
                'owner_id' => '-'.$group_info->idd,
                'access_token' => $access_token,
                'post_id' => $post->vk_id,
                'v' => '5.92',
            ];

            $url = "https://api.vk.com/method/wall.unpin?".http_build_query($data);
            $response = $this->makeRequestGet($url);
            $response = json_encode($response);

            CompanyPostToGroup::where('company_post_id', $company_post_id)->where('group_id', $post->group_id)->update(['pin' => 0, 'pin_message' => null]);
        }

        CompanyPost::where('pin', 1)->update(['pin' => 0, 'pin_to' => null]);
        CompanyPostToGroup::where('pin', 1)->update(['pin' => 0, 'pin_message' => null]);*/
    }

    public function checkGroupDateByDays(Request $request){
        $group_id = $request->group_id;
        $k = $request->k;

        if ($k == 0){
            $shedule = date('Y-m-d H:i', strtotime($request->date_start.' '.date("H:i")));
        }
        else{
            $date = new \DateTime(date('Y-m-d', strtotime($request->date_start)));
            $date->modify('+'.$k.' day');
            $shedule = $date->format('Y-m-d')." 00:00";
        }

        $date = date('Y-m-d', strtotime($shedule));
        $time = date('H:i', strtotime($shedule));
        $publish_date = date('Y-m-d H:i:s', strtotime($shedule));

        $response = false;
        if (date('Y-m-d') != date('Y-m-d', strtotime($shedule))){
            $shedules = Group::find($group_id)->shedule()->where('value','!=',$time)->type('adv')->get();
        }
        else{
            $shedules = Group::find($group_id)->shedule()->where('value','!=',$time)->type('adv')->overnow($time)->get();
        }
        
        $added_posts = [];
        $items = Group::find($group_id)->company_group_post()->where('publish_date','>=',now())->get();
        foreach ($items as $ke => $item) {
            $added_posts[date('Y-m-d H:i', strtotime($item->publish_date))] = 1;
        }

        if ($shedules->count() > 0){
            foreach ($shedules as $key => $shedule) {
                if ($response) continue;
                /*$posts = Group::find($group_id)->company_group_post()->where('publish_date', $date.' '.$shedule->value)->get();

                if ($posts->count() == 0){
                    $response = date('d.m.Y', strtotime($date)).' '.$shedule->value;
                }*/
                if (!isset($added_posts[$date.' '.$shedule->value]))
                    $response = date('d.m.Y', strtotime($date)).' '.$shedule->value;
            }
        }

        if (!$response){
            /** UPDATED 25.01*/
            //$shedules = Group::find($group_id)->shedule()->type('adv')->overnow($publish_date)->ordered()->get();
            /** END UPDATED 25.01*/
            $shedules = Group::find($group_id)->shedule()->type('adv')->ordered()->get();
            if ($shedules->count() > 0){
                $i = 1;
                while(!$response){
                    foreach ($shedules as $key => $shedule) {
                        if ($response) continue;
                        /*$posts = Group::find($group_id)->company_group_post()->where('publish_date', date('Y-m-d',strtotime($date.'+'.$i.' day')).' '.$shedule->value)->get();

                        if ($posts->count() == 0){
                            $response = date('d.m.Y',strtotime($date.'+'.$i.' day')).' '.$shedule->value;
                        }*/
                        if (!isset($added_posts[date('Y-m-d',strtotime($date.'+'.$i.' day')).' '.$shedule->value]))
                            $response = date('d.m.Y',strtotime($date.'+'.$i.' day')).' '.$shedule->value;
                    }
                    $i++;
                }
            }
            else{
                $response = 'Нету рассписания';
            }
        }

        echo $response;
    }

    public function timable(){
        $data = [];

        $data['breadcrumbs'][] = [
            'link' => route('company-timable'),
            'title' => 'Расписание рекламы'
        ];

        $data['years'] = ['2019','2020','2021'];
        $data['months'] = [
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь'
        ];

        $data['current_month'] = date('m');
        $data['current_year'] = date('Y');
        //die($data['current_year']);
        return view('panel.company.timable')->with('data',$data);
    }

    public function timableTable(Request $request){
        $month = $request->month;
        $year = $request->year;

        $days = [
            'Sun' => 'Воскресенье',
            'Mon' => 'Понедельник',
            'Tue' => 'Вторник',
            'Wed' => 'Среда',
            'Thu' => 'Четверг',
            'Fri' => 'Пятница',
            'Sat' => 'Суббота'
        ];

        $data = [];

        $dates = [];
        $number = cal_days_in_month(CAL_GREGORIAN, (int)$month, $year);
        for ($i=1;$i<=$number;$i++){
            $dates[] = [
                'dt' => date('d.m.Y', strtotime($year.'-'.$month.'-'.$i)),
                'day' => $days[date('D', strtotime($year.'-'.$month.'-'.$i))]
            ];
        }
        
        $data['dates'] = $dates;

        $data['groups'] = array();

        $groups = Group::orderBy('users_current','DESC')->get();
        foreach ($groups as $key => $group) {
            $items = [];
            for ($i=1;$i<=$number;$i++){
                $ii = $i;
                if (strlen($i) == 1)
                    $ii = '0'.$i;
                $date = $year.'-'.$month.'-'.$ii;
                $posts = CompanyPostToGroup::where('group_id',$group->id)->where('publish_date','like',$date.'%')->where('not_count',0)->orderBy('publish_date', 'ASC')->get();
                if ($posts->count() > 0)
                    $items[$date] = $posts;
                else
                    $items[$date] = false;
            }
            
            $data['groups'][] = [
                'name' => $group->name,
                'href' => $group->url,
                'id' => $group->id,
                'posts' => $items
            ];
        }

        return view('panel.company.timable-table')->with('data',$data);
    }

    public function checkGroupDateByDaysNative(Request $request){
        $group_id = $request->group_id;
        $k = $request->k;

        if ($k == 0){
            $shedule = date('Y-m-d H:i', strtotime($request->date_start.' '.date("H:i")));
        }
        else{
            $date = new \DateTime(date('Y-m-d', strtotime($request->date_start)));
            $date->modify('+'.$k.' day');
            $shedule = $date->format('Y-m-d')." 00:00";
        }

        $date = date('Y-m-d', strtotime($shedule));
        $time = date('H:i', strtotime($shedule));
        $publish_date = date('Y-m-d H:i:s', strtotime($shedule));

        $response = false;
        if (date('Y-m-d') != date('Y-m-d', strtotime($shedule))){
            $shedules = Group::find($group_id)->shedule()->where('value','!=',$time)->type('native')->get();
        }
        else{
            $shedules = Group::find($group_id)->shedule()->where('value','!=',$time)->type('native')->overnow($time)->get();
        }

        $added_posts = [];
        $items = Group::find($group_id)->company_group_post()->where('publish_date','>=',now())->get();
        foreach ($items as $ke => $item) {
            $added_posts[date('Y-m-d H:i', strtotime($item->publish_date))] = 1;
        }
        

        if ($shedules->count() > 0){
            foreach ($shedules as $key => $shedule) {
                if ($response) continue;
                /*$posts = Group::find($group_id)->company_group_post()->where('publish_date', $date.' '.$shedule->value)->get();

                if ($posts->count() == 0){
                    $response = date('d.m.Y', strtotime($date)).' '.$shedule->value;
                }*/
                if (!isset($added_posts[$date.' '.$shedule->value]))
                    $response = date('d.m.Y', strtotime($date)).' '.$shedule->value;
            }
        }

        if (!$response){
            /** UPDATED 25.01*/
            //$shedules = Group::find($group_id)->shedule()->type('native')->overnow($publish_date)->ordered()->get();
            /** END UPDATED 25.01*/
            $shedules = Group::find($group_id)->shedule()->type('native')->ordered()->get();
            //$shedules = Group::find($group_id)->shedule()->type('native')->ordered()->get();
            if ($shedules->count() > 0){
                $i = 1;
                while(!$response){
                    foreach ($shedules as $key => $shedule) {
                        if ($response) continue;
                        /*$posts = Group::find($group_id)->company_group_post()->where('publish_date', date('Y-m-d',strtotime($date.'+'.$i.' day')).' '.$shedule->value)->get();

                        if ($posts->count() == 0){
                            $response = date('d.m.Y',strtotime($date.'+'.$i.' day')).' '.$shedule->value;
                        }*/
                        if (!isset($added_posts[$date.' '.$shedule->value]))
                            $response = date('d.m.Y', strtotime($date.'+'.$i.' day')).' '.$shedule->value;
                    }
                    $i++;
                }
            }
        }

        echo $response;
    }

    public function makeReport(){
        require_once 'simple_html_dom.php';
        
        /*$companies = Company::where('report',0)->where('dt_end',null)->get();
        foreach ($companies as $key => $company) {
            $show_tags = $this->getShowTags($company);

            $finish_dt = strtotime('+1 day', strtotime(date('Y-m-d')));
            $last_date = false;
            $complete = true;

            foreach ($company->companyposts as $key => $companypost) {
                echo $companypost->id."<br>";
                foreach ($companypost->post_groups as $key => $company_post_to_group) {
                    $pd = strtotime(date($company_post_to_group->publish_date));
                    if ($pd > $finish_dt)
                        $complete = false;
                    else{
                        if (!$last_date)
                            $last_date = strtotime(date($company_post_to_group->publish_date));
                        else{
                            if($last_date < strtotime(date($company_post_to_group->publish_date)))
                                $last_date = strtotime(date($company_post_to_group->publish_date));
                        }
                    }
                }
            }

            if (!$show_tags && $finish_dt){
                $company->dt_end = date('Y-m-d', $last_date);
                $company->save();
            }
        }*/
        $items = [];
        //Company::where('report',0)->where('dt_end','<=',now())->update(['report' => 1]);
        $companies = Company::where('report',0)->where('dt_end','<=',now())->orderBy('id', 'DESC')->get();
        foreach ($companies as $ke => $company) {
            $items[] = $company;
        }

        $companies = Company::where('report',0)->orderBy('id', 'DESC')->get();
        foreach ($companies as $ke => $company) {
            $views = 0;
            $posts = 0;
            foreach ($company->companyposts as $key => $post) {
                $views += $post->viewed;
                $posts++;
            }

            if ($views >= $company->views && $posts >= $company->quantity){
                echo $company->id."<br>";
                echo $company->views.' --- '.$views."<br>";
                echo $company->quantity.' --- '.$posts."<br><br>";
                $items[] = $company;
            }
        }
        

        foreach ($items as $key => $company_info) {
            $company_id = $company_info->id;
            CompanyPostActivity::where('company_id', $company_id)->delete();
            foreach ($company_info->companyposts as $key => $post) {
                foreach ($post->post_groups as $key => $post_group) {
                    if (!$post_group->vk_id)
                        continue;

                    $comments = 0;
                    $likes = 0;
                    $reposts = 0;
                    $comment_likes = 0;

                    if ($company_id > 948){
                        $group_id = "-".$post_group->group->idd;

                        $data = [
                            'owner_id' => $group_id,
                            'post_id' => $post_group->vk_id,
                            'access_token' => $post_group->group->user->vk_token,
                            'count' => 1000,
                            'v' => '5.131',
                        ];

                        $url = "https://api.vk.com/method/wall.getReposts?".http_build_query($data);
                        $response = $this->makeRequestGet($url);
                        $info = $response;
                        if (isset($info->response)){
                            foreach ($info->response->profiles as $key => $profile) {

                                $activity = new CompanyPostActivity();
                                $activity->group_id = $post_group->group_id;
                                $activity->company_post_id = $post->id;
                                $activity->company_id = $company_id;
                                $activity->type = 'repost';
                                $activity->value = $profile->id;
                                $activity->save();
                                $reposts++;
                            }
                        }

                        $data['need_likes'] = 1;
                        $data['count'] = 100;
                        $data['extended'] = 1;

                        $url = "https://api.vk.com/method/wall.getComments?".http_build_query($data);
                        $response = $this->makeRequestGet($url);
                        $info = $response;
                        
                        if (isset($info->response)){
                            $comments += $info->response->count;
                            foreach ($info->response->profiles as $key => $profile) {
                                $activity = new CompanyPostActivity();
                                $activity->group_id = $post_group->group_id;
                                $activity->company_post_id = $post->id;
                                $activity->company_id = $company_id;
                                $activity->type = 'comment';
                                $activity->value = $profile->id;
                                $activity->save();
                            }
                            if (isset($info->response->items)){
                                foreach ($info->response->items as $key => $item) {
                                    $comment_likes += isset($item->likes->count) ? $item->likes->count : 0;
                                }
                            }

                        
                            if ($comments > 100){
                                for ($i=100;$i<=$comments;$i+=100){
                                    $data['offset'] = $i;
                                    $url = "https://api.vk.com/method/wall.getComments?".http_build_query($data);
                                    $response = $this->makeRequestGet($url);
                                    $info = $response;
                                    $comments += $info->response->count;
                                    foreach ($info->response->profiles as $key => $profile) {
                                        $activity = new CompanyPostActivity();
                                        $activity->group_id = $post_group->group_id;
                                        $activity->company_post_id = $post->id;
                                        $activity->company_id = $company_id;
                                        $activity->type = 'comment';
                                        $activity->value = $profile->id;
                                        $activity->save();
                                    }
                                    if (isset($info->response->items)){
                                        foreach ($info->response->items as $key => $item) {
                                            $comment_likes += $item->likes->count;
                                        }
                                    }
                                }
                            }
                        }

                        $likes += $comment_likes;

                        $data = [
                            'owner_id' => $group_id,
                            'item_id' => $post_group->vk_id,
                            'access_token' => $post_group->group->user->vk_token,
                            'count' => 1000,
                            'v' => '5.131',
                            'extended' => 1,
                            'type' => 'post'
                        ];
                        $url = "https://api.vk.com/method/likes.getList?".http_build_query($data);
                        $response = $this->makeRequestGet($url);
                        $info = $response;
                        if (isset($info->response->items)){
                            $likes += $info->response->count;
                            foreach ($info->response->items as $key => $item) {
                                $activity = new CompanyPostActivity();
                                $activity->group_id = $post_group->group_id;
                                $activity->company_post_id = $post->id;
                                $activity->company_id = $company_id;
                                $activity->type = 'like';
                                $activity->value = $item->id;
                                $activity->save();
                            }
                        

                            if ($info->response->count > 1000){
                                for ($i=1000;$i<=$info->response->count;$i+=1000){
                                    $data['offset'] = $i;
                                    $url = "https://api.vk.com/method/likes.getList?".http_build_query($data);
                                    $response = $this->makeRequestGet($url);
                                    $info = $response;
                                    if (isset($info->response->items)){
                                        foreach ($info->response->items as $key => $item) {
                                            $activity = new CompanyPostActivity();
                                            $activity->group_id = $post_group->group_id;
                                            $activity->company_post_id = $post->id;
                                            $activity->company_id = $company_id;
                                            $activity->type = 'like';
                                            $activity->value = $item->id;
                                            $activity->save();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $vk_reposts = 0;
                    $html = file_get_html($post_group->vk_link);
                    if ($html){ 
                        $items = $html->find(".v_share");
                        if (isset($items[0])){
                            $vk_reposts = (int)str_replace(' ', '', $items[0]->plaintext);
                        }
                    }

                    if ($vk_reposts > $reposts)
                        $reposts = $vk_reposts;

                    DB::table('company_posts_to_groups')->where([
                        'company_post_id' => $post->id,
                        'group_id' => $post_group->group_id
                    ])->update(['repost' => $reposts, 'comment' => $comments, 'like' => $likes]);

                    //usleep(500000);
                    if ($company_id > 948){
                        sleep(1);
                    }
                }


                
            }

            Company::where('id', $company_info->id)->update(['report' => 1, 'activity' => '1', 'report_dt' => now()]);
        }
        //Company::where('report',0)->where('dt_end','<=',now())->update(['report' => 1, 'activity' => '1']);
    }

    public function report(){
        $data = [];

        return view('panel.company.report')->with('data',$data);
    }

    public function reportAjax(){
        $companies = [];
        $results = Company::where('report',1)->where('status', 0)->orderBy('report_dt', 'DESC')->get(); 
        
        foreach ($results as $key => $result) {
            $count = 0;
            $activitys = 0;
            $posts = CompanyPost::where('company_id', '=', $result['id'])->get();
            foreach ($posts as $ke => $post) {
                foreach ($post->post_groups as $k => $post_group) {
                    if (!$post_group->viewed)
                        $post_group->viewed = 0;
                    if (!$post_group->comment)
                        $post_group->comment = 0;
                    if (!$post_group->like)
                        $post_group->like = 0;
                    if (!$post_group->repost)
                        $post_group->repost = 0;
                    $count += $post_group->viewed;
                    $activitys += ($post_group->comment + $post_group->like + $post_group->repost);
                }
            }

            $message = Setting::where('key','report_template')->first()->value;
            $message = str_replace('{client}', $result->client->name, $message);
            $message = str_replace('{views}', $count, $message);
            $message = str_replace('{price}', number_format((($count != 0) ? ($result->total/$count)*1000 : 0),2), $message);
            /*echo ($result->total/$count)."<br>";
            echo (($result->total/$count)*1000)."<br>";
            echo Setting::where('key','report_target')->first()->value;*/
            $message = str_replace('{odds}', ((int)Setting::where('key','report_target')->first()->value -  (int)number_format((($count != 0 && $count != '') ? ($result->total/$count)*1000 : 0),2)), $message);
            $message = str_replace('{activations}', $activitys, $message);

            $activity = [];
            $activity [] = '<button class="btn btn-primary btn-sm gather-activity" href="'.route('company-report-activity', $result->id).'" data-id="'.$result->id.'">Собрать</button>';

            if ($result->activity == 1){
                $activity[] = "<span style='color:green'>Активности собраны</span><br> <span style='color:green'>Найдено $activitys активностей</span>";

                $activity[] = '<a href="'.route('company-dowload-report', $result->id).'">Скачать</a>'; 
            }

            $flag = false;
            $flag = Company::where('id',$result->id)->where('client_report_dt', '<', DB::raw('NOW() - INTERVAL 2 MONTH'))->first();
            
            $cy = Company::find($result->id);
            if ($flag){
                $cy->client_report = null;
                $cy->save();
            }

            if ($cy->client_report){
                $client_report = "<a target='_blank' href='http://rmo-report.top/company-report.php?id=$cy->client_report'>Отчет</a>";
            }
            else{
                $client_report = '<button class="btn btn-primary btn-sm generate-report" data-id="'.$result->id.'">Сгенерировать</button>';
            }
            
            $count = 0;
            $posts = CompanyPost::where('company_id', '=', $result['id'])->get();
            foreach ($posts as $ke => $post) {
                foreach ($post->post_groups as $k => $post_group) {
                    if (!$post_group->viewed)
                        $post_group->viewed = 0;
                    $count += $post_group->viewed;
                }
            }

            $companies[] = [
                $result->id,
                ($result->report_dt) ? date('d.m.Y', strtotime($result->report_dt)) : '',
                (isset($result->client->manager->name)) ? $result->client->manager->name : '',
                $result->title,
                '<a href="'.$result->client->vk_profile.'" target="_blank">'.$result->client->name.'</a>',
                $message,
                '<a class="btn btn-default btn-sm copy-clipboard" href="#" data-text="'.$message.'"><i class="fa fa-save"></i></a>',
                number_format((($count != 0 && $count != '') ? ($result->total/$count)*1000 : 0),2),
                $count,
                implode($activity, '<br>'),
                $client_report,
                ($result->readed == 0) ? '<a class="btn btn-success btn-sm do-readed" href="'.route('company-report-readed',$result->id).'"><i class="fa fa-pencil"></i>&nbsp;Отписать</a>' : '<a class="btn btn-danger btn-sm">Отписано</a>',
                '<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>'
                
            ];
        }

        echo json_encode(['aaData' => $companies]);
    }

    public function downloadReport(Request $request, $company_id){
        $file = "storage/reports/report-".$company_id.".txt";
        $txt = fopen($file, "w");
        $activities = CompanyPostActivity::where('company_id', $company_id)->get();
        foreach ($activities as $key => $activity) {
            fwrite($txt, $activity->value."\r\n");
        }
        fclose($txt);


        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename='.basename($file));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        header("Content-Type: text/plain");
        readfile($file);

    }

    public function reportReaded(Request $request, $company_id){
        $company = Company::find($request->company_id);
        $company->readed = 1;
        $company->readed_dt = now();
        $company->save();

        echo 1;
    }

    public function dropReport(Request $request){
        $company = Company::find($request->id);
        $company->status = 1;
        $company->save();

        echo "1";
    }

    public function reportSettings(){
        $template = Setting::where('key','report_template')->first()->value;
        $target = Setting::where('key','report_target')->first()->value;

        echo json_encode(['template' => $template, 'target' => $target]);
    }

    public function reportSettingsSave(Request $request){
        Setting::where('key','report_template')->update(['value' => $request->template]);
        Setting::where('key','report_target')->update(['value' => $request->target]);
    }

    public function reportActivity(Request $request, $company_id){
        CompanyPostActivity::where('company_id', $company_id)->delete();

        $company_info = Company::find($company_id);
        foreach ($company_info->companyposts as $key => $post) {
            foreach ($post->post_groups as $key => $post_group) {
                if (!$post_group->vk_id)
                    continue;

                $comments = 0;
                $likes = 0;
                $reposts = 0;
                $comment_likes = 0;

                $group_id = "-".$post_group->group->idd;

                $data = [
                    'owner_id' => $group_id,
                    'post_id' => $post_group->vk_id,
                    'access_token' => $post_group->group->user->vk_token,
                    'count' => 1000,
                    'v' => '5.131',
                ];

                $url = "https://api.vk.com/method/wall.getReposts?".http_build_query($data);
                $response = $this->makeRequestGet($url);
                $info = $response;
                foreach ($info->response->profiles as $key => $profile) {
                    $activity = new CompanyPostActivity();
                    $activity->group_id = $post_group->group_id;
                    $activity->company_post_id = $post->id;
                    $activity->company_id = $company_id;
                    $activity->type = 'repost';
                    $activity->value = $profile->id;
                    $activity->save();
                    $reposts++;
                }

                $data['need_likes'] = 1;
                $data['count'] = 100;
                $data['extended'] = 1;

                $url = "https://api.vk.com/method/wall.getComments?".http_build_query($data);
                $response = $this->makeRequestGet($url);
                $info = $response;
                $comments += $info->response->count;
                foreach ($info->response->profiles as $key => $profile) {
                    $activity = new CompanyPostActivity();
                    $activity->group_id = $post_group->group_id;
                    $activity->company_post_id = $post->id;
                    $activity->company_id = $company_id;
                    $activity->type = 'comment';
                    $activity->value = $profile->id;
                    $activity->save();
                }
                if (isset($info->response->items)){
                    foreach ($info->response->items as $key => $item) {
                        $comment_likes += $item->likes->count;
                    }
                }
                
                if ($comments > 100){
                    for ($i=100;$i<=$comments;$i+=100){
                        $data['offset'] = $i;
                        $url = "https://api.vk.com/method/wall.getComments?".http_build_query($data);
                        $response = $this->makeRequestGet($url);
                        $info = $response;
                        $comments += $info->response->count;
                        foreach ($info->response->profiles as $key => $profile) {
                            $activity = new CompanyPostActivity();
                            $activity->group_id = $post_group->group_id;
                            $activity->company_post_id = $post->id;
                            $activity->company_id = $company_id;
                            $activity->type = 'comment';
                            $activity->value = $profile->id;
                            $activity->save();
                        }
                        if (isset($info->response->items)){
                            foreach ($info->response->items as $key => $item) {
                                $comment_likes += $item->likes->count;
                            }
                        }

                        sleep(1);
                    }
                }
                
                $likes += $comment_likes;

                $data = [
                    'owner_id' => $group_id,
                    'item_id' => $post_group->vk_id,
                    'access_token' => $post_group->group->user->vk_token,
                    'count' => 1000,
                    'v' => '5.131',
                    'extended' => 1,
                    'type' => 'post'
                ];
                $url = "https://api.vk.com/method/likes.getList?".http_build_query($data);
                $response = $this->makeRequestGet($url);
                $info = $response;
                if (isset($info->response->items)){
                    $likes += $info->response->count;
                    foreach ($info->response->items as $key => $item) {
                        $activity = new CompanyPostActivity();
                        $activity->group_id = $post_group->group_id;
                        $activity->company_post_id = $post->id;
                        $activity->company_id = $company_id;
                        $activity->type = 'like';
                        $activity->value = $item->id;
                        $activity->save();
                    }
                }

                if ($info->response->count > 1000){
                    for ($i=1000;$i<=$info->response->count;$i+=1000){
                        $data['offset'] = $i;
                        $url = "https://api.vk.com/method/likes.getList?".http_build_query($data);
                        $response = $this->makeRequestGet($url);
                        $info = $response;
                        if (isset($info->response->items)){
                            foreach ($info->response->items as $key => $item) {
                                $activity = new CompanyPostActivity();
                                $activity->group_id = $post_group->group_id;
                                $activity->company_post_id = $post->id;
                                $activity->company_id = $company_id;
                                $activity->type = 'like';
                                $activity->value = $item->id;
                                $activity->save();
                            }
                        }

                        sleep(1);
                    }
                }

                DB::table('company_posts_to_groups')->where([
                    'company_post_id' => $post->id,
                    'group_id' => $post_group->group_id
                ])->update(['repost' => $reposts, 'comment' => $comments, 'like' => $likes]);

                //usleep(500000);
                sleep(1);
            }
            
        }

        $company_info->activity = 1;
        $company_info->save();

        echo 1;
    }

    public function generateCompanyClientReport(Request $request){
        $company_id = $request->id;

        $key = 0;

        $flag = false;
        while (!$flag) {
            $key =  rand(10000000000,99999999999);

            $company = Company::where('client_report', $key)->get();

            if ($company->count() <= 0){
                $company = Company::find($company_id);
                $company->client_report = $key.'';
                $company->client_report_dt = now();
                $company->save();
                $flag = true;
            }
        } 

        echo "<a target='_blank' href='http://rmo-report.top/company-report.php?id=$key'>Отчет</a>";
    }

    public function moveCompanyPost(Request $request){
        $company_id = $request->to_company_id;
        $company_post_id = $request->company_post_id;

        $company = Company::find($company_id);
        if ($company){
            CompanyPost::where('id', $company_post_id)->update(['company_id' => $company_id]);
            CompanyPostActivity::where('company_post_id', $company_post_id)->update(['company_id' => $company_id]);
            echo "1";
        }
        else{
            echo "0";
        }
        
    }

    public function copyCompanyPost(Request $request){
        $company_id = $request->to_company_id;
        $company_post_id = $request->company_post_id;

        $company = Company::find($company_id);
        if ($company){
            $post_info = CompanyPost::find($company_post_id);
            if ($post_info){
                $new_post = new CompanyPost();
                $new_post->description = $post_info->description;
                $new_post->title = $post_info->title;
                $new_post->link = $post_info->link;
                $new_post->user_id = $post_info->user_id;
                $new_post->report = $post_info->report;
                $new_post->company_id = $company_id;
                $new_post->report_dt = $post_info->report_dt;
                $new_post->created_at = $post_info->created_at;
                $new_post->updated_at = $post_info->updated_at;
                $new_post->type = $post_info->type;
                $new_post->pin = $post_info->pin;
                $new_post->pin_to = $post_info->pin_to;
                $new_post->copyright = $post_info->copyright;
                $new_post->comments = $post_info->comments;
                $new_post->tag_id = $post_info->tag_id;
                $new_post->ttag_id = $post_info->ttag_id;
                $new_post->save();

                $new_post_id = $new_post->id;

                $cpas = CompanyPostActivity::where('company_post_id', $company_post_id)->get();
                foreach ($cpas as $key => $cpa) {
                    $activity = new CompanyPostActivity();
                    $activity->group_id = $cpa->group_id;
                    $activity->company_post_id = $new_post_id;
                    $activity->company_id = $company_id;
                    $activity->type = $cpa->type;
                    $activity->value = $cpa->value;
                    $activity->created_at = $cpa->created_at;
                    $activity->updated_at = $cpa->updated_at;
                    $activity->save();
                }

                $company_post_to_groups = $post_info->post_groups;
                foreach ($company_post_to_groups as $key => $company_post_to_group) {
                    $cp2g = new CompanyPostToGroup();
                    $cp2g->company_post_id = $new_post_id;
                    $cp2g->group_id = $company_post_to_group->group_id;
                    $cp2g->vk_id = $company_post_to_group->vk_id;
                    $cp2g->vk_link = $company_post_to_group->vk_link;
                    $cp2g->publish_date = $company_post_to_group->publish_date;
                    $cp2g->status = $company_post_to_group->status;
                    $cp2g->created_at = $company_post_to_group->created_at;
                    $cp2g->updated_at = $company_post_to_group->updated_at;
                    $cp2g->viewed = $company_post_to_group->viewed;
                    $cp2g->not_count = $company_post_to_group->not_count;
                    $cp2g->pin = $company_post_to_group->pin;
                    $cp2g->pin_message = $company_post_to_group->pin_message;
                    $cp2g->repost = $company_post_to_group->repost;
                    $cp2g->comment = $company_post_to_group->comment;
                    $cp2g->like = $company_post_to_group->like;
                    $cp2g->story_shares = $company_post_to_group->story_shares;
                    $cp2g->story_subscribers = $company_post_to_group->story_subscribers;
                    $cp2g->story_bans = $company_post_to_group->story_bans;
                    $cp2g->story_opens = $company_post_to_group->story_opens;
                    $cp2g->story_views = $company_post_to_group->story_views;
                    $cp2g->story_likes = $company_post_to_group->story_likes;
                    $cp2g->save();
                }

                $attachments = $post_info->attachments;
                foreach ($attachments as $key => $attachment) {
                    $atta = new CompanyPostAttachment();
                    $atta->company_post_id = $new_post_id;
                    $atta->attachment = $attachment->attachment;
                    $atta->content = $attachment->content;
                    $atta->type = $attachment->type;
                    $atta->title = $attachment->title;
                    $atta->sort_order = $attachment->sort_order;
                    $atta->save();
                }

                $ends = CompanyPostEnd::where('company_post_id', $company_post_id)->get();
                foreach ($ends as $key => $end) {
                    $e = new CompanyPostEnd();
                    $e->company_post_id = $new_post_id;
                    $e->dt = $end->dt;
                    $e->created_at = $end->created_at;
                    $e->updated_at = $end->updated_at;
                    $e->save();
                }

                $deleteds = CompanyPostEndDeleted::where('company_post_id', $company_post_id)->get();
                foreach ($deleteds as $key => $deleted) {
                    $e = new CompanyPostEndDeleted();
                    $e->company_post_id = $new_post_id;
                    $e->created_at = $deleted->created_at;
                    $e->updated_at = $deleted->updated_at;
                    $e->save();
                }
            }
            
            echo "1";
        }
        else{
            echo "0";
        }
        
    }

    public function clientIndex(){
        $data = [];

        return view('panel.company.client.report')->with('data',$data);
    }

    public function reportAjaxClient(){
        $companies = [];

        $client = Client::where('system_id', Auth::user()->id)->first();

        $results = Company::/*where('report',1)->where('status', 0)->*/where('client_id', $client->id)->orderBy('readed', 'ASC')->get(); 

        foreach ($results as $key => $result) {
            $count = 0;
            $activitys = 0;
            $posts = CompanyPost::where('company_id', '=', $result['id'])->get();
            foreach ($posts as $ke => $post) {
                foreach ($post->post_groups as $k => $post_group) {
                    if (!$post_group->viewed)
                        $post_group->viewed = 0;
                    if (!$post_group->comment)
                        $post_group->comment = 0;
                    if (!$post_group->like)
                        $post_group->like = 0;
                    if (!$post_group->repost)
                        $post_group->repost = 0;
                    $count += $post_group->viewed;
                    $activitys += ($post_group->comment + $post_group->like + $post_group->repost);
                }
            }

            $message = Setting::where('key','report_template')->first()->value;
            $message = str_replace('{client}', $result->client->name, $message);
            $message = str_replace('{views}', $count, $message);
            $message = str_replace('{price}', number_format((($count != 0) ? ($result->total/$count)*1000 : 0),2), $message);
            
            $message = str_replace('{odds}', ((int)Setting::where('key','report_target')->first()->value -  (int)number_format((($count != 0 && $count != '') ? ($result->total/$count)*1000 : 0),2)), $message);
            $message = str_replace('{activations}', $activitys, $message);

            $activity = [];
            $activity [] = '<button class="btn btn-primary btn-sm gather-activity" href="'.route('company-report-activity', $result->id).'" data-id="'.$result->id.'">Собрать</button>';

            if ($result->activity == 1){
                $activity[] = "<span style='color:green'>Активности собраны</span><br> <span style='color:green'>Найдено $activitys активностей</span>";

                $activity[] = '<a href="'.route('company-dowload-report', $result->id).'">Скачать</a>'; 
            }

            $flag = false;
            $flag = Company::where('id',$result->id)->where('client_report_dt', '<', DB::raw('NOW() - INTERVAL 2 MONTH'))->first();
            
            $cy = Company::find($result->id);
            if ($flag){
                $cy->client_report = null;
                $cy->save();
            }

            if ($cy->client_report){
                $client_report = "<a target='_blank' href='http://rmo-report.top/company-report.php?id=$cy->client_report'>Отчет</a>";
            }
            else{
                $client_report = '<button class="btn btn-primary btn-sm generate-report" data-id="'.$result->id.'">Сгенерировать</button>';
            }
            
            $count = 0;
            $posts = CompanyPost::where('company_id', '=', $result['id'])->get();
            foreach ($posts as $ke => $post) {
                foreach ($post->post_groups as $k => $post_group) {
                    if (!$post_group->viewed)
                        $post_group->viewed = 0;
                    $count += $post_group->viewed;
                }
            }

            $companies[] = [
                $result->id,
                $result->title,
                number_format((($count != 0 && $count != '') ? ($result->total/$count)*1000 : 0),2),
                $count,
                $client_report
            ];
        }

        echo json_encode(['aaData' => $companies]);
    }


    public function addPostFormStory($company_id){
        $data = [];

        $company_info = Company::find($company_id);

        $data['company_id'] = $company_id;

        $data['breadcrumbs'][] = [
            'link' => route('companys'),
            'title' => 'Рекламные компании'
        ];

        $data['breadcrumbs'][] = [
            'link' => route('company-edit', $company_id),
            'title' => $company_info['title']
        ];

        $data['breadcrumbs'][] = [
            'link' => route('company-posts', $company_id),
            'title' => 'Посты'
        ];

        $data['breadcrumbs'][] = [
            'link' => route('company-post-add', $company_id),
            'title' => 'Добавить'
        ];

        $data['title'] = 'VK.POSTER - Добавить stories в рекламную компанию';
        $data['h1'] = 'Добавить stories в рекламную компанию';

        $data['description'] = '';
        $data['link'] = '';
        $data['comments'] = '';
        $data['copyright'] = '';
        $data['ttitle'] = '';
        $data['post_id'] = false;

        $groups = Group::all();
        $data['groups'] = [];
        foreach ($groups as $key => $group) {
            $response = false;
            $shedule_id = 0;
            /*$shedules =  $group->shedule()->type('story')->overnow(date('H:i:s'))->get();
            if ($shedules->count() > 0){
                foreach ($shedules as $key => $shedule) {
                    if ($response) continue;
                    $posts = $group->company_group_post()->where('publish_date', date('Y-m-d').' '.$shedule->value)->get();

                    if ($posts->count() == 0){
                        $response = date('d.m.Y').' '.$shedule->value;
                        $shedule_id = $shedule->id;
                    }

                }
            }

            if (!$response){
                $shedules = $group->shedule()->type('story')->ordered()->get();
                if ($shedules->count() > 0){
                    $i = 1;
                    while(!$response){
                        foreach ($shedules as $key => $shedule) {
                            if ($response) continue;
                            $posts = $group->company_group_post()->where('publish_date', date('Y-m-d', strtotime('+'.$i.' day')).' '.$shedule->value)->get();

                            if ($posts->count() == 0){
                                $response = date('d.m.Y',strtotime('+'.$i.' day')).' '.$shedule->value;
                                $shedule_id = $shedule->id;
                            }
                        }
                        $i++;
                    }
                }
            }*/
           
            if (!$response){
                //$response = 'Нету рассписания';
                $response = 'Поиск...';
            }

            $data['groups'][] = [
                'id' => $group->id,
                'url' => $group->url,
                'name' => $group->name,
                'shedule' => $response,
                'data-shedule' => $shedule_id
            ];

        }

        $data['packages'] = Package::all();

        return view('panel.company.post-add-story')->with('data',$data);
    }

     public function checkGroupDateCountStory(Request $request){
        return json_encode(['success' => true]);
    }

    public function checkGroupDateByDaysStory(Request $request){
        $group_id = $request->group_id;
        $k = $request->k;

        if ($k == 0){
            $shedule = date('Y-m-d H:i', strtotime($request->date_start.' '.date("H:i")));
        }
        else{
            $date = new \DateTime(date('Y-m-d', strtotime($request->date_start)));
            $date->modify('+'.$k.' day');
            $shedule = $date->format('Y-m-d')." 00:00";
        }

        $date = date('Y-m-d', strtotime($shedule));
        $time = date('H:i', strtotime($shedule));
        $publish_date = date('Y-m-d H:i:s', strtotime($shedule));

        $response = false;
        if (date('Y-m-d') != date('Y-m-d', strtotime($shedule))){
            $shedules = Group::find($group_id)->shedule()->where('value','!=',$time)->type('story')->get();
        }
        else{
            $shedules = Group::find($group_id)->shedule()->where('value','!=',$time)->type('story')->overnow($time)->get();
        }
        

        if ($shedules->count() > 0){
            foreach ($shedules as $key => $shedule) {
                if ($response) continue;
                $posts = Group::find($group_id)->company_group_post()->where('publish_date', $date.' '.$shedule->value)->get();

                if ($posts->count() == 0){
                    $response = date('d.m.Y', strtotime($date)).' '.$shedule->value;
                }

            }
        }

        if (!$response){
            /** UPDATED 25.01*/
            //$shedules = Group::find($group_id)->shedule()->type('native')->overnow($publish_date)->ordered()->get();
            /** END UPDATED 25.01*/
            $shedules = Group::find($group_id)->shedule()->type('story')->ordered()->get();
            //$shedules = Group::find($group_id)->shedule()->type('native')->ordered()->get();
            if ($shedules->count() > 0){
                $i = 1;
                while(!$response){
                    foreach ($shedules as $key => $shedule) {
                        if ($response) continue;
                        $posts = Group::find($group_id)->company_group_post()->where('publish_date', date('Y-m-d',strtotime($date.'+'.$i.' day')).' '.$shedule->value)->get();

                        if ($posts->count() == 0){
                            $response = date('d.m.Y',strtotime($date.'+'.$i.' day')).' '.$shedule->value;
                        }
                    }
                    $i++;
                }
            }
        }

        echo $response;
    }

    public function checkGroupSheduleStory(Request $request){
        $group_id = $request->group_id;
        $shedule = $request->shedule;

        $date = date('Y-m-d', strtotime($shedule));
        $time = date('H:i', strtotime($shedule));
        $publish_date = date('Y-m-d H:i:s', strtotime($shedule));

        $response = false;
        $shedules = Group::find($group_id)->shedule()->where('value','!=',$time)->type('story')->overnow($time)->get();

        if ($shedules->count() > 0){
            foreach ($shedules as $key => $shedule) {
                if ($response) continue;
                $posts = Group::find($group_id)->company_group_post()->where('publish_date', $date.' '.$shedule->value)->get();

                if ($posts->count() == 0){
                    $response = date('d.m.Y', strtotime($date)).' '.$shedule->value;
                }

            }
        }

        if (!$response){
            $shedules = Group::find($group_id)->shedule()->type('story')->ordered()->get();
            if ($shedules->count() > 0){
                $i = 1;
                while(!$response){
                    foreach ($shedules as $key => $shedule) {
                        if ($response) continue;
                        $posts = Group::find($group_id)->company_group_post()->where('publish_date', date('Y-m-d',strtotime($date.'+'.$i.' day')).' '.$shedule->value)->get();

                        if ($posts->count() == 0){
                            $response = date('d.m.Y',strtotime($date.'+'.$i.' day')).' '.$shedule->value;
                        }
                    }
                    $i++;
                }
            }
        }

        echo $response;
    }

    public function checkGroupDateStory(Request $request){
        $json = ['success' => false];
        $group_id = $request->group_id;
        $date = $request->date;
        $time = $request->time;

        $posts = Group::find($group_id)->company_group_post()->where('publish_date', $date.' '.$time)->get();
        if ($posts->count() != 0){
            $json = ['success' => true];
        }

        return response(json_encode($json))->header('Content-Type', 'text/json');
    }

    public function active(){
        $data = [];

        $data['breadcrumbs'][] = [
            'link' => route('company-active'),
            'title' => 'Активные рекламные компании'
        ];

        return view('panel.company.active')->with('data',$data);
    }

    public function ajaxActive(){
        $companies = [];
        $results = Company::all();
        
        foreach ($results as $key => $result) {
            $quantity = $result->quantity;
            $views = $result->views;
            $tags = false;
            $totals = 0;
            $posts_count = 0;
        
            $post_views = 0;
            $posts = CompanyPost::where('company_id', '=', $result['id'])->get();
            foreach ($posts as $ke => $post) {
                foreach ($post->post_groups as $k => $post_group) {
                    $post_views += $post_group->viewed;
                    //$posts_count++;
                }
                $totals += $post->tag_quantity + $post->ttag_quantity;
                $posts_count += $post->post_groups->count();
            }
            //$posts_count = $posts->count();

            $data['show_tags'] = $this->getShowTags($result);
            /*if ($result->id == '1183'){
                echo $quantity.'---'.$posts_count.'<br>';
                echo $views.'---'.$post_views.'<br>';
                print_r($data['show_tags']); die();
            }*/
            
            if ($quantity > $totals || $views > $post_views || $data['show_tags']){
                $count = 0;
                $posts = CompanyPost::where('company_id', '=', $result['id'])->get();
                foreach ($posts as $ke => $post) {
                    foreach ($post->post_groups as $k => $post_group) {
                        $count += $post_group->viewed;
                    }
                }
                $companies[] = [
                    $result->id/*.' quantity:'.$quantity.' count:'.$posts_count.' views:'.$views.' post_views:'.$post_views*/,
                    '<a href="'.route('company-posts',$result->id).'">'.$result->title.'</a>',
                    
                    '<a href="'.$result->client->vk_profile.'" target="_blank">'.$result->client->name.'</a>',
                    $result->total,
                    number_format((($count != 0) ? ($result->total/$count)*1000 : 0),2),
                    $count,
                    $result->views,
                    $result->quantity,
                    $totals,
                    date('d.m.Y', strtotime($result->created_at)),
                    '<a href="'.route('company-edit',$result->id).'" class="on-default edit-row" style="margin-left: 10px"><i class="fa fa-pencil"></i></a>'
                ];
            }
            
        }

        echo json_encode(['aaData' => $companies]);
    }

    public function changePostId(Request $request){
        $cp = CompanyPost::find($request->post_id);
        $cp->tag_id = $request->tag_id;
        $cp->save();

        echo 1;
    }

    public function changePPostId(Request $request){
        $cp = CompanyPost::find($request->post_id);
        $cp->ttag_id = $request->tag_id;
        $cp->save();

        echo 1;
    }

    public function changePostQuantityTag(Request $request){
        $cp = CompanyPost::find($request->post_id);
        if ($request->type == 'tag_quantity')
            $cp->tag_quantity = $request->quantity;
        else
            $cp->ttag_quantity = $request->quantity;
        $cp->save();

        echo 1;
    }

    public function initGroupShedule(Request $request){
        $group_id = $request->group_id;

        $shedule = date('Y-m-d H:i');

        $date = date('Y-m-d', strtotime($shedule));
        $time = date('H:i', strtotime($shedule));
        $publish_date = date('Y-m-d H:i:s', strtotime($shedule));

        $response = false;
        if (date('Y-m-d') != date('Y-m-d', strtotime($shedule))){
            $shedules = Group::find($group_id)->shedule()->where('value','!=',$time)->type('adv')->get();
        }
        else{
            $shedules = Group::find($group_id)->shedule()->where('value','!=',$time)->type('adv')->overnow($time)->get();
        }
        
        $added_posts = [];
        $items = Group::find($group_id)->company_group_post()->where('publish_date','>=',now())->get();
        foreach ($items as $ke => $item) {
            $added_posts[date('Y-m-d H:i', strtotime($item->publish_date))] = 1;
        }

        if ($shedules->count() > 0){
            foreach ($shedules as $key => $shedule) {
                if ($response) continue;
                /*$posts = Group::find($group_id)->company_group_post()->where('publish_date', $date.' '.$shedule->value)->get();

                if ($posts->count() == 0){
                    $response = date('d.m.Y', strtotime($date)).' '.$shedule->value;
                }*/
                if (!isset($added_posts[$date.' '.$shedule->value]))
                    $response = date('d.m.Y', strtotime($date)).' '.$shedule->value;
            }
        }
        //echo "<pre>"; print_r($added_posts); die();

        if (!$response){
            /** UPDATED 25.01*/
            //$shedules = Group::find($group_id)->shedule()->type('adv')->overnow($publish_date)->ordered()->get();
            /** END UPDATED 25.01*/
            $shedules = Group::find($group_id)->shedule()->type('adv')->ordered()->get();
            if ($shedules->count() > 0){
                $i = 1;
                while(!$response){
                    foreach ($shedules as $key => $shedule) {
                        if ($response) continue;
                        /*$posts = Group::find($group_id)->company_group_post()->where('publish_date', date('Y-m-d',strtotime($date.'+'.$i.' day')).' '.$shedule->value)->get();

                        if ($posts->count() == 0){
                            $response = date('d.m.Y',strtotime($date.'+'.$i.' day')).' '.$shedule->value;
                        }*/
                        if (!isset($added_posts[date('Y-m-d',strtotime($date.'+'.$i.' day')).' '.$shedule->value]))
                            $response = date('d.m.Y',strtotime($date.'+'.$i.' day')).' '.$shedule->value;
                    }
                    $i++;
                }
            }
        }

        if (!$response)
            $response = 'Нету рассписания';

        echo $response;
    }

    public function initGroupSheduleNative(Request $request){
        $group_id = $request->group_id;

        $shedule = date('Y-m-d H:i');

        $date = date('Y-m-d', strtotime($shedule));
        $time = date('H:i', strtotime($shedule));
        $publish_date = date('Y-m-d H:i:s', strtotime($shedule));

        $response = false;
        if (date('Y-m-d') != date('Y-m-d', strtotime($shedule))){
            $shedules = Group::find($group_id)->shedule()->where('value','!=',$time)->type('native')->get();
        }
        else{
            $shedules = Group::find($group_id)->shedule()->where('value','!=',$time)->type('native')->overnow($time)->get();
        }

        $added_posts = [];
        $items = Group::find($group_id)->company_group_post()->where('publish_date','>=',now())->get();
        foreach ($items as $ke => $item) {
            $added_posts[date('Y-m-d H:i', strtotime($item->publish_date))] = 1;
        }
        

        if ($shedules->count() > 0){
            foreach ($shedules as $key => $shedule) {
                if ($response) continue;
                /*$posts = Group::find($group_id)->company_group_post()->where('publish_date', $date.' '.$shedule->value)->get();

                if ($posts->count() == 0){
                    $response = date('d.m.Y', strtotime($date)).' '.$shedule->value;
                }*/
                if (!isset($added_posts[$date.' '.$shedule->value]))
                    $response = date('d.m.Y', strtotime($date)).' '.$shedule->value;
            }
        }

        if (!$response){
            /** UPDATED 25.01*/
            //$shedules = Group::find($group_id)->shedule()->type('adv')->overnow($publish_date)->ordered()->get();
            /** END UPDATED 25.01*/
            $shedules = Group::find($group_id)->shedule()->type('native')->ordered()->get();
            if ($shedules->count() > 0){
                $i = 1;
                while(!$response){
                    foreach ($shedules as $key => $shedule) {
                        if ($response) continue;
                        /*$posts = Group::find($group_id)->company_group_post()->where('publish_date', date('Y-m-d',strtotime($date.'+'.$i.' day')).' '.$shedule->value)->get();

                        if ($posts->count() == 0){
                            $response = date('d.m.Y',strtotime($date.'+'.$i.' day')).' '.$shedule->value;
                        }*/
                        if (!isset($added_posts[date('Y-m-d',strtotime($date.'+'.$i.' day')).' '.$shedule->value]))
                            $response = date('d.m.Y',strtotime($date.'+'.$i.' day')).' '.$shedule->value;
                    }
                    $i++;
                }
            }
        }

        if (!$response)
            $response = 'Нету рассписания';

        echo $response;
    }

    public function initGroupSheduleStory(Request $request){
        $group_id = $request->group_id;

        $shedule = date('Y-m-d H:i');

        $date = date('Y-m-d', strtotime($shedule));
        $time = date('H:i', strtotime($shedule));
        $publish_date = date('Y-m-d H:i:s', strtotime($shedule));



        $response = false;
        if (date('Y-m-d') != date('Y-m-d', strtotime($shedule))){
            $shedules = Group::find($group_id)->shedule()->where('value','!=',$time)->type('story')->get();
        }
        else{
            $shedules = Group::find($group_id)->shedule()->where('value','!=',$time)->type('story')->overnow($time)->get();
        }
        
        $added_posts = [];
        $items = Group::find($group_id)->company_group_post()->where('publish_date','>=',now())->get();
        foreach ($items as $ke => $item) {
            $added_posts[date('Y-m-d H:i', strtotime($item->publish_date))] = 1;
        }

        if ($shedules->count() > 0){
            foreach ($shedules as $key => $shedule) {
                if ($response) continue;
                /*$posts = Group::find($group_id)->company_group_post()->where('publish_date', $date.' '.$shedule->value)->get();

                if ($posts->count() == 0){
                    $response = date('d.m.Y', strtotime($date)).' '.$shedule->value;
                }*/
                if (!isset($added_posts[$date.' '.$shedule->value]))
                    $response = date('d.m.Y', strtotime($date)).' '.$shedule->value;
            }
        }

        if (!$response){
            /** UPDATED 25.01*/
            //$shedules = Group::find($group_id)->shedule()->type('adv')->overnow($publish_date)->ordered()->get();
            /** END UPDATED 25.01*/
            $shedules = Group::find($group_id)->shedule()->type('story')->ordered()->get();
            if ($shedules->count() > 0){
                $i = 1;
                while(!$response){
                    foreach ($shedules as $key => $shedule) {
                        if ($response) continue;
                        /*$posts = Group::find($group_id)->company_group_post()->where('publish_date', date('Y-m-d',strtotime($date.'+'.$i.' day')).' '.$shedule->value)->get();

                        if ($posts->count() == 0){
                            $response = date('d.m.Y',strtotime($date.'+'.$i.' day')).' '.$shedule->value;
                        }*/

                        if (!isset($added_posts[date('Y-m-d',strtotime($date.'+'.$i.' day')).' '.$shedule->value]))
                            $response = date('d.m.Y',strtotime($date.'+'.$i.' day')).' '.$shedule->value;
                    }
                    $i++;
                }
            }
        }

        if (!$response)
            $response = 'Нету рассписания';

        echo $response;
    }


    public function makeReportSingle(Request $request, $company_id){
        $company_info = Company::find($company_id);
        CompanyPostActivity::where('company_id', $company_id)->delete();
        foreach ($company_info->companyposts as $key => $post) {
            foreach ($post->post_groups as $key => $post_group) {
                if (!$post_group->vk_id)
                    continue;

                $comments = 0;
                $likes = 0;
                $reposts = 0;
                $comment_likes = 0;

                if ($company_id > 948){
                    $group_id = "-".$post_group->group->idd;

                    $data = [
                        'owner_id' => $group_id,
                        'post_id' => $post_group->vk_id,
                        'access_token' => $post_group->group->user->vk_token,
                        'count' => 1000,
                        'v' => '5.131',
                    ];

                    $url = "https://api.vk.com/method/wall.getReposts?".http_build_query($data);
                    $response = $this->makeRequestGet($url);
                    $info = $response;
                    if (isset($info->response)){
                        foreach ($info->response->profiles as $key => $profile) {

                            $activity = new CompanyPostActivity();
                            $activity->group_id = $post_group->group_id;
                            $activity->company_post_id = $post->id;
                            $activity->company_id = $company_id;
                            $activity->type = 'repost';
                            $activity->value = $profile->id;
                            $activity->save();
                            $reposts++;
                        }
                    }

                    $data['need_likes'] = 1;
                    $data['count'] = 100;
                    $data['extended'] = 1;

                    $url = "https://api.vk.com/method/wall.getComments?".http_build_query($data);
                    $response = $this->makeRequestGet($url);
                    $info = $response;
                    
                    if (isset($info->response)){
                        $comments += $info->response->count;
                        foreach ($info->response->profiles as $key => $profile) {
                            $activity = new CompanyPostActivity();
                            $activity->group_id = $post_group->group_id;
                            $activity->company_post_id = $post->id;
                            $activity->company_id = $company_id;
                            $activity->type = 'comment';
                            $activity->value = $profile->id;
                            $activity->save();
                        }
                        if (isset($info->response->items)){
                            foreach ($info->response->items as $key => $item) {
                                $comment_likes += isset($item->likes->count) ? $item->likes->count : 0;
                            }
                        }

                    
                        if ($comments > 100){
                            for ($i=100;$i<=$comments;$i+=100){
                                $data['offset'] = $i;
                                $url = "https://api.vk.com/method/wall.getComments?".http_build_query($data);
                                $response = $this->makeRequestGet($url);
                                $info = $response;
                                $comments += $info->response->count;
                                foreach ($info->response->profiles as $key => $profile) {
                                    $activity = new CompanyPostActivity();
                                    $activity->group_id = $post_group->group_id;
                                    $activity->company_post_id = $post->id;
                                    $activity->company_id = $company_id;
                                    $activity->type = 'comment';
                                    $activity->value = $profile->id;
                                    $activity->save();
                                }
                                if (isset($info->response->items)){
                                    foreach ($info->response->items as $key => $item) {
                                        $comment_likes += $item->likes->count;
                                    }
                                }
                            }
                        }
                    }

                    $likes += $comment_likes;

                    $data = [
                        'owner_id' => $group_id,
                        'item_id' => $post_group->vk_id,
                        'access_token' => $post_group->group->user->vk_token,
                        'count' => 1000,
                        'v' => '5.131',
                        'extended' => 1,
                        'type' => 'post'
                    ];
                    $url = "https://api.vk.com/method/likes.getList?".http_build_query($data);
                    $response = $this->makeRequestGet($url);
                    $info = $response;
                    if (isset($info->response->items)){
                        $likes += $info->response->count;
                        foreach ($info->response->items as $key => $item) {
                            $activity = new CompanyPostActivity();
                            $activity->group_id = $post_group->group_id;
                            $activity->company_post_id = $post->id;
                            $activity->company_id = $company_id;
                            $activity->type = 'like';
                            $activity->value = $item->id;
                            $activity->save();
                        }
                    

                        if ($info->response->count > 1000){
                            for ($i=1000;$i<=$info->response->count;$i+=1000){
                                $data['offset'] = $i;
                                $url = "https://api.vk.com/method/likes.getList?".http_build_query($data);
                                $response = $this->makeRequestGet($url);
                                $info = $response;
                                if (isset($info->response->items)){
                                    foreach ($info->response->items as $key => $item) {
                                        $activity = new CompanyPostActivity();
                                        $activity->group_id = $post_group->group_id;
                                        $activity->company_post_id = $post->id;
                                        $activity->company_id = $company_id;
                                        $activity->type = 'like';
                                        $activity->value = $item->id;
                                        $activity->save();
                                    }
                                }
                            }
                        }
                    }
                }

                DB::table('company_posts_to_groups')->where([
                    'company_post_id' => $post->id,
                    'group_id' => $post_group->group_id
                ])->update(['repost' => $reposts, 'comment' => $comments, 'like' => $likes]);

                //usleep(500000);
                if ($company_id > 948){
                    sleep(1);
                }
            }


            
        }

        Company::where('id', $company_info->id)->update(['report' => 1, 'activity' => '1', 'report_dt' => now()]);

        $request->session()->put('company_post_added', 'Рекламная компания в итоговом отчете.');

        return redirect(route('company-posts', $company_id));
    }

    public function removeReportSingle(Request $request, $company_id){
        Company::where('id', $company_id)->update(['report' => 0, 'activity' => '0', 'report_dt' => null]);

        $request->session()->put('company_post_added', 'Итоговый отчет по рекламной компании удален.');

        return redirect(route('company-posts', $company_id));
    }
}
