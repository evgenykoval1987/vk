<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Client;
use App\Models\Manager;

class ManagerController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('manager'),
    		'title' => 'Менеджеры'
    	];

    	$data['success'] = false;
    	if ($request->session()->get('manager_added')){
    		$data['success'] = $request->session()->pull('manager_added');
    	}

        return view('panel.manager.list')->with('data',$data);
    }

    public function addForm(){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('manager'),
    		'title' => 'Менеджеры'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('manager-add'),
    		'title' => 'Добавить'
    	];

        $data['action'] = route('manager-save');

		$data['name'] = "";
		$data['manager_id'] = false;

    	return view('panel.manager.add')->with('data',$data);
    }

    public function editForm(Request $request, $manager_id){
    	$data = [];
    	$data['breadcrumbs'][] = [
            'link' => route('manager'),
            'title' => 'Менеджеры'
        ];

    	$data['breadcrumbs'][] = [
    		'link' => route('manager-edit', $manager_id),
    		'title' => 'Редактировать'
    	];
    	
    	if ($manager_id && Manager::find($manager_id)){ 
    		$manager_info = Manager::find($manager_id);
    	}

    	if ($manager_info){
    		$data['name'] = $manager_info->name;
    		$data['manager_id'] = $manager_id;
            $data['action'] = route('manager-esave');
    	}

    	return view('panel.manager.add')->with('data',$data);
    }

    public function addManager(Request $request){
    	$manager = new Manager;

	    $manager->name = $request->name;

	    $manager->save();

	    $request->session()->put('manager_added', 'Мнеджер <strong>'.$request->name.'</strong> добавлен');

	    return redirect(route('manager'));
    }

    public function ajax(Request $request){
    	$managers = [];
    	$results = Manager::all();
    	
    	foreach ($results as $key => $result) {
    		$managers[] = [
    			$result->id,
    			$result->name,
    			'<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>&nbsp;<a href="'.route('manager-edit',$result->id).'" class="on-default edit-row" style="margin-left: 10px"><i class="fa fa-pencil"></i></a>'
    		];
    	}

    	echo json_encode(['aaData' => $managers]);
    }

    public function dropManager(Request $request){
    	$id = $request->post('id');

    	Manager::destroy($id);

    	return response('1')->header('Content-Type', 'text/html');
    }

    public function editManager(Request $request){
    	$manager = Manager::find($request->manager_id);

    	$manager->name = $request->name;
	    $manager->save();

	    $request->session()->put('manager_added', 'Менеджер изминен');

	    return redirect(route('manager'));
    }
}
