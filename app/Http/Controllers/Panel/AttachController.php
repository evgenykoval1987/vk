<?php
namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AttachController extends Controller
{
    public function picture(Request $request, $operation, $id = false){
    	switch ($operation) {
    		case 'get-form': $response = $this->pictureGetForm($request); break;
            case 'get-albums': $response = $this->pictureGetAlbums($id); break;
            case 'images': $response = $this->pictureGetImages($request); break;
            case 'files': $response = $this->files($request); break;
            case 'drop-file': $response = $this->dropFile($request->path); break;
    	}

    	return $response;
    }



    public function files($request){
        $path = $request->file->store('public');
        $path = str_replace('public/', '', $path);
        $path = url('/').Storage::url($path); 

        /*$path1 = $request->file->move(storage_path('app/public'), $request->file->getClientOriginalName());
        echo $path1;*/

        return $path;
    }

    public function afiles($request){
        /*$path = $request->file->store('public');
        $path = str_replace('public/', '', $path);
        $path = url('/').Storage::url($path); */

        $path1 = $request->file->move(storage_path('app/public'), $request->file->getClientOriginalName());
        $path1 = url('/').Storage::url($request->file->getClientOriginalName()); 

        return $path1;
    }

    public function pictureGetForm($request){
    	$data = [];

    	$datan = [
    		'user_id' => Auth::user()->vk_id,
    		'access_token' => Auth::user()->vk_token,
    		'filter' => 'admin, editor',
    		'extended' => 1,
            'count' => 100,
    		'v' => '5.126',
    	];

    	$url = "https://api.vk.com/method/groups.get?".http_build_query($datan);
    	//$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
    	$response = json_decode($response);
        
        $data['groups'] = [];
        if (isset($response->response->items)){
            $data['groups'] = $response->response->items;
        }

        $data['onlypc'] = false;
        if (isset($request->onlypc))
            $data['onlypc'] = true;

        $data['user_vk_id'] = Auth::user()->vk_id;
       
    	return view('panel.attach.picture-form')->with('data',$data);
    }

    public function pictureGetAlbums($id){
        $data = [];

        $datan = [
            'owner_id' => $id,
            'access_token' => Auth::user()->vk_token,
            'v' => '5.126'
        ];

        $url = "https://api.vk.com/method/photos.getAlbums?".http_build_query($datan);

        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $response = json_decode($response);

        $return = [];
        $return[] = '<option>-</option>';
        if (isset($response->response->items)){
            foreach ($response->response->items as $key => $album) {
                $return[] = '<option value="'.$album->id.'">'.$album->title.'</option>';
            }
        }

        return implode('', $return);
    }

    public function pictureGetImages($request){
        $owner_id = $request->group_id;
        $arr = explode('/', $owner_id);
        $owner_id = $arr[count($arr)-1];
        $album_id = $request->album_id;
        $offset = $request->offset;
        $limit = $request->limit;

        $data = [];

        $datan = [
            'owner_id' => $owner_id,
            'access_token' => Auth::user()->vk_token,
            'rev' => 1,
            'extended' => 0,
            'feed_type' => 'photo',
            'offset' => $offset,
            'count' => $limit,
            'v' => '5.126'
        ];
        if ($album_id != '-' && $album_id != '')
            $datan['album_id'] = $album_id;
        else
            $datan['album_id'] = 'saved';

        $url = "https://api.vk.com/method/photos.get?".http_build_query($datan);
        //echo $url;
        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $response = json_decode($response);
        //dd($response);
        $data['images'] = [];
        if (isset($response->response->items)){
            foreach ($response->response->items as $key => $item) {
                $cl = new \stdClass();
                $cl->id = $item->id;
                $cl->owner_id = $item->owner_id;
                $cl->thumb = $item->sizes[1]->url;
                $cl->dt = date('d.m.Y',$item->date);
                $cl->title = ($item->text == '') ? 'Без названия' : $item->text;
                $data['images'][] = $cl;
            }
        }

        $data['prev'] = true;
        if ($offset == 0)
            $data['prev'] = false;

        $data['next'] = false;
        if (count($data['images']) >= $limit)
            $data['next'] = true;
        
        //echo "<pre>";print_R($data['images']);
        return view('panel.attach.picture-image')->with('data',$data);
    }

    public function dropFile($path){
        $arr = explode('/', $path);
        $file = $arr[count($arr)-1];
        if (is_file(storage_path('app/public').'/'.$file)){
            unlink(storage_path('app/public').'/'.$file);
        }
        return 1;
    }

    public function video(Request $request, $operation, $id = false){
        switch ($operation) {
            case 'get-form': $response = $this->videoGetForm($request); break;
            case 'get-albums': $response = $this->videoGetAlbums($id); break;
            case 'videos': $response = $this->videoGetVideos($request); break;
            case 'files': $response = $this->files($request); break;
            case 'drop-file': $response = $this->dropFile($request->path); break;
        }

        return $response;
    }

    public function videoGetForm($request){
        $data = [];

        $datan = [
            'user_id' => Auth::user()->vk_id,
            'access_token' => Auth::user()->vk_token,
            'filter' => 'admin, editor',
            'extended' => 1,
            'count' => 100,
            'v' => '5.126',
        ];

        $url = "https://api.vk.com/method/groups.get?".http_build_query($datan);
        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $response = json_decode($response);
        
        $data['groups'] = [];
        if (isset($response->response->items)){
            $data['groups'] = $response->response->items;
        }

        $data['onlypc'] = false;
        if (isset($request->onlypc))
            $data['onlypc'] = true;

        $data['user_vk_id'] = Auth::user()->vk_id;
       
        return view('panel.attach.video-form')->with('data',$data);
    }

    public function videoGetAlbums($id){
        $data = [];

        $datan = [
            'owner_id' => $id,
            'access_token' => Auth::user()->vk_token,
            'v' => '5.126'
        ];

        $url = "https://api.vk.com/method/video.getAlbums?".http_build_query($datan);
        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $response = json_decode($response);

        $return = [];
        $return[] = '<option>-</option>';
        if (isset($response->response->items)){
            foreach ($response->response->items as $key => $album) {
                $return[] = '<option value="'.$album->id.'">'.$album->title.'</option>';
            }
        }

        return implode('', $return);
    }

    public function videoGetVideos($request){
        $owner_id = $request->group_id;
        $arr = explode('/', $owner_id);
        $owner_id = $arr[count($arr)-1];
        $album_id = $request->album_id;
        $offset = $request->offset;
        $limit = $request->limit;

        $data = [];

        $datan = [
            'owner_id' => $owner_id,
            'access_token' => Auth::user()->vk_token,
            'extended' => 0,
            'offset' => $offset,
            'count' => $limit,
            'v' => '5.131'
        ];
        if ($album_id != '-' && $album_id != '')
            $datan['album_id'] = $album_id;

        $url = "https://api.vk.com/method/video.get?".http_build_query($datan);
        //echo $url;
        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $response = json_decode($response);
        //echo "<pre>";print_r($response);
        $data['videos'] = [];
        if (isset($response->response->items)){
            foreach ($response->response->items as $key => $item) {
                //print_r($item);
                $cl = new \stdClass();
                $cl->id = $item->id;
                $cl->owner_id = $item->owner_id;
                $cl->thumb = (isset($item->image[2]) ? $item->image[2]->url : ''); //$item->photo_320;
                $cl->dt = date('d.m.Y',$item->date);
                $cl->title = ($item->title == '') ? 'Без названия' : $item->title;
                $data['videos'][] = $cl;
            }
        }

        $data['prev'] = true;
        if ($offset == 0)
            $data['prev'] = false;

        $data['next'] = false;
        if (count($data['videos']) >= $limit)
            $data['next'] = true;
        
        //echo "<pre>";print_R($data['images']);
        return view('panel.attach.video-image')->with('data',$data);
    }

    public function audio(Request $request, $operation, $id = false){
        switch ($operation) {
            case 'get-form': $response = $this->audioGetForm(); break;
            case 'get-albums': $response = $this->audioGetAlbums($id); break;
            case 'audios': $response = $this->audioGetAudios($request); break;
            case 'search': $response = $this->audioGetAudiosSearch($request); break;
            case 'files': $response = $this->afiles($request); break;
            case 'drop-file': $response = $this->dropFile($request->path); break;
        }

        return $response;
    }

    public function audioGetForm(){
        $data = [];

        $datan = [
            'user_id' => Auth::user()->vk_id,
            'access_token' => Auth::user()->vk_token,
            'filter' => 'admin, editor',
            'extended' => 1,
            'count' => 100,
            'v' => '5.135',
        ];

        $url = "https://api.vk.com/method/groups.get?".http_build_query($datan);
        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $response = json_decode($response);
        
        $data['groups'] = [];
        if (isset($response->response->items)){
            $data['groups'] = $response->response->items;
        }

        $data['user_vk_id'] = Auth::user()->vk_id;

        $data['groups'] = [];
        $data['user_vk_id'] = '';
       
        return view('panel.attach.audio-form')->with('data',$data);
    }

    public function audioGetAlbums($id){
        $data = [];

        $datan = [
            'owner_id' => $id,
            'access_token' => Auth::user()->vk_token,
            'v' => '5.135'
        ];

        $url = "https://api.vk.com/method/audio.getAlbums?".http_build_query($datan);
        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $response = json_decode($response);

        $return = [];
        $return[] = '<option>-</option>';
        if (isset($response->response->items)){
            foreach ($response->response->items as $key => $album) {
                $return[] = '<option value="'.$album->id.'">'.$album->title.'</option>';
            }
        }

        return implode('', $return);
    }

    public function audioGetAudios($request){
        $owner_id = $request->group_id;
        $arr = explode('/', $owner_id);
        $owner_id = $arr[count($arr)-1];
        $album_id = $request->album_id;
        $offset = $request->offset;
        $limit = $request->limit;

        $data = [];

        $datan = array(
            'v' => '5.131',
            'https' => 1,
            'ref' => 'search',
            'extended' => 1,
            'offset' => $offset,
            'count' => $limit,
            'owner_id' => $owner_id,
            'device_id' => Auth::user()->vk_audio_device_id,
            'lang' => 'en',
            'access_token' => Auth::user()->vk_audio_token
        );

        $datan['sig'] = md5("/method/audio.get?".http_build_query($datan).Auth::user()->vk_audio_secret);
        
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_HTTPHEADER, array('User-Agent: VKAndroidApp/5.23-2978 (Android 4.4.2; SDK 19; x86; unknown Android SDK built for x86; en; 320x240)'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt(
            $ch,
            CURLOPT_URL,
            "https://api.vk.com/method/audio.get"
        );

        curl_setopt($ch,
            CURLOPT_POSTFIELDS,
            $datan
        );

        /*$datan = array(
            'v' => '5.93',
            'https' => 1,
            'ref' => 'search',
            'extended' => 1,
            'offset' => $offset,
            'count' => $limit,
            'q' => 'Полякова',
            'device_id' => Auth::user()->vk_audio_device_id,
            'lang' => 'en',
            'access_token' => Auth::user()->vk_audio_token
        );

        $datan['sig'] = md5("/method/audio.search?".http_build_query($datan).Auth::user()->vk_audio_secret);
        
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_HTTPHEADER, array('User-Agent: VKAndroidApp/5.23-2978 (Android 4.4.2; SDK 19; x86; unknown Android SDK built for x86; en; 320x240)'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt(
            $ch,
            CURLOPT_URL,
            "https://api.vk.com/method/audio.search"
        );

        curl_setopt($ch,
            CURLOPT_POSTFIELDS,
            $datan
        );*/

        $response = json_decode(curl_exec($ch));
        file_put_contents('12121212', json_encode($response));
        file_put_contents('21212121', json_encode($datan));
        $total = 0;
        $data['audios'] = [];
        if (isset($response->response->items)){
            $total = $response->response->count;
            foreach ($response->response->items as $key => $item) {
                $thumb = '';
                if (isset($item->album->thumb->photo_135))
                    $thumb = $item->album->thumb->photo_135;
                else
                    $thumb = '/images/audio.jpg';
                $cl = new \stdClass();
                $cl->id = $item->id;
                $cl->owner_id = $item->owner_id;
                $cl->thumb = $thumb;
                $cl->dt = date('d.m.Y',$item->date);
                $cl->title = ($item->title == '') ? 'Без названия' : $item->artist.' - '.$item->title;
                $data['audios'][] = $cl;
            }
        }

        $data['prev'] = true;
        if ($offset == 0)
            $data['prev'] = false;

        $data['next'] = false;
        if (/*count($data['audios']) >= $limit*/ $total > ($offset+$limit))
            $data['next'] = true;
        
        //echo "<pre>";print_R($data['images']);
        return view('panel.attach.audio-image')->with('data',$data);
    }

    public function audioGetAudiosSearch($request){
        $q = $request->q;
        $offset = $request->offset;
        $limit = $request->limit;

        $data = [];

        $datan = array(
            'v' => '5.135',
            'https' => 1,
            'ref' => 'search',
            'extended' => 1,
            'offset' => $offset,
            'count' => $limit,
            'q' => $q,
            'device_id' => Auth::user()->vk_audio_device_id,
            'lang' => 'en',
            'access_token' => Auth::user()->vk_audio_token
        );

        $datan['sig'] = md5("/method/audio.search?".http_build_query($datan).Auth::user()->vk_audio_secret);
        
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_HTTPHEADER, array('User-Agent: VKAndroidApp/5.23-2978 (Android 4.4.2; SDK 19; x86; unknown Android SDK built for x86; en; 320x240)'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt(
            $ch,
            CURLOPT_URL,
            "https://api.vk.com/method/audio.search"
        );

        curl_setopt($ch,
            CURLOPT_POSTFIELDS,
            $datan
        );

        $response = json_decode(curl_exec($ch));
        
        $total = 0;
        $data['audios'] = [];
        if (isset($response->response->items)){
            $total = $response->response->count;
            foreach ($response->response->items as $key => $item) {
                $thumb = '';
                if (isset($item->album->thumb->photo_135))
                    $thumb = $item->album->thumb->photo_135;
                else
                    $thumb = '/images/audio.jpg';
                $cl = new \stdClass();
                $cl->id = $item->id;
                $cl->owner_id = $item->owner_id;
                $cl->thumb = $thumb;
                $cl->dt = date('d.m.Y',$item->date);
                $cl->title = ($item->title == '') ? 'Без названия' : $item->artist.' - '.$item->title;
                $data['audios'][] = $cl;
            }
        }

        $data['prev'] = true;
        if ($offset == 0)
            $data['prev'] = false;

        $data['next'] = false;
        if (/*count($data['audios']) >= $limit*/ $total > ($offset+$limit))
            $data['next'] = true;
        
        //echo "<pre>";print_R($data['images']);
        return view('panel.attach.audio-image')->with('data',$data);
    }

    public function playlist(Request $request, $operation){
        switch ($operation) {
            case 'get-form': $response = $this->playlistGetForm(); break;
            /*case 'get-albums': $response = $this->audioGetAlbums($id); break;
            case 'audios': $response = $this->audioGetAudios($request); break;
            case 'files': $response = $this->afiles($request); break;
            case 'drop-file': $response = $this->dropFile($request->path); break;*/
        }

        return $response;
    }

    public function playlistGetForm(){
        $data = [];
       
        return view('panel.attach.playlist-form')->with('data',$data);
    }

    public function makeRequest($url, $data = []){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data; charset=UTF-8'));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $server_output = curl_exec($ch);
        curl_close ($ch);
        
        return $server_output;
    }

    public function makeRequestGet($url, $data = []){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $server_output = curl_exec($ch);
        curl_close ($ch);
        
        return $server_output;
    }

    public function doc(Request $request, $operation, $id = false){
        switch ($operation) {
            case 'get-form': $response = $this->docGetForm(); break;
            //case 'get-albums': $response = $this->docGetAlbums($id); break;
            case 'docs': $response = $this->docGetDocs($request); break;
            /*case 'files': $response = $this->files($request); break;
            case 'drop-file': $response = $this->dropFile($request->path); break;*/
        }

        return $response;
    }

    public function docGetForm(){
        $data = [];

        $datan = [
            'user_id' => Auth::user()->vk_id,
            'access_token' => Auth::user()->vk_token,
            'filter' => 'admin, editor',
            'extended' => 1,
            'count' => 100,
            'v' => '5.126',
        ];

        $url = "https://api.vk.com/method/groups.get?".http_build_query($datan);
        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        //file_put_contents('111111', $response);
        $response = json_decode($response);
        
        $data['groups'] = [];
        if (isset($response->response->items)){
            $data['groups'] = $response->response->items;
        }

        $data['user_vk_id'] = Auth::user()->vk_id;
       
        return view('panel.attach.doc-form')->with('data',$data);
    }

    public function docGetDocs($request){
        $owner_id = $request->group_id;
        $arr = explode('/', $owner_id);
        $owner_id = $arr[count($arr)-1];
        $album_id = $request->album_id;
        $offset = $request->offset;
        $limit = $request->limit;

        $data = [];

        $datan = [
            'owner_id' => $owner_id,
            'access_token' => Auth::user()->vk_token,
            //'extended' => 0,
            'type' => 3,
            'offset' => $offset,
            'count' => $limit,
            'v' => '5.126'
        ];
        if ($album_id != '-' && $album_id != '')
            $datan['album_id'] = $album_id;

        $url = "https://api.vk.com/method/docs.get?".http_build_query($datan);
        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $response = json_decode($response);
        //dd($response);
        $data['docs'] = [];
        if (isset($response->response->items)){
            //dd($response->response->items);
            foreach ($response->response->items as $key => $item) {
                $cl = new \stdClass();
                $cl->id = $item->id;
                $cl->owner_id = $item->owner_id;
                $cl->thumb = $item->preview->photo->sizes[2]->src;
                $cl->dt = date('d.m.Y',$item->date);
                $cl->title = ($item->title == '') ? 'Без названия' : $item->title;
                $data['docs'][] = $cl;
            }
        }

        $data['prev'] = true;
        if ($offset == 0)
            $data['prev'] = false;

        $data['next'] = false;
        if (count($data['docs']) >= $limit)
            $data['next'] = true;
        
        //echo "<pre>";print_R($data['images']);
        return view('panel.attach.doc-image')->with('data',$data);
    }
}