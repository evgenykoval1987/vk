<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Answer;

class AnswerController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('answer'),
    		'title' => 'Заготовки ответов'
    	];

    	$data['success'] = false;
    	if ($request->session()->get('answer_added')){
    		$data['success'] = $request->session()->pull('answer_added');
    	}

        return view('panel.answer.list')->with('data',$data);
    }

    public function addForm(){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('answer'),
    		'title' => 'Заготовки ответов'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('answer-add'),
    		'title' => 'Добавить'
    	];

        $data['text'] = "";
        $data['description'] = "";
		$data['action'] = route('answer-save');
		$data['answer_id'] = false;

    	return view('panel.answer.add')->with('data',$data);
    }

    public function editForm(Request $request, $answer_id){
    	$data = [];
    	$data['breadcrumbs'][] = [
    		'link' => route('answer'),
    		'title' => 'Заготовки ответов'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('answer-edit', $answer_id),
    		'title' => 'Редактировать'
    	];
    	
    	if ($answer_id && Answer::find($answer_id)){ 
    		$answer_info = Answer::find($answer_id);
    	}

    	if ($answer_info){
    		$data['text'] = $answer_info->text;
            $data['description'] = $answer_info->description;
    		$data['action'] = route('answer-esave');
    		$data['answer_id'] = $answer_id;
    	}


    	return view('panel.answer.add')->with('data',$data);
    }

    public function addAnswer(Request $request){
    	$answer = new Answer;
        
	    $answer->text = $request->text;
        $answer->description = $request->description;

	    $answer->save();

	    $request->session()->put('answer_added', 'Заготовка <strong>'.$request->name.'</strong> добавлен');

	    return redirect(route('answer'));
    }

    public function ajax(Request $request){
    	$answers = [];
    	$results = Answer::all();
    	
    	foreach ($results as $key => $result) {
    		$answers[] = [
    			$result->id,
    			$result->text,
    			'<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>&nbsp;<a href="'.route('answer-edit',$result->id).'" class="on-default edit-row" style="margin-left: 10px"><i class="fa fa-pencil"></i></a>'
    		];
    	}

    	echo json_encode(['aaData' => $answers]);
    }

    public function dropAnswer(Request $request){
    	$id = $request->post('id');

    	Answer::destroy($id);

    	return response('1')->header('Content-Type', 'text/html');
    }

    public function editAnswer(Request $request){
    	$answer = Answer::find($request->answer_id);

    	$answer->text = $request->text;
        $answer->description = $request->description;
    	
	    $answer->save();

	    $request->session()->put('answer_added', 'Заготовка изминена');

	    return redirect(route('answer'));
    }
}
