<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pay;
use App\Models\Post;
use App\Models\PostToPay;
use App\Models\Price;
use App\Models\Group;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Models\Setting;

class PayController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('pays'),
    		'title' => 'Заявки на выплаты'
    	];

    	$pays = Pay::all();
    	$data['pays'] = [];
    	foreach ($pays as $key => $pay) {
    		$data['pays'][] = [
    			'user' => $pay->user->name,
    			'dt' => date('d.m.Y H:i', strtotime($pay->created_at)),
    			'total' => $pay->total,
    			'status' => $pay->status,
    			'count' => count($pay->posts)
    		];
    	}

    	$data['success'] = false;

    	if ($request->session()->get('pay-paied')){
    		$data['success'] = $request->session()->pull('pay-paied');
    	}

        $setting = Setting::where('key','min_pay')->first();
        if ($setting)
            $data['min_pay'] = $setting->value;
        else
            $data['min_pay'] = 0;

        return view('panel.pay.list')->with($data);
    }

    public function ajax(Request $request){
    	$pays = [];
    	   
    	$results = Pay::all();

    	foreach ($results as $key => $pay) {
    		$pays[] = [
    			$pay->id,
    			$pay->user->name,
    			date('d.m.Y H:i', strtotime($pay->created_at)),
    			count($pay->posts),
    			$pay->total.' р.',
    			($pay->status == 1) ? '<span style="color:blue">Новый</span>' : '<span style="color:green">Оплачен</span>',
    			($pay->status == 1) ? '<a href="'.route('pays-info',$pay->id).'" class="on-default"><i class="fa fa-share"></i>&nbsp;Оплатить</a>' : '',
    		];
    	}

    	echo json_encode(['aaData' => $pays]);
    }

    public function info(Request $request, $pay_id){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('pays'),
    		'title' => 'Заявки на выплаты'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('pays-info',$pay_id),
    		'title' => 'Счет №'.$pay_id
    	];

    	$data['pay_info'] = Pay::find($pay_id);
    	$data['pay_info']->dt = date('d.m.Y H:i', strtotime($data['pay_info']->created_at));

    	return view('panel.pay.info')->with($data);
    }

    public function paid($pay_id, Request $request){
    	$pay = Pay::find($pay_id);
    	$pay->status = 0;
    	$pay->save();

    	$posts = PostToPay::where('pay_id',$pay_id)->get();
    	foreach ($posts as $key => $post) {
    		$p = Post::find($post->post_id);
    		$p->paied = 1;
    		$p->save();
    	}

    	$request->session()->put('pay-paied', 'Счет успешно оплачен');

    	return redirect(route('pays'));
    }

    public function price(Request $request){
        $data = [];

        $data['breadcrumbs'][] = [
            'link' => route('pays-price'),
            'title' => 'Цены'
        ];

        $data['prices'] = Price::all();

        $data['success'] = false;

        if ($request->session()->get('pays-prices')){
            $data['success'] = $request->session()->pull('pays-prices');
        }

        return view('panel.pay.price')->with($data);
    }

    public function priceEdit(Request $request){
        $data = [];
        foreach ($request->prices as $key => $price) {
            $p = Price::find($key);
            if ($p){
                $p->value = $price;
                $p->save();
            }
            else{
                $p = new Price;
                $p->value = $price;
                $p->save();
            }
            $data[] = $p->id;
        }

        DB::table('prices')->whereNotIn('id',$data)->delete();

        $request->session()->put('pays-prices', 'Данные обновлены');

        return redirect(route('pays-price'));
    }

    public function table(){
        $data = [];

        $data['breadcrumbs'][] = [
            'link' => route('pays-table'),
            'title' => 'Сводная таблица'
        ];

        $data['groups'] = [];
        $results = Group::all();
        foreach ($results as $key => $result) {
            $prices = [];

            $res = DB::table('group_prices')->where('group_id', $result->id)->get();
            foreach ($res as $ke => $item) {
                $prices[$item->price_id] = [
                    'from' => $item->from,
                    'to' => $item->to
                ];
            }

            $data['groups'][] = [
                'url' => $result->url,
                'prices' => $prices
            ];
        }

        $data['prices'] = Price::all();

        return view('panel.pay.table')->with($data);
    }

    public function saveMin(Request $request){
        $setting = Setting::where('key','min_pay')->first();
        if ($setting){
            $setting->value = $request->setting_min_pay;
            $setting->save();
        }
        else{
            $setting = new Setting;
            $setting->key = 'min_pay';
            $setting->value = $request->setting_min_pay;
            $setting->save();
        }

        $request->session()->put('pay-paied', 'Настройки сохранены');

        return redirect(route('pays'));
    }
}
