<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Models\Setting;
use App\Models\Monitoring;
use App\Models\Client;
use App\Models\Spiderlog;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MonitoringController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('monitoring'),
    		'title' => 'Мониторинг групп'
    	];

    	$data['success'] = false;
    	if ($request->session()->get('setting_added')){
    		$data['success'] = $request->session()->pull('setting_added');
    	}

    	$data['monitoring_keywords'] = Setting::where('key','monitoring_keywords')->first()->value;
    	$data['monitoring_groups'] = Setting::where('key','monitoring_groups')->first()->value;
        $data['monitoring_not_groups'] = Setting::where('key','monitoring_not_groups')->first()->value;

        return view('panel.monitoring.list')->with($data);
    }

    public function add(Request $request){
    	foreach ($request->config as $key => $value){
    		$setting = Setting::where('key',$key)->first();
    		if ($setting){
    			$setting->key = $key;
    			$setting->value = $value;
    			$setting->save();
    		}
    		else{
    			$setting = new Setting;
    			$setting->key = $key;
    			$setting->value = $value;
    			$setting->save();
    		}
    	}

    	$request->session()->put('setting_added', 'Настройки для мониторинга изменены');

	    return redirect(route('monitoring'));
    }

    public function ajax(Request $request){
    	$posts = [];
    	$results = Monitoring::where('readed',0)->where('status',1)->get();

    	foreach ($results as $key => $result) {
    		$posts[] = [
    			$result->id,
    			"<a href='$result->group_url' target='_blank'>$result->group_name</a>",
    			"<a href='$result->link' target='_blank'>$result->link</a>",
    			$result->keyword,
    			$result->text,
    			date('d.m.Y H:i', strtotime($result->vk_dt)),
    			$result->status,
                '<a class="btn btn-danger btn-sm remove-monitoring" href="'.route('monitoring-remove-one',$result->id).'"><i class="fa fa-pencil"></i>&nbsp;Отписал</a>'
    		];
    	}
        /*$update = ['status' => 0];
        DB::table('monitoring')->update($update);*/

    	echo json_encode(['aaData' => $posts]);
    }

    public function dropOne(Request $request, $id){
        $monitoring = Monitoring::find($id);
        $monitoring->readed = 1;
        $monitoring->readed_dt = now();
        $monitoring->status = 0;
        $monitoring->save();

        echo 1;
    }

    public function dropAll(Request $request){
        $update = ['status' => 0];
        DB::table('monitoring')->update($update);
    
        return redirect()->to(route('monitoring'));
    }

    public function spider(){
        ini_set('max_execution_time', 720); 
    	$groups = [];

        $user_info = User::where('role_id',1)->where('login', 'artyom')->first();
        $token = $user_info->vk_token;

        $not = Setting::where('key','monitoring_not_groups')->first();
        $not = $not->value;
        $not = explode("\r\n", $not);

    	$results = Setting::where('key','monitoring_groups')->first();
    	$results = $results->value;
    	$results = explode("\r\n", $results);

    	foreach ($results as $key => $result) {
            if (in_array($result, $not))
                continue;
    		$result = explode('/', $result);
    		$result = str_replace(['club','public'], '', $result[count($result)-1]);
            $group_info = json_decode($this->makeRequest("https://api.vk.com/method/groups.getById?group_id=$result&access_token=$token&v=5.95"));
            //echo "<br>https://api.vk.com/method/groups.getById?group_id=$result&access_token=$token&v=5.95";
            //print_r($group_info);
            if (isset($group_info->response[0])){
                $group_info = $group_info->response[0];
                $group_id = $group_info->id;

                $log = Spiderlog::where('group', "https://vk.com/".$group_info->screen_name)->where('updated_at', '<', 'DATE_SUB(NOW(), INTERVAL 2 HOUR)')->first();
                if ($log && !in_array($group_id, $groups))
                    $groups[] = $group_id;
                else{
                    $log = Spiderlog::where('group', "https://vk.com/".$group_info->screen_name)->first();
                    if (!$log && !in_array($group_id, $groups))
                        $groups[] = $group_id;
                }
            }
            sleep(1);
    	}
        //dd($groups);
    	$clients = Client::all();
    	foreach ($clients as $key => $client) {
    		$result = explode('/', $client->group_link);
    		$client->group_link = str_replace(['club','public'], '', $result[count($result)-1]);

            $group_info = json_decode($this->makeRequest("https://api.vk.com/method/groups.getById?group_id=$client->group_link&access_token=$token&v=5.95"));
            if (isset($group_info->response[0])){
                $group_info = $group_info->response[0];
                $group_id = $group_info->id;

                //"https://vk.com/".$group_info->screen_name
                $log = Spiderlog::where('group', "https://vk.com/".$group_info->screen_name)->where('updated_at', '<', DB::raw('DATE_SUB(NOW(), INTERVAL 2 HOUR)'))->first();
                
                if ($log && !in_array($group_id, $groups)){
                    $groups[] = $group_id;
                }
                
            }
            sleep(1);
    	}
        //dd($groups);
    	$results = Setting::where('key','monitoring_keywords')->first();
    	$results = $results->value;
    	$keywords = explode(",", $results);

    	if ($keywords && $groups){
    		foreach ($groups as $group) {
    			$group_info = json_decode($this->makeRequest("https://api.vk.com/method/groups.getById?group_id=$group&access_token=$token&v=5.95")); 
    			if (isset($group_info->response[0])){
    				$group_info = $group_info->response[0];
    				$group_id = $group_info->id;
    				
    				foreach ($keywords as $keyword) {
                        $keyword = str_replace(' ', '%20', $keyword);
    					$posts = json_decode($this->makeRequest("https://api.vk.com/method/wall.search?owner_id=-$group_id&access_token=$token&count=10&query=$keyword&v=5.95")); 
                        //echo "https://api.vk.com/method/wall.search?owner_id=-$group_id&access_token=$token&count=10&query=$keyword&v=5.95<br>";
    					
    					if (!isset($posts->response)) continue;
    					if ($posts->response->count > 0){
    						foreach ($posts->response->items as $key => $post) {
                                if ($post->post_type != 'post') continue;
    							$item = Monitoring::where('vk_id',$post->id)->first();
    							if (!$item){
    								$monitoring = new Monitoring;
    								$monitoring->group_id = $group_id;
    								$monitoring->group_url = "https://vk.com/".$group_info->screen_name;
    								$monitoring->group_name = $group_info->name;
    								$monitoring->text = addslashes($post->text);
    								$monitoring->vk_id = $post->id;
    								$monitoring->link = "https://vk.com/$group_info->screen_name?w=wall-".$group_info->id."_".$post->id;
    								$monitoring->vk_dt = date('Y-m-d H:i:s', $post->date);
    								$monitoring->keyword = $keyword;
    								$monitoring->save();
    							}
    						}
    					}
    					sleep(1);
    				}
    				$log = Spiderlog::where('group', "https://vk.com/".$group_info->screen_name)->first();
                    if ($log){
                        Spiderlog::where('group', "https://vk.com/".$group_info->screen_name)
                          ->update(['updated_at' => now()]);
                    }
                    else{
                        $spider = new Spiderlog();
                        $spider->group = "https://vk.com/".$group_info->screen_name;
                        $spider->save();
                    }
    			}
    		}
    	}
        ini_set('max_execution_time', 90); 
    }

    public function makeRequest($url, $data = []){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data; charset=UTF-8'));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $server_output = curl_exec($ch);
        curl_close ($ch);
        
        return $server_output;
    }
}
