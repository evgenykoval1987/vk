<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Client;
use App\Models\Chat;
use App\User;

class ChatController extends Controller
{
	public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('chat'),
    		'title' => 'Поддержка'
    	];

    	$data['role_id'] = Auth::user()->role_id;

        return view('panel.chat.list')->with('data',$data);
    }

    public function data(Request $request){
    	$response = [];
    	if (Auth::user()->role->slug == 'editor')
    		$users = User::where('role_id','1')->get();
    	else
    		$users = User::where('role_id','2')->get();

    	if ($users){
    		foreach ($users as $key => $user) {
    			$messages = [];
    			$last_message = "Сообщений еще нету";
    			$unreaded = 0;

    			$chats = Chat::whereIn('user_from', [$user->id, Auth::user()->id])->whereIn('user_to', [$user->id, Auth::user()->id])->get();
    			foreach ($chats as $key => $chat) {
    				$class = "";
    				if ($chat->user_from == $user->id)
    					$class = "incoming_msg";
    				else{
    					$class = "outgoing_msg";
    				}
    				$messages[] = [
    					'text' => $chat->text,
    					'class' => $class,
    					'date' => date('d.m.Y H:i:s', strtotime($chat->created_at))
    				];

    				$last_message = $chat->text;
    				if ($chat->user_to == Auth::user()->id && $chat->status == 1)
    					$unreaded++;
    			}

    			$response[] = [
    				'id' => $user->id,
    				'name' => $user->name,
    				'messages' => $messages,
    				'last_message' => $last_message,
    				'unreaded' => $unreaded
    			];
    		}
    	}

    	echo json_encode($response);
    }

    public function send(Request $request){
    	$chat = new Chat();
    	$chat->user_from = Auth::user()->id;
    	$chat->user_to = $request->to_user;
    	$chat->text = $request->message;
    	$chat->save();

    	return response('1')->header('Content-Type', 'text/html');
    }

    public function unread(Request $request){
    	$user_id = $request->from_user;
    	$chats = Chat::where('user_from',$user_id)->where('status', 1)->get();
    	foreach ($chats as $key => $chat) {
    		$chat->status = 0;
    		$chat->save();
    	}

    	return response('1')->header('Content-Type', 'text/html');
    }
}