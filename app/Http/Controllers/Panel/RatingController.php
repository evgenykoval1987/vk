<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Models\Allpost;

class RatingController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('rating'),
    		'title' => 'Статистика редакторов'
    	];

        $data['editors'] = [];
        $editors = User::where('role_id', 2)->get();
        foreach ($editors as $key => $editor) {
            //dd($editor->allpost);
            $stat = [];
            //count
            //publish
            //pays
            //strikes
            //views
            $stat['name'] = $editor->name;
            $stat['count'] = $editor->allpost->count();
            $stat['publish'] = $editor->allpostpublish->count();
            $stat['cost'] = $editor->allpostcosts->sum('cost').' руб.';
            $stat['strike'] = $editor->allpoststriked->count();

            $stat['viewed'] = $editor->allpostcosts->sum('viewed');

            $data['editors'][] = $stat;
        }

        usort($data['editors'], function ($item1, $item2) {
            return $item2['viewed'] <=> $item1['viewed'];
        });
        
        return view('panel.rating.list')->with($data);
    }
}