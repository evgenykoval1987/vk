<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Tagpack;
use App\Models\Tag;
use App\Models\TagToTagpack;

class TagpackController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('tagpack'),
    		'title' => 'Метки'
    	];

    	$data['success'] = false;
    	if ($request->session()->get('tagpack_added')){
    		$data['success'] = $request->session()->pull('tagpack_added');
    	}

        return view('panel.tagpack.list')->with('data',$data);
    }

    public function addForm(){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('tagpack'),
    		'title' => 'Метки'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('tagpack-add'),
    		'title' => 'Добавить'
    	];

        $data['action'] = route('tagpack-save');

		$data['name'] = "";
        $data['quantity'] = 0;
        $data['views'] = 0;
		$data['tagpack_id'] = false;
        $data['selected'] = [];

        $data['tags'] = Tag::all();

    	return view('panel.tagpack.add')->with('data',$data);
    }

    public function editForm(Request $request, $tagpack_id){
    	$data = [];
    	$data['breadcrumbs'][] = [
            'link' => route('tagpack'),
            'title' => 'Метки'
        ];

    	$data['breadcrumbs'][] = [
    		'link' => route('tagpack-edit', $tagpack_id),
    		'title' => 'Редактировать'
    	];
    	
    	if ($tagpack_id && Tagpack::find($tagpack_id)){ 
    		$tagpack_info = Tagpack::find($tagpack_id);
    	}

        $data['tags'] = Tag::all();

    	if ($tagpack_info){
    		$data['name'] = $tagpack_info->name;
            $data['quantity'] = $tagpack_info->quantity;
            $data['views'] = $tagpack_info->views;
    		$data['tagpack_id'] = $tagpack_id;
            $data['action'] = route('tagpack-esave');

            $data['selected'] = TagToTagpack::where('tagpack_id', $tagpack_id)->get();
    	}

    	return view('panel.tagpack.add')->with('data',$data);
    }

    public function addTagpack(Request $request){
    	$tagpack = new Tagpack;

	    $tagpack->name = $request->name;
        $tagpack->quantity = $request->quantity;
        $tagpack->views = $request->views;

	    $tagpack->save();

        if ($request->tags){
            foreach ($request->tags as $key => $tag) {
                $ttt = new TagToTagpack();
                $ttt->tag_id = $tag;
                $ttt->tagpack_id = $tagpack->id;
                $ttt->save();
            }
        }

	    $request->session()->put('tagpack_added', 'Метка <strong>'.$request->name.'</strong> добавлена');

	    return redirect(route('tagpack'));
    }

    public function ajax(Request $request){
    	$tagpacks = [];
    	$results = Tagpack::all();
    	
    	foreach ($results as $key => $result) {
    		$tagpacks[] = [
    			$result->id,
    			$result->name,
                $result->views,
                $result->quantity,
    			'<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>&nbsp;<a href="'.route('tagpack-edit',$result->id).'" class="on-default edit-row" style="margin-left: 10px"><i class="fa fa-pencil"></i></a>'
    		];
    	}

    	echo json_encode(['aaData' => $tagpacks]);
    }

    public function dropTagpack(Request $request){
    	$id = $request->post('id');

    	Tagpack::destroy($id);
        TagToTagpack::where('tagpack_id', $id)->delete();

    	return response('1')->header('Content-Type', 'text/html');
    }

    public function editTagpack(Request $request){
    	$tagpack = Tagpack::find($request->tagpack_id);

    	$tagpack->name = $request->name;
        $tagpack->quantity = $request->quantity;
        $tagpack->views = $request->views;
	    $tagpack->save();

        TagToTagpack::where('tagpack_id', $request->tagpack_id)->delete();

        if ($request->tags){
            foreach ($request->tags as $key => $tag) {
                $ttt = new TagToTagpack();
                $ttt->tag_id = $tag;
                $ttt->tagpack_id = $request->tagpack_id;
                $ttt->save();
            }
        }

	    $request->session()->put('tagpack_added', 'Метка изминена');

	    return redirect(route('tagpack'));
    }
}
