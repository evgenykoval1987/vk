<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Tag;
use App\Models\Taggroup;

class TagController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('tag'),
    		'title' => 'Метки'
    	];

    	$data['success'] = false;
    	if ($request->session()->get('tag_added')){
    		$data['success'] = $request->session()->pull('tag_added');
    	}

        return view('panel.tag.list')->with('data',$data);
    }

    public function addForm(){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('tag'),
    		'title' => 'Метки'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('tag-add'),
    		'title' => 'Добавить'
    	];

        $data['action'] = route('tag-save');

		$data['name'] = "";
        $data['quantity'] = 0;
		$data['tag_id'] = false;
        $data['tag_group_id'] = false;

        $data['taggroups'] = Taggroup::all();

    	return view('panel.tag.add')->with('data',$data);
    }

    public function editForm(Request $request, $tag_id){
    	$data = [];
    	$data['breadcrumbs'][] = [
            'link' => route('tag'),
            'title' => 'Метки'
        ];

    	$data['breadcrumbs'][] = [
    		'link' => route('tag-edit', $tag_id),
    		'title' => 'Редактировать'
    	];
    	
    	if ($tag_id && Tag::find($tag_id)){ 
    		$tag_info = Tag::find($tag_id);
    	}

    	if ($tag_info){
    		$data['name'] = $tag_info->name;
            $data['quantity'] = $tag_info->quantity;
            $data['tag_group_id'] = $tag_info->tag_group_id;
    		$data['tag_id'] = $tag_id;
            $data['action'] = route('tag-esave');
    	}

        $data['taggroups'] = Taggroup::all();

    	return view('panel.tag.add')->with('data',$data);
    }

    public function addTag(Request $request){
    	$tag = new Tag;

	    $tag->name = $request->name;
        $tag->quantity = $request->quantity;
        $tag->tag_group_id = $request->tag_group_id;

	    $tag->save();

	    $request->session()->put('tag_added', 'Метка <strong>'.$request->name.'</strong> добавлена');

	    return redirect(route('tag'));
    }

    public function ajax(Request $request){
    	$tags = [];
    	$results = Tag::all();
    	
    	foreach ($results as $key => $result) {
    		$tags[] = [
    			$result->id,
    			$result->name,
                $result->quantity,
    			'<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>&nbsp;<a href="'.route('tag-edit',$result->id).'" class="on-default edit-row" style="margin-left: 10px"><i class="fa fa-pencil"></i></a>'
    		];
    	}

    	echo json_encode(['aaData' => $tags]);
    }

    public function dropTag(Request $request){
    	$id = $request->post('id');

    	Tag::destroy($id);

    	return response('1')->header('Content-Type', 'text/html');
    }

    public function editTag(Request $request){
    	$tag = Tag::find($request->tag_id);

    	$tag->name = $request->name;
        $tag->quantity = $request->quantity;
        $tag->tag_group_id = $request->tag_group_id;
	    $tag->save();

	    $request->session()->put('tag_added', 'Метка изминена');

	    return redirect(route('tag'));
    }
}
