<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Post;
use App\User;
use App\Models\CompanyPostToGroup;
use App\Models\CompanyPost;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\PostAttachment;
use App\Models\Price;
use App\Models\Log;
use App\Models\Client;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\CompanyPostActivity;
use App\Models\Stat;
use App\Models\Company;
use App\Models\Monitoring;
use App\Models\Sentence;
use App\Models\Message;
use App\Models\CompanyPostEndDeleted;

class VkController extends Controller
{
    public function publish(Request $request){
    	$post_id = $request->post_id;

    	$post_info = Post::find($post_id);
    	$group_info = Group::find($post_info->group_id);

        $request->session()->put('post_group_id', $post_info->group_id);
        

    	//$access_token = Auth::user()->vk_token;
        $access_token =  User::find($group_info->user_id)->vk_token;

    	$data = [
    		'owner_id' => '-'.$group_info->idd,
    		'access_token' => $access_token,
    		'from_group' => '1',
    		'message' => $post_info->description,
    		'v' => '5.126',
    	];
        $attachments = $post_info->attachments()->get();
        foreach ($attachments as $key => $attachment) { 
            if ($attachment->attachment == '0' && $attachment->type == 'image-local'){ 
                $this->transformPostAtachmentImage($attachment, $group_info->idd, $access_token);
                sleep(1);
            }
            if ($attachment->attachment == '0' && $attachment->type == 'video-local'){ 
                $this->transformPostAtachmentVideo($attachment, $group_info->idd, $access_token);
                sleep(1);
            }
            if ($attachment->attachment == '0' && $attachment->type == 'video-youtube'){ 
                $this->transformPostAtachmentVideoYoutube($attachment, $group_info->idd, $access_token);
                sleep(1);
            }
            if ($attachment->attachment == '0' && $attachment->type == 'audio-local'){ 
                $this->transformPostAtachmentAudio($attachment, $group_info->idd, $access_token);
                sleep(1);
            }
            if ($attachment->attachment == '0' && $attachment->type == 'playlist-vk'){ 
                $this->transformPostAtachmentPlaylist($attachment);
                sleep(1);
            }
            if ($attachment->attachment == '0' && $attachment->type == 'image-url'){ 
                $this->transformPostAtachmentImageUrl($attachment, $group_info->idd, $access_token);
                sleep(1);
            }
        }
        
        $attachments = $post_info->attachments()->get();
        $atta = array();
        foreach ($attachments as $key => $attachment) { 
            if ($attachment->attachment != '0'){ 
                $atta[] = $attachment->attachment;
            }
        }
        if ($atta){
            $data['attachments'] = implode(',', $atta);
        }
        if ($post_info->link != ''){
            $data['attachments'] .= ','.$post_info->link;
        }
        //print_R($data); die();
    	$url = "https://api.vk.com/method/wall.post?".http_build_query($data);

    	//$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);

        $log = new Log();
        $log->text = 'Publish - '.$url.' -'.$post_id.' - '.json_encode($response);
        $log->save();

    	//echo $url;
    	$vk_info = $response; 
        //dd($vk_info);
    	if (isset($vk_info->response->post_id)){
    		$vk_id = $vk_info->response->post_id;

    		$post = Post::find($post_id);
    		$post->vk_id = $vk_id;
    		$post->vk_link = 'https://vk.com/'.$group_info->code.'?w=wall-'.$group_info->idd.'_'.$vk_id;
    		$post->publish_date = date('Y-m-d H:i:s');
    		$post->status_id = 3;
    		$post->save();

            ///////////
            /*$data = [
                'owner_id' => '-'.$group_info->idd,
                'access_token' => $access_token,
                'post_id' => $vk_id,
                'from_group' => 1685159,
                'message' => 'AH22222HHHHHH',
                //'from_group' => '1',
                //'message' => $post_info->description,
                'v' => '5.126',
            ];
            $url = "https://api.vk.com/method/wall.createComment?".http_build_query($data);
            $response = $this->makeRequestGet($url);
            dd($response);
*/
    		return response()->json(['success' => true]);
    	}
    	else{
            $post = Post::find($post_id);
            $post->group_id = null;
            $post->save();
    		return response()->json(['success' => false]);
    	}
    }

    public function publishVkTurn(Request $request){ 
    	$date = $request->dt;
    	$time = $request->time.':00';
    	$dt = $date.' '.$time;
    	$publish_date = strtotime($dt);
    	$post_id = $request->post_id;

    	$post_info = Post::find($post_id);
    	$group_info = Group::find($post_info->group_id);
    	//$access_token = Auth::user()->vk_token;
        $access_token =  User::find($group_info->user_id)->vk_token;

        $request->session()->put('post_group_id', $post_info->group_id);

        if (!$request->novk){
        	$data = [
        		'owner_id' => '-'.$group_info->idd,
        		'access_token' => $access_token,
        		'from_group' => '1',
        		'message' => $post_info->description,
        		'publish_date' => $publish_date,
        		'v' => '5.126',
        	];

            $attachments = $post_info->attachments()->get();

            foreach ($attachments as $key => $attachment) { 
                if ($attachment->attachment == '0' && $attachment->type == 'image-local'){ 
                    $this->transformPostAtachmentImage($attachment, $group_info->idd, $access_token);
                    sleep(1);
                }
                if ($attachment->attachment == '0' && $attachment->type == 'video-local'){ 
                    $this->transformPostAtachmentVideo($attachment, $group_info->idd, $access_token);
                    sleep(1);
                }
                if ($attachment->attachment == '0' && $attachment->type == 'video-youtube'){ 
                    $this->transformPostAtachmentVideoYoutube($attachment, $group_info->idd, $access_token);
                    sleep(1);
                }
                if ($attachment->attachment == '0' && $attachment->type == 'audio-local'){ 
                    $this->transformPostAtachmentAudio($attachment, $group_info->idd, $access_token);
                    sleep(1);
                }
                if ($attachment->attachment == '0' && $attachment->type == 'playlist-vk'){ 
                    $this->transformPostAtachmentPlaylist($attachment);
                    sleep(1);
                }
                if ($attachment->attachment == '0' && $attachment->type == 'image-url'){ 
                    $this->transformPostAtachmentImageUrl($attachment, $group_info->idd, $access_token);
                    sleep(1);
                }
            }
            
            $attachments = $post_info->attachments()->get();
            $atta = array();
            foreach ($attachments as $key => $attachment) { 
                if ($attachment->attachment != '0'){ 
                    $atta[] = $attachment->attachment;
                }
            }
            if ($atta){
                $data['attachments'] = implode(',', $atta);
            }

        	$url = "https://api.vk.com/method/wall.post?".http_build_query($data); 
        	//$response = file_get_contents($url);
            
            $response = $this->makeRequestGet($url);

            $log = new Log();
            $log->text = 'PublishVKTURN - '.$url.' -'.$post_id.' - '.json_encode($response);
            $log->save();
        	
        	$vk_info = $response;
        	if (isset($vk_info->response->post_id)){
        		$vk_id = $vk_info->response->post_id;

        		$post = Post::find($post_id);
        		$post->vk_id = $vk_id;
        		$post->vk_link = 'https://vk.com/'.$group_info->code.'?w=wall-'.$group_info->idd.'_'.$vk_id;
        		$post->publish_date = date('Y-m-d H:i:s', strtotime($dt));
        		$post->status_id = 3;
        		$post->save();

        		return response()->json(['success' => true]);
        	}
        	else{
                $post = Post::find($post_id);
                $post->group_id = null;
                $post->save();
        		return response()->json(['success' => false]);
        	}
        }
        else{
            $post = Post::find($post_id);
            $post->publish_date = date('Y-m-d H:i:s', strtotime($dt));
            $post->status_id = 2;
            $post->save();

            return response()->json(['success' => true]);
        }
    }

    public function publishTurn(Request $request){
    	$post_id = $request->post_id;

    	$post_info = Post::find($post_id);
    	$group_info = Group::find($post_info->group_id);
    	//$access_token = Auth::user()->vk_token;
        $access_token =  User::find($group_info->user_id)->vk_token;

    	$group_id = $post_info->group_id;

        $request->session()->put('post_group_id', $post_info->group_id);

        $response = false;
        $shedule_id = false;

        if ($request->user_shedule == 'true')
            $shedules =  Group::find($group_id)->shedule()->type('usual')->where('user_id', $post_info->user_id)->overnow(date('H:i:s'))->get();
        else
            $shedules =  Group::find($group_id)->shedule()->type('usual')->overnow(date('H:i:s'))->get();

        if ($shedules->count() > 0){
            foreach ($shedules as $key => $shedule) {
                if ($response) continue;
                $posts = Group::find($group_id)->post()->where('publish_date', date('Y-m-d').' '.$shedule->value)->get();

                if ($posts->count() == 0){
                    $response = date('d.m.Y').' '.$shedule->value;
                    $shedule_id = $shedule->id;
                }

            }
        }

        if (!$response){
            if ($request->user_shedule == 'true')
                $shedules = Group::find($group_id)->shedule()->type('usual')->where('user_id', $post_info->user_id)->ordered()->get();
            else
                $shedules = Group::find($group_id)->shedule()->type('usual')->ordered()->get();
            
            if ($shedules->count() > 0){
                $i = 1;
                while(!$response){
                    foreach ($shedules as $key => $shedule) {
                        if ($response) continue;
                        $posts = Group::find($group_id)->post()->where('publish_date', date('Y-m-d', strtotime('+'.$i.' day')).' '.$shedule->value)->get();

                        if ($posts->count() == 0){
                            $response = date('d.m.Y',strtotime('+'.$i.' day')).' '.$shedule->value;
                            $shedule_id = $shedule->id;
                        }
                    }
                    $i++;
                }
            }
        }

        $publish_date = strtotime($response);

    	if (!$request->novk){
            $data = [
                'owner_id' => '-'.$group_info->idd,
                'access_token' => $access_token,
                'from_group' => '1',
                'message' => $post_info->description,
                'publish_date' => $publish_date,
                'v' => '5.126',
            ];

            $attachments = $post_info->attachments()->get();
            foreach ($attachments as $key => $attachment) { 
                if ($attachment->attachment == '0' && $attachment->type == 'image-local'){ 
                    $this->transformPostAtachmentImage($attachment, $group_info->idd, $access_token);
                    sleep(1);
                }
                if ($attachment->attachment == '0' && $attachment->type == 'video-local'){ 
                    $this->transformPostAtachmentVideo($attachment, $group_info->idd, $access_token);
                    sleep(1);
                }
                if ($attachment->attachment == '0' && $attachment->type == 'video-youtube'){ 
                    $this->transformPostAtachmentVideoYoutube($attachment, $group_info->idd, $access_token);
                    sleep(1);
                }
                if ($attachment->attachment == '0' && $attachment->type == 'audio-local'){ 
                    $this->transformPostAtachmentAudio($attachment, $group_info->idd, $access_token);
                    sleep(1);
                }
                if ($attachment->attachment == '0' && $attachment->type == 'playlist-vk'){ 
                    $this->transformPostAtachmentPlaylist($attachment);
                    sleep(1);
                }
                if ($attachment->attachment == '0' && $attachment->type == 'image-url'){ 
                    $this->transformPostAtachmentImageUrl($attachment, $group_info->idd, $access_token);
                    sleep(1);
                }
            }
            
            $attachments = $post_info->attachments()->get();
            $atta = array();
            foreach ($attachments as $key => $attachment) { 
                if ($attachment->attachment != '0'){ 
                    $atta[] = $attachment->attachment;
                }
            }
            if ($atta){
                $data['attachments'] = implode(',', $atta);
            }

            $url = "https://api.vk.com/method/wall.post?".http_build_query($data); 
            //$response = file_get_contents($url);
            $response = $this->makeRequestGet($url);

            $log = new Log();
            $log->text = 'PublishTURN - '.$url.' -'.$post_id.' - '.json_encode($response);
            $log->save();

            $vk_info = $response;
            if (isset($vk_info->response->post_id)){
                $vk_id = $vk_info->response->post_id;

                $post = Post::find($post_id);
                $post->vk_id = $vk_id;
                $post->vk_link = 'https://vk.com/'.$group_info->code.'?w=wall-'.$group_info->idd.'_'.$vk_id;
                $post->publish_date = date('Y-m-d H:i:s', $publish_date);
                $post->status_id = 3;
                $post->shedule_id = $shedule->id;
                $post->save();

                return response()->json(['success' => true]);
            }
            else{
                $post = Post::find($post_id);
                $post->group_id = null;
                $post->save();
                return response()->json(['success' => false]);
            }
        }
        else{
            $post = Post::find($post_id);
            $post->publish_date = date('Y-m-d H:i:s', $publish_date);
            $post->status_id = 2;
            $post->shedule_id = $shedule->id;
            $post->save();

            return response()->json(['success' => true]);
        }
    	
    }

    public function groupStat(){
        $groups = Group::all();
        foreach ($groups as $key => $group) {
            echo $group->name."<br>";
            
            $data = [
                'group_id' => $group->idd,
                'access_token' => $group->user->vk_token,
                'v' => '5.126',
            ];
            
            $url = "https://api.vk.com/method/groups.getMembers?".http_build_query($data); 
            //$response = file_get_contents($url);
            $response = $this->makeRequestGet($url);
            $info = $response;
            
            if (isset($info->response->count)){
                $gr = Group::find($group->id);
                $gr->users_current = $info->response->count;
                $gr->users_before = $group->users_current;
                $gr->save();
            }
            sleep(1);
        }
    }

    public function cronPublish(){
       
        $posts = Post::term()->earlier()->get(); 
        
        foreach ($posts as $key => $post) {
            $group_info = Group::find($post->group_id);
            $access_token = User::find($group_info->user_id)->vk_token;

            if ($group_info && $access_token){ 
                $data = [
                    'owner_id' => '-'.$group_info->idd,
                    'access_token' => $access_token,
                    'from_group' => '1',
                    'message' => $post->description,
                    'v' => '5.126',
                ];

                $attachments = $post->attachments()->get();
                foreach ($attachments as $key => $attachment) { 
                    if ($attachment->attachment == '0' && $attachment->type == 'image-local'){ 
                        if (is_file(str_replace('http://rmocrm.top/', '', $attachment->content))){
                            $this->transformPostAtachmentImage($attachment, $group_info->idd, $access_token);
                            sleep(1);
                        }
                    }
                    if ($attachment->attachment == '0' && $attachment->type == 'video-local'){ 
                        $this->transformPostAtachmentVideo($attachment, $group_info->idd, $access_token);
                        sleep(1);
                    }
                    if ($attachment->attachment == '0' && $attachment->type == 'video-youtube'){ 
                        $this->transformPostAtachmentVideoYoutube($attachment, $group_info->idd, $access_token);
                        sleep(1);
                    }
                    if ($attachment->attachment == '0' && $attachment->type == 'audio-local'){ 
                        $this->transformPostAtachmentAudio($attachment, $group_info->idd, $access_token);
                        sleep(1);
                    }
                    if ($attachment->attachment == '0' && $attachment->type == 'playlist-vk'){ 
                        $this->transformPostAtachmentPlaylist($attachment);
                        sleep(1);
                    }
                    if ($attachment->attachment == '0' && $attachment->type == 'image-url'){ 
                        $this->transformPostAtachmentImageUrl($attachment, $group_info->idd, $access_token);
                        sleep(1);
                    }
                }
                
                $attachments = $post->attachments()->get();
                $atta = array();
                foreach ($attachments as $key => $attachment) { 
                    if ($attachment->attachment != '0'){ 
                        $atta[] = $attachment->attachment;
                    }
                }
                if ($atta){
                    $data['attachments'] = implode(',', $atta);
                }

                $url = "https://api.vk.com/method/wall.post?".http_build_query($data);
                //$response = file_get_contents($url);
                $response = $this->makeRequestGet($url);
                $vk_info = $response;
                
                if (isset($vk_info->response->post_id)){
                    $vk_id = $vk_info->response->post_id;

                    $post = Post::find($post->id);
                    $post->vk_id = $vk_id;
                    $post->vk_link = 'https://vk.com/'.$group_info->code.'?w=wall-'.$group_info->idd.'_'.$vk_id;
                    $post->publish_date = date('Y-m-d H:i:s');
                    $post->status_id = 3;
                    $post->save();
                }
                else{
                    $post = Post::find($post->id);
                    $post->status_id = 4;
                    $post->error_msg = json_encode($vk_info);
                    $post->save();
                }
            }
            sleep(1);
        }
    }

    public function editorAccessToken(Request $request){
        $vk_token = $request->vk_token;
        if (Auth::user()){
            $user = User::find(Auth::user()->id);
            $user->vk_token = $vk_token;
            $user->save();
        }

        echo 1;
    }

    public function postStat(){
        ini_set('max_execution_time', 600);

        $groups = Group::all();
        foreach ($groups as $key => $group) {
            $group_id = "-".$group->idd;

            $posts = $group->post()->whereNotNull('vk_id')->where('publish_date','>', Carbon::today()->subDays(2))->where('publish_date','<=',now())->get();

            foreach ($posts as $key => $post) {
                $data = [
                    'owner_id' => $group_id,
                    'post_id' => $post->vk_id,
                    'access_token' => $group->user->vk_token,
                    'v' => '5.126',
                ];
                
                $url = "https://api.vk.com/method/stats.getPostReach?".http_build_query($data); 
                //$response = file_get_contents($url);
                $response = $this->makeRequestGet($url);
                $info = $response;
                /*f ($post->id == 198)
                    dd($info);*/
                if (isset($info->error))
                    continue;
                else{
                    if (isset($info->response[0])){
                        $viewed = $info->response[0]->reach_total;

                        $p = Post::find($post->id);
                        $p->viewed = $viewed;
                        $p->save();
                    }
                }

                sleep(1);
                
            }

            /*if ($group->id == 107) dd($posts);*/
        }
        //die();
        foreach ($groups as $key => $group) {
            $group_id = "-".$group->idd;

            $posts = $group->company_group_post()->whereNotNull('vk_id')->where('publish_date','>', Carbon::today()->subDays(2))->where('publish_date','<=',now())->get();
            echo $group_id.' --- '.$posts->count().'<br>';


            //echo date('d.m.Y H:i');
            //die();
            foreach ($posts as $key => $post) {
                echo $post->company_post_id.' --- '.$post->group_id."<br>";
                $data = [
                    'owner_id' => $group_id,
                    'post_id' => $post->vk_id,
                    'access_token' => $group->user->vk_token,
                    'v' => '5.126',
                ];
                
                $url = "https://api.vk.com/method/stats.getPostReach?".http_build_query($data); 
                //$response = file_get_contents($url);
                $response = $this->makeRequestGet($url);
                $info = $response;
                
                if (isset($info->error))
                    continue;
                else{
                    if (isset($info->response[0])){
                        $viewed = $info->response[0]->reach_total;

                        $update = [
                            'viewed' => $viewed
                        ];

                        DB::table('company_posts_to_groups')->where([
                            'company_post_id' => $post->company_post_id,
                            'group_id' => $post->group_id
                        ])->update($update);
                    }
                }
                
                sleep(1);
            }

        }

        $posts = Post::/*where('cost',0)->*/where('strike', 0)->where('publish_date','<','NOW() - INTERVAL 2 DAY')->whereNotNull('viewed')->whereNotNull('publish_date')->where('paied',0)->get();
        foreach ($posts as $key => $post) {
            $viewed = $post->viewed;
            $group_id = $post->group_id;

            $res = DB::table('group_prices')->where('group_id', $group_id)->where('from','<=',$viewed)->where('to','>',$viewed)->first();
            if ($res){
                $price_id = $res->price_id;
                $price = Price::find($price_id);
                $price = $price->value;

                $post->cost = $price;
                $post->save();
            }
        }

        ini_set('max_execution_time', 300);
    }

    public function transformPostAtachmentImage($attachment, $group_id, $access_token){
        $da = [
            'group_id' => $group_id,
            'access_token' => $access_token,
            'v' => '5.126',
        ];

        $url = "https://api.vk.com/method/photos.getWallUploadServer?".http_build_query($da);
        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $info = $response;

        if (isset($info->response->upload_url)){
            $url = $info->response->upload_url;
            $arr = explode('/', $attachment->content);
            $name = $arr[count($arr)-1];
            $response = $this->transformImageToVkImage($url, $group_id, $access_token, $name);
            
            if (isset($response->response)){
                $response = $response->response[0];

                $code = "photo".$response->owner_id.'_'.$response->id;
                $content = $response->sizes[count($response->sizes)-1]->url;
                $type = 'image-vk';
                
                //unlink(storage_path('app/public').'/'.$name);

                $atach = PostAttachment::find($attachment->id);
                $atach->new_content = $content;
                $atach->attachment = $code;
                //$atach->content = $content;
                $atach->type = $type;
                $atach->save();
            }
        }
        else{
            $log = new Log();
            $log->text = 'getWallUploadServer - '.$url.' - '.json_encode($info);
            $log->save();
        }
    }

    public function transformImageToVkImage($vk_url, $group_id, $access_token, $content){
        $filename = storage_path('app/public').'/'.$content;
        //echo $filename.'AAA'; echo $content; die();
        $cfile = curl_file_create($filename,mime_content_type($filename),$content);
        //print_r($cfile); die();
        //echo $vk_url.'AAA';
        $response = $this->makeRequest($vk_url, ['photo' => $cfile]);
        
        $json = [];
        if ($response->photo){
            $da = [
                'group_id' => $group_id,
                'access_token' => $access_token,
                'photo' => $response->photo,
                'server' => $response->server,
                'hash' => $response->hash,
                'v' => '5.126',
            ];

            $url = "https://api.vk.com/method/photos.saveWallPhoto?".http_build_query($da);
            //$response = file_get_contents($url);
            $response = $this->makeRequestGet($url);
            //dd($response);
            $json = $response;
        }
        else{
            $log = new Log();
            $log->text = 'photos.saveWallPhoto - '.$vk_url.' - '.json_encode($response);
            $log->save();
        }

        return $json;
    }

    public function transformPostAtachmentVideo($attachment, $group_id, $access_token){
        $arr = explode('/', $attachment->content);
        $name = $arr[count($arr)-1];
        
        $da = [
            'name' => $attachment->title,
            'wallpost' => 0,
            'group_id' => $group_id,
            'access_token' => $access_token,
            'v' => '5.126',
        ];

        $url = "https://api.vk.com/method/video.save?".http_build_query($da);
        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $info = $response;
        
        if (isset($info->response->upload_url)){
            $url = $info->response->upload_url;
           
            $response = $this->transformVideoToVkVideo($url, $group_id, $access_token, $name);
            
            if (isset($response->size)){
                $code = "video".$response->owner_id.'_'.$response->video_id;
                $content = '';
                $type = 'video-vk';
                
                //unlink(storage_path('app/public').'/'.$name);

                $atach = PostAttachment::find($attachment->id);
                $atach->new_content = $content;
                $atach->attachment = $code;
                //$atach->content = $content;
                $atach->type = $type;
                $atach->save();
            }
        }
    }

    public function transformVideoToVkVideo($vk_url, $group_id, $access_token, $content){
        $filename = storage_path('app/public').'/'.$content;
        $cfile = curl_file_create($filename,mime_content_type($filename),$content);
        $response = $this->makeRequest($vk_url, ['photo' => $cfile]);
        $json = $response;

        return $json;
    }

    /**Картинка с УРЛА*/

    public function transformPostAtachmentImageUrl($attachment, $group_id, $access_token){
        $da = [
            'group_id' => $group_id,
            'access_token' => $access_token,
            'v' => '5.126',
        ];

        $url = "https://api.vk.com/method/photos.getWallUploadServer?".http_build_query($da);

        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $info = $response;


        if (isset($info->response->upload_url)){
            $url = $info->response->upload_url;
            $arr = explode('/', $attachment->content);

            $url2 = $attachment->content;
            $contents = file_get_contents($url2);
            $name = substr($url2, strrpos($url2, '/') + 1);
            Storage::put('public/'.$name, $contents);
            
            //$name = $arr[count($arr)-1];
            $response = $this->transformImageToVkImage($url, $group_id, $access_token, $name);
            //dd($response);
            if (isset($response->response)){
                $response = $response->response[0];

                $code = "photo".$response->owner_id.'_'.$response->id;
                $content = $response->sizes[count($response->sizes)-1]->url;
                $type = 'image-vk';
                
                //unlink(storage_path('app/public').'/'.$name);

                $atach = PostAttachment::find($attachment->id);
                $atach->new_content = $content;
                $atach->attachment = $code;
                //$atach->content = $content;
                $atach->type = $type;
                $atach->save();
            }
        }
    }

    /**Картинка с УРЛА*/

    public function transformPostAtachmentVideoYoutube($attachment, $group_id, $access_token){        
        $da = [
            'name' => '',
            'wallpost' => 0,
            'link' => $attachment->content,
            'group_id' => $group_id,
            'access_token' => $access_token,
            'v' => '5.126',
        ];

        $log = new Log();
        $log->text = 'transformPostAtachmentVideoYoutube begin - ';
        $log->save();

        $url = "https://api.vk.com/method/video.save?".http_build_query($da);
        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $info = $response;

        $log = new Log();
        $log->text = 'transformPostAtachmentVideoYoutube end - '.json_encode($info);
        $log->save();

        //print_R($info); die();
        if (isset($info->response->upload_url)){
            $url = $info->response->upload_url;
            file_get_contents($url);
            
            $code = "video".$info->response->owner_id.'_'.$info->response->video_id;

            $content = '';
            $type = 'video-vk';

            $atach = PostAttachment::find($attachment->id);
            $atach->attachment = $code;
            $atach->content = $content;
            $atach->type = $type;
            $atach->save();
            
        }
    }

    public function transformPostAtachmentAudio($attachment, $group_id, $access_token){
        $da = [
            /*'group_id' => $group_id,*/
            'access_token' => $access_token,
            'v' => '5.126',
        ];

        $url = "https://api.vk.com/method/audio.getUploadServer?".http_build_query($da);
        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $info = $response;
        
        if (isset($info->response->upload_url)){
            $url = $info->response->upload_url;
            $arr = explode('/', $attachment->content);
            $name = $arr[count($arr)-1];
            $response = $this->transformAudioToVkImage($url, $group_id, $access_token, $name);
            
            if (isset($response->response)){
                $response = $response->response;

                $code = "audio".$response->owner_id.'_'.$response->id;
                $content = $response->url;
                $type = 'audio-vk';
                
                //unlink(storage_path('app/public').'/'.$name);

                $atach = PostAttachment::find($attachment->id);
                $atach->new_content = $content;
                $atach->attachment = $code;
                //$atach->content = $content;
                $atach->type = $type;
                $atach->save();
            }
        }
    }

    public function transformAudioToVkImage($vk_url, $group_id, $access_token, $content){
        $filename = storage_path('app/public').'/'.$content;

        $cfile = curl_file_create($filename,'audio/mp3',$content);
        $response = $this->makeRequest($vk_url, ['file' => $cfile]);

        $json = [];
        if (isset($response->audio)){
            $da = [
                'group_id' => $group_id,
                'access_token' => $access_token,
                'audio' => $response->audio,
                'server' => $response->server,
                'hash' => $response->hash,
                'v' => '5.126',
            ];

            $url = "https://api.vk.com/method/audio.save?".http_build_query($da);
            //$response = file_get_contents($url);
            $response = $this->makeRequestGet($url);
            $json = $response;
        }

        return $json;
    }

    public function makeRequest($url, $data = []){
        $ch = curl_init();
        //echo $url; dd($data);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data; charset=UTF-8'));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $server_output = curl_exec($ch);
        /*$info = curl_getinfo($ch);
        print_r($info);*/
        curl_close ($ch);
        
        return json_decode($server_output);
    }

    public function transformPostAtachmentPlaylist($attachment){
        $link = $attachment->content;
        $arr = explode("z=", $link);
        if(isset($arr[1])){
            $url = $arr[1];
            $url = str_replace(['/','%2F'], '_', $url);
            
            $atach = PostAttachment::find($attachment->id);
            $atach->attachment = $url;
            $atach->save();
        }
        else{

        }
        
    }

    public function checkAudioToken(){
        $owner_id = Auth::user()->vk_id;
        $datan = array(
            'v' => '5.131',
            'https' => 1,
            'ref' => 'search',
            'extended' => 1,
            'offset' => 0,
            'count' => 10,
            'owner_id' => $owner_id,
            'device_id' => Auth::user()->vk_audio_device_id,
            'lang' => 'en',
            'access_token' => Auth::user()->vk_audio_token
        );


        $datan['sig'] = md5("/method/audio.get?".http_build_query($datan).Auth::user()->vk_audio_secret);
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_HTTPHEADER, array('User-Agent: VKAndroidApp/5.52-4543 (Android 5.1.1; SDK 22; x86_64; unknown Android SDK built for x86_64; en; 320x240)'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt(
            $ch,
            CURLOPT_URL,
            "https://api.vk.com/method/audio.get"
        );

        curl_setopt($ch,
            CURLOPT_POSTFIELDS,
            $datan
        );

        $response = json_decode(curl_exec($ch)); //print_R($response);//dd($response);
        if (!isset($response->response->items)){
            $access = 'false';
        }
        else{
            $access = 'true';
        }

        echo $access;
    }

    public function postSingleStat(Request $request){
        $posts = CompanyPostToGroup::where('company_post_id', $request->id)->get();

        foreach ($posts as $key => $post) {
            $group_info = Group::find($post->group_id);
            $group_id = "-".$group_info->idd;
            $data = [
                'owner_id' => $group_id,
                'post_id' => $post->vk_id,
                'access_token' => $group_info->user->vk_token,
                'v' => '5.131',
            ];
            
            $url = "https://api.vk.com/method/stats.getPostReach?".http_build_query($data); 
            //$response = file_get_contents($url);
            $response = $this->makeRequestGet($url);
            $info = $response;
            /*echo $url; print_R($info); 
            if ($key > 2)
                die();*/
            if (isset($info->error)){
                print_r($info);
                echo $url;
                continue;
            }
            else{
                if (isset($info->response[0])){
                    $viewed = $info->response[0]->reach_total;

                    $update = [
                        'viewed' => $viewed
                    ];

                    DB::table('company_posts_to_groups')->where([
                        'company_post_id' => $post->company_post_id,
                        'group_id' => $post->group_id
                    ])->update($update);
                }
            }
            
            sleep(1);
        }

        require_once 'simple_html_dom.php';

        $post_id = $request->id;
        $post = CompanyPost::find($post_id);
        foreach ($post->post_groups as $key => $post_group) {

            if (!$post_group->vk_id)
                continue;



            $comments = 0;
            $likes = 0;
            $reposts = 0;
            $comment_likes = 0;

            $group_id = "-".$post_group->group->idd;

            $data = [
                'owner_id' => $group_id,
                'post_id' => $post_group->vk_id,
                'access_token' => $post_group->group->user->vk_token,
                'count' => 1000,
                'v' => '5.126',
            ];

            $url = "https://api.vk.com/method/wall.getReposts?".http_build_query($data);
            $response = $this->makeRequestGet($url);

            $info = $response;
            if (isset($info->response)){
                foreach ($info->response->profiles as $key => $profile) {
                    /*$activity = new CompanyPostActivity();
                    $activity->group_id = $post_group->group_id;
                    $activity->company_post_id = $post->id;
                    $activity->company_id = $post->company_id;
                    $activity->type = 'repost';
                    $activity->value = $profile->id;
                    $activity->save();*/
                    $reposts++;
                }
            }

            $vk_reposts = 0;
            $html = file_get_html($post_group->vk_link);
            if ($html){ 
                $items = $html->find(".v_share");
                if (isset($items[0])){
                    $vk_reposts = (int)str_replace(' ', '', $items[0]->plaintext);
                }
            }

            if ($vk_reposts > $reposts)
                $reposts = $vk_reposts;
            /*echo $post_group->vk_link."<br>";
            echo $reposts."<br>";
            echo $vk_reposts."<br>";
            die();*/

            $data['need_likes'] = 1;
            $data['count'] = 100;
            $data['extended'] = 1;

            $url = "https://api.vk.com/method/wall.getComments?".http_build_query($data);
            $response = $this->makeRequestGet($url);
            $info = $response;
            
            if (isset($info->response)){
                $comments += $info->response->count;
                foreach ($info->response->profiles as $key => $profile) {
                   /* $activity = new CompanyPostActivity();
                    $activity->group_id = $post_group->group_id;
                    $activity->company_post_id = $post->id;
                    $activity->company_id = $post->company_id;
                    $activity->type = 'comment';
                    $activity->value = $profile->id;
                    $activity->save();*/
                }
                if (isset($info->response->items)){
                    foreach ($info->response->items as $key => $item) {
                        $comment_likes += isset($item->likes->count) ? $item->likes->count : 0;
                    }
                }

            
                if ($comments > 100){
                    for ($i=100;$i<=$comments;$i+=100){
                        $data['offset'] = $i;
                        $url = "https://api.vk.com/method/wall.getComments?".http_build_query($data);
                        $response = $this->makeRequestGet($url);
                        $info = $response;
                        $comments += $info->response->count;
                        foreach ($info->response->profiles as $key => $profile) {
                           /* $activity = new CompanyPostActivity();
                            $activity->group_id = $post_group->group_id;
                            $activity->company_post_id = $post->id;
                            $activity->company_id = $post->company_id;
                            $activity->type = 'comment';
                            $activity->value = $profile->id;
                            $activity->save();*/
                        }
                        if (isset($info->response->items)){
                            foreach ($info->response->items as $key => $item) {
                                $comment_likes += $item->likes->count;
                            }
                        }
                    }
                }
            }

            $likes += $comment_likes;

            $data = [
                'owner_id' => $group_id,
                'item_id' => $post_group->vk_id,
                'access_token' => $post_group->group->user->vk_token,
                'count' => 1000,
                'v' => '5.126',
                'extended' => 1,
                'type' => 'post'
            ];
            $url = "https://api.vk.com/method/likes.getList?".http_build_query($data);
            $response = $this->makeRequestGet($url);
            $info = $response;

            if (isset($info->response->items)){
                $likes += $info->response->count;
                foreach ($info->response->items as $key => $item) {
                    /*$activity = new CompanyPostActivity();
                    $activity->group_id = $post_group->group_id;
                    $activity->company_post_id = $post->id;
                    $activity->company_id = $post->company_id;
                    $activity->type = 'like';
                    $activity->value = $item->id;
                    $activity->save();*/
                }
            

                if ($info->response->count > 1000){
                    for ($i=1000;$i<=$info->response->count;$i+=1000){
                        $data['offset'] = $i;
                        $url = "https://api.vk.com/method/likes.getList?".http_build_query($data);
                        $response = $this->makeRequestGet($url);
                        $info = $response;
                        if (isset($info->response->items)){
                            foreach ($info->response->items as $key => $item) {
                                /*$activity = new CompanyPostActivity();
                                $activity->group_id = $post_group->group_id;
                                $activity->company_post_id = $post->id;
                                $activity->company_id = $post->company_id;
                                $activity->type = 'like';
                                $activity->value = $item->id;
                                $activity->save();*/
                            }
                        }
                    }
                }
            }


            //echo $reposts."<br>";echo $comments."<br>";echo $likes."<br>"; die();
            DB::table('company_posts_to_groups')->where([
                'company_post_id' => $post->id,
                'group_id' => $post_group->group_id
            ])->update(['repost' => $reposts, 'comment' => $comments, 'like' => $likes]);

            //usleep(500000);
            sleep(1);
        }

        echo 1;
    }

    public function checkToken(){
        $vk_id = Auth::user()->vk_id;
        if (!$vk_id && Auth::user()->role_id == 3){
            $client_info = Client::where('system_id', Auth::user()->id)->first();
            $vk_profile = $client_info->vk_profile;
            $vk_profile = explode('/', $vk_profile);
            $vk_profile = $vk_profile[count($vk_profile)-1];
            $datan = [
                'user_ids' => $vk_profile,
                'access_token' => Auth::user()->vk_token,
                /*'filter' => 'admin, editor',
                'extended' => 1,
                'count' => 100,*/
                'v' => '5.131',
            ];

            $url = "https://api.vk.com/method/users.get?".http_build_query($datan);
            $response = $this->makeRequestGet($url);
            if ($response->response[0]->id){
                $vk_id = $response->response[0]->id;

                $user = User::find(Auth::user()->id);
                $user->vk_id = $vk_id;
                $user->save();
            }
        }
        $datan = [
            'user_id' => $vk_id,
            'access_token' => Auth::user()->vk_token,
            'filter' => 'admin, editor',
            'extended' => 1,
            'count' => 100,
            'v' => '5.131',
        ];

        $url = "https://api.vk.com/method/groups.get?".http_build_query($datan);
        //$response = file_get_contents($url);
        $response = $this->makeRequest($url);
        //$response = json_decode($response);
        
        
        if (isset($response->response->items)){
            $access = 'true';
        }
        else{
            $access = 'false';
        }

        echo $access;
    }

    public function makeRequestGet($url, $data = []){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $server_output = curl_exec($ch);
        curl_close ($ch);
        
        return json_decode($server_output);
    }

    public function rubbish(){
        /*$pas = PostAttachment::whereNotNull('old_content')->where('old_content','!=','content')->get();
        foreach ($pas as $key => $pa) {
            echo $pa->old_content."<br>";

            $arr = explode('/', $pa->old_content);
            $name = $arr[count($arr)-1];

            unlink(storage_path('app/public').'/'.$name);
        }*/

        $posts = Post::whereNotNull('vk_id')->where('created_at','>=','2020-05-01')->get();
        foreach ($posts as $key => $post) {
            foreach ($post->attachments as $atach){
                if (strpos($atach->content, 'http://rmocrm.top/storage/') !== false){
                    echo $atach->content."<br>";
                    $arr = explode('/', $atach->content);
                    $name = $arr[count($arr)-1];
                    if(is_file(storage_path('app/public').'/'.$name)){
                        unlink(storage_path('app/public').'/'.$name);
                    }
                }
            }
            echo $post->id."<br>";
        }
    }


    public function storyStat(){
        ini_set('max_execution_time', 600);

        $posts = CompanyPost::where('type', 'story')->get();
        foreach ($posts as $key => $post) {
            $groups_posts = $post->post_groups()->whereNotNull('vk_id')->get();
            foreach ($groups_posts as $key => $groups_post) {
                $group = $groups_post->group;
                $data = [
                    'owner_id' => "-".$group->idd,
                    'story_id' => $groups_post->vk_id,
                    /*'owner_id' => '-27794386',
                    'story' => '456239018',*/
                    'access_token' => $group->user->vk_token,
                    'v' => '5.123',
                ];
               
                $url = "https://api.vk.com/method/stories.getStats?".http_build_query($data); 
                $response = $this->makeRequestGet($url);
                if (isset($response->response)){
                    $views = $response->response->views->count;
                    $shares = $response->response->shares->count;
                    $bans = $response->response->bans->count;
                    $subscribers = $response->response->subscribers->count;
                    $opens = $response->response->open_link->count;
                    $likes = $response->response->likes->count;

                    $update = [
                        'story_shares' => $shares,
                        'story_subscribers' => $subscribers,
                        'story_bans' => $bans,
                        'story_opens' => $opens,
                        'story_views' => $views,
                        'story_likes' => $likes,
                    ];

                    DB::table('company_posts_to_groups')->where([
                        'company_post_id' => $groups_post->company_post_id,
                        'group_id' => $groups_post->group_id
                    ])->update($update);
                }
            }
        }

        ini_set('max_execution_time', 300);

        echo 1;
    }

    public function postSingleStatStory(Request $request){
        $posts = CompanyPostToGroup::where('company_post_id', $request->id)->get();

        foreach ($posts as $key => $post) {
            $group = Group::find($post->group_id);
            $data = [
                'owner_id' => "-".$group->idd,
                'story_id' => $post->vk_id,
                'access_token' => $group->user->vk_token,
                'v' => '5.123',
            ];
           
            $url = "https://api.vk.com/method/stories.getStats?".http_build_query($data); 
            $response = $this->makeRequestGet($url);
            if (isset($response->response)){
                $views = $response->response->views->count;
                $shares = $response->response->shares->count;
                $bans = $response->response->bans->count;
                $subscribers = $response->response->subscribers->count;
                $opens = $response->response->open_link->count;
                $likes = $response->response->likes->count;

                $update = [
                    'story_shares' => $shares,
                    'story_subscribers' => $subscribers,
                    'story_bans' => $bans,
                    'story_opens' => $opens,
                    'story_views' => $views,
                    'story_likes' => $likes,
                ];

                DB::table('company_posts_to_groups')->where([
                    'company_post_id' => $post->company_post_id,
                    'group_id' => $post->group_id
                ])->update($update);
            }
        
            sleep(1);
        }

        echo 1;
    }

    public function storyPosting(){
        ini_set('max_execution_time', 600);

        $posts = CompanyPost::where('type', 'story')->get();
        foreach ($posts as $key => $post) {

            $attachments = $post->attachments()->get();
            foreach ($attachments as $key => $atta) {
                $type = $atta->type;
            }

            $groups_posts = $post->post_groups()->whereNull('vk_id')->where('publish_date','<=',now())->get();
            foreach ($groups_posts as $key => $groups_post) {
                $group_info = $groups_post->group;
                $access_token =  User::find($group_info->user_id)->vk_token;

                $story_type = 'image';
                if (strpos($type, 'video') !== false)
                    $story_type = 'video';

                if ($story_type == 'image'){
                    $data = [
                        'group_id' => $group_info->idd,
                        'access_token' => $access_token,
                        'add_to_news' => '1',
                        'link_text' => $post->title,
                        'link_url' => $post->link,
                        'v' => '5.130',
                    ];

                    $url = "https://api.vk.com/method/stories.getPhotoUploadServer"; 

                    $res = $this->makeRequestWithoutFile($url, $data);

                    if (isset($res->response->upload_url)){
                        foreach ($attachments as $key => $attach) {
                            $url = $res->response->upload_url;
                            $arr = explode('/', $attach->content);
                            $name = substr($attach->content, strrpos($attach->content, '/') + 1);
                            
                            
                            $filename = storage_path('app/public').'/'.$name;
                            $cfile = curl_file_create($filename,mime_content_type($filename),$name);

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS,['photo' => $cfile]);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data; charset=UTF-8'));
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
                            curl_setopt($ch, CURLOPT_TIMEOUT, 10);

                            $response = curl_exec($ch);

                            $story_info = json_decode($response); 
                            //$story_id = $story_info->response->story->id;
                            
                            curl_close ($ch);
                        }

                        $data = [
                            'group_id' => $group_info->idd,
                            'access_token' => $access_token,
                            'upload_results' => $story_info->response->upload_result,
                            'v' => '5.130',
                        ];
                        
                       
                        $url = "https://api.vk.com/method/stories.save"; 
                        $res = $this->makeRequestWithoutFile($url, $data);
                        $story_id = $res->response->items[0]->id;
                        $update = [
                            'vk_id' => $story_id,
                            'vk_link' => 'https://vk.com/'.$group_info->code.'?act=stories&w=story-'.$group_info->idd.'_'.$story_id,
                            'status' => 1
                        ];

                        DB::table('company_posts_to_groups')->where([
                            'company_post_id' => $groups_post->company_post_id,
                            'group_id' => $groups_post->group_id
                        ])->update($update);
                    }
                }
                else{
                    $data = [
                        'group_id' => $group_info->idd,
                        'access_token' => $access_token,
                        'add_to_news' => '1',
                        'link_text' => $post->title,
                        'link_url' => $post->link,
                        'v' => '5.130',
                    ];
                    //print_R($data);
                    $url = "https://api.vk.com/method/stories.getVideoUploadServer"; 

                    $res = $this->makeRequestWithoutFile($url, $data);

                    //dd($attachments);

                    if (isset($res->response->upload_url)){
                        foreach ($attachments as $key => $attach) {
                            $url = $res->response->upload_url;
                            $arr = explode('/', $attach->content);
                            $name = substr($attach->content, strrpos($attach->content, '/') + 1);
                            
                            
                            $filename = storage_path('app/public').'/'.$name;
                            $cfile = curl_file_create($filename,mime_content_type($filename),$name);

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS,['photo' => $cfile]);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data; charset=UTF-8'));
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
                            curl_setopt($ch, CURLOPT_TIMEOUT, 10);

                            $response = curl_exec($ch);

                            $story_info = json_decode($response); 
                            //$story_id = $story_info->response->story->id;
                            
                            curl_close ($ch);
                        }

                        $data = [
                            'group_id' => $group_info->idd,
                            'access_token' => $access_token,
                            'upload_results' => $story_info->response->upload_result,
                            'v' => '5.130',
                        ];
                        
                       // print_R($data);
                       
                        $url = "https://api.vk.com/method/stories.save"; 
                        $res = $this->makeRequestWithoutFile($url, $data);
                        $story_id = $res->response->items[0]->id;
                        //print_R($res);
                        $update = [
                            'vk_id' => $story_id,
                            'vk_link' => 'https://vk.com/'.$group_info->code.'?act=stories&w=story-'.$group_info->idd.'_'.$story_id,
                            'status' => 1
                        ];

                        DB::table('company_posts_to_groups')->where([
                            'company_post_id' => $groups_post->company_post_id,
                            'group_id' => $groups_post->group_id
                        ])->update($update);
                    }
                }
            }
        }
    }

    public function makeRequestWithoutFile($url, $data = []){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $server_output = curl_exec($ch);
        curl_close ($ch);
        
        return json_decode($server_output);
    }

    public function refreshStat(){
        Stat::truncate();

        $data = [];

        $ru_month = array( 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь' );

        $data['items'] = [];
        $months = $this->getMonthsInRange('01.06.2019', date('d.m.Y'));
        $months = array_reverse($months);
        foreach ($months as $key => $month) {
            $last_day = '';
            $date = new \DateTime($month['year'].'-'.$month['month'].'-01');
            $date->modify('last day of this month');
            $last_day = $date->format('d'); 
            

            $date_from = $month['year'].'-'.$month['month'].'-01';
            $date_to = $month['year'].'-'.$month['month'].'-'.$last_day;
            $json = [];
            $json['month'] = $month['year'].'-'.$month['month'];
            $json['month'] = $ru_month[(int)$month['month']-1].' '.$month['year'];

            $clients_ids = [];
            $results = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get();
            foreach ($results as $ke => $value) {
                if(!in_array($value->client_id, $clients_ids))
                    $clients_ids[] = $value->client_id;
            }

            $clients = Client::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->whereIn('id',$clients_ids)->get()->count();
            $sum = 0;
            $cls = Client::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->whereIn('id',$clients_ids)->get();
            foreach ($cls as $ke => $cl) {
                /*$orders = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->where('client_id', $cl->client_id)->get()->count();
                echo $orders."<br>";
                if ($orders)*/
                    $sum += $cl->company->sum('total');
            }
            $json['p1'] = $clients." ( $sum )";


            $clients = Client::where('created_at','<', $date_from)->whereIn('id',$clients_ids)->get()->count();
            $sum = 0;
            $cls = Client::where('created_at','<', $date_from)->whereIn('id',$clients_ids)->get();
            foreach ($cls as $ke => $cl) {
                $sum += $cl->company->where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->sum('total');
            }
            $json['p2'] = $clients." ( $sum )";

            $crm = 0;
            $results = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get();
            foreach ($results as $ke => $result) {
                $count = 0;
                $posts = CompanyPost::where('company_id', '=', $result['id'])->where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get();
                foreach ($posts as $ke => $post) {
                    foreach ($post->post_groups as $k => $post_group) {
                        $count += $post_group->viewed;
                    }
                }
                if ($count != 0)
                    $crm += ($result->total/$count)*1000;
            }
            $company_count = $results = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get()->count();
            if ($company_count != 0){
                $crm = $crm/$company_count;
            }
            else
                $crm = 0;
            $json['p7'] = number_format($crm,2,'.',' ');

            $clients = [];
            $total = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->sum('total');
            $results = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get();
            foreach ($results as $ke => $result) {
                if(!in_array($result->client_id, $clients) && $result->total != 0)
                    $clients[] = $result->client_id;
            }
            if (count($clients) != 0)
                $json['p8'] = number_format($total/count($clients),2,'.',' ');
            else
                $json['p8'] = 0;



            $total = Company::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get()->count();
            $json['p9'] = $total;

            $json['p5'] = Monitoring::where('readed_dt','>=', $date_from)->where('readed_dt','<=', $date_to)->get()->count();
            $json['p4'] = Sentence::where('status_dt','>=', $date_from)->where('status_dt','<=', $date_to)->get()->count();
            $json['p3'] = Message::where('updated_at','>=', $date_from)->where('updated_at','<=', $date_to)->whereNotNull('answer')->get()->count();
            $json['p6'] = CompanyPostEndDeleted::where('created_at','>=', $date_from)->where('created_at','<=', $date_to)->get()->count();
            $json['p10'] = Company::where('readed', 1)->where('readed_dt','>=', $date_from)->where('readed_dt','<=', $date_to)->get()->count();

            $data['items'][] = $json;

            $stat = new Stat();
            $stat->month = $json['month'];
            $stat->p1 = $json['p1'];
            $stat->p2 = $json['p2'];
            $stat->p8 = $json['p8'];
            $stat->p9 = $json['p9'];
            $stat->p7 = $json['p7'];
            $stat->p3 = $json['p3'];
            $stat->p4 = $json['p4'];
            $stat->p5 = $json['p5'];
            $stat->p6 = $json['p6'];
            $stat->p10 = $json['p10'];
            $stat->sort = $key;
            $stat->save();
        }

        /*$data['leftposts'] = false;
        if (Auth::user()->role->slug == 'client')
            $data['items'] = [];
        if (Auth::user()->role->slug != 'admin' && Auth::user()->role->slug != 'client') {
            $data['items'] = [];

            $shedules = Shedule::where('type','usual')->get()->count();

            $last_day = '';
            $date = new \DateTime(date('Y-m-d'));
            $date->modify('last day of this month');
            $last_day = $date->format('d'); 
            $date_from = date('Y-m').'-01';
            $date_to = date('Y-m').'-'.$last_day;

            $shedules = $shedules * ($last_day - (date('d')-1));

            $posts = Post::whereNotNull('shedule_id')->where('publish_date','>=', $date_from)->where('publish_date','<=', $date_to)->get()->count();
            //dd($posts);
            if ($shedules > $posts){
                $month = (int)date('n');
                $month = $month-1;
                $count_left = $shedules-$posts;
                $data['leftposts'] = 'На '.$ru_month[$month].' '.date('Y').' года, необходимо '.$count_left.' постов.';
            }
            else{
                $date = date('Y-m-d', strtotime('+1 month'));

                $date = new \DateTime(date('Y-m-d', strtotime('+1 month')));
                $date->modify('last day of this month');
                $last_day = $date->format('d'); 
                $date_from = date('Y-m', strtotime('+1 month')).'-01';
                $date_to = date('Y-m', strtotime('+1 month')).'-'.$last_day;

                $shedules = $shedules * $last_day;
                
                $posts = Post::whereNotNull('shedule_id')->where('publish_date','>=', $date_from)->where('publish_date','<=', $date_to)->get()->count();
                if ($shedules > $posts){
                    $month = (int)date('n', strtotime('+1 month'));
                    $month = $month-1;
                    $count_left = $shedules-$posts;
                    $data['leftposts'] = 'На '.$ru_month[$month].' '.date('Y', strtotime('+1 month')).' года, необходимо '.$count_left.' постов.';
                }
            }
        }*/



        echo "1";
    }

    public function getMonthsInRange($startDate, $endDate) {
        $months = array();
        while (strtotime($startDate) <= strtotime($endDate)) {
            $months[] = array('year' => date('Y', strtotime($startDate)), 'month' => date('m', strtotime($startDate)), );
            $startDate = date('d M Y', strtotime($startDate.
                '+ 1 month'));
        }

        return $months;
    }
}
