<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sentence;
use Illuminate\Support\Facades\DB;

class SentenceController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('sentences'),
    		'title' => 'Предложка'
    	];

        return view('panel.sentence.list')->with($data);
    }

    public function ajax(Request $request){
    	$sentences = [];
    	$results = Sentence::where('status','1')->get();

    	foreach ($results as $key => $result) { 
    		$sentences[] = [
    			$result->id,
    			"<a href='https://vk.com/id$result->user_id' target='_blank'>$result->firstname $result->lastname</a>",
    			"<a href='".$result->group->url."' target='_blank'>$result->groupname</a>",
    			$result->text,
    			date('d.m.Y H:i', strtotime($result->created_at)),
                '<a class="btn btn-danger btn-sm remove-sentence" href="'.route('sentences-remove-one',$result->id).'"><i class="fa fa-pencil"></i>&nbsp;Отписал клиенту</a>'
    		];
    	}

        /*$update = ['status' => 0];
        DB::table('sentences')->update($update);*/

    	echo json_encode(['aaData' => $sentences]);
    }

    public function dropAll(){
        /*$update = ['status' => '0', 'status_dt' => now()];
        DB::table('sentences')->update($update);*/

        Sentence::where('status', '1')->delete();

        return redirect()->to(route('sentences'));
    }

    public function dropOne(Request $request, $sentence_id){
        $update = ['status' => '0', 'status_dt' => now()];
        Sentence::find($sentence_id)->update($update);

        echo 1;
    }
}
