<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Answer;
use App\Models\Parsegroup;
use App\Models\Parseresult;

class AnalyticController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('analytic'),
    		'title' => 'Анализ рекламных записей'
    	];

        return view('panel.analytic.list')->with('data',$data);
    }

    public function ajax(Request $request){
        $groups = [];
        $results = Parsegroup::all();
        
        foreach ($results as $key => $result) {
            $groups[] = [
                $result->id,
                $result->name,
                $result->start,
                $result->end,
                $result->current,
                $result->parseresult->count(),
                $result->vk_id,
                '<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>&nbsp;<a href="#" class="on-default edit-row" style="margin-left: 10px" data-parsegroup-id="'.$result->id.'"><i class="fa fa-pencil"></i></a>&nbsp;<a href="#" class="on-default play-row" style="margin-left: 10px" data-parsegroup-id="'.$result->id.'"><i class="fa fa-play"></i></a>'
            ];
        }

        echo json_encode(['aaData' => $groups]);
    }

    public function drop(Request $request){
        $id = $request->post('id');

        Parsegroup::destroy($id);
        Parseresult::where('parsegroup_id',$id)->delete();

        return response('1')->header('Content-Type', 'text/html');
    }

    public function add(Request $request){
        $parsegroup = new Parsegroup;
        
        $parsegroup->name = $request->name;
        $parsegroup->start = $request->start;
        $parsegroup->end = $request->end;
        $parsegroup->current = $request->current;
        $parsegroup->vk_id = $request->vk_id;

        $parsegroup->save();

        return response('1')->header('Content-Type', 'text/html');
    }

    public function edit(Request $request){
        $parsegroup = Parsegroup::find($request->id);

        $parsegroup->name = $request->name;
        $parsegroup->start = $request->start;
        $parsegroup->end = $request->end;
        $parsegroup->current = $request->current;
        $parsegroup->vk_id = $request->vk_id;
        
        $parsegroup->save();

        return response('1')->header('Content-Type', 'text/html');
    }

    public function do(Request $request){
        $key = 'qwer123';
        $name = $request->name;
        $current = $request->current;
        $end = $request->end;

        $group = Parsegroup::find($request->id);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://adlook.top/analytic.php');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,['key' => $key, 'name' => $name, 'current' => $current, 'end' => $end, 'vk_id' => $group->vk_id]);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data; charset=UTF-8'));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,60); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        $server_output = curl_exec($ch);
        curl_close ($ch);
        
        $output = json_decode($server_output);

        $current = $output->current;
        $urls = $output->data;
        foreach ($urls as $key => $url) {
            $parseresult = new Parseresult();
            $parseresult->parsegroup_id = $request->id;
            $parseresult->link = $url;
            $parseresult->date_added = $request->date_added;
            $parseresult->save();
        }

        $recount = '0';
        if ($urls)
            $recount = '1';

        $parsegroup = Parsegroup::find($request->id);
        $parsegroup->current = $current;
        $parsegroup->save();

        $count = Parseresult::where('parsegroup_id', $request->id)->get()->count();

        header('Content-Type: application/json');
        echo json_encode([
            'current' => $current,
            'count' => $count,
            'recount' => $recount
        ]);
    }

    public function topicture(Request $request){
        $id = $request->id;

        $data = [];

        $results = Parseresult::where('parsegroup_id', $request->id)->orderBy('date_added','DESC')->orderBy('created_at','DESC')->get();
        foreach ($results as $key => $result) {
            $data[date('d.m.Y', strtotime($result->date_added))][] = $result;
        }

        return view('panel.analytic.topicture')->with('results',$data);
    }
}