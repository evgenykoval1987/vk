<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Company;
use App\Models\Post;
use App\Models\Message;
use App\Models\CompanyPost;
use App\Models\CompanyPostToGroup;
use App\Models\Sentence;
use App\Models\Price;
use App\Models\Package;
use App\User;
use App\Models\PackageGroup;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\CompanyPostEnd;
use App\Models\Log;

class GroupController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('groups'),
    		'title' => 'Группы'
    	];

    	$data['success'] = false;
    	if ($request->session()->get('group_added')){
    		$data['success'] = $request->session()->pull('group_added');
    	}

        if ($request->session()->get('group-prices')){
            $data['success'] = $request->session()->pull('group-prices');
        }
        

        return view('panel.group.list')->with('data',$data);
    }

    public function addForm(){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('groups'),
    		'title' => 'Группы'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('groups-add'),
    		'title' => 'Добавить'
    	];

    	return view('panel.group.add')->with('data',$data);
    }

    public function addGroup(Request $request){
    	$group = new Group;
    	$request->thumb = str_replace('?ava=1', '', $request->thumb);
    	$path = $request->thumb;
		$filename = basename($path);
		file_put_contents(public_path('images/group/' . $filename), file_get_contents( $request->thumb));

    	$group->url = $request->group_url;
    	$group->name = $request->name;
    	$group->code = $request->group_code;
    	$group->idd = $request->group_id;
    	$group->token = $request->vktoken;
    	$group->image = 'images/group/' . $filename;
        $group->user_id = Auth::user()->id;
    	$group->users_current = $request->users;
    	$group->users_before = $request->users;
        $group->token_for_messages = $request->token_for_messages;

	    $group->save();

        $group_id = $group->id;

        $data = [
            'group_id' => $request->group_id,
            'access_token' => $group->user->vk_token,
            'v' => '5.131',
        ];
        
        $url = "https://api.vk.com/method/groups.getCallbackConfirmationCode?".http_build_query($data); 
        //$response = file_get_contents($url);
        $response = $this->makeRequestGet($url);
        $info = json_decode($response);
        if (isset($info->response->code)){
            $group = Group::find($group_id);
            $group->callback_confirm_code = $info->response->code;
            $group->save();

            $data = [
                'group_id' => $request->group_id,
                'url' => route('groups-callback'),
                'title' => 'ADMIN PANEL',
                'access_token' => $group->user->vk_token,
                'v' => '5.131',
            ];

            $url = "https://api.vk.com/method/groups.addCallbackServer?".http_build_query($data); 
            //$response = file_get_contents($url);
            $response = $this->makeRequestGet($url);
            $info = json_decode($response);
            if (isset($info->response->server_id)){
                $group = Group::find($group_id);
                $group->callback_server_id = $info->response->server_id;
                $group->save();

                $data = [
                    'group_id' => $request->group_id,
                    'server_id' => $info->response->server_id,
                    'api_version' => '5.92',
                    'wall_post_new' => 1,
                    'message_new' => 1,
                    'access_token' => $group->user->vk_token,
                    'v' => '5.131',
                ];
                $url = "https://api.vk.com/method/groups.setCallbackSettings?".http_build_query($data); 
                //$response = file_get_contents($url);
                $response = $this->makeRequestGet($url);
                $info = json_decode($response);

            }
        }
        
	    $request->session()->put('group_added', 'Сообщество <strong>'.$request->name.'</strong> добавлено');

	    return redirect(route('groups'));
    }

    public function ajax(Request $request){
    	$groups = [];
    	$results = Group::all();

        $usrs = User::all();


    	foreach ($results as $key => $result) {
    		
            $select = '<select class="form-control group-user-id" data-group-id="'.$result->id.'">';
            foreach ($usrs as $key => $u) {
                if ($u->id == $result->user_id){
                    $select .= '<option value="'.$u->id.'" selected>'.$u->name.'</option>';
                }   
                else
                    $select .= '<option value="'.$u->id.'">'.$u->name.'</option>';
            }
            $select .= '</select>';

            $users = (int)$result->users_current-(int)$result->users_before;
    		$thumb = '<img src="'.str_replace("?", "_", $result->image).'" width="50px">';
    		$groups[] = [
    			$result->id,
    			$thumb,
    			'<a href="'.$result->url.'" target="_blank">'.$result->name.'</a>',
                //$result->user->name,
                $select,
                "<input type='text' value='".$result->callback_confirm_code."' class='group-code form-control' data-id='".$result->id."'>",
    			($users >= 0) ? $result->users_current.' (+'.$users.')' : $result->users_current.' ('.$users.')',
                ($result->shedule()->get()->count() == 0) ? 'Нет' : 'Есть',
    			'<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>',
                '<a href="'.route('groups-prices', $result->id).'" class="on-default"><i class="fa fa-money"></i></a>'
    		];
    	}

    	echo json_encode(['aaData' => $groups]);
    }

    public function drop(Request $request){
    	$id = $request->post('id');

    	Group::destroy($id);

    	return response('1')->header('Content-Type', 'text/html');
    }

    public function statistic(Request $request){
        if(isset($request->type)){
            /** Телеграм бот */
            $bot_id = '790678748';
            $bot_key = 'AAE2lNq4liJRii5KqkIlM_pUxuNyQ5vtAfs';
            $chat_id = '-352301046';
            /** Телеграм бот */

            $log = new Log();
            $log->text = 'CALLBACK - '.' - '.json_encode($request);
            $log->save();

            if($request->type == 'confirmation'){
                $group_id = $request->group_id;

                $group_info = Group::where('idd', $group_id)->firstOrFail();
                echo $group_info->callback_confirm_code;
            }
            if($request->type == 'wall_post_new'){
                $post_vk_id = 0;
                $new_vk_id = $request->object['id'];
                $group_id = $request->group_id;

                $group_info = Group::where('idd', $group_id)->firstOrFail();

                $data = [
                    'posts' => $request->object['owner_id'].'_'.$new_vk_id,
                    'extended' => 0,
                    'copy_history_depth' => 2,
                    'access_token' => $group_info->user->vk_token,
                    'v' => '5.131',
                ];

                $url = "https://api.vk.com/method/wall.getById?".http_build_query($data); 
                //$response = file_get_contents($url);
                $response = $this->makeRequestGet($url);
                $info = json_decode($response);
               
                if(isset($info->response[0]->postponed_id)){
                    $post_vk_id = $info->response[0]->postponed_id;
                }
                
                $log = new Log();
                $log->text = 'CALLBACK 2 - '.' - '.$post_vk_id;
                $log->save();
                
                if($post_vk_id){ 
                    
                    $post = Post::where('vk_id', $post_vk_id)->where('group_id', $group_info['id'])->first();
                    if ($post) {
                       
                        $post->vk_id = $new_vk_id;
                        $post->vk_link = 'https://vk.com/'.$group_info->code.'?w=wall-'.$group_info->idd.'_'.$new_vk_id;
                        $post->save();
                    }
                    else{
                        
                        $post = CompanyPostToGroup::where('vk_id', $post_vk_id)->where('group_id', $group_info['id'])->first();
                        if ($post) {
                            
                            $update = [
                                'vk_id' => $new_vk_id,
                                'vk_link' => 'https://vk.com/'.$group_info->code.'?w=wall-'.$group_info->idd.'_'.$new_vk_id,
                                'status' => 1
                            ];

                            DB::table('company_posts_to_groups')->where([
                                'vk_id' => $post_vk_id,
                                'group_id' => $group_info['id']
                            ])->update($update);

                            $company_post_id = $post->company_post_id;

                            $flag = true;
                            $cps = CompanyPostToGroup::where('company_post_id', $company_post_id)->get();
                            foreach ($cps as $key => $cp) {
                                if (strtotime(date('d.m.Y H:i:s')) < strtotime($cp->publish_date))
                                    $flag = false;
                            }
                            if ($flag) {
                                $cpe = new CompanyPostEnd();
                                $cpe->company_post_id = $company_post_id;
                                $cpe->dt = now();
                                $cpe->save();

                                $this->recalculate($company_post_id);
                            }
                            $company_post_info = CompanyPost::find($company_post_id);  
                            $company_info = Company::find($company_post_info->company_id);
                           
                            if ($company_info->client->system_id){
                                file_put_contents('Q5', $company_post_info->comments);
                                $comments = explode("\r\n", $company_post_info->comments); 
                                if ($comments){
                                    $comment = $comments[array_rand($comments)];
                                    file_put_contents('Q6', $comment);
                                    $data = [
                                        'group_ids' => str_replace('https://vk.com/', '', $company_info->client->group_link),
                                        'access_token' => $group_info->user->vk_token,
                                        'group_id' => '',
                                        'fields' => '',
                                        'v' => '5.131',
                                    ];

                                    $vk_group_id = false;
                                    $url = "https://api.vk.com/method/groups.getById?".http_build_query($data); 
                                    $response = $this->makeRequestGet($url);
                                    file_put_contents('Q7', $response);
                                    $info = json_decode($response);
                                    if(isset($info->response[0]->id)){
                                        $vk_group_id = $info->response[0]->id;
                                    }
                                    file_put_contents('ZZZZZZ2', $vk_group_id);
                                    if ($vk_group_id){  file_put_contents('ZZZZZZ3', $vk_group_id);
                                        $ui = User::find($company_info->client->system_id);
                                        $data = [
                                            'owner_id' => $request->object['owner_id'],
                                            'access_token' => $ui->vk_token,
                                            'post_id' => $new_vk_id,
                                            'from_group' => $vk_group_id,
                                            'message' => $comment,
                                            'v' => '5.131',
                                        ];
                                        $url = "https://api.vk.com/method/wall.createComment?".http_build_query($data);
                                        $response = $this->makeRequestGet($url);
                                        file_put_contents('AAAAA', $url);
                                        file_put_contents('BBBBB', $response);
                                    }
                                }
                            }
                            //(strtotime(date('d.m.Y H:i:s')) > strtotime($post->publish_date))
                        }
                    }
                }

                /** TELEGRAM BOT */
                if($request->object['post_type'] == 'suggest') {
                    $user_id = $request->object['from_id'];
                    $text = $request->object['text'];
                    $owner_id = $request->object['owner_id'];
                    $group_id = str_replace('-','',$owner_id);

                    $group_info = Group::where('idd', $group_id)->firstOrFail();
                    $token = $group_info->user->vk_token;
                    $group = $group_info->name;

                    $user_info = json_decode($this->makeRequestGet("https://api.vk.com/method/users.get?user_ids={$user_id}&access_token={$token}&v=5.81")); 

                    $user_first_name = $user_info->response[0]->first_name;
                    
                    $user_last_name = $user_info->response[0]->last_name; 

                    $link = '<a href="https://vk.com/id'.$user_id.'">'.$user_first_name.'</a>';
                    
                    $telegram_send = file_get_contents("https://api.telegram.org/bot{$bot_id}:{$bot_key}/sendMessage?disable_web_page_preview=true&chat_id={$chat_id}&parse_mode=HTML&text=".$text." ".$link.' группа: '.$group); 
                    
                    $telegram_json = json_decode($telegram_send);

                    $sentence = new Sentence;
                    $sentence->user_id = $user_id;
                    $sentence->group_id = $group_info->id;
                    $sentence->firstname = $user_first_name;
                    $sentence->lastname = $user_last_name;
                    $sentence->groupname = $group;
                    $sentence->text = $text;
                    $sentence->save();
                    
                }
                /** TELEGRAM BOT */

                echo "ok";
            }
            if($request->type == 'message_new'){
                
                if (isset($request->object['from_id'])){
                    $user_id = $request->object['from_id'];
                    $date = $request->object['date'];
                    $body = $request->object['text'];
                    $out = $request->object['out'];
                    $vk_id = $request->object['id'];
                }
                else{
                    $user_id = $request->object['message']['from_id'];
                    $date = $request->object['message']['date'];
                    $body = $request->object['message']['text'];
                    $out = $request->object['message']['out'];
                    $vk_id = $request->object['message']['id'];
                }
                $group_id = $request->group_id;

                $group_info = Group::where('idd', $group_id)->firstOrFail();
                $token = $group_info->user->vk_token;
                $user_info = json_decode($this->makeRequestGet("https://api.vk.com/method/users.get?user_ids={$user_id}&access_token={$token}&v=5.81")); 

                $user_first_name = $user_info->response[0]->first_name; 
                $user_last_name = $user_info->response[0]->last_name; 

                Message::where('user_id',$user_id)->where('group_id',$group_info->id)->delete();

                $message = new Message;
                $message->firstname = $user_first_name;
                $message->lastname = $user_last_name;
                $message->out = $out;
                $message->vk_dt = date('Y-m-d H:i:s', $date);
                $message->text = $body;
                $message->group_id = $group_info->id;
                $message->user_id = $user_id;
                $message->vk_id = $vk_id;
                $message->save();

                $email = 'khoma.a.a@gmail.com';
                //$email = 'evgenykoval@gmail.com';

                $message = "
                    Группа: ".$group_info->name."<br>
                    Сообщение: ".$body."<br>
                    Отправитель: ".$user_first_name." ".$user_last_name."<br>
                    Дата: ".date("Y/m/d H:i",$date)."";
                     
                $headers = "MIME-Version: 1.0" . PHP_EOL .
                    "Content-Type: text/html; charset=utf-8" . PHP_EOL .
                    'From: info@vk.ru' . PHP_EOL;

                mail($email, "Сообщение из группы Вконтакте", $message, //
                         $headers);

                echo "ok";
            }
        }
        
    }

    public function prices($group_id){
        $group_info = Group::find($group_id);

        $data = [];

        $data['breadcrumbs'][] = [
            'link' => route('groups-prices', $group_id),
            'title' => 'Цены группы'
        ];

        $data['group_info'] = $group_info;

        $data['prices'] = Price::all();

        $data['group_prices'] = [];
        $results = DB::table('group_prices')->where('group_id', $group_id)->get();
        foreach ($results as $key => $result) {
            $data['group_prices'][$result->price_id] = [
                'from' => $result->from,
                'to' => $result->to
            ];
        }

        return view('panel.group.price')->with($data);
    }

    public function pricesSave($group_id, Request $request){
        DB::table('group_prices')->where('group_id',$group_id)->delete();
        foreach ($request->prices as $key => $price) {
            DB::table('group_prices')->insert(
                ['group_id' => $group_id, 'price_id' => $key, 'from' => $price['from'], 'to' => $price['to']]
            );
        }

        $request->session()->put('group-prices', 'Цены сохранены');

        return redirect(route('groups'));
    }

    public function packages(Request $request){
        $data = [];

        $data['breadcrumbs'][] = [
            'link' => route('packages'),
            'title' => 'Пакеты групп'
        ];

        $data['success'] = false;
        if ($request->session()->get('package_added')){
            $data['success'] = $request->session()->pull('package');
        }

        return view('panel.group.package.list')->with('data',$data);
    }

    public function packagesAjax(Request $request){
        $packages = [];
        $results = Package::all();

        foreach ($results as $key => $result) {
            $groups = [];
            $pgs = PackageGroup::where('package_id', $result->id)->get();
            foreach ($pgs as $ke => $pg) {
                $groups[] = $pg->group->url;
            }
            $packages[] = [
                $result->id,
                $result->name,
                implode('<br>', $groups),
                '<a href="'.route('packages-edit', $result->id).'" class="on-default"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;&nbsp;<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>'
            ];
        }

        echo json_encode(['aaData' => $packages]);
    }

    public function packageDrop(Request $request){
        $id = $request->post('id');

        DB::table('packages_groups')->where('package_id', '=', $id)->delete();

        Package::destroy($id);

        return response('1')->header('Content-Type', 'text/html');
    }

    public function addPackagesForm(){
        $data = [];

        $data['breadcrumbs'][] = [
            'link' => route('packages'),
            'title' => 'Пакеты групп'
        ];

        $data['breadcrumbs'][] = [
            'link' => route('packages-add'),
            'title' => 'Добавить'
        ];

        $data['groups'] = Group::all();

        return view('panel.group.package.add')->with('data',$data);
    }

    public function addPackage(Request $request){

        /*$package = new Package;
        $package->name = $request->name;
        $package->day = $request->day;
        $package->time = $request->time;
        $package->save();

        $package_id = $package->id;

        if (isset($request->groups)){
            foreach ($request->groups as $key => $group) {
                $pg = new PackageGroup();
                $pg->package_id = $package_id;
                $pg->group_id = $group;
                $pg->save();
            }
        }*/

        $package = new Package;
        $package->name = $request->name;
        $package->save();

        $package_id = $package->id;

        if (isset($request->groups)){
            foreach ($request->groups as $key => $group) {
                if (isset($group['id'])){
                    $pg = new PackageGroup();
                    $pg->package_id = $package_id;
                    $pg->group_id = $group['id'];
                    $pg->day = $group['day'];
                    $pg->time = $group['time'];
                    $pg->save();
                }
                
            }
        }
        
        
        $request->session()->put('package_added', 'Пакет групп <strong>'.$request->name.'</strong> добавлен');

        return redirect(route('packages'));
    }

    public function editPackageForm(Request $request, $id){
        $data = [];

        $data['breadcrumbs'][] = [
            'link' => route('packages'),
            'title' => 'Пакеты групп'
        ];

        $data['breadcrumbs'][] = [
            'link' => route('packages-add'),
            'title' => 'Редактировать'
        ];

        $data['groups'] = Group::all();
        $data['selected'] = [];
        $selected = PackageGroup::where('package_id', $id)->get();
        foreach ($selected as $key => $select) {
            $data['selected'][$select->group_id] = [
                'day' => $select->day,
                'time' => $select->time
            ];
        }

        $data['package_info'] = Package::find($id);

        $data['in_groups'] = [];
        $pgs = PackageGroup::where('package_id', $id)->get();
        foreach ($pgs as $key => $pg) {
            $data['in_groups'][] = $pg->group_id;
        }

        return view('panel.group.package.edit')->with('data',$data);
    }

    public function editPackage(Request $request, $id){
        $package = Package::find($id);
        $package->name = $request->name;
        $package->save();

        DB::table('packages_groups')->where('package_id', '=', $id)->delete();

        if (isset($request->groups)){
            foreach ($request->groups as $key => $group) {
                if (isset($group['id'])){
                    $pg = new PackageGroup();
                    $pg->package_id = $id;
                    $pg->group_id = $group['id'];
                    $pg->day = $group['day'];
                    $pg->time = $group['time'];
                    $pg->save();
                }
                /*$pg = new PackageGroup();
                $pg->package_id = $id;
                $pg->group_id = $group;
                $pg->save();*/
            }
        }
        
        
        $request->session()->put('package_added', 'Пакет групп <strong>'.$request->name.'</strong> изменен');

        return redirect(route('packages'));
    }

    public function makeRequest($url, $data = []){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data; charset=UTF-8'));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $server_output = curl_exec($ch);
        curl_close ($ch);
        
        return $server_output;
    }

    public function makeRequestGet($url, $data = []){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $server_output = curl_exec($ch);
        curl_close ($ch);
        
        return $server_output;
    }

    public function editGroupConfirm(Request $request, $id){
        $group = Group::find($id);
        $group->callback_confirm_code = $request->value;
        $group->save();

        return response('1')->header('Content-Type', 'text/html');
    }

    public function editUser(Request $request, $id){
        $group = Group::find($id);
        $group->user_id = $request->user_id;
        $group->save();

        return response('1')->header('Content-Type', 'text/html');
    }

    public function recalculate($post_id){
        require_once 'simple_html_dom.php';
        $post = CompanyPost::find($post_id);
        foreach ($post->post_groups as $key => $post_group) {
            if (!$post_group->vk_id)
                continue;

            $comments = 0;
            $likes = 0;
            $reposts = 0;
            $comment_likes = 0;

            $group_id = "-".$post_group->group->idd;

            $data = [
                'owner_id' => $group_id,
                'post_id' => $post_group->vk_id,
                'access_token' => $post_group->group->user->vk_token,
                'count' => 1000,
                'v' => '5.131',
            ];

            $url = "https://api.vk.com/method/wall.getReposts?".http_build_query($data);
            $response = $this->makeRequestGet($url);

            $info = $response;
            if (isset($info->response)){
                foreach ($info->response->profiles as $key => $profile) {
                    /*$activity = new CompanyPostActivity();
                    $activity->group_id = $post_group->group_id;
                    $activity->company_post_id = $post->id;
                    $activity->company_id = $post->company_id;
                    $activity->type = 'repost';
                    $activity->value = $profile->id;
                    $activity->save();*/
                    $reposts++;
                }
            }

            $vk_reposts = 0;
            $html = file_get_html($post_group->vk_link);
            if ($html){ 
                $items = $html->find(".v_share");
                if (isset($items[0])){
                    $vk_reposts = (int)str_replace(' ', '', $items[0]->plaintext);
                }
            }

            if ($vk_reposts > $reposts)
                $reposts = $vk_reposts;

            $data['need_likes'] = 1;
            $data['count'] = 100;
            $data['extended'] = 1;

            $url = "https://api.vk.com/method/wall.getComments?".http_build_query($data);
            $response = $this->makeRequestGet($url);
            $info = $response;
            
            if (isset($info->response)){
                $comments += $info->response->count;
                foreach ($info->response->profiles as $key => $profile) {
                   /* $activity = new CompanyPostActivity();
                    $activity->group_id = $post_group->group_id;
                    $activity->company_post_id = $post->id;
                    $activity->company_id = $post->company_id;
                    $activity->type = 'comment';
                    $activity->value = $profile->id;
                    $activity->save();*/
                }
                if (isset($info->response->items)){
                    foreach ($info->response->items as $key => $item) {
                        $comment_likes += isset($item->likes->count) ? $item->likes->count : 0;
                    }
                }

            
                if ($comments > 100){
                    for ($i=100;$i<=$comments;$i+=100){
                        $data['offset'] = $i;
                        $url = "https://api.vk.com/method/wall.getComments?".http_build_query($data);
                        $response = $this->makeRequestGet($url);
                        $info = $response;
                        $comments += $info->response->count;
                        foreach ($info->response->profiles as $key => $profile) {
                           /* $activity = new CompanyPostActivity();
                            $activity->group_id = $post_group->group_id;
                            $activity->company_post_id = $post->id;
                            $activity->company_id = $post->company_id;
                            $activity->type = 'comment';
                            $activity->value = $profile->id;
                            $activity->save();*/
                        }
                        if (isset($info->response->items)){
                            foreach ($info->response->items as $key => $item) {
                                $comment_likes += $item->likes->count;
                            }
                        }
                    }
                }
            }

            $likes += $comment_likes;

            $data = [
                'owner_id' => $group_id,
                'item_id' => $post_group->vk_id,
                'access_token' => $post_group->group->user->vk_token,
                'count' => 1000,
                'v' => '5.131',
                'extended' => 1,
                'type' => 'post'
            ];
            $url = "https://api.vk.com/method/likes.getList?".http_build_query($data);
            $response = $this->makeRequestGet($url);
            $info = $response;

            if (isset($info->response->items)){
                $likes += $info->response->count;
                foreach ($info->response->items as $key => $item) {
                    /*$activity = new CompanyPostActivity();
                    $activity->group_id = $post_group->group_id;
                    $activity->company_post_id = $post->id;
                    $activity->company_id = $post->company_id;
                    $activity->type = 'like';
                    $activity->value = $item->id;
                    $activity->save();*/
                }
            

                if ($info->response->count > 1000){
                    for ($i=1000;$i<=$info->response->count;$i+=1000){
                        $data['offset'] = $i;
                        $url = "https://api.vk.com/method/likes.getList?".http_build_query($data);
                        $response = $this->makeRequestGet($url);
                        $info = $response;
                        if (isset($info->response->items)){
                            foreach ($info->response->items as $key => $item) {
                                /*$activity = new CompanyPostActivity();
                                $activity->group_id = $post_group->group_id;
                                $activity->company_post_id = $post->id;
                                $activity->company_id = $post->company_id;
                                $activity->type = 'like';
                                $activity->value = $item->id;
                                $activity->save();*/
                            }
                        }
                    }
                }
            }
            //echo $reposts."<br>";echo $comments."<br>";echo $likes."<br>"; die();
            DB::table('company_posts_to_groups')->where([
                'company_post_id' => $post->id,
                'group_id' => $post_group->group_id
            ])->update(['repost' => $reposts, 'comment' => $comments, 'like' => $likes]);

            //usleep(500000);
            sleep(1);
        }
    }
}
