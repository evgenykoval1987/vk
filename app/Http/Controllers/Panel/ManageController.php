<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Group;
use App\User;

class ManageController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('manage'),
    		'title' => 'Посты'
    	];

        if (isset($request->user_id)){
            $request->session()->put('manage_user_id', $request->user_id);
            return redirect(route('manage'));
        }

        $data['user_id'] = 0;

        if ($request->session()->get('manage_user_id')){
            $user_id = $request->session()->get('manage_user_id');
            $data['user_id'] = $user_id;
            $data['count_created_posts'] = Post::noterm()->where('user_id',$user_id)->get()->count();
            $skip = 1;
            if(isset($request->skip)){
                $skip = $request->skip+1;
                $data['post'] = POST::noterm()->where('user_id',$user_id)->skip($request->skip)->take(1)->orderby('created_at','DESC')->get()->first();
            }
            else{
                $data['post'] = POST::noterm()->where('user_id',$user_id)->orderby('created_at','DESC')->get()->first();
            }

            if(!$data['post']){
                $data['post'] = POST::noterm()->where('user_id',$user_id)->orderby('created_at','DESC')->get()->first();
                $skip = 1;
            }
            $data['skip'] = route('manage').'?skip='.$skip;
        }
        else{
            $data['count_created_posts'] = Post::noterm()->get()->count();
            $skip = 1;
            if(isset($request->skip)){
                $skip = $request->skip+1;
                $data['post'] = POST::noterm()->skip($request->skip)->take(1)->orderby('created_at','DESC')->get()->first();
            }
            else{
                $data['post'] = POST::noterm()->orderby('created_at','DESC')->get()->first();
            }

            if(!$data['post']){
                $data['post'] = POST::noterm()->orderby('created_at','DESC')->get()->first();
                $skip = 1;
            }
            $data['skip'] = route('manage').'?skip='.$skip;
        }
        

        $data['groups'] = Group::all();

        $data['group_id'] = 0;
        if ($request->session()->has('post_group_id')){
            $data['group_id'] = $request->session()->get('post_group_id');
        }

        
        $data['users'] = User::where('role_id',2)->get();

        return view('panel.manage.main')->with($data);
    }
}
