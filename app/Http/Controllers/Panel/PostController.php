<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Post;
use App\Models\PostAttachment;
use App\User;
use App\Models\Pay;
use App\Models\PostToPay;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Setting;

class PostController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('posts'),
    		'title' => 'Посты'
    	];

    	$data['success'] = false;
    	if ($request->session()->get('post_added')){
    		$data['success'] = $request->session()->pull('post_added');
    	}
        if ($request->session()->get('pay_added')){
            $data['success'] = $request->session()->pull('pay_added');
        }

        $data['count_created_posts'] = Post::noterm()->get()->count();

        $data['total'] = false;
        if (Auth::user()){
            if (Auth::user()->role_id != 1){
                $result = Post::where('paied',0)->where('strike', 0)->sum('cost');
                $data['total'] = $result;
            }
        }

        $data['min_pay'] = 0;
        $setting = Setting::where('key','min_pay')->first();
        if ($setting)
            $data['min_pay'] = $setting->value;

        return view('panel.post.list')->with('data',$data);
    }

    public function addForm(){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('posts'),
    		'title' => 'Посты'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('posts-add'),
    		'title' => 'Добавить'
    	];

    	$data['title'] = 'VK.POSTER - Добавить пост';
    	$data['h1'] = 'Добавить пост';
    	$data['post_id'] = false;
    	$data['group_id'] = false;
    	$data['description'] = false;
        $data['link'] = false;
        $data['attachments'] = [];

    	$data['groups'] = Group::all();

    	$data['action'] = route('posts-save');

        $data['slug'] = Auth::user()->role->slug;

    	return view('panel.post.add')->with('data',$data);
    }

    public function editForm(Request $request, $post_id){
    	$data = [];
    	$data['breadcrumbs'][] = [
    		'link' => route('posts'),
    		'title' => 'Посты'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('posts-add'),
    		'title' => 'Редактировать'
    	];
    	
    	if (!$post_id || !Post::find($post_id)){ 
    		$data['title'] = 'VK.POSTER - Добавить пост';
    		$data['h1'] = 'Добавить пост';
    		$post_info = array();

    	}
    	else{
    		$data['title'] = 'VK.POSTER - Редактировать пост';
    		$data['h1'] = 'Редактировать пост';
    		$post_info = Post::find($post_id);
    	}

    	if ($post_info){
    		$data['description'] = $post_info->description;
            $data['link'] = $post_info->link;
    		$data['group_id'] = $post_info->group_id;
    		$data['action'] = route('posts-esave');
    		$data['post_id'] = $post_id;

            $data['attachments'] = $post_info->attachments()->get();
    	}
    	else{
            $data['attachments'] = [];
    		$data['description'] = '';
            $data['link'] = false;
    		$data['group_id'] = '';
    		$data['action'] = route('posts-save');
    		$data['post_id'] = false;
    	}

        $data['slug'] = Auth::user()->role->slug;

    	$data['groups'] = Group::all();

        if (isset($request->back)){
            if ($request->back == 'pending')
                $request->session()->put('back', route('pendings'));
        }

    	return view('panel.post.add')->with('data',$data);
    }

    public function addPost(Request $request){
    	$post = new Post;
        //dd($request->attachments);
    	$post->description = $request->description;
        $post->link = $request->link;
    	$post->group_id = null;
    	$post->user_id = Auth::user()->id;
    	$post->status_id = 1;

	    $post->save();

        $post_id = $post->id;

        if ($request->attachments){
            foreach ($request->attachments as $key => $attachment) {
                $arr = explode(':::', $attachment);
                $attachment = $arr[0];
                $content = $arr[1];
                $type = $arr[2];
                $title = $arr[3];
                if ($type == 'video-local'){
                    $arr2 = explode('.', $title);
                    unset($arr2[count($arr2)-1]);
                    $title = implode('.', $arr2);
                }

                $post_attachment = new PostAttachment;
                $post_attachment->attachment = $arr[0];
                $post_attachment->content = $arr[1];
                $post_attachment->type = $arr[2];
                $post_attachment->sort_order = $key;
                $post_attachment->title = $title;
                $post_attachment->post_id = $post_id;
                $post_attachment->save();
            }
        }

	    $request->session()->put('post_added', 'Пост добавлен');

	    return redirect(route('posts'));
    }

    public function ajax(Request $request){
    	$users = [];
    	$results = Post::all();
    	//Auth::user()->role_id != 1
    	foreach ($results as $key => $result) {
            $strike = '';
            if (Auth::user()->role_id == 1){
                if ($result->strike == '1'){
                    $strike = '<a class="btn btn-sm btn-danger strike-popup-set" data-message="'.$result->strike_comment.'" data-id="'.$result->id.'"><i class="fa fa-exclamation"></i></a>';
                }
                else{
                    $strike = '<a class="btn btn-sm btn-primary strike-popup-set" data-message="" data-id="'.$result->id.'"><i class="fa fa-exclamation"></i></a>';
                }
            }
            else{
                if ($result->strike == '1'){
                    $strike = '<a class="btn btn-sm btn-danger strike-popup-info" data-message="'.$result->strike_comment.'"><i class="fa fa-exclamation"></i></a>';
                }
            }
           
   			$users[] = [
    			$result->id,
    			($result->group_id != '') ? '<a href="'.$result->group->url.'" target="_blank">'.$result->group->code.'</a>' : '',
    			$result->description,
    			 $result->status->name,
    			$result->user->name,
    			($result->publish_date != '') ? "<a href='".$result->vk_link."' target='blank'>".date('d.m.Y H:i', strtotime($result->publish_date)).'</a>' : '',
                ($result->viewed == '') ? 0 : $result->viewed,
                ($result->paied != 0 || $result->cost == 0) ? $result->cost.' р.' : '<span style="color:green">'.$result->cost.' р.</span>',
                $strike,
    			'<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>'.(($result->status_id == 1) ? '<a href="'.route('posts-edit',$result->id).'" class="on-default edit-row" style="margin-left: 10px"><i class="fa fa-pencil"></i></a>' : '')
    		];
    	}

    	echo json_encode(['aaData' => $users]);
    }

    public function drop(Request $request){
    	$id = $request->post('id');

    	Post::destroy($id);

    	return response('1')->header('Content-Type', 'text/html');
    }

    public function editPost(Request $request){
    	$post = Post::find($request->post_id);

    	$post->description = $request->description;
        $post->link = $request->link;

	    $post->save();

        DB::table('post_attachments')->where('post_id', '=', $request->post_id)->delete();

        if ($request->attachments){
            foreach ($request->attachments as $key => $attachment) {
                $arr = explode(':::', $attachment);
                $attachment = $arr[0];
                $content = $arr[1];
                $type = $arr[2];
                $title = $arr[3];

                $post_attachment = new PostAttachment;
                $post_attachment->attachment = $arr[0];
                $post_attachment->content = $arr[1];
                $post_attachment->type = $arr[2];
                $post_attachment->title = $title;
                $post_attachment->sort_order = $key;

                $post_attachment->post_id = $request->post_id;
                $post_attachment->save();
            }
        }

	    $request->session()->put('post_added', 'Пост изминен');

        if ($request->session()->get('back')){
            return redirect($request->session()->get('back'));
        }
        else{
            return redirect(route('posts'));
        }

	    
    }

    public function editPostAjax(Request $request){
        $post = Post::find($request->post_id);

        $post->description = $request->description;
        $post->link = $request->link;
        $post->group_id = $request->group_id;

        DB::table('post_attachments')->where('post_id', '=', $request->post_id)->delete();

        if ($request->attachments){
            foreach ($request->attachments as $key => $attachment) {
                $arr = explode(':::', $attachment);
                $attachment = $arr[0];
                $content = $arr[1];
                $type = $arr[2];

                $post_attachment = new PostAttachment;
                $post_attachment->attachment = $arr[0];
                $post_attachment->content = $arr[1];
                $post_attachment->type = $arr[2];
                $post_attachment->title = $arr[3];
                $post_attachment->sort_order = $key;
                $post_attachment->post_id = $request->post_id;
                $post_attachment->save();
            }
        }

        $post->save();

        return response()->json(['success' => true]);
    }

    public function payRequest(Request $request){
        if (Auth::user()){
            $user_id = Auth::user()->id;
                
            $result = Post::where('paied',0)->where('strike', 0)->sum('cost');
            $total = $result;
            if ($total != 0){
                $pay = new Pay;
                $pay->user_id = $user_id;
                $pay->total = $total;
                $pay->save();

                $pay_id = $pay->id;

                $posts = Post::where('paied',0)->where('cost','!=',0)->where('strike', 0)->get();
                foreach ($posts as $key => $post) {
                    $post->paied = 2;
                    $post->save();

                    $p2p = new PostToPay;
                    $p2p->pay_id = $pay_id;
                    $p2p->post_id = $post->id;
                    $p2p->save();
                }

                $request->session()->put('pay_added', 'Запрос на выплату отправлен');
            }
        }

        return redirect(route('posts'));
    }

    public function strike(Request $request){
        $message = $request->message;
        $id = $request->id;
        $strike = $request->strike;
        if ($strike == 'true'){
            $strike = '1';
        }
        else{
            $strike = '0';
        }

        $post = Post::find($id);
        $post->strike_comment = $message;
        $post->strike = $strike;
        $post->save();

        return response()->json(['success' => true]);
    }

    public function pendings(){
        $data = [];

        $data['breadcrumbs'][] = [
            'link' => route('pendings'),
            'title' => 'Отложенные посты'
        ];

        $data['groups'] = Group::all();

        return view('panel.post.pending')->with('data',$data);
    }

    public function pendingsAjax(Request $request){
        $posts = [];

        $results = Post::where('status_id', 2)->get();
        /*print_r('AAA2');*/
        foreach ($results as $key => $result) {
            $posts[] = [
                $result->id,
                ($result->group_id != '') ? '<a href="'.$result->group->url.'" target="_blank">'.$result->group->name.'</a>' : '',
                nl2br(mb_substr($result->description, 0, 100)) . '..',
                $result->user->name,
                date('d.m.Y H:i', strtotime($result->publish_date)),
                '<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>&nbsp;<a href="'.route('posts-edit',$result->id).'?back=pending" class="on-default edit-row" style="margin-left: 10px"><i class="fa fa-pencil"></i></a>'
            ];
        }
        /*print_r('AAA1');
        echo json_encode(['aaData' => $posts]);*/
        return response()->json(['aaData' => $posts]);

    }

    public function errors(){
        $data = [];

        $data['breadcrumbs'][] = [
            'link' => route('post-errors'),
            'title' => 'Ошибки публикации'
        ];

        $data['groups'] = Group::all();

        return view('panel.post.errors')->with('data',$data);
    }

    public function errorsAjax(Request $request){
        $posts = [];
        $results = Post::where('status_id', 4)->get();
       
        foreach ($results as $key => $result) {
            $posts[] = [
                $result->id,
                ($result->group_id != '') ? '<a href="'.$result->group->url.'" target="_blank">'.$result->group->name.'</a>' : '',
                nl2br(mb_substr($result->description, 0, 100)) . '..',
                $result->user->name,
                date('d.m.Y H:i', strtotime($result->publish_date)),  
                '<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>&nbsp;&nbsp;<a href="#" class="on-default to-post" title="Отправить на постинг"><i class="fa fa-paper-plane"></i></a>'
            ];
        }

        echo json_encode(['aaData' => $posts]);

    }

    public function toManage(Request $request){
        $post = Post::findOrFail($request->id);
        $post->group_id = null;
        $post->status_id = 1;
        $post->vk_id = null;
        $post->publish_date = null;
        $post->vk_link = null;
        $post->shedule_id = null;

        $post->save();

        return response()->json(['success' => true]);
    }
}
