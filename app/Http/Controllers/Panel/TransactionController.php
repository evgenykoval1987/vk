<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pay;
use App\Models\PostToPay;

class TransactionController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('transactions'),
    		'title' => 'Мои выплаты'
    	];

    	$pays = Pay::all();
    	$data['pays'] = [];
    	foreach ($pays as $key => $pay) {
    		$data['pays'][] = [
    			'dt' => date('d.m.Y H:i', strtotime($pay->created_at)),
    			'total' => $pay->total,
    			'status' => $pay->status,
    			'count' => count($pay->posts)
    		];
    	}

        return view('panel.transaction.list')->with($data);
    }
}
