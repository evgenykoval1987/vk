<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\Sentence;
use App\Models\Pay;
use App\Models\Post;
use App\Models\Answer;
use App\Models\Monitoring;
use App\Models\CompanyPostEnd;
use App\Models\CompanyPost;
use App\Models\Company;
use App\Models\ClientLast;
use App\Models\Client;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\Chat;
use App\Models\Reminder;

class MessageController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('messages'),
    		'title' => 'Сообщения'
    	];

    	$data['success'] = false;
    	$data['error'] = false;

    	if ($request->session()->get('message_send')){
    		$data['success'] = $request->session()->pull('message_send');
    	}

    	if ($request->session()->get('message_nosend')){
    		$data['error'] = $request->session()->pull('message_nosend');
    	}

        return view('panel.message.list')->with($data);
    }

    public function ajax(Request $request){
    	$messages = [];
    	$results = Message::where('deleted','0')->get();

    	foreach ($results as $key => $result) {
    		$messages[] = [
    			$result->id,
    			$result->group->code,
    			'<a href="https://vk.com/id'.$result->user_id.'" target="_blank">'.$result->firstname.' '.$result->lastname.'</a>',
    			$result->text,
    			date('d.m.Y H:i:s', strtotime($result->vk_dt)),
    			'<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>&nbsp;&nbsp;&nbsp;<a href="'.route('messages-edit',$result->id).'" class="on-default"><i class="fa fa-envelope"></i></a>',
                $result->status,
    			($result->answer == '') ? 0 : 1,
    		];
    	}
        $update = ['status' => 0];
        DB::table('messages')->update($update);

    	echo json_encode(['aaData' => $messages]);
    }

    public function drop(Request $request){
    	$id = $request->post('id');

    	/*Message::destroy($id);*/
        $message = Message::find($id);
        $message->deleted = 1;
        $message->save();

    	return response('1')->header('Content-Type', 'text/html');
    }

    public function dropAll(){
    	//Message::truncate();
        $update = ['deleted' => 1];
        DB::table('messages')->update($update);

    	return redirect()->to(route('messages'));
    }

    public function answer(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('messages'),
    		'title' => 'Сообщения'
    	];

    	$data['message_id'] = $request->message_id;

    	$message_info = Message::find($request->message_id);
    	$data['text'] = $message_info->text;

    	$group_id = $message_info->group->idd;
    	$group = $message_info->group->name;
    	$user_id = $message_info->user_id;
    	$token = $message_info->group->user->vk_token;

    	$message_token = $message_info->group->token_for_messages;

		$user_name = $message_info->firstname;
		$user_lastname = $message_info->lastname;

        
		$messages_getHistory = json_decode($this->makeRequest("https://api.vk.com/method/messages.getHistory?user_id=$user_id&access_token=$message_token&group_id=$group_id&v=5.81"));

		$data['messages'] = [];

		foreach($messages_getHistory->response->items as $item) {
			if($item->out==1) {
				$user_m = $group;
			} else {
				$user_m = $user_name.' '.$user_lastname;		
			}
            
			$data['messages'][] = [
				'user_id' => $user_id,
				'name' => $user_m,
				'group_id' => $group_id,
				'dt' => date("d.m.Y H:i",$item->date),
				'text' => $item->text
			];
		}

        $data['answers'] = Answer::all();

    	return view('panel.message.answer')->with($data);
    }

    public function sendAnswer(Request $request){
    	$message_info = Message::find($request->message_id);
    	
    	$group_id = $message_info->group->idd;
    	$group = $message_info->group->name;
    	$user_id = $message_info->user_id;
    	$message_token = $message_info->group->token_for_messages;

    	$mess = $request->answer;


		$post = array( 
			'message' => $mess, 
			'user_id' => $user_id, 
			'access_token' => $message_token, 
			'v' => '5.131' 
		); 

		$gets = http_build_query($post); 
	
		$res = $this->makeRequestGet('https://api.vk.com/method/messages.send?'.$gets);
        /*echo 'https://api.vk.com/method/messages.send?'.$gets;
        dd($res);*/
        $res = json_decode($res);

        if (!$res){
            $request->session()->put('message_nosend', 'Ошибка! Сообщение не отправлено.');
                
            return redirect(route('messages'));
        }
        else{
            if ($res->response > 0) {
                $request->session()->put('message_send', 'Сообщение удачно отправлено');

                $message_info->answer = $mess;
                $message_info->save();
                
                return redirect(route('messages'));             
            } else {
                $request->session()->put('message_nosend', 'Ошибка! Сообщение не отправлено.'.$res->error->error_msg);
                
                return redirect(route('messages'));
            }
        }
		
    	
    }

    public function counters(){ //die();
        $new_messages = Message::where('status',1)->get()->count();
        $new_sentences = Sentence::where('status',1)->get()->count();
        $new_pays = Pay::where('status',1)->get()->count();
        $new_monitorings = Monitoring::where('readed',0)->where('status',1)->get()->count();
        $company_ends = CompanyPost::whereIn('id', function($query){
            $query->select('company_post_id')
            ->from(with(new CompanyPostEnd)->getTable())->where('dt', '<=', Carbon::now()->subDays(2));
        })->get()->count();
        //dd(CompanyPostEnd::where('dt', '<=', Carbon::now()->subDays(1))->get());
        $post_errors = Post::where('status_id',4)->get()->count();
        $chats = Chat::where('status',1)->where('user_to', Auth::user()->id)->get()->count();
        $reminders = Reminder::where('status',0)->where('dt','<=',now())->get()->count();
        $reports = Company::where('report',1)->where('status',0)->where('readed', 0)->get()->count();

        $days = Setting::where('key','client_last_days')->first()->value;
        $days = explode("\r\n", $days);

        $clients = Client::all();
        foreach ($clients as $key => $client) {
            $last_order_dt = $client->company->first();
            if (!$last_order_dt) continue;
            $last_order_dt = $last_order_dt->created_at;
            if ($last_order_dt == '') continue;
            $last_in_days = 0;
            $now = new \DateTime();
            $date = \DateTime::createFromFormat("Y-m-d H:i:s", $last_order_dt);
            $interval = $now->diff($date);
            $last_in_days = $interval->days;

            foreach ($days as $k => $day) {
                if ($last_in_days < $day) 
                    continue;
                else{
                    $client_last = ClientLast::where('client_id', $client->id)->where('days',$day)->first();
                    if (!$client_last){
                        $cl = new ClientLast();
                        $cl->client_id = $client->id;
                        $cl->days = $day;
                        $cl->last_dt = $last_order_dt;
                        $cl->save();
                    }
                }
            }
        }

        $client_last = ClientLast::where('status','0')->get()->count();

        return response()->json(['messages' => $new_messages, 'sentences' => $new_sentences, 'pays' => $new_pays, 'monitorings' => $new_monitorings, 'company_ends' => $company_ends, 'client_last' => $client_last, 'post_errors' => $post_errors, 'chats' => $chats, 'reminder' => $reminders, 'reports' => $reports]);
    }

    public function makeRequest($url, $data = []){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data; charset=UTF-8'));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $server_output = curl_exec($ch);
        curl_close ($ch);
        
        return $server_output;
    }

     public function makeRequestGet($url, $data = []){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $server_output = curl_exec($ch);
        curl_close ($ch);
        
        return $server_output;
    }
}
