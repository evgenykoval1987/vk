<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Client;
use App\Models\Manager;
use App\Models\Setting;
use App\Models\ClientLast;
use App\Models\Group;
use App\User;
use Illuminate\Support\Facades\Hash;

class ClientController extends Controller
{
    public function index(Request $request){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('client'),
    		'title' => 'Клиенты'
    	];

    	$data['success'] = false;
    	if ($request->session()->get('client_added')){
    		$data['success'] = $request->session()->pull('client_added');
    	}

        /*$data['count_created_posts'] = Post::noterm()->get()->count();*/

        return view('panel.client.list')->with('data',$data);
    }

    public function addForm(){
        header('Access-Control-Allow-Origin: *');
        
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('client'),
    		'title' => 'Клиенты'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('client-add'),
    		'title' => 'Добавить'
    	];

    	$data['vk_profile'] = "";
		$data['name'] = "";
		$data['group_name'] = "";
		$data['group_link'] = "";
        $data['email'] = "";
        $data['manager_id'] = 0;
		$data['action'] = route('client-save');
		$data['client_id'] = false;
        $data['system_id'] = false;
        $data['system_info'] = false;
        $data['comments'] = "";

        $data['managers'] = Manager::all();

    	return view('panel.client.add')->with('data',$data);
    }

    public function editForm(Request $request, $client_id){
    	$data = [];
    	$data['breadcrumbs'][] = [
    		'link' => route('client'),
    		'title' => 'Клиенты'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('client-edit', $client_id),
    		'title' => 'Редактировать'
    	];
    	
    	if ($client_id && Client::find($client_id)){ 
    		$client_info = Client::find($client_id);
    	}

    	if ($client_info){
    		$data['vk_profile'] = $client_info->vk_profile;
    		$data['name'] = $client_info->name;
    		$data['group_name'] = $client_info->group_name;
    		$data['group_link'] = $client_info->group_link;
    		$data['action'] = route('client-esave');
    		$data['client_id'] = $client_id;
            $data['email'] = $client_info->email;
            $data['manager_id'] = $client_info->manager_id;
            $data['system_id'] = $client_info->system_id;
            $data['comments'] = $client_info->comments;

            if ($client_info->system_id)
                $data['system_info'] = User::find($client_info->system_id);
            else
                $data['system_info'] = false;

            $access = false;
            if ($client_info->system_id){
                if ($data['system_info']->vk_id){
                    $datan = [
                        'user_id' => $data['system_info']->vk_id,
                        'access_token' => $data['system_info']->vk_token,
                        'filter' => 'admin, editor',
                        'extended' => 1,
                        'count' => 100,
                        'v' => '5.101',
                    ];

                    $url = "https://api.vk.com/method/groups.get?".http_build_query($datan);
                    $response = $this->makeRequest($url);
                    
                    if (isset($response->response->items)){
                        $access = true;
                    }
                }
            }

            $data['access'] = $access;
    	}

        $data['managers'] = Manager::all();

    	return view('panel.client.add')->with('data',$data);
    }

    public function addClient(Request $request){
    	$client = new Client;

	    $client->name = $request->name;
	    $client->vk_profile = $request->client_profile;
	    $client->group_link = $request->group_link;
	    $client->group_name = $request->group_name;
        $client->manager_id = $request->manager_id;
        $client->email = $request->email;
        $client->comments = $request->comments;
	    $client->user_id = Auth::user()->id;

	    $client->save();

	    $request->session()->put('client_added', 'Клиент <strong>'.$request->name.'</strong> добавлен');

	    return redirect(route('client'));
    }

    public function ajax(Request $request){
    	$clients = [];
    	$results = Client::all();
    	
    	foreach ($results as $key => $result) {
            $summary = 0;
            $aov = 0;
            $alt = 0;

            $now = new \DateTime();
            if (isset($result->company->last()->created_at)){
                $date = \DateTime::createFromFormat("Y-m-d H:i:s", $result->company->last()->created_at);
                $interval = $now->diff($date); 
                $months_count = $interval->m;
                if ($months_count == 0)
                    $months_count = 1;
                $summary = $result->company->count()/$months_count;
                $summary = number_format($summary,1,'.','');
            }
            $company_count = $result->company->count();
            if ($company_count == 0)
                $company_count = 1;
            $aov = number_format(($result->company->sum('total')/$company_count),1,'.','');

            $first_month = false;
            $first_year = false;
            $companies = $result->companyr;
            foreach ($companies as $key => $company) {
                $month = date('m', strtotime($company->created_at));
                if (!$first_month){
                    $first_month = $month;
                    $first_year = date('Y', strtotime($company->created_at));
                }
                $this_year =  date('Y', strtotime($company->created_at));
                $this_month =  date('m', strtotime($company->created_at));
                $alt += ($this_month - $first_month) + 1;
                
                if ($first_year != $this_year){
                    $alt += (($this_year - $first_year) * 12) + 1;
                }
            }

            if ($result->companyr->count() != 0){
                $alt = $alt / $result->companyr->count();
                $alt = number_format($alt,2,'.','');
            }

    		$clients[] = [
    			$result->id,
    			'<a href="'.$result->vk_profile.'" target="_blank">'.$result->name.'</a>',
    			'<a href="'.$result->group_link.'" target="_blank">'.$result->group_name.'</a>',
                $result->company->sum('total'),
                ($result->manager_id != 0) ? $result->manager->name : '',
    			date('d.m.Y', strtotime($result->created_at)),
                $summary,
                $aov,
                $alt,
                $summary*$aov*$alt,
    			'<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>&nbsp;<a href="'.route('client-edit',$result->id).'" class="on-default edit-row" style="margin-left: 10px"><i class="fa fa-pencil"></i></a>'
    		];
    	}

    	echo json_encode(['aaData' => $clients]);
    }

    public function dropClient(Request $request){
    	$id = $request->post('id');

    	Client::destroy($id);

    	return response('1')->header('Content-Type', 'text/html');
    }

    public function editClient(Request $request){
    	$client = Client::find($request->client_id);

    	$client->vk_profile = $request->client_profile;
    	$client->name = $request->name;
    	$client->group_name = $request->group_name;
    	$client->group_link = $request->group_link;
        $client->manager_id = $request->manager_id;
        $client->comments = $request->comments;
        $client->email = $request->email;

	    $client->save();

	    $request->session()->put('client_added', 'Клиент изминен');

	    return redirect(route('client'));
    }

    public function last(Request $request){
        $data = [];

        $data['breadcrumbs'][] = [
            'link' => route('client-last'),
            'title' => 'Отписать клиенту'
        ];

        $data['success'] = false;
        if ($request->session()->get('setting_added')){
            $data['success'] = $request->session()->pull('setting_added');
        }

        $data['client_last_days'] = Setting::where('key','client_last_days')->first()->value;

        return view('panel.client.last')->with($data);
    }

    public function lastEdit(Request $request){
        foreach ($request->config as $key => $value){
            $setting = Setting::where('key',$key)->first();
            if ($setting){
                $setting->key = $key;
                $setting->value = $value;
                $setting->save();
            }
            else{
                $setting = new Setting;
                $setting->key = $key;
                $setting->value = $value;
                $setting->save();
            }
        }

        $request->session()->put('setting_added', 'Настройки для клиентов изменены');

        return redirect(route('client-last'));
    }

    public function lastAjax(){
        $clients = [];
        $results = ClientLast::all();
        
        foreach ($results as $key => $result) {
            if (!$result->client) continue;
            $clients[] = [
                $result->id,
                '<a href="'.$result->client->vk_profile.'" taget="_blank">'.$result->client->name.'</a>',
                $result->client->company->first()->total,
                date('d.m.Y', strtotime($result->client->company->first()->created_at)),
                (isset($result->client->manager->name)) ? $result->client->manager->name : '',
                $result->days,
                ($result->status == 0) ? '<a class="btn btn-danger btn-sm remove-last" href="'.route('client-last-remove-one',$result->id).'"><i class="fa fa-pencil"></i>&nbsp;Отписать</a>' : '<a class="btn btn-success btn-sm">Отписано</a>'
            ];
        }

        echo json_encode(['aaData' => $clients]);
    }

    public function lastDropAll(){
        $last = ClientLast::all();
        foreach ($last as $key => $item) {
            $item->status = 1;
            $item->save();
        }
    
        return redirect()->to(route('client-last'));
    }

    public function lastDropOne(Request $request, $id){
        $last = ClientLast::find($id);
        $last->status = 1;
        $last->save();

        echo 1;
    }

    public function unloadClients(){
        header('Content-type: text/plain');
        header("Cache-control: private");
        header("Content-type: application/force-download");
        header("Content-transfer-encoding: binary\n");
        header("Content-disposition: attachment; filename=clients.txt");
        //header("Content-Length: ".filesize($filepath));
        //readfile($filepath);
        

        $clients = Client::all();
        foreach ($clients as $key => $client) {
            echo $client->vk_profile."\r\n";
        }

        exit;
    }

    public function unloadGroups(){
        header('Content-type: text/plain');
        header("Cache-control: private");
        header("Content-type: application/force-download");
        header("Content-transfer-encoding: binary\n");
        header("Content-disposition: attachment; filename=groups.txt");
        //header("Content-Length: ".filesize($filepath));
        //readfile($filepath);
        

        $clients = Client::all();
        foreach ($clients as $key => $client) {
            if ($client->group_link == '') continue;
            echo $client->group_link."\r\n";
        }

        exit;
    }

    public function addClientUser(Request $request){
        $client_id = $request->client_id;
        $client_info = Client::find($client_id);

        if ($client_info->system_id){
            $user = User::find($client_info->system_id);
            $user->password = Hash::make($request->password);
            $user->passwordd = $request->password;
            $user->save();
        }
        else{
            $user = new User;

            $user->name = $client_info->name;
            $user->login = $request->login;
            $user->password = Hash::make($request->password);
            $user->passwordd = $request->password;
            $user->role_id = 3;
            $user->vk_id = '';
            $user->wallet = '';

            $user->save();

            $client_info->system_id = $user->id;
            $client_info->save();
        }

        $request->session()->put('client_added', 'Клиент изминен');
        return redirect(route('client'));
    }

    public function makeRequest($url, $data = []){
        $ch = curl_init();
        //echo $url; dd($data);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data; charset=UTF-8'));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,10); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $server_output = curl_exec($ch);
        /*$info = curl_getinfo($ch);
        print_r($info);*/
        curl_close ($ch);
        
        return json_decode($server_output);
    }
}
