<?php
namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Reminder;
use App\Models\Client;
use App\Models\Manager;

class ReminderController extends Controller
{
	public function index(Request $request)
    {
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('reminder'),
    		'title' => 'Напоминания'
    	];

    	$data['success'] = false;
    	if ($request->session()->get('reminder_added')){
    		$data['success'] = $request->session()->pull('reminder_added');
    	}

    	$data['reminders'] = Reminder::where('status',0)->where('dt','<=',now())->get();

        return view('panel.reminder.list')->with('data',$data);
    }

    public function ajax(Request $request){
    	$reminders = [];
    	$results = Reminder::all();
    	
    	foreach ($results as $key => $result) {
    		$reminders[] = [
    			$result->id,
    			$result->text,
    			($result->client) ? $result->client->name : '',
                ($result->manager) ? $result->manager->name : '',
    			date('d.m.Y', strtotime($result->dt)),
    			($result->status == 0) ? 'Новый' : 'Прочтено',
    			'<a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>'
    			/*'<a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a>'*/
    		];
    	}

    	echo json_encode(['aaData' => $reminders]);
    }

    public function addForm(){
    	$data = [];

    	$data['breadcrumbs'][] = [
    		'link' => route('reminder'),
    		'title' => 'Напоминания'
    	];

    	$data['breadcrumbs'][] = [
    		'link' => route('reminder-add'),
    		'title' => 'Добавить'
    	];

    	$data['clients'] = Client::all();
        $data['managers'] = Manager::all();

    	return view('panel.reminder.add')->with('data',$data);
    }

    public function drop(Request $request){
    	$id = $request->post('id');

    	Reminder::destroy($id);

    	return response('1')->header('Content-Type', 'text/html');
    }

    public function add(Request $request){
    	$reminder = new Reminder();

	    $reminder->text = $request->text;
	    $reminder->client_id = $request->client_id;
        $reminder->manager_id = $request->manager_id;
	    $reminder->dt = $request->dt;

	    $reminder->save();

	    $request->session()->put('reminder_added', 'Напоминание добавлено');

	    return redirect(route('reminder'));
    }

    public function close(Request $request){
    	$id = $request->post('id');

    	$rem = Reminder::find($id);
    	$rem->status = 1;
    	$rem->save();
    	
    	return response('1')->header('Content-Type', 'text/html');
    }
}