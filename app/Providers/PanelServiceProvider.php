<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Reminder;
use App\Models\Chat;

class PanelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Auth $auth, Request $request)
    {    
         
            view()->composer('*', function($view) use ($auth, $request) {

                $reminders = Reminder::where('status',0)->where('dt','<=',now())->get()->count();

                $menu = [];

                if (Auth::user()){
                    $menu[] = [
                        'link' => route('home'),
                        'title' => 'Главная',
                        'icon'  => 'fa fa-home',
                        'children' => [],
                        'active' => ($request->url() == route('home')) ? true : false
                    ];
                    if (Auth::user()->role->slug == 'client') {
                        $menu[] = [
                            'link' => route('client-companies'),
                            'title' => 'Мои рекламные кампании',
                            'icon'  => 'fa fa-file-text-o',
                            'children' => [],
                            'active' => ($request->url() == route('client-companies')) ? true : false
                        ];
                    }
                    if (Auth::user()->role->slug == 'admin') {
                        $menu[] = [
                            'link' => route('users'),
                            'title' => 'Пользователи',
                            'icon'  => 'fa fa-user',
                            'children' => [],
                            'active' => ($request->url() == route('users')) ? true : false
                        ];
                        $menu[] = [
                            'link' => '#',
                            'title' => 'Группы',
                            'icon'  => 'fa fa-database',
                            'active' => ($request->url() == route('groups') || $request->url() == route('packages') || $request->url() == route('shedule')) ? true : false,
                            'children' => [
                                [
                                    'link' => route('groups'),
                                    'title' => 'Группы',
                                    'active' => ($request->url() == route('groups')) ? true : false
                                ],
                                [
                                    'link' => route('packages'),
                                    'title' => 'Пакеты групп',
                                    'active' => ($request->url() == route('packages')) ? true : false
                                ],
                                [
                                    'link' => route('shedule'),
                                    'title' => 'Рассписание',
                                    'active' => ($request->url() == route('shedule')) ? true : false
                               ]
                            ]
                            
                        ];
                        $menu[] = [
                            'link' => '#',
                            'title' => 'Публикация контента',
                            'icon'  => 'fa fa-list',
                            'counter' => 'post_errors',
                            'count' => '',
                            'active' => ($request->url() == route('manage') || $request->url() == route('posts') || $request->url() == route('posts-add') || $request->url() == route('pendings') || $request->url() == route('post-errors') || $request->url() == route('video')) ? true : false,
                            'children' => [
                                [
                                    'link' => route('manage'),
                                    'title' => 'Постинг',
                                    'active' => ($request->url() == route('manage')) ? true : false
                                ],
                                [
                                    'link' => route('posts-add'),
                                    'title' => 'Добавить пост',
                                    'active' => ($request->url() == route('posts-add')) ? true : false
                                ],
                                [
                                    'link' => route('posts'),
                                    'title' => 'Список постов',
                                    'active' => ($request->url() == route('posts')) ? true : false
                                ],
                                [
                                    'link' => route('pendings'),
                                    'title' => 'Отложенные записи',
                                    'active' => ($request->url() == route('pendings')) ? true : false
                                ],
                                [
                                    'link' => route('post-errors'),
                                    'title' => 'Ошибки постинга',
                                    'counter' => 'post_errors_detail',
                                    'count' => '',
                                    'active' => ($request->url() == route('post-errors')) ? true : false
                                ],
                                [
                                    'link' => route('video'),
                                    'title' => 'Видеоинструкции',
                                    'active' => ($request->url() == route('video')) ? true : false
                                ]
                            ]
                            
                        ];

                        $menu[] = [
                            'link' => '#',
                            'title' => 'Клиенты',
                            /*'counter' => 'clients',
                            'count' => 0,*/
                            'icon'  => 'fa fa-users',
                            'active' => ($request->url() == route('client') || $request->url() == route('manager')) ? true : false,
                            'children' => [
                                [
                                    'link' => route('client'),
                                    'title' => 'Клиенты',
                                    'active' => ($request->url() == route('client')) ? true : false
                                ],
                                [
                                    'link' => route('manager'),
                                    'title' => 'Менеджеры',
                                    'active' => ($request->url() == route('manager')) ? true : false
                                ]
                            ]
                            
                        ];

                        $menu[] = [
                            'link' => '#',
                            'title' => 'Рекламные кампании',
                            'icon'  => 'fa fa-file-text-o',
                            'active' => ($request->url() == route('companys') || $request->url() == route('company-add')) ? true : false,
                            'children' => [
                                [
                                    'link' => route('company-active'),
                                    'title' => 'Активные рекл. кампании',
                                    'active' => ($request->url() == route('company-active')) ? true : false
                                ],
                                [
                                    'link' => route('companys'),
                                    'title' => 'Рекламные кампании',
                                    'active' => ($request->url() == route('companys')) ? true : false
                                ],
                                [
                                    'link' => route('company-add'),
                                    'title' => 'Добавить',
                                    'active' => ($request->url() == route('company-add')) ? true : false
                                ],
                                [
                                    'link' => route('company-pin'),
                                    'title' => 'Закрепленная запись',
                                    'active' => ($request->url() == route('company-pin')) ? true : false
                                ],
                                [
                                    'link' => route('company-timable'),
                                    'title' => 'Расписание рекламы',
                                    'active' => ($request->url() == route('company-timable')) ? true : false
                                ],
                                [
                                    'link' => route('tag'),
                                    'title' => 'Метки',
                                    'active' => ($request->url() == route('tag')) ? true : false
                                ],
                                [
                                    'link' => route('tagpack'),
                                    'title' => 'Рекламные пакеты меток',
                                    'active' => ($request->url() == route('tagpack')) ? true : false
                                ],
                                [
                                    'link' => route('taggroup'),
                                    'title' => 'Группы меток',
                                    'active' => ($request->url() == route('taggroup')) ? true : false
                                ]
                            ]
                        ];

                        $menu[] = [
                            'link' => '#',
                            'title' => 'Лидогенерация',
                            'counter' => 'ludo',
                            'count' => '',
                            'icon'  => 'fa fa-bell',
                            'active' => ($request->url() == route('reminder') || $request->url() == route('companys-end') || $request->url() == route('messages') || $request->url() == route('sentences') || $request->url() == route('monitoring') || $request->url() == route('client-last') || $request->url() == route('company-report')) ? true : false,
                            'children' => [
                                [
                                    'link' => route('reminder'),
                                    'title' => 'Напоминания',
                                    'counter' => 'reminder',
                                    'count' => '',
                                    'active' => ($request->url() == route('reminder')) ? true : false
                                ],
                                [
                                    'link' => route('companys-end'),
                                    'title' => 'Промежуточний отчет',//'Отчеты по рекламной кампании',
                                    'counter' => 'company_ends',
                                    'count' => '',
                                    'active' => ($request->url() == route('companys-end')) ? true : false
                                ],
                                [
                                    'link' => route('messages'),
                                    'title' => 'Сообщения',
                                    'counter' => 'messages',
                                    'count' => '',
                                    'active' => ($request->url() == route('messages') || $request->url() == route('answer')) ? true : false
                                ],
                                [
                                    'link' => route('sentences'),
                                    'title' => 'Предложка',
                                    'counter' => 'sentences',
                                    'count' => '',
                                    'active' => ($request->url() == route('sentences')) ? true : false
                                ],
                                [
                                    'link' => route('monitoring'),
                                    'title' => 'Мониторинг',
                                    'counter' => 'monitorings',
                                    'count' => '',
                                    'active' => ($request->url() == route('monitoring')) ? true : false
                                ],
                                [
                                    'link' => route('client-last'),
                                    'title' => 'Отписать клиенту',
                                    'counter' => 'clients',
                                    'count' => 0,
                                    'active' => ($request->url() == route('client-last')) ? true : false
                                ],
                                [
                                    'link' => route('company-report'),
                                    'title' => 'Итоговый отчет',
                                    'counter' => 'reports',
                                    'count' => 0,
                                    'active' => ($request->url() == route('company-report')) ? true : false
                                ]
                            ]
                        ];

                        $menu[] = [
                            'link' => route('statistic'),
                            'title' => 'Статистика',     
                            'icon'  => 'fa fa-bar-chart-o',   
                            'children' => [],                            
                            'active' => ($request->url() == route('statistic')) ? true : false
                        ];
                        
                        $menu[] = [
                            'link' => '#',
                            'title' => 'Выплаты',
                            'icon'  => 'fa fa-money',
                            'active' => ($request->url() == route('pays') || $request->url() == route('pays-table') || $request->url() == route('pays-price')) ? true : false,
                            'counter' => 'pays',
                            'children' => [
                                [
                                    'link' => route('pays'),
                                    'title' => 'Заявки',
                                    'counter' => 'pays_detail',
                                    'count' => '',
                                    'active' => ($request->url() == route('pays')) ? true : false
                                ],
                                [
                                    'link' => route('pays-table'),
                                    'title' => 'Таблица расчетов',
                                    'active' => ($request->url() == route('pays-table')) ? true : false
                                ]
                                ,
                                [
                                    'link' => route('pays-price'),
                                    'title' => 'Цены',
                                    'active' => ($request->url() == route('pays-price')) ? true : false
                                ]
                            ]
                        ];
                        $menu[] = [
                            'link' => '#',
                            'title' => 'Поддержка',
                            'icon'  => 'fa fa-comment',
                            'counter' => 'chats',
                            'count' => 0,
                            'active' => ($request->url() == route('news') || $request->url() == route('chat')) ? true : false,
                            'children' => [
                                [
                                    'link' => route('news'),
                                    'title' => 'Новости',
                                    'active' => ($request->url() == route('news')) ? true : false
                                ],
                                [
                                    'link' => route('chat'),
                                    'title' => 'Переписка',
                                    'counter' => 'chats_detail',
                                    'count' => 0,
                                    'active' => ($request->url() == route('chat')) ? true : false
                                ]
                            ]
                            
                        ];
                        $menu[] = [
                            'link' => route('analytic'),
                            'title' => 'Анализ рекламных записей',
                            'icon'  => 'fa fa-bar-chart-o',
                            'count' => 0,
                            'active' => ($request->url() == route('analytic')) ? true : false,
                            'children' => []
                        ];

                        
                    }
                    if (Auth::user()->role->slug == 'editor') {
                        $menu[] = [
                            'link' => '#',
                            'title' => 'Посты',
                            'icon'  => 'fa fa-list',
                            'active' => ($request->url() == route('posts') || $request->url() == route('posts-add')) ? true : false,
                            'children' => [
                                [
                                    'link' => route('posts'),
                                    'title' => 'Список постов',
                                    'active' => ($request->url() == route('posts')) ? true : false
                                ],
                                [
                                    'link' => route('posts-add'),
                                    'title' => 'Добавить пост',
                                    'active' => ($request->url() == route('posts-add')) ? true : false
                                ]
                            ]
                            
                        ];
                        $menu[] = [
                            'link' => route('transactions'),
                            'title' => 'Мои выплаты',
                            'icon'  => 'fa fa-money',
                            'children' => [],
                            'active' => ($request->url() == route('transactions')) ? true : false
                        ];
                        $menu[] = [
                            'link' => route('rating'),
                            'title' => 'Статистика',
                            'icon'  => 'fa fa-star',
                            'children' => [],
                            'active' => ($request->url() == route('rating')) ? true : false
                        ];
                        $menu[] = [
                            'link' => route('chat'),
                            'title' => 'Переписка',
                            'icon'  => 'fa fa-comment',
                            'counter' => 'chats',
                            'count' => '',
                            'children' => [],
                            'active' => ($request->url() == route('chat')) ? true : false
                        ];
                        
                    }
                }
                $view->with('menu', $menu);

                $vk_token = env('VK_APP');

                $view->with('app_id', $vk_token);

                $access = true;
                if (Auth::user()) {
                    if (Auth::user()->role_id == '1') {
                        if (Auth::user()->vk_audio_token == ''){
                            $access = 'admin-audio';
                        }
                        else{
                            /*$owner_id = Auth::user()->vk_id;
                            $datan = array(
                                'v' => '5.93',
                                'https' => 1,
                                'ref' => 'search',
                                'extended' => 1,
                                'offset' => 0,
                                'count' => 10,
                                'owner_id' => $owner_id,
                                'device_id' => Auth::user()->vk_audio_device_id,
                                'lang' => 'en',
                                'access_token' => Auth::user()->vk_audio_token
                            );

                            $datan['sig'] = md5("/method/audio.get?".http_build_query($datan).Auth::user()->vk_audio_secret);
                            
                            $ch = curl_init();
                            curl_setopt($ch,CURLOPT_HTTPHEADER, array('User-Agent: VKAndroidApp/5.23-2978 (Android 4.4.2; SDK 19; x86; unknown Android SDK built for x86; en; 320x240)'));
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_POST, 1);

                            curl_setopt(
                                $ch,
                                CURLOPT_URL,
                                "https://api.vk.com/method/audio.get"
                            );

                            curl_setopt($ch,
                                CURLOPT_POSTFIELDS,
                                $datan
                            );

                            $response = json_decode(curl_exec($ch)); //dd($response);
                            if (!isset($response->response->items)){
                                $access = 'admin-audio';
                            }*/
                        }

                        if (Auth::user()->vk_token != ''){
                            //$access = 'admin-check';
                            if (Auth::user()->vk_audio_token == ''){
                                $access = 'admin-audio';
                            }
                        }
                        else{
                            $access = 'admin-new';
                        }
                    }
                    else{
                        if (Auth::user()->vk_token != ''){
                            $access = 'editor-check';
                        }
                        else{
                            $access = 'editor-new';
                        }

                        if (Auth::user()->vk_audio_token == ''){
                            $access = 'admin-audio';
                        }

                    }
                }
                //echo $access;die();
                $view->with('access', $access);

                if (Auth::user()) {
                    $type_of_token = 'admin';
                    if (Auth::user()->role_id == 3)
                        $type_of_token = 'client';

                    $view->with('type_of_token', $type_of_token);
                }
            });
       
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
