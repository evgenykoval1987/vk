$.fn.vkeditor = function() {
	var self_ = this;
    var $container = $(this);
    var idd = $container.attr('id');
    this.droparea = false;
    this.start = 0;
	this.limit = 20;
	this.selected = 0;

    var init = function(){
 
    	$('[ajax-modal]').magnificPopup({
			type: 'ajax',
			modal: true,
			callbacks: {
    			ajaxContentAdded: function() {
    				Dropzone.autoDiscover = false;
					this.droparea = new Dropzone("#vk-picture-local-area", {
			            url: "/vkattach/picture/files",
			            maxFileSize: 50,
			            paramName: 'file',
			            addRemoveLinks: true,
			            acceptedFiles: ".jpeg,.jpg,.png,.gif",
			            init: function(){
			            	$(this.element).addClass("dropzone");
			            },
			            sending: function(file, xhr, formData) { 
						    formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
						},
			        });
			        this.droparea.on('success', function(e, response){
			        	
			        	$("#vk-picture-container").append('<input type="hidden" name="attachments[]" value="'+e.name+'" data-attachment-id="0" data-image="'+response+'" data-type="image-local" title="'+e.name+'">');
			        	self_.update_popup_count();

			        })
			        this.droparea.on('removedfile', function(e,a){
			        	name = e.name;
			        	path = false;
			        	$("#vk-picture-container input").each(function(){
			        		if ($(this).val() == name){
			        			$(this).remove();
			        			path = $(this).data('image');
			        		}
			        	})
			        	self_.update_popup_count();
			        	if (path){
			        		self_.remove_file(path);
			        	}
			        })
    			}
    		}
		});

		$(document).on('click', '.modal-close', function( e ) {
			e.preventDefault();
			$.magnificPopup.close();
			self_.start = 0;
			self_.selected = 0;
		});

		$("body").on('change', "#dialog #group-id", function(){ 
			if ($(this).val() != ''){
				$("#album-id").load($(this).val(), function(){
					$("#album-id").prop('disabled',false);
				});
				self_.start = 0;
				self_.selected = 0;
				self_.vk_find_images();
			}
			else
				$("#album-id").prop('disabled',true);

		})

		$(document).on('change', "#dialog #album-id", function(){
			self_.start = 0;
			self_.selected = 0;
			self_.vk_find_images();
		});

		$(document).on('click','#dialog #vk-next-page', function(){
			self_.vk_find_images(true);
		});

		$(document).on('click','#dialog #vk-prev-page', function(){
			self_.start-=limit;
			self_.vk_find_images();
		})

		$(document).on('click','.vk-image', function(){
			$input = $(this).find('input');
			$img = $input.parent().find('img').attr('src');
			title = $input.parent().find('a').text();
			if ($input.prop('checked') == true)
				$input.prop('checked', false);
			else
				$input.prop('checked', true);

			if ($input.prop('checked') == true){
				$(this).addClass('vk-active');
				$("#vk-picture-container").append('<input type="hidden" name="attachments[]" value="'+$input.val()+'" data-attachment-id="'+$input.val()+'" data-image="'+$img+'" data-type="image-vk" title="'+title+'">');
			}
			else{
				$(this).removeClass('vk-active');
				$("[data-attachment-id="+$input.val()+"]").remove();
			}

			/*self_.selected = $("#vk-picture-container input").length;
			$("#vk-picture-total").text(self_.selected);*/
			self_.update_popup_count();
		})

		$(document).on('click','#vk-add-photo', function(){
			$("#url-picture-container .picture-url-item").each(function(){
				$("#vk-picture-container").append('<input type="hidden" name="attachments[]" value="'+$(this).val()+'" data-attachment-id="0" data-image="'+$(this).val()+'" data-type="image-url" title="Изображение">');
			});


			$("#vk-picture-container input").each(function(){
				if($("#attachments input").length > 9){
					alert('Достигнуто макс число прикрепленных записей к посту');
					return;
				}
				title = $(this).attr('title');
				var image_container = '<div class="image">';
				image_container += '<a href="#" class="remove-item btn btn-danger"><i class="fa fa-trash-o"></i></a>';
				image_container += '<a href="'+$(this).data('image')+'" target="_blank"><img src="'+$(this).data('image')+'">'+title+'<input type="hidden" name="attachments[]" value="'+$(this).data('attachment-id')+':::'+$(this).data('image')+':::'+$(this).data('type')+':::'+title+'"></a>';
				image_container += '</div>';
				$("#attachments").append(image_container);
			})
			$.magnificPopup.close();   
		})
		
		$(document).on('click', '#attachments .image .remove-item', function(){
			val = $(this).parent().find('input').val();
			arr = val.split(':::');
			if (arr[0] == 0){
				self_.remove_file(arr[1]);
			}
			$(this).parent().remove();
		})

		/************ VIDEO */
		$('[vajax-modal]').magnificPopup({
			type: 'ajax',
			modal: true,
			callbacks: {
    			ajaxContentAdded: function() {
    				Dropzone.autoDiscover = false;
					this.droparea = new Dropzone("#vk-video-local-area", {
			            url: "/vkattach/video/files",
			            maxFileSize: 1000,
			            paramName: 'file',
			            addRemoveLinks: true,
			            acceptedFiles: ".avi, .mp4, .3gp, .mpeg, .mov, .mp3, .flv, .wmv",
			            init: function(){
			            	$(this.element).addClass("dropzone");
			            },
			            sending: function(file, xhr, formData) { 
						    formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
						},
			        });
			        this.droparea.on('success', function(e, response){
			        	
			        	$("#vk-video-container").append('<input type="hidden" name="attachments[]" value="'+e.name+'" data-attachment-id="0" data-image="'+response+'" data-type="video-local" title="'+e.name+'">');
			        	self_.update_popup_count_video();

			        })
			        this.droparea.on('removedfile', function(e,a){
			        	name = e.name;
			        	path = false;
			        	$("#vk-video-container input").each(function(){
			        		if ($(this).val() == name){
			        			$(this).remove();
			        			path = $(this).data('image');
			        		}
			        	})
			        	self_.update_popup_count_video();
			        	if (path){
			        		self_.remove_file(path);
			        	}
			        })
    			}
    		}
		});

		$("body").on('change', "#vdialog #group-id", function(){ 
			if ($(this).val() != ''){
				$("#album-id").load($(this).val(), function(){
					$("#album-id").prop('disabled',false);
				});
				self_.start = 0;
				self_.selected = 0;
				self_.vk_find_videos();
			}
			else
				$("#album-id").prop('disabled',true);

		})

		$(document).on('change', "#vdialog #album-id", function(){
			self_.start = 0;
			self_.selected = 0;
			self_.vk_find_videos();
		});

		$(document).on('click','.vk-video', function(){
			$input = $(this).find('input');
			$img = $input.parent().find('img').attr('src');
			title = $input.parent().find('a').text();
			if ($input.prop('checked') == true)
				$input.prop('checked', false);
			else
				$input.prop('checked', true);

			if ($input.prop('checked') == true){
				$(this).addClass('vk-active');
				$("#vk-video-container").append('<input type="hidden" name="attachments[]" value="'+$input.val()+'" data-attachment-id="'+$input.val()+'" data-image="'+$img+'" data-type="video-vk" title="'+title+'">');
			}
			else{
				$(this).removeClass('vk-active');
				$("[data-attachment-id="+$input.val()+"]").remove();
			}

			self_.update_popup_count_video();
		})

		$(document).on('click','#vk-add-video', function(){

			$("#youtube-container .youtube-item").each(function(){
				//text = $(this.parent().parent().parent().find('.youtube-item-text').val());
				$("#vk-video-container").append('<input type="hidden" name="attachments[]" value="'+$(this).val()+'" data-attachment-id="0" data-image="'+$(this).val()+'" data-type="video-youtube" title="Youtube">');
			})
			
			$("#vk-video-container input").each(function(){
				if($("#attachments input").length > 9){
					alert('Достигнуто макс число прикрепленных записей к посту');
					return;
				}
				title = $(this).attr('title');
				var video_container = '<div class="image">';
				video_container += '<a href="#" class="remove-item btn btn-danger"><i class="fa fa-trash-o"></i></a>';
				if ($(this).data('type') == 'video-youtube' || $(this).data('type') == 'video-local')
					video_container += '<a href="'+$(this).data('image')+'" target="_blank"><img src="/images/you.png">'+title+'<input type="hidden" name="attachments[]" value="'+$(this).data('attachment-id')+':::'+$(this).data('image')+':::'+$(this).data('type')+':::'+title+'"></a>';
				else
					video_container += '<a href="'+$(this).data('image')+'" target="_blank"><img src="'+$(this).data('image')+'">'+title+'<input type="hidden" name="attachments[]" value="'+$(this).data('attachment-id')+':::'+$(this).data('image')+':::'+$(this).data('type')+':::'+title+'"></a>';
				video_container += '</div>';
				$("#attachments").append(video_container);
			})
			$.magnificPopup.close();   
		})

		$(document).on('click','#vdialog #vk-next-page', function(){
			self_.vk_find_videos(true);
		});

		$(document).on('click','#vdialog #vk-prev-page', function(){
			self_.start-=limit;
			self_.vk_find_videos();
		})

		$(document).on('click','#picture-url-add-item', function(){
			var html = '<div class="form-group">';
				/*html += '<label class="col-md-3 control-label">Название</label>';
				html += '<div class="input-group mb-md col-md-9">';
					html += '<input type="text" class="form-control youtube-item-text">';
					html += '<span class="input-group-btn">';
						
					html += '</span>';
				html += '</div>';*/
				html += '<label class="col-md-3 control-label">Ссылка</label>';
				html += '<div class="input-group mb-md col-md-9">';
					html += '<input type="text" class="form-control picture-url-item">';
					html += '<span class="input-group-btn">';
						html += '<button class="btn btn-danger picture-url-remove-item" type="button"><i class="fa fa-trash-o"></i></button>';
					html += '</span>';
				html += '</div>';
			html += '</div>';
			$("#url-picture-container").append(html);
		})

		$(document).on('click','.picture-url-remove-item', function(){
			$(this).parent().parent().parent().remove();
		})

		$(document).on('click','#video-youtube-add-item', function(){
			var html = '<div class="form-group">';
				/*html += '<label class="col-md-3 control-label">Название</label>';
				html += '<div class="input-group mb-md col-md-9">';
					html += '<input type="text" class="form-control youtube-item-text">';
					html += '<span class="input-group-btn">';
						
					html += '</span>';
				html += '</div>';*/
				html += '<label class="col-md-3 control-label">Ссылка</label>';
				html += '<div class="input-group mb-md col-md-9">';
					html += '<input type="text" class="form-control youtube-item">';
					html += '<span class="input-group-btn">';
						html += '<button class="btn btn-danger video-youtube-remove-item" type="button"><i class="fa fa-trash-o"></i></button>';
					html += '</span>';
				html += '</div>';
			html += '</div>';
			$("#youtube-container").append(html);
		})

		$(document).on('click','.video-youtube-remove-item', function(){
			$(this).parent().parent().parent().remove();
		})

		$('[aajax-modal]').magnificPopup({
			type: 'ajax',
			modal: true,
			callbacks: {
    			ajaxContentAdded: function() {
    				Dropzone.autoDiscover = false;
					this.droparea = new Dropzone("#vk-audio-local-area", {
			            url: "/vkattach/audio/files",
			            maxFileSize: 50,
			            paramName: 'file',
			            addRemoveLinks: true,
			            acceptedFiles: ".mp3",
			            init: function(){
			            	$(this.element).addClass("dropzone");
			            },
			            sending: function(file, xhr, formData) { 
						    formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
						},
			        });
			        this.droparea.on('success', function(e, response){
			        	$("#vk-audio-container").append('<input type="hidden" name="attachments[]" value="'+e.name+'" data-attachment-id="0" data-image="'+response+'" data-type="audio-local" title="'+e.name+'">');
			        	self_.update_popup_count();

			        })
			        this.droparea.on('removedfile', function(e,a){
			        	/*name = e.name;
			        	path = false;
			        	$("#vk-picture-container input").each(function(){
			        		if ($(this).val() == name){
			        			$(this).remove();
			        			path = $(this).data('image');
			        		}
			        	})
			        	self_.update_popup_count();
			        	if (path){
			        		self_.remove_file(path);
			        	}*/
			        })
    			}
    		}
		});

		/** AUDIO SEARCH */
		$("body").on('click', "#vk-audio-search-run", function(){ 
			if ($("#vk-audio-search").val() != ''){
				self_.start = 0;
				self_.selected = 0;
				self_.vk_find_audios_search();
			}
			else
				$("#album-id").prop('disabled',true);
		})

		$("body").on('change', "#adialog #group-id", function(){ 
			if ($(this).val() != ''){
				$("#album-id").load($(this).val(), function(){
					$("#album-id").prop('disabled',false);
				});
				self_.start = 0;
				self_.selected = 0;
				self_.vk_find_audios();
			}
			else
				$("#album-id").prop('disabled',true);
		})

		$(document).on('click','.vk-audio', function(){
			$input = $(this).find('input');
			$img = $input.parent().find('img').attr('src');
			title = $input.parent().find('a').text();
			if ($input.prop('checked') == true)
				$input.prop('checked', false);
			else
				$input.prop('checked', true);

			if ($input.prop('checked') == true){
				$(this).addClass('vk-active');
				$("#vk-audio-container").append('<input type="hidden" name="attachments[]" value="'+$input.val()+'" data-attachment-id="'+$input.val()+'" data-image="'+$img+'" data-type="audio-vk" title="'+title+'">');
			}
			else{
				$(this).removeClass('vk-active');
				$("[data-attachment-id="+$input.val()+"]").remove();
			}

			self_.update_popup_count_audio();
		})

		$(document).on('click','#vk-add-audio', function(){
			$.each($('#manual-audios').val().split(/\n/), function(i, line){
		        if(line && $("#attachments input").length <= 9){
		            var audio_container = '<div class="image image-audio">';
					audio_container += '<a href="#" class="remove-item btn btn-danger"><i class="fa fa-trash-o"></i></a>';
					audio_container += '<a href="#">'+line+'<input type="hidden" name="attachments[]" value="'+line+':::null:::audio-vk:::'+line+'"></a>';
					audio_container += '</div>';
		        	$("#attachments").append(audio_container);
		        }
		    });

			$("#vk-audio-container input").each(function(){
				if($("#attachments input").length > 9){
					alert('Достигнуто макс число прикрепленных записей к посту');
					return;
				}
				title = $(this).attr('title');
				var audio_container = '<div class="image image-audio">';
				audio_container += '<a href="#" class="remove-item btn btn-danger"><i class="fa fa-trash-o"></i></a>';
				audio_container += '<a href="'+$(this).data('image')+'" target="_blank">'+title+'<input type="hidden" name="attachments[]" value="'+$(this).data('attachment-id')+':::'+$(this).data('image')+':::'+$(this).data('type')+':::'+title+'"></a>';
				audio_container += '</div>';
				$("#attachments").append(audio_container);
			})
			$.magnificPopup.close();   
		})

		$(document).on('click','#adialog #vk-next-page', function(){
			self_.vk_find_audios(true);
		});

		$(document).on('click','#adialog #vk-prev-page', function(){
			self_.start-=limit;
			self_.vk_find_audios();
		})

		$(document).on('click','#search #vk-next-page', function(){
			self_.vk_find_audios_search(true);
		});

		$(document).on('click','#search #vk-prev-page', function(){
			self_.start-=limit;
			self_.vk_find_audios_search();
		})

		$('[lajax-modal]').magnificPopup({
			type: 'ajax',
			modal: true,
			callbacks: {
    			
    		}
		});

		$(document).on("click","#vk-add-playlist", function(){
			var value = $("#playlist-input").val();

			var playlist_container = '<div class="image image-audio">';
			playlist_container += '<a href="#" class="remove-item btn btn-danger"><i class="fa fa-trash-o"></i></a>';
			playlist_container += '<a href="'+value+'" target="_blank">'+value+'<input type="hidden" name="attachments[]" value="0:::'+value+':::playlist-vk:::'+value+'"></a>';
			playlist_container += '</div>';
			$("#attachments").append(playlist_container);

			$.magnificPopup.close();
		})

		/** DOC */

		$('[dajax-modal]').magnificPopup({
			type: 'ajax',
			modal: true,
			callbacks: {
    			
    		}
		});

		$("body").on('change', "#ddialog #group-id", function(){ 
			if ($(this).val() != ''){
				/*$("#album-id").load($(this).val(), function(){
					$("#album-id").prop('disabled',false);
				});*/
				self_.start = 0;
				self_.selected = 0;
				self_.vk_find_docs();
			}
			else
				$("#album-id").prop('disabled',true);
		})

		$(document).on('click','.vk-doc', function(){
			$input = $(this).find('input');
			$img = $input.parent().find('img').attr('src');
			title = $input.parent().find('a').text();
			if ($input.prop('checked') == true)
				$input.prop('checked', false);
			else
				$input.prop('checked', true);

			if ($input.prop('checked') == true){
				$(this).addClass('vk-active');
				$("#vk-doc-container").append('<input type="hidden" name="attachments[]" value="'+$input.val()+'" data-attachment-id="'+$input.val()+'" data-image="'+$img+'" data-type="doc-vk" title="'+title+'">');
			}
			else{
				$(this).removeClass('vk-active');
				$("[data-attachment-id="+$input.val()+"]").remove();
			}

			self_.update_popup_count_doc();
		})

		$(document).on('click','#vk-add-doc', function(){
			$("#vk-doc-container input").each(function(){
				if($("#attachments input").length > 9){
					alert('Достигнуто макс число прикрепленных записей к посту');
					return;
				}
				title = $(this).attr('title');
				var doc_container = '<div class="image">';
				doc_container += '<a href="#" class="remove-item btn btn-danger"><i class="fa fa-trash-o"></i></a>';
				doc_container += '<a href="'+$(this).data('image')+'" target="_blank"><img src="'+$(this).data('image')+'">'+title+'<input type="hidden" name="attachments[]" value="'+$(this).data('attachment-id')+':::'+$(this).data('image')+':::'+$(this).data('type')+':::'+title+'"></a>';
				doc_container += '</div>';
				$("#attachments").append(doc_container);
			})
			$.magnificPopup.close();   
		})

		$(document).on('click','#ddialog #vk-next-page', function(){
			self_.vk_find_docs(true);
		});

		$(document).on('click','#ddialog #vk-prev-page', function(){
			self_.start-=limit;
			self_.vk_find_docs();
		})
    }

    this.update_popup_count = function(){
    	self_.selected = $("#vk-picture-container input").length;
		$("#vk-picture-total").text(self_.selected);
    }

    this.update_popup_count_video = function(){
    	self_.selected = $("#vk-video-container input").length;
		$("#vk-video-total").text(self_.selected);
    }

    this.update_popup_count_doc = function(){
    	self_.selected = $("#vk-doc-container input").length;
		$("#vk-doc-total").text(self_.selected);
    }

    this.update_popup_count_audio = function(){
    	self_.selected = $("#vk-audio-container input").length;
		$("#vk-audio-total").text(self_.selected);
    }

    this.remove_file = function(path){
    	var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	path: path
		}
		$.ajax({
			type: "POST",
			url: "/vkattach/picture/drop-file",
			data: data,
			dataType: "html",
			success: function (html) {
				
			},
			error: function(xhr, ajaxOptions, thrownError) {
	           
	        }
		})
    }

    this.vk_find_images = function(increment=false){
 
		if (increment)
			self_.start+=self_.limit;
		var group_id = $("#group-id").val();
		var album_id = $("#album-id").val();

		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	group_id: group_id,
          	album_id: album_id,
          	offset: self_.start,
          	limit: self_.limit
		}

		$.ajax({
			type: "GET",
			url: "/vkattach/picture/images",
			data: data,
			dataType: "html",
			success: function (html) {
				$(".vk-picture-area").html(html);
				$("#vk-picture-container input").each(function(){
					id = $(this).data('attachment-id');
					$(".vk-picture-area input").each(function(){
						if ($(this).val() == id){ 
							$(this).prop('checked',true);
							$(this).closest('.vk-image').addClass('vk-active');
						}
					})
				})
			},
			error: function(xhr, ajaxOptions, thrownError) {
	           
	        }
		})
	}

	this.vk_find_videos = function(increment=false){
 
		if (increment)
			self_.start+=self_.limit;
		var group_id = $("#group-id").val();
		var album_id = $("#album-id").val();

		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	group_id: group_id,
          	album_id: album_id,
          	offset: self_.start,
          	limit: self_.limit
		}

		$.ajax({
			type: "GET",
			url: "/vkattach/video/videos",
			data: data,
			dataType: "html",
			success: function (html) {
				$(".vk-video-area").html(html);
				$("#vk-video-container input").each(function(){
					id = $(this).data('attachment-id');
					$(".vk-video-area input").each(function(){
						if ($(this).val() == id){ 
							$(this).prop('checked',true);
							$(this).closest('.vk-video').addClass('vk-active');
						}
					})
				})
			},
			error: function(xhr, ajaxOptions, thrownError) {
	           
	        }
		})
	}

	this.vk_find_audios_search = function(increment=false){
		var q = $("#vk-audio-search").val();

		if (increment)
			self_.start+=self_.limit;

		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	q: q,
          	offset: self_.start,
          	limit: self_.limit
		}

		$.ajax({
			type: "GET",
			url: "/vkattach/audio/search",
			data: data,
			dataType: "html",
			success: function (html) {
				$("#audio_audios_s .vk-audio-area").html(html);
				$("#vk-audio-container-s input").each(function(){
					id = $(this).data('attachment-id');
					$("#audio_audios_s .vk-audio-area input").each(function(){
						if ($(this).val() == id){ 
							$(this).prop('checked',true);
							$(this).closest('.vk-image').addClass('vk-active');
						}
					})
				})
			},
			error: function(xhr, ajaxOptions, thrownError) {
	           
	        }
		})
	}
	

	this.vk_find_audios = function(increment=false){
 
		if (increment)
			self_.start+=self_.limit;
		var group_id = $("#group-id").val();
		var album_id = $("#album-id").val();

		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	group_id: group_id,
          	album_id: album_id,
          	offset: self_.start,
          	limit: self_.limit
		}

		$.ajax({
			type: "GET",
			url: "/vkattach/audio/audios",
			data: data,
			dataType: "html",
			success: function (html) {
				$("#audio_audios .vk-audio-area").html(html);
				$("#vk-audio-container input").each(function(){
					id = $(this).data('attachment-id');
					$("#audio_audios .vk-audio-area input").each(function(){
						if ($(this).val() == id){ 
							$(this).prop('checked',true);
							$(this).closest('.vk-image').addClass('vk-active');
						}
					})
				})
			},
			error: function(xhr, ajaxOptions, thrownError) {
	           
	        }
		})
	}

	this.vk_find_docs = function(increment=false){
 
		if (increment)
			self_.start+=self_.limit;
		var group_id = $("#group-id").val();
		var album_id = $("#album-id").val();

		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	group_id: group_id,
          	album_id: album_id,
          	offset: self_.start,
          	limit: self_.limit
		}

		$.ajax({
			type: "GET",
			url: "/vkattach/doc/docs",
			data: data,
			dataType: "html",
			success: function (html) {
				$(".vk-doc-area").html(html);
				$("#vk-doc-container input").each(function(){
					id = $(this).data('attachment-id');
					$(".vk-doc-area input").each(function(){
						if ($(this).val() == id){ 
							$(this).prop('checked',true);
							$(this).closest('.vk-doc').addClass('vk-active');
						}
					})
				})
			},
			error: function(xhr, ajaxOptions, thrownError) {
	           
	        }
		})
	}

    init();
    return this;
}