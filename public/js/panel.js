var vk_container = {
	publish: function(post_id, btn){
		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	post_id: post_id
		}
		$.ajax({
			type: "POST",
			url: "/vk/publish",
			data: data,
			dataType: "json",
			success: function (json) {
				btn.button('reset');
				if (json.success == true){
					btn.replaceWith('<div class="alert alert-success"><strong>Готово!</strong> Запись опубликована. </div>');
					setTimeout(function(){
						document.location.reload();
					}, 1000)
				}
				else{
					btn.before('<div class="alert alert-danger"><strong>Ошибка!</strong> Запись не опубликована. </div>');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
	            btn.button('reset');
				btn.before('<div class="alert alert-danger"><strong>Ошибка!</strong> Запись не опубликована. </div>');
	        }
		})
	},
	publish_vk_later: function(post_id, dt, time, novk,  btn){
		if (novk === undefined)
			novk = 0;
		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	post_id: post_id,
          	dt: dt,
          	time: time,
          	novk: novk
		}
		$.ajax({
			type: "POST",
			url: "/vk/publishVkTurn",
			data: data,
			dataType: "json",
			success: function (json) {
				btn.button('reset');
				if (json.success == true){
					btn.replaceWith('<div class="alert alert-success"><strong>Готово!</strong> Запись добавлена в очередь. </div>');
					setTimeout(function(){
						document.location.reload();
					}, 1000)
				}
				else{
					btn.before('<div class="alert alert-danger"><strong>Ошибка!</strong> Запись не добавлена в очередь. </div>');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {

	            btn.button('reset');
				btn.before('<div class="alert alert-danger"><strong>Ошибка!</strong> Запись не добавлена в очередь. </div>');
	        }
		})
	},
	publish_later: function(post_id, novk, btn, user_shedule){
		if (novk === undefined)
			novk = 0;
		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	post_id: post_id,
          	novk: novk,
          	user_shedule: user_shedule
		}

		$.ajax({
			type: "POST",
			url: "/vk/publishTurn",
			data: data,
			dataType: "json",
			success: function (json) {
				btn.button('reset');
				if (json.success == true){
					btn.replaceWith('<div class="alert alert-success"><strong>Готово!</strong> Запись добавлена в очередь. </div>');
					setTimeout(function(){
						document.location.reload();
					}, 1000)
				}
				else{
					btn.before('<div class="alert alert-danger"><strong>Ошибка!</strong> Запись не добавлена в очередь. </div>');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	            btn.button('reset');
				btn.before('<div class="alert alert-danger"><strong>Ошибка!</strong> Запись не добавлена в очередь. </div>');
	        }
		})
	}
}
$(function(){
	$('[data-plugin-datepicker]').datepicker('setDate', 'now');
	$('[data-plugin-datepickerr]').datepicker({
		format: "yyyy-mm-dd", 
		language: "ru", 
		autoclose:"true", 
		locale: "ru"
	});

	$("#logout").click(function(post_id, btn){
		$("#logout-form").submit();
		return false;
	})

	$('.token-button').magnificPopup({
		type: 'inline',
		preloader: false,
		modal: true
	});
	/*$('.audio-token-button').magnificPopup({
		type: 'inline',
		preloader: false,
		modal: true
	});*/

	$(document).on('click', '.modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	if (access == 'admin-new'){ 
		$('.token-button').trigger('click');
	}
	if (access == 'admin-audio'){
		$(".audio-token-button").trigger('click');
	}

	$("#refresh-audio-token").click(function(){
		$.magnificPopup.open({
			items: {
				src: '#modalAudioToken',
				type: 'inline'
			}
		});

		return false;
	})

	$("#check-audio-token").click(function(){
		$("#audio-success .alert").hide();
		var data = {
			_token: function() {
	        	return $('meta[name="csrf-token"]').attr('content');
	      	},
		}

		$.ajax({
			type: "GET",
			url: "/vk/check-audio-token",
			data: data,
			dataType: "html",
			success: function (html) {
				if (html == 'true'){
					$("#audio-success-text").show();
				}
				else{
					$("#audio-error-text").show();
				}

				$.magnificPopup.open({
					items: {
						src: '#audio-success',
						type: 'inline'
					}
				});
			},
			error: function(xhr, ajaxOptions, thrownError) {
				
	        }
		})

		return false;
	})

	$("#refresh-token").click(function(){
		$('.token-button').trigger('click');

		return false;
	})

	$("#check-token").click(function(){
		$("#token-success .alert").hide();
		var data = {
			_token: function() {
	        	return $('meta[name="csrf-token"]').attr('content');
	      	},
		}

		$.ajax({
			type: "GET",
			url: "/vk/check-token",
			data: data,
			dataType: "html",
			success: function (html) {
				if (html == 'true'){
					$("#token-success-text").show();
				}
				else{
					$("#token-error-text").show();
				}

				$.magnificPopup.open({
					items: {
						src: '#token-success',
						type: 'inline'
					}
				});
			},
			error: function(xhr, ajaxOptions, thrownError) {
				
	        }
		})

		return false;
	})

	$("#refresh-stat").click(function(){
		$("#refresh-stat").button('loading');
		$.ajax({
			type: "GET",
			url: "/vk/refresh-stat",
			data: data,
			dataType: "html",
			success: function (html) {
				$("#refresh-stat").button('reset');
				document.location.reload();
			},
			error: function(xhr, ajaxOptions, thrownError) {
				
	        }
		})

		return false;
	})

	$(".panel-actions .close").click(function(){
		$.magnificPopup.close();
	})

	$("#get-admin-token").click(function(){
		window.open("https://oauth.vk.com/authorize?client_id="+app_id+"&display=popup&redirect_uri=https://oauth.vk.com/blank.html&scope=groups,photos,video,audio,stories,wall,offline,email,docs,stats&response_type=token&v=5.92&revoke=1", 'vk_auth', {});
		
		return false;
	})

	$("#get-client-token").click(function(){
		window.open("https://oauth.vk.com/authorize?client_id="+app_id+"&display=popup&redirect_uri=https://oauth.vk.com/blank.html&scope=groups,offline,email,stats,stories&response_type=token&v=5.92&revoke=1", 'vk_auth', {});
		
		return false;
	})

	$("#admin-token-form").validate({
		highlight: function(label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function(label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function( error, element ) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
		    user_token: {
		      	required: true
		    }
  		},
	  	messages: {
	  		user_token: {
		      	required: "Поле обязательное для заполнения"
		    }
	  	},
	  	submitHandler: function(form) {
		    $(form).ajaxSubmit({
		    	success: function(response){
		    		$(".alert-danger").remove();
		    		if (response.success){
		    			$("#admin-token-form").before('<div class="alert alert-success"><strong>Выполнено!</strong> Токен успешно получен.</div>');
		    			$('.audio-token-button').trigger('click');
		    			setTimeout(function(){
		    				//document.location.reload();
		    				$.magnificPopup.close();
		    			}, 2000)
		    		}
		    		else{
		    			$("#admin-token-form").before('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Ошибка!</strong> Ошибка получения токена.</div>');
		    		}
		    	},
		    	error: function(response){
		    		
		    	}
		    });
		}

	});

	$("#admin-audio-token-form").validate({
		highlight: function(label) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function(label) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function( error, element ) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
	  	submitHandler: function(form) {
		    $(form).ajaxSubmit({
		    	success: function(response){
		    		$(".alert-danger").remove();
		    		if (response.success){
		    			document.location.reload();
		    		}
		    		else{
		    			$("#admin--audio-token-form").before('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Ошибка!</strong> Ошибка получения токена.</div>');
		    		}
		    	},
		    	error: function(response){
		    		$("#admin--audio-token-form").before('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Ошибка!</strong> Ошибка получения токена.</div>');
		    	}
		    });
		}

	});
	updateCounters(false);
	setInterval(function(){
		updateCounters();
	}, 15000);
})

var soundID = "Thunder";
createjs.Sound.registerSound("js/KirbyStyleLaser.ogg", soundID);

function updateCounters(sound=false){
	var data = {
		_token: function() {
        	return $('meta[name="csrf-token"]').attr('content');
      	},
	}
	$.ajax({
		type: "GET",
		url: "/messages/get-count",
		data: data,
		dataType: "json",
		success: function (json) {
			var ludo = false;

			if (json.messages != 0){
				if ($("#counter-messages").text() != json.messages && sound)
					createjs.Sound.play(soundID);
				$("#counter-messages").text(json.messages);	
				ludo = true;
			}
			else{
				$("#counter-messages").text('');
			}
			
			if (json.sentences != 0){
				if ($("#counter-sentences").text() != json.sentences && sound)
					createjs.Sound.play(soundID);
				$("#counter-sentences").text(json.sentences);
				ludo = true;
			}
			else{
				$("#counter-sentences").text('');
			}
			
			if (json.pays != 0){
				if ($("#counter-pays").text() != json.pays && sound)
					createjs.Sound.play(soundID);
				$("#counter-pays").text(json.pays);
				$("#counter-pays_detail").text(json.pays);
			}
			else{
				$("#counter-pays").text('');
				$("#counter-pays_detail").text('');
			}

			if (json.monitorings != 0){
				if ($("#counter-monitorings").text() != json.monitorings && sound)
					createjs.Sound.play(soundID);
				$("#counter-monitorings").text(json.monitorings);
				ludo = true;
			}
			else{
				$("#counter-monitorings").text('');
			}

			if (json.company_ends != 0){
				if ($("#counter-company_ends").text() != json.company_ends && sound)
					createjs.Sound.play(soundID);

				$("#counter-company_ends").text(json.company_ends);
				ludo = true;
			}
			else{
				$("#counter-company_ends").text('');
			}

			if (json.client_last != 0){
				if ($("#counter-clients").text() != json.client_last && sound)
					createjs.Sound.play(soundID);

				$("#counter-clients").text(json.client_last);
				ludo = true;
			}
			else{
				$("#counter-clients").text('');
			}

			if (json.reminder != 0){
				if ($("#counter-reminder").text() != json.reminder && sound)
					createjs.Sound.play(soundID);

				$("#counter-reminder").text(json.reminder);
				ludo = true;
			}
			else{
				$("#counter-reminder").text('');
			}

			if (json.reports != 0){
				if ($("#counter-reports").text() != json.reports && sound)
					createjs.Sound.play(soundID);

				$("#counter-reports").text(json.reports);
				ludo = true;
			}
			else{
				$("#counter-reports").text('');
			}

			if (json.post_errors != 0){
				if ($("#counter-post_errors").text() != json.post_errors && sound)
					createjs.Sound.play(soundID);

				$("#counter-post_errors").text(json.post_errors);
				$("#counter-post_errors_detail").text(json.post_errors);
			}
			else{
				$("#counter-post_errors").text('');
				$("#counter-post_errors_detail").text('');
			}

			if (json.chats != 0){
				if ($("#counter-chats").text() != json.chats && sound)
					createjs.Sound.play(soundID);

				$("#counter-chats").text(json.chats);
				$("#counter-chats_detail").text(json.chats);
			}
			else{
				$("#counter-chats").text('');
				$("#counter-chats_detail").text('');
			}

			if (ludo){
				$("#counter-ludo").html("<i class='fa fa-bell' style='font-size: 10px; margin-right:0px'></i>");
			}
			else{
				$("#counter-ludo").text('');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			
        }
	})
}