(function($) {
	'use strict';

	var $submit = $("#refresh");
	var $form = $("#refresh-form");

	var init = function(){
		refresh();
	}

	var refresh = function(){
		$.ajax({
			url: $form.attr('action'),
			type: 'post',
			data: $("#refresh-form input, #refresh-form input[name=\'_token\']"),
			dataType: 'json',
			success: function(json) {
				for (var item in json.data){ 
					$("#"+item).text(json.data[item]);
				}
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	}

	$submit.click(function(){
		refresh();
	})

	$(function() {
		init(); 
	});

}).apply( this, [ jQuery ]);