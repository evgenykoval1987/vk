(function( $ ) {
	'use strict';

	var $table = $('#messages-list');
	var dt = false;

	var datatableInit = function() {
		dt = $table.DataTable( {
			bProcessing: true,
		    ajax: $table.data('url'),
		    ordering: false,
		    createdRow: function( row, data, dataIndex){
                if (data[7] == 0){
                    $(row).addClass('greenClass');
                }
                else{

                }
            },
            language: {
            	"processing": "Подождите...",
				"search": "",
				"lengthMenu": "Показать _MENU_ записей",
				"info": "Записи с _START_ до _END_ из _TOTAL_ записей",
				"infoEmpty": "Записи с 0 до 0 из 0 записей",
				"infoFiltered": "(отфильтровано из _MAX_ записей)",
				"infoPostFix": "",
				"loadingRecords": "Загрузка записей...",
				"zeroRecords": "Записи отсутствуют.",
				"emptyTable": "В таблице отсутствуют данные",
				"paginate": {
					"first": "Первая",
					"previous": "Предыдущая",
					"next": "Следующая",
					"last": "Последняя"
				},
				"aria": {
					"sortAscending": ": активировать для сортировки столбца по возрастанию",
					"sortDescending": ": активировать для сортировки столбца по убыванию"
				}
            },
            "columnDefs": [
	            {
	                "targets": [0],
	                "visible": false
	            },
	            {
	                "targets": [6],
	                "visible": false
	            },
	            {
	                "targets": [7],
	                "visible": false
	            },
	            {	
	            	"targets": [5],
	            	"orderable": false,
	            	"className": "text-center",
	            }
	        ]
		});
	};

	$table.on('click', 'a.remove-row', function(e) { 
		e.preventDefault();

		var $row = $(this).closest('tr');
		var id = dt.row($(this).closest('tr')).data()["0"];

		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	id: id
		}

		$.ajax({
			url: '/messages/drop',
			type: 'post',
			data: data,
			dataType: 'json',
			success: function(json) {
				dt.ajax.reload();
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	});

	$("#refresh").click(function(){
		dt.ajax.reload();
		updateCounters();
	})


	$(function() { 
		datatableInit();
	});

}).apply( this, [ jQuery ]);