(function($) {
	'use strict';

	var $submit = $("#run-token");
	var $group = $("#vk-group");
	var $token = $("#enter-token");
	var flag = true;

	var getgroup = function(group){
		return VK.Api.call('groups.getById', {group_id: group, v:"5.73"}, function(r) {   
		  	if(r.response) {
		  		$("#group-name").val(r.response[0].name);
		  		$("#group-thumb").val(r.response[0].photo_200);
		  		$("#group-token").val('');
		  		$("#group-id").val(r.response[0].id);
		  		$("#group-code").val(r.response[0].screen_name);
		  		$(".panel-footer button").prop('disabled', false);
		  	}
		  	else{
		  		err();
		  	}

			return flag;
		})
	}

	var getmembers = function(group){
		return VK.Api.call('groups.getMembers', {group_id: group.replace("club", ""), v:"5.73"}, function(r) {
		  	if(r.response) {
		    	$("#group-users").val(r.response.count);
		    	$(".panel-footer button").prop('disabled', false);
		  	}
		  	else{
		  		err();
		  	}

			return flag;
		})

	}

	/*var getposts = function(group){
		return VK.Api.call('wall.get', {owner_id: '-'+group.replace("club", ""), filter: 'postponed', access_token: $token.val(), v:"5.73"}, function(r) {
		  	if(r.response) {
		  		$("#group-count-post").val(r.response.count);
		  		if (r.response.count != 0){
		  			var dt = r.response.items[r.response.items.length-1].date;
		  			$("#group-last-post-dt").val(dt);
		  		}
		  	}

			return flag;
		})
	}*/

	var err = function(){
		if (flag){
			$('.form-horizontal .panel-body').prepend('<div class="alert alert-danger"><i class="fa fa-check-circle"></i> Группа <span>'+$group.val()+'</span> не найдена.</div>');
			$(".panel-footer button").prop('disabled', true);
			flag = false;
		}
	}

	var init = function(){
		var info = explode('/', $group.val());
		var group = info[info.length-1];
		group = group.replace('public', '');
		
		$(".alert-danger").remove();
		flag = true;
		getgroup(group),
		getmembers(group),
		/*getposts(group)*/

		$submit.button('reset');
	}

	$(function() {
		
		
		$submit.click(function(e) { 
			e.preventDefault();
			$submit.button('loading');
			init();
			/*$.magnificPopup.open({
				items: {
					src: '#dialog',
					type: 'inline'
				},
				preloader: false,
				modal: true,
				callbacks: {
					open: function() { 
						$('#dialog #dialogConfirm').on( 'click', function( e ) {
							e.preventDefault();

							init();

							$.magnificPopup.close();

							$('#dialog #dialogConfirm').off('click');
						});
					},
					close: function() {
						$('#dialog #dialogConfirm').off('click');
					}
				}
			});*/
		});

		/*$('#dialog #dialogCancel').on( 'click', function( e ) {
			e.preventDefault();
			$.magnificPopup.close();
			$submit.button('reset');
		});*/
	});

	function explode( delimiter, string ) {	
		var emptyArray = { 0: '' };

		if ( arguments.length != 2
			|| typeof arguments[0] == 'undefined'
			|| typeof arguments[1] == 'undefined' )
		{
			return null;
		}

		if ( delimiter === ''
			|| delimiter === false
			|| delimiter === null )
		{
			return false;
		}

		if ( typeof delimiter == 'function'
			|| typeof delimiter == 'object'
			|| typeof string == 'function'
			|| typeof string == 'object' )
		{
			return emptyArray;
		}

		if ( delimiter === true ) {
			delimiter = '1';
		}

		return string.toString().split ( delimiter.toString() );
	}


}).apply( this, [ jQuery ]);