var token = document.head.querySelector('meta[name="csrf-token"]');
var user_id = document.querySelector('#user_id').value;
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

var app = new Vue({
	el: '#shedule',
	data: {
		selects: [],
		groups: [],
		group: 0,
		time_to_add: '00:00',
		user_id: user_id,
		time: ''
	},
	methods: {
		getSelects: function(){
			var vm = this;
			axios.get('/user/shedule-groups')
		    .then(function (response) {
		        vm.selects = response.data.groups;
		    });
		},
		getGroups: function(){
			var vm = this;
			axios.get('/user/groups/'+vm.user_id)
		    .then(function (response) {
		        vm.groups = response.data.groups;
		    });
		},
		addShedule: function(type){ 
			vm = this;
			

			axios.post('/user/add-shedule', {
				shedule_id: this.time,
				user_id: user_id
			}).then(function (response) {
		        vm.getGroups();
		        vm.getSelects();
		        vm.group = '';
		    });
		},
		removeShedule: function(id){
			vm = this;

			axios.post('/user/remove-shedule', {
				id: id
			}).then(function (response) {
		        vm.getGroups();
		        vm.getSelects();
		        vm.group = '';
		    });
		}
	},
	computed:{
		items: function(){
			if (this.groups)
				return this.groups;
			else
				return [];
		},
		times: function(){
			//console.log(this.selects);
			if (this.group)
				return this.selects[this.group].shedule;
			else
				return [];
		},
		groupname: function(){
			if (this.group)
				return this.groups[this.group].name;
			else
				return [];
		}
	},
	created: function(){
		this.getGroups();
		this.getSelects();
	}
});