(function($) {
	'use strict';

	var datatableInit = function() {
		var $table = $('#user-list');
		var dt = $table.DataTable({
			bProcessing: true,
			sAjaxSource: $table.data('url'),
            language: {
            	"processing": "Подождите...",
				"search": "",
				"lengthMenu": "Показать _MENU_ записей",
				"info": "Записи с _START_ до _END_ из _TOTAL_ записей",
				"infoEmpty": "Записи с 0 до 0 из 0 записей",
				"infoFiltered": "(отфильтровано из _MAX_ записей)",
				"infoPostFix": "",
				"loadingRecords": "Загрузка записей...",
				"zeroRecords": "Записи отсутствуют.",
				"emptyTable": "В таблице отсутствуют данные",
				"paginate": {
					"first": "Первая",
					"previous": "Предыдущая",
					"next": "Следующая",
					"last": "Последняя"
				},
				"aria": {
					"sortAscending": ": активировать для сортировки столбца по возрастанию",
					"sortDescending": ": активировать для сортировки столбца по убыванию"
				}
            },
            "columnDefs": [
	            {
	                "targets": [0],
	                "visible": false
	            },
	            {	
	            	"targets": [5],
	            	"orderable": false,
	            	"className": "text-center",
	            }
	        ]
		});
		$table.on('click', 'a.remove-row', function(e) { 
			e.preventDefault();

			var $row = $(this).closest('tr');
			var id = dt.row($(this).closest('tr')).data()["0"];
			var user = dt.row($(this).closest('tr')).data()["1"]
			$.magnificPopup.open({
				items: {
					src: '#dialog',
					type: 'inline'
				},
				preloader: false,
				modal: true,
				callbacks: {
					open: function() { 
						$('#dialog #dialogConfirm').on( 'click', function( e ) {
							e.preventDefault();

							$.magnificPopup.close();
							$row.hide('slow', function(){ 
								dt.row($row.get(0)).remove(); 
							});

							$('#user-list').before('<div class="alert alert-success success-absolute"><i class="fa fa-check-circle"></i> Пользователь <span>'+user+'</span> удален.</div>');

							setTimeout(function () {
								$('.success-absolute').hide('slow', function(){ 
									$(this).remove(); 
								});
							}, 2000);

							var data = {
								_token: function() {
					            	return $('meta[name="csrf-token"]').attr('content');
					          	},
					          	id: id
							}

							$.ajax({
								url: '/user/drop',
								type: 'post',
								data: data,
								dataType: 'json',
								success: function(json) {
									//$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
								},
						        error: function(xhr, ajaxOptions, thrownError) {
						            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						        }
							});

							
						});
					},
					close: function() {
						$('#dialog #dialogConfirm').off('click');
					}
				}
			});
		});

		$('#dialog #dialogCancel').on( 'click', function( e ) {
			e.preventDefault();
			$.magnificPopup.close();
		});
	};

	$(function() {
		datatableInit();
	});

}).apply( this, [ jQuery ]);