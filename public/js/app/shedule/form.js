var token = document.head.querySelector('meta[name="csrf-token"]');
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

var app = new Vue({
	el: '#shedule-app',
	data: {
		groups: [],
		group: 0,
		time_to_add: '00:00'
	},
	methods: {
		getGroups: function(){
			var vm = this;
			axios.get('/shedule/groups')
		    .then(function (response) {
		        vm.groups = response.data.groups;
		    });
		},
		addShedule: function(type){ 
			vm = this;
			this.time_to_add = $("#time_to_add").val();
			if (type == 'adv')
				this.time_to_add = $("#time_to_add_adv").val();
			if (type == 'native')
				this.time_to_add = $("#time_to_add_adv_native").val();
			if (type == 'story')
				this.time_to_add = $("#time_to_add_adv_story").val();

			axios.post('/shedule/add', {
				group_id: this.group,
				value: this.time_to_add,
				type: type
			}).then(function (response) {
		        vm.getGroups();
		    });
		},
		removeShedule: function(id){
			vm = this;

			axios.post('/shedule/remove', {
				id: id
			}).then(function (response) {
		        vm.getGroups();
		    });
		}
	},
	computed:{
		items: function(){
			if (this.group)
				return this.groups[this.group].shedule;
			else
				return [];
		},
		itemsa: function(){
			if (this.group)
				return this.groups[this.group].shedulea;
			else
				return [];
		},
		itemsn: function(){
			if (this.group)
				return this.groups[this.group].shedulen;
			else
				return [];
		},
		itemss: function(){
			if (this.group)
				return this.groups[this.group].shedules;
			else
				return [];
		},
		groupname: function(){
			if (this.group)
				return this.groups[this.group].name;
			else
				return [];
		}
	},
	created: function(){
		this.getGroups();
	}
});