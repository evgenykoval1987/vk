(function($) {
	'use strict';
	
	var datatableInit = function() { 
		var $table = $('#analytic-list');
		var dt = $table.DataTable({
			bProcessing: true,
			ordering: false,
			sAjaxSource: $table.data('url'),
            language: {
            	"processing": "Подождите...",
				"search": "",
				"lengthMenu": "Показать _MENU_ записей",
				"info": "Записи с _START_ до _END_ из _TOTAL_ записей",
				"infoEmpty": "Записи с 0 до 0 из 0 записей",
				"infoFiltered": "(отфильтровано из _MAX_ записей)",
				"infoPostFix": "",
				"loadingRecords": "Загрузка записей...",
				"zeroRecords": "Записи отсутствуют.",
				"emptyTable": "В таблице отсутствуют данные",
				"paginate": {
					"first": "Первая",
					"previous": "Предыдущая",
					"next": "Следующая",
					"last": "Последняя"
				},
				"aria": {
					"sortAscending": ": активировать для сортировки столбца по возрастанию",
					"sortDescending": ": активировать для сортировки столбца по убыванию"
				}
            }
		});
		$("#add-analytic").click(function(){
			$.magnificPopup.open({
				items: {
					src: '#dialog-add',
					type: 'inline'
				},
				preloader: false,
				modal: true,
				callbacks: {
					open: function() { 
						$('#dialog-add #dialogAddConfirm').on( 'click', function( e ) {
							e.preventDefault();

							var data = $("#add-form input")

							$.ajax({
								url: '/analytic/add',
								type: 'post',
								data: data,
								dataType: 'json',
								success: function(json) {
									$.magnificPopup.close();
									$('#analytic-list').before('<div class="alert alert-success success-absolute"><i class="fa fa-check-circle"></i> Группа добавлена.</div>');
									setTimeout(function () {
										$('.success-absolute').hide('slow', function(){ 
											$(this).remove(); 
										});
									}, 1000);
									$table.DataTable().ajax.reload();
								},
						        error: function(xhr, ajaxOptions, thrownError) {
						            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						        }
							});

							
						});
					}
				}
			});
		});
		$table.on('click', 'a.remove-row', function(e) { 
			e.preventDefault();

			var $row = $(this).closest('tr');
			var id = dt.row($(this).closest('tr')).data()["0"];
			var name = dt.row($(this).closest('tr')).data()["1"]
			$.magnificPopup.open({
				items: {
					src: '#dialog',
					type: 'inline'
				},
				preloader: false,
				modal: true,
				callbacks: {
					open: function() { 
						$('#dialog #dialogConfirm').on( 'click', function( e ) {
							e.preventDefault();

							$.magnificPopup.close();

							$('#analytic-list').before('<div class="alert alert-success success-absolute"><i class="fa fa-check-circle"></i> Група <span>'+name+'</span> удалена.</div>');

							setTimeout(function () {
								$('.success-absolute').hide('slow', function(){ 
									$(this).remove(); 
								});
							}, 2000);

							var data = {
								_token: function() {
					            	return $('meta[name="csrf-token"]').attr('content');
					          	},
					          	id: id
							}

							$.ajax({
								url: '/analytic/drop',
								type: 'post',
								data: data,
								dataType: 'json',
								success: function(json) {
									$table.DataTable().ajax.reload();
								},
						        error: function(xhr, ajaxOptions, thrownError) {
						            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						        }
							});

							
						});
					},
					close: function() {
						$('#dialog #dialogConfirm').off('click');
					}
				}
			});
		});

		$table.on('click', 'a.edit-row', function(e) { 
			e.preventDefault();

			var id = dt.row($(this).closest('tr')).data()["0"];
			var name = dt.row($(this).closest('tr')).data()["1"];
			var start = dt.row($(this).closest('tr')).data()["2"];
			var end = dt.row($(this).closest('tr')).data()["3"];
			var current = dt.row($(this).closest('tr')).data()["4"];
			var vk_id = dt.row($(this).closest('tr')).data()["6"];

			$("#edit-form [name=id]").val(id);
			$("#edit-form [name=name]").val(name);
			$("#edit-form [name=start]").val(start);
			$("#edit-form [name=end]").val(end);
			$("#edit-form [name=current]").val(current);
			$("#edit-form [name=vk_id]").val(vk_id);

			$.magnificPopup.open({
				items: {
					src: '#dialog-edit',
					type: 'inline'
				},
				preloader: false,
				modal: true,
				callbacks: {
					open: function() { 
						$('#dialog-edit #dialogEditConfirm').on( 'click', function( e ) {
							e.preventDefault();

							var data = $("#edit-form input")

							$.ajax({
								url: '/analytic/edit',
								type: 'post',
								data: data,
								dataType: 'json',
								success: function(json) {
									$.magnificPopup.close();
									$('#analytic-list').before('<div class="alert alert-success success-absolute"><i class="fa fa-check-circle"></i> Группа отредактирована.</div>');
									setTimeout(function () {
										$('.success-absolute').hide('slow', function(){ 
											$(this).remove(); 
										});
									}, 1000);
									$table.DataTable().ajax.reload();
								},
						        error: function(xhr, ajaxOptions, thrownError) {
						            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						        }
							});

							
						});
					},
					close: function() {
						$('#dialog #dialogEditConfirm').off('click');
					}
				}
			});
		});

		$('#dialog #dialogCancel').on( 'click', function( e ) {
			e.preventDefault();
			$.magnificPopup.close();
		});

		$('#dialog-add #dialogAddCancel').on( 'click', function( e ) {
			e.preventDefault();
			$.magnificPopup.close();
		});

		$('#dialog-edit #dialogEditCancel').on( 'click', function( e ) {
			e.preventDefault();
			$.magnificPopup.close();
		});




		$table.on('click', 'a.play-row', function(e) { 
			e.preventDefault();

			var id = dt.row($(this).closest('tr')).data()["0"];
			var name = dt.row($(this).closest('tr')).data()["1"];
			var start = dt.row($(this).closest('tr')).data()["2"];
			var end = dt.row($(this).closest('tr')).data()["3"];
			var current = dt.row($(this).closest('tr')).data()["4"];
			var quantity = dt.row($(this).closest('tr')).data()["5"];

			topicture(id);

			$.magnificPopup.open({
				items: {
					src: '#dialog-play',
					type: 'inline'
				},
				preloader: false,
				modal: true,
				callbacks: {
					open: function() { 
						$("#play-name").text(name);
						$("#dialog-play #start").text(start);
						$("#dialog-play #end").text(end);
						$("#dialog-play #current").text(current);
						$("#dialog-play #quantity").text(quantity);
						$("#dialog-play #analytic-id").val(id);
					}
				}
			});
		});

		$('#dialog-play #dialogPlayCancel').on( 'click', function( e ) {
			e.preventDefault();
			$.magnificPopup.close();
			$table.DataTable().ajax.reload();
		});

		$("#begin").click(function(){
			init();
			$(this).prop('disabled', true);
		})

		function init(){
			var today = new Date();
			var dd = String(today.getDate()).padStart(2, '0');
			var mm = String(today.getMonth() + 1).padStart(2, '0');
			var yyyy = today.getFullYear();
			today = yyyy + '-' + mm + '-' + dd;

			$("#date_parser").val(today);
			$("#progress").show();
			progress();
			run();
		}

		function run(){
			var name = $("#play-name").text();
			var start = parseInt($("#start").text());
			var end = parseInt($("#end").text());
			var current = parseInt($("#current").text());
			var id = $("#dialog-play #analytic-id").val();
			var date_added = $("#date_parser").val();

			$.ajax({
				url: '/analytic/do',
				type: 'post',
				data: {
					_token: function() {
		            	return $('meta[name="csrf-token"]').attr('content');
		          	},
					'name' : name,
					'current': current,
					'end' : end,
					'id' : id,
					'date_added' : date_added
				},
				dataType: 'json',
				success: function(json) {
					if (json.recount == 1){
						topicture(id);
					}
					$("#current").text(json.current);
					$("#quantity").text(json.count);
					progress();
					if (parseInt($("#current").text()) >= parseInt($("#end").text())){
						$("#begin").prop('disabled', false);
					}
					else{
						run();
					}
					
				},
		        error: function(xhr, ajaxOptions, thrownError) {
		            run();
		        }
			});
		}

		function topicture(id){
			$.ajax({
				url: '/analytic/topicture',
				type: 'post',
				data: {
					_token: function() {
		            	return $('meta[name="csrf-token"]').attr('content');
		          	},
					'id' : id
				},
				dataType: 'html',
				success: function(html) {
					$("#play-result").html(html);
				},
		        error: function(xhr, ajaxOptions, thrownError) {
		            
		        }
			});
		}

		function progress(){
			var name = $("#play-name").text();
			var start = parseInt($("#start").text());
			var end = parseInt($("#end").text());
			var current = parseInt($("#current").text());
			var id = $("#dialog-play #analytic-id").val();
			var date_added = $("#date_parser").val();

			var progress = 0;
			var val = (end-start)/100; 
			progress = parseInt((current-start)/val);
			$("#progress span").text(progress+'%');
			$("#progress .progress-bar-success").attr('aria-valuenow',progress);
			$("#progress .progress-bar-success").css('width', progress+'%');
		}
	};

	$(function() {
		datatableInit();
	});

	

}).apply( this, [ jQuery ]);