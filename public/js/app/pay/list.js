(function( $ ) {
	'use strict';

	var $table = $('#pays-list');
	var dt = false;

	var datatableInit = function() {
		dt = $table.DataTable( {
			bProcessing: true,
		    ajax: $table.data('url'),
		    ordering: false,
            language: {
            	"processing": "Подождите...",
				"search": "",
				"lengthMenu": "Показать _MENU_ записей",
				"info": "Записи с _START_ до _END_ из _TOTAL_ записей",
				"infoEmpty": "Записи с 0 до 0 из 0 записей",
				"infoFiltered": "(отфильтровано из _MAX_ записей)",
				"infoPostFix": "",
				"loadingRecords": "Загрузка записей...",
				"zeroRecords": "Записи отсутствуют.",
				"emptyTable": "В таблице отсутствуют данные",
				"paginate": {
					"first": "Первая",
					"previous": "Предыдущая",
					"next": "Следующая",
					"last": "Последняя"
				},
				"aria": {
					"sortAscending": ": активировать для сортировки столбца по возрастанию",
					"sortDescending": ": активировать для сортировки столбца по убыванию"
				}
            },
            "columnDefs": [
	            {
	                "targets": [0],
	                "visible": false
	            },
	            {
	                "targets": [4],
	                "className": 'text-right'
	            },
	            {
	                "targets": [5],
	                "className": 'text-center'
	            },
	            {
	                "targets": [6],
	                "className": 'text-center'
	            }
	        ]
		});
	};

	$("#refresh").click(function(){
		dt.ajax.reload();
		updateCounters();
	})


	$(function() { 
		datatableInit();
	});

}).apply( this, [ jQuery ]);