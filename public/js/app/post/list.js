(function( $ ) {
	'use strict';

	var $table = $('#post-list');
	var dt = false;

	var datatableInit = function() {
		dt = $table.DataTable( {
			bProcessing: true,
		    ajax: $table.data('url'),
		    ordering: false,
            language: {
            	"processing": "Подождите...",
				"search": "",
				"lengthMenu": "Показать _MENU_ записей",
				"info": "Записи с _START_ до _END_ из _TOTAL_ записей",
				"infoEmpty": "Записи с 0 до 0 из 0 записей",
				"infoFiltered": "(отфильтровано из _MAX_ записей)",
				"infoPostFix": "",
				"loadingRecords": "Загрузка записей...",
				"zeroRecords": "Записи отсутствуют.",
				"emptyTable": "В таблице отсутствуют данные",
				"paginate": {
					"first": "Первая",
					"previous": "Предыдущая",
					"next": "Следующая",
					"last": "Последняя"
				},
				"aria": {
					"sortAscending": ": активировать для сортировки столбца по возрастанию",
					"sortDescending": ": активировать для сортировки столбца по убыванию"
				}
            },
            "columnDefs": [
	            /*{
	                "targets": [0],
	                "visible": false
	            },*/
	            {	
	            	"targets": [8],
	            	"className": "text-center",
	            }
	        ]
		});
	};

	$table.on('click', 'a.remove-row', function(e) { 
		e.preventDefault();

		var $row = $(this).closest('tr');
		var id = dt.row($(this).closest('tr')).data()["0"];
		
		$.magnificPopup.open({
			items: {
				src: '#dialog',
				type: 'inline'
			},
			preloader: false,
			modal: true,
			callbacks: {
				open: function() { 
					$('#dialog #dialogConfirm').on( 'click', function( e ) {
						e.preventDefault();

						$.magnificPopup.close();
						$row.hide('slow', function(){ 
							dt.row($row.get(0)).remove(); 
						});

						$('.panel .panel-body').prepend('<div class="alert alert-success success-absolute"><i class="fa fa-check-circle"></i> Пост успешно удален.</div>');

						setTimeout(function () {
							$('.success-absolute').hide('slow', function(){ 
								$(this).remove(); 
							});
						}, 2000);

						var data = {
							_token: function() {
				            	return $('meta[name="csrf-token"]').attr('content');
				          	},
				          	id: id
						}

						$.ajax({
							url: '/post/drop',
							type: 'post',
							data: data,
							dataType: 'json',
							success: function(json) {
								//$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
							},
					        error: function(xhr, ajaxOptions, thrownError) {
					            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					        }
						});

						
					});
				},
				close: function() {
					$('#dialog #dialogConfirm').off('click');
				}
			}
		});
	});

	$('#dialog #dialogCancel').on( 'click', function( e ) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	$table.on('click', 'a.strike-popup-set', function(e) { 
		var $this = $(this);
		$.magnificPopup.open({
			items: {
				src: '#strike-set',
				type: 'inline'
			},
			preloader: false,
			modal: true,
			callbacks: {
				open: function() { 
					var id = $this.data('id');
					var message = $this.data('message');
					if ($this.hasClass('btn-danger'))
						$("#strike").prop('checked',true);
					else
						$("#strike").prop('checked',false);

					$("#strike_comment").val(message);

					$('#strike-set #strikeConfirm').on( 'click', function( e ) {
						e.preventDefault();
						//$.magnificPopup.close();

						var data = {
							_token: function() {
				            	return $('meta[name="csrf-token"]').attr('content');
				          	},
				          	id: id,
				          	message: $("#strike_comment").val(),
				          	strike: $("#strike").prop('checked')
						}

						$.ajax({
							url: '/post/strike',
							type: 'post',
							data: data,
							dataType: 'json',
							success: function(json) {
								$.magnificPopup.close();
								dt.ajax.reload();
							},
					        error: function(xhr, ajaxOptions, thrownError) {
					            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					        }
						});

						
					});
				},
				close: function() {
				}
			}
		});
	});

	$table.on('click', 'a.strike-popup-info', function(e) { 
		var $this = $(this);
		$.magnificPopup.open({
			items: {
				src: '#strike-info',
				type: 'inline'
			},
			preloader: false,
			modal: true,
			callbacks: {
				open: function() { 
					var message = $this.data('message');
					$("#strike_info_comment").text(message);
				},
				close: function() {
				}
			}
		});
	});

	$('#strike-info #strikeInfoCancel, #strike-set #strikeCancel').on( 'click', function( e ) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	$(function() { 
		datatableInit();
	});

}).apply( this, [ jQuery ]);