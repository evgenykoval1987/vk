(function( $ ) {
	'use strict';

	var $table = $('#pendings-list');
	var dt = false;

	var datatableInit = function() {
		dt = $table.DataTable( {
			bProcessing: true,
		    ajax: $table.data('url'),
		    ordering: false,
            language: {
            	"processing": "Подождите...",
				"search": "",
				"lengthMenu": "Показать _MENU_ записей",
				"info": "Записи с _START_ до _END_ из _TOTAL_ записей",
				"infoEmpty": "Записи с 0 до 0 из 0 записей",
				"infoFiltered": "(отфильтровано из _MAX_ записей)",
				"infoPostFix": "",
				"loadingRecords": "Загрузка записей...",
				"zeroRecords": "Записи отсутствуют.",
				"emptyTable": "В таблице отсутствуют данные",
				"paginate": {
					"first": "Первая",
					"previous": "Предыдущая",
					"next": "Следующая",
					"last": "Последняя"
				},
				"aria": {
					"sortAscending": ": активировать для сортировки столбца по возрастанию",
					"sortDescending": ": активировать для сортировки столбца по убыванию"
				}
            },
            "columnDefs": [
	            /*{
	                "targets": [0],
	                "visible": false
	            },*/
	            /*{	
	            	"targets": [8],
	            	"className": "text-center",
	            }*/
	        ]
		});

		$.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var fields = [];
                var group = $('#group-tree li.active').data('group');
                if (group === undefined)
                	group = '';
                var table_group = data[1];

                if (group != ''){
                    if (table_group.indexOf(group) !== -1){
                        fields.push(true);
                    }
                    else{
                        fields.push(false);
                    }
                }
                

                var res = true;
                for (var field in fields){
                    if (!fields[field])
                        res = false;
                }

                return res;
            }
        );

        $("#group-tree li").click(function(){
			if ($(this).hasClass('active')){
				$(this).removeClass('active');
                dt.draw();
			}
			else{
				$("#group-tree li.active").removeClass('active');
				$(this).addClass('active');
				dt.draw();
			}
		})

		$table.on('click', 'a.remove-row', function(e) { 
			e.preventDefault();

			var $row = $(this).closest('tr');
			var id = dt.row($(this).closest('tr')).data()["0"];
			
			$.magnificPopup.open({
				items: {
					src: '#dialog',
					type: 'inline'
				},
				preloader: false,
				modal: true,
				callbacks: {
					open: function() { 
						$('#dialog #dialogConfirm').on( 'click', function( e ) {
							e.preventDefault();

							$.magnificPopup.close();
							$row.hide('slow', function(){ 
								dt.row($row.get(0)).remove(); 
							});

							$('.panel .panel-body').prepend('<div class="alert alert-success success-absolute"><i class="fa fa-check-circle"></i> Пост успешно удален.</div>');

							setTimeout(function () {
								$('.success-absolute').hide('slow', function(){ 
									$(this).remove(); 
								});
							}, 2000);

							var data = {
								_token: function() {
					            	return $('meta[name="csrf-token"]').attr('content');
					          	},
					          	id: id
							}

							$.ajax({
								url: '/post/drop',
								type: 'post',
								data: data,
								dataType: 'json',
								success: function(json) {
									//$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
								},
						        error: function(xhr, ajaxOptions, thrownError) {
						            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						        }
							});

							
						});
					},
					close: function() {
						$('#dialog #dialogConfirm').off('click');
					}
				}
			});
		});

		$('#dialog #dialogCancel').on( 'click', function( e ) {
			e.preventDefault();
			$.magnificPopup.close();
		});

		$table.on('click', 'a.to-post', function(e) { 
			e.preventDefault();

			var $row = $(this).closest('tr');
			var id = dt.row($(this).closest('tr')).data()["0"];
			
			var data = {
				_token: function() {
	            	return $('meta[name="csrf-token"]').attr('content');
	          	},
	          	id: id
			}

			$.ajax({
				url: '/post/to-manage',
				type: 'post',
				data: data,
				dataType: 'json',
				success: function(json) {
					$row.hide('slow', function(){ 
						dt.row($row.get(0)).remove(); 
					});

					dt.draw();

					$('.panel .panel-body').prepend('<div class="alert alert-success success-absolute"><i class="fa fa-check-circle"></i> Пост успешно отправлен для постинга.</div>');

					setTimeout(function () {
						$('.success-absolute').hide('slow', function(){ 
							$(this).remove(); 
						});
					}, 2000);
				},
		        error: function(xhr, ajaxOptions, thrownError) {
		            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		        }
			});
		});
	};

	$(function() { 
		datatableInit();

		
	});

}).apply( this, [ jQuery ]);