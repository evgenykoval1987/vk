$(function(){
	$("#group_id").val($("#post-group").val());
	$("#post-group").change(function(){
		$("#group_id").val($(this).val());
	})
	$("#publish").click(function(){
		$(".alert-danger").remove();
		btn = $(this);
		btn.button('loading');

		$("#post-body").ajaxSubmit({
			success:  function(){
				vk_container.publish($("#post_id").val(), btn);
			},
            error:function(){
            	btn.button('reset');
            }
		}); 
	})

	$("#publish_vk_later").click(function(){
		$(".alert-danger").remove();
		btn = $(this);
		btn.button('loading');

		$("#post-body").ajaxSubmit({
			success:  function(){
				vk_container.publish_vk_later($("#post_id").val(), $("#publish-date").val(), $("#publish-time").val(), $("#novkd:checked").val(), btn);
			},
            error:function(){
            	btn.button('reset');
            }
		}); 
	})

	$("#publish_later").click(function(){
		$(".alert-danger").remove();
		btn = $(this);
		btn.button('loading');

		$("#post-body").ajaxSubmit({
			success:  function(){
				vk_container.publish_later($("#post_id").val(), $("#novk:checked").val(), btn, $("#user_shedule").prop('checked'));
			},
            error:function(){
            	btn.button('reset');
            }
		}); 
	})

	$("#group-stat-"+$("#post-group").val()).show();

	$("#post-group").change(function(){
		$(".group-stat").hide();
		$("#group-stat-"+$(this).val()).show();
		find_current_turn($(this).val())
	})

	$("#user_shedule").change(function(){
		$(".group-stat").hide();
		$("#group-stat-"+$("#post-group").val()).show();
		find_current_turn($("#post-group").val())
	})

	find_current_turn($("#post-group").val())

	$("#atach").vkeditor();
})

function find_current_turn(group_id){
	if (group_id){
		$(".tabs .btn-primary").prop('disabled', false);
		$("#group-turn").empty();
		var data = {
			_token: function() {
	        	return $('meta[name="csrf-token"]').attr('content');
	      	},
	      	group_id: group_id,
	      	user_shedule: $("#user_shedule").prop('checked'),
	      	user_id: $("#user_id").val()
		}

		$.ajax({
			type: "POST",
			url: "/shedule/getByGroup",
			data: data,
			dataType: "json",
			success: function (json) {
				$("#group-turn").text(json.response);
				$("#group-turn").show();
				if (json.success == false)
					$("#publish_later").prop('disabled', true);
				else
					$("#publish_later").prop('disabled', false);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				
	        }
		})
	}
	else{
		$("#group-turn").hide();
		$(".tabs .btn-primary").prop('disabled', true);
	}
}