(function( $ ) {
	'use strict';

	var init = function(){

	}

	$(function() { 
		init();
	});

}).apply( this, [ jQuery ]);

var token = document.head.querySelector('meta[name="csrf-token"]');
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

var app = new Vue({
  	el: '#vue-app',
  	data: {
    	message: 'Hello Vue!',
    	users: [],
    	active_user: 0,
    	active_user_id: 0,
    	message: ''
  	},
	methods: {
		refresh: function(){
			var vm = this;
			axios.get('/chat/data').then(function (response) {
		        vm.users = response.data;
		        setTimeout(function(){
		        	
					var objDiv = document.getElementById("history");
					objDiv.scrollTop = objDiv.scrollHeight;
		        }, 500);
		        
		    });
		},
		scroll: function(){
			setTimeout(function(){
				var objDiv = document.getElementById("history");
				objDiv.scrollTop = objDiv.scrollHeight;
	        }, 500);
		},
		unreaded: function(){
			var vm = this;
			var params = {
				'from_user' : vm.active_user.id,
				'message' : vm.message
			}
			axios.post('/chat/unread', params).then(function (response) {
		        vm.refresh();
		    });
		},
		send: function(){
			var vm = this;
			var params = {
				'to_user' : vm.active_user.id,
				'message' : vm.message
			}
			axios.post('/chat/send', params).then(function (response) {
		        vm.refresh();
		        vm.message = '';
		    });
		}
	},
	computed: {
		getmessages: function(){ 
			var u = false;
			for (user in this.users){ 
				if (this.users[user].id == this.active_user_id){
					this.active_user = this.users[user];
				}
			}
			return this.active_user;
		}
	},
	mounted: function () {
  		window.setInterval(() => {
    		this.refresh()
  		}, 5000)
	},
	created: function(){ 
		this.refresh();
	}
})