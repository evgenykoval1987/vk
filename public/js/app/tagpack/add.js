(function($) {
	'use strict';

	$("#tag-add-form").validate({
		highlight: function( label ) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function( label ) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function( error, element ) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
		    name: {
		      	required: true,
		      	minlength: 3
		    }
  		},
	  	messages: {
	  		name: {
		      	required: "Поле обязательное для заполнения"
		    }
	  	}
	});

	$("#addTag").click(function(){
		var tag_id = $("#tag").val();
		var title = $("#tag option:selected").text();
		var qua = parseInt($("#tag option:selected").data('quantity'));

		var html = '<tr><td>'+title+'<input type="hidden" value="'+tag_id+'" name="tags[]"><input type="hidden" value="'+qua+'" class="quantity"></td><td class="actions"><a href="" class="delete-row"><i class="fa fa-trash-o"></i></a></td></tr>';
		$("#total-tags tbody").append(html);
		

		var quantity = 0;
		$("#total-tags tbody .quantity").each(function(){
			quantity += parseInt($(this).val());
		});
		$("[name='quantity']").val(quantity);

		return false;
	});

	$(document).on('click', ".delete-row", function(){
		$(this).parent().parent().remove();

		var quantity = 0;
		$("#total-tags tbody .quantity").each(function(){
			quantity += parseInt($(this).val());
		});
		$("[name='quantity']").val(quantity);
		
		return false;
	});

}).apply( this, [ jQuery ]);