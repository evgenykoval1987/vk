(function($) {
	'use strict';

	$("#manager-add-form").validate({
		highlight: function( label ) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function( label ) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function( error, element ) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
		    name: {
		      	required: true,
		      	minlength: 3
		    }
  		},
	  	messages: {
	  		name: {
		      	required: "Поле обязательное для заполнения"
		    }
	  	}
	});


}).apply( this, [ jQuery ]);