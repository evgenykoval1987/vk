(function($) {
	'use strict';

	var initUser = function(url, button) {
		var info = explode('/', url);
		var user = info[info.length-1];
		
		VK.Api.call('users.get', {user_ids: user, v:"5.131"}, function(r) {
		  	if(r.response) {
		  		$("#client-name").val(r.response[0].first_name+' '+r.response[0].last_name);
		  	}
		  	else{
		  		alert('Не удалось получить данные о пользователе. Проверьте правильность введенных данных.');
		  	}

		  	button.button('reset');
		});
	};

	$(function() {
		$("#check-vk").click(function(){
			$(this).button('loading');
			initUser($("#vk").val(), $(this));
		})
		
	});

	$("#user-add-form").validate({
		highlight: function( label ) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function( label ) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function( error, element ) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
		    name: {
		      	required: true,
		      	minlength: 3
		    }
  		},
	  	messages: {
	  		name: {
		      	required: "Поле обязательное для заполнения"
		    }
	  	}
	});

	function explode( delimiter, string ) {	
		var emptyArray = { 0: '' };

		if ( arguments.length != 2
			|| typeof arguments[0] == 'undefined'
			|| typeof arguments[1] == 'undefined' )
		{
			return null;
		}

		if ( delimiter === ''
			|| delimiter === false
			|| delimiter === null )
		{
			return false;
		}

		if ( typeof delimiter == 'function'
			|| typeof delimiter == 'object'
			|| typeof string == 'function'
			|| typeof string == 'object' )
		{
			return emptyArray;
		}

		if ( delimiter === true ) {
			delimiter = '1';
		}

		return string.toString().split ( delimiter.toString() );
	}


}).apply( this, [ jQuery ]);