(function( $ ) {
	'use strict';

	var $table = $('#client-list');
	var dt = false;

	var datatableInit = function() {
		dt = $table.DataTable( {
			bProcessing: true,
		    ajax: $table.data('url'),
		    ordering: false,
		    order: [[ 3, "desc" ]],
		    createdRow: function( row, data, dataIndex){
                if (data[6] == 1){
                    $(row).addClass('new-monitoring');
                }
                else{

                }
            },
            language: {
            	"processing": "Подождите...",
				"search": "",
				"lengthMenu": "Показать _MENU_ записей",
				"info": "Записи с _START_ до _END_ из _TOTAL_ записей",
				"infoEmpty": "Записи с 0 до 0 из 0 записей",
				"infoFiltered": "(отфильтровано из _MAX_ записей)",
				"infoPostFix": "",
				"loadingRecords": "Загрузка записей...",
				"zeroRecords": "Записи отсутствуют.",
				"emptyTable": "В таблице отсутствуют данные",
				"paginate": {
					"first": "Первая",
					"previous": "Предыдущая",
					"next": "Следующая",
					"last": "Последняя"
				},
				"aria": {
					"sortAscending": ": активировать для сортировки столбца по возрастанию",
					"sortDescending": ": активировать для сортировки столбца по убыванию"
				}
            },
            "columnDefs": [
	            {
	                "targets": [0],
	                "visible": false
	            }
	        ]
		});
	};

	$("#refresh").click(function(){
		dt.ajax.reload();
	})

	$(document).on('click','.remove-last', function(){
		$.ajax({
			url: $(this).attr('href'),
			type: 'get',
			data: {},
			dataType: 'json',
			success: function(json) {
				dt.ajax.reload();
				updateCounters();
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});

		return false;
	})


	$(function() { 
		datatableInit();
	});

}).apply( this, [ jQuery ]);