(function($) {
	'use strict';

	var Init = function() {
		var month = $("#month").val();
		var year = $("#year").val();

		refresh(month, year);

		$("#month, #year").change(function(){
			var month = $("#month").val();
			var year = $("#year").val();
			refresh(month, year);
		})
	}

	var refresh = function(month, year){
		$("#month").prop('disabled', true);
		$("#year").prop('disabled', true);
		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	month: month,
          	year: year
		}

		$.ajax({
			url: '/company/timable-table',
			type: 'post',
			data: data,
			dataType: 'html',
			success: function(html) {
				$("#result").empty().html(html);
				$("#month").prop('disabled', false);
				$("#year").prop('disabled', false);
				$('.my-table').DataTable({
					paging: false,
			        searching: false,
    				ordering:  false,
			        scrollX: true,
        			scrollCollapse: true,
					fixedColumns:   {
			            leftColumns: 1
			        }
				});
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});
	}

	$(function() {
		Init();
	});

}).apply( this, [ jQuery ]);