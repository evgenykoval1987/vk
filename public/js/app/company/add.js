(function($) {
	'use strict';

	$("#company-add-form").validate({
		highlight: function( label ) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function( label ) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function( error, element ) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		},
		rules: {
		    title: {
		      	required: true,
		      	minlength: 3
		    },
		    client_id: {
		      	required: true
		    }
  		},
	  	messages: {
	  		title: {
		      	required: "Поле обязательное для заполнения"
		    },
		    client_id: {
		      	required: "Поле обязательное для заполнения"
		    }
	  	}
	});



	$("#addTag").click(function(){
		var tag_id = $("#tag").val();
		var title = $("#tag option:selected").text();
		var qua = parseInt($("#tag option:selected").data('quantity'));

		var html = '<tr><td>'+title+'<input type="hidden" value="'+tag_id+'" name="tags[]"><input type="hidden" value="'+qua+'" class="quantity"></td><td class="actions"><a href="" class="delete-row"><i class="fa fa-trash-o"></i></a></td></tr>';
		$("#total-tags tbody").append(html);
		

		refresh_q();

		return false;
	});

	$(document).on('click', ".delete-row", function(){
		$(this).parent().parent().remove();

		refresh_q();
		
		return false;
	});

	$("[name='tagpack_id']").change(function(){
		refresh_q();
	});

	function refresh_q() {
		var quantity = 0;
		var views = 0;

		if ($("[name='tagpack_id']").val()){
			var quantity = parseInt($("[name='tagpack_id'] option:selected").data('quantity'));
			var views = parseInt($("[name='tagpack_id'] option:selected").data('views'));
		}

		$("#total-tags tbody .quantity").each(function(){
			quantity += parseInt($(this).val());
		});
		$("[name='quantity']").val(quantity);
		$("[name='views']").val(views);
	}
}).apply( this, [ jQuery ]);