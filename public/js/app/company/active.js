(function($) {
	'use strict';

	var datatableInit = function() {
		var $table = $('#company-list');
		var dt = $table.DataTable({
			bProcessing: true,
			ordering: false,
			sAjaxSource: $table.data('url'),
            language: {
            	"processing": "Подождите...",
				"search": "",
				"lengthMenu": "Показать _MENU_ записей",
				"info": "Записи с _START_ до _END_ из _TOTAL_ записей",
				"infoEmpty": "Записи с 0 до 0 из 0 записей",
				"infoFiltered": "(отфильтровано из _MAX_ записей)",
				"infoPostFix": "",
				"loadingRecords": "Загрузка записей...",
				"zeroRecords": "Записи отсутствуют.",
				"emptyTable": "В таблице отсутствуют данные",
				"paginate": {
					"first": "Первая",
					"previous": "Предыдущая",
					"next": "Следующая",
					"last": "Последняя"
				},
				"aria": {
					"sortAscending": ": активировать для сортировки столбца по возрастанию",
					"sortDescending": ": активировать для сортировки столбца по убыванию"
				}
            },
            "columnDefs": [
	            {	
	            	"targets": [2],
	            	"className": "text-center"
	            },
	            {	
	            	"targets": [3],
	            	"className": "text-center"
	            },
	            {	
	            	"targets": [4],
	            	"className": "text-center"
	            }/*,
	            {	
	            	"targets": [3],
	            	"className": "text-center"
	            }*/
	        ]
		});
	};

	$(function() {
		datatableInit();
	});

}).apply( this, [ jQuery ]);