(function($) {
	'use strict';
	var dt = '';
	$(document).on('click','#copy-to-bufer', function(){
		var id = $(this).data('post-id');

		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	id: id,
		}

		$.ajax({
			type: "POST",
			url: "/company/post-report-link",
			data: data,
			dataType: "html",
			success: function (html) {
				//$("#dialog .modal-text").html(html);
				$("#copy-to-bufer").text('http://rmo-report.top?id='+html);
				$("#copy-to-bufer").attr('href', 'http://rmo-report.top?id='+html);
				$("#copy-to-bufer").attr('id','0');
			},
			error: function(xhr, ajaxOptions, thrownError) {
	            
	        }
		})

		return false;
	})

	var datatableInit = function() {
		var checked_report = false;
		var $table = $('#company-ended-list');
		dt = $table.DataTable({
			bProcessing: true,
			ordering: false,
			sAjaxSource: $table.data('url'),
            language: {
            	"processing": "Подождите...",
				"search": "",
				"lengthMenu": "Показать _MENU_ записей",
				"info": "Записи с _START_ до _END_ из _TOTAL_ записей",
				"infoEmpty": "Записи с 0 до 0 из 0 записей",
				"infoFiltered": "(отфильтровано из _MAX_ записей)",
				"infoPostFix": "",
				"loadingRecords": "Загрузка записей...",
				"zeroRecords": "Записи отсутствуют.",
				"emptyTable": "В таблице отсутствуют данные",
				"paginate": {
					"first": "Первая",
					"previous": "Предыдущая",
					"next": "Следующая",
					"last": "Последняя"
				},
				"aria": {
					"sortAscending": ": активировать для сортировки столбца по возрастанию",
					"sortDescending": ": активировать для сортировки столбца по убыванию"
				}
            },
            "columnDefs": [
            	{	
	            	"targets": [0],
	            	"className": "text-center"
	            },
            	{	
	            	"targets": [1],
	            	"className": "text-center"
	            },
	            {	
	            	"targets": [3],
	            	"className": "text-center"
	            },
	            {	
	            	"targets": [4],
	            	"className": "text-center"
	            }
	        ]
		});

		$(document).on('click', 'a.company-post-edit', function(e) { 
			e.preventDefault();

			var id = $(this).data('id');
			var group_id = $(this).data('group-id');
			var viewed = $(this).data('viewed');
			var vk_link = $(this).data('vk-link');

			$("#form-post-edit input[name='vk_link']").val(vk_link);
			$("#form-post-edit input[name='viewed']").val(viewed);
			$("#form-post-edit input[name='company_post_id']").val(id);
			$("#form-post-edit input[name='company_post_group_id']").val(group_id);

			$.magnificPopup.open({
				items: {
					src: '#dialog-edit',
					type: 'inline'
				},
				preloader: false,
				modal: true,
				callbacks: {
					open: function() { 
						
					},
					close: function() {
						$('#dialog-edit #dialogEditConfirm').off('click');
					}
				}
			});
		});

		$('#dialog-edit #dialogEditCancel').on( 'click', function( e ) {
			e.preventDefault();
			$.magnificPopup.close();
			checked_report.trigger('click');
		});
		$('#dialog-edit #dialogEditConfirm').on( 'click', function( e ) {
			e.preventDefault();

			$.ajax({
				type: "POST",
				url: "/company/company-edit-post",
				data: $("#form-post-edit input"),
				dataType: "html",
				success: function (html) {
					//$("#dialog .modal-text").html(html);

					setTimeout(function(){
						$.magnificPopup.close();
						checked_report.trigger('click');
					}, 500)
					
				},
				error: function(xhr, ajaxOptions, thrownError) {
		            
		        }
			})

			
		});

		$table.on('click', 'a.company-post-report', function(e) { 
			e.preventDefault();

			checked_report = $(this);
			var $row = $(this).closest('tr');
			var id = dt.row($(this).closest('tr')).data()["0"];
			$("#report-post-id").text(id);

			var data = {
				_token: function() {
	            	return $('meta[name="csrf-token"]').attr('content');
	          	},
	          	id: id,
			}

			$.ajax({
				type: "POST",
				url: "/company/post-report",
				data: data,
				dataType: "html",
				success: function (html) {
					$("#dialog .modal-text").html(html);
				},
				error: function(xhr, ajaxOptions, thrownError) {
		            
		        }
			})

			$.magnificPopup.open({
				items: {
					src: '#dialog',
					type: 'inline'
				},
				preloader: false,
				modal: true,
				callbacks: {
					open: function() { 
						
					},
					close: function() {
						$('#dialog #dialogConfirm').off('click');
					}
				}
			});
		});

		$('#dialog #dialogCancel').on( 'click', function( e ) {
			e.preventDefault();
			$.magnificPopup.close();
		});


		$('#dialog-remove #dialogRemoveCancel').on( 'click', function( e ) {
			e.preventDefault();
			$.magnificPopup.close();
		});
	};

	$(function() {
		datatableInit();

		$(document).on('click', "#recalculate", function(){
			var post_id = $(this).data('post-id');

			var data = {
				_token: function() {
	            	return $('meta[name="csrf-token"]').attr('content');
	          	},
	          	id: post_id
			}

			$(this).button('loading');
			
			$.ajax({
				url: '/vk/post-single-stat',
				type: 'post',
				data: data,
				dataType: 'json',
				success: function(json) {
					$.ajax({
						type: "POST",
						url: "/company/post-report",
						data: data,
						dataType: "html",
						success: function (html) {
							$("#dialog .modal-text").html(html);
							$(this).button('reset');
						},
						error: function(xhr, ajaxOptions, thrownError) {
				            
				        }
					})
				},
		        error: function(xhr, ajaxOptions, thrownError) {
		            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		            $(this).button('reset');
		        }
			});
		})

		$(document).on('click','.remove-ended', function(){
		$.ajax({
			url: $(this).attr('href'),
			type: 'get',
			data: {},
			dataType: 'json',
			success: function(json) {
				dt.ajax.reload();
				updateCounters();
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});

		return false;
	})
	});

}).apply( this, [ jQuery ]);

/*function copyToClipboard(text) { alert(text);
    if (window.clipboardData && window.clipboardData.setData) { alert(1);
        // Internet Explorer-specific code path to prevent textarea being shown while dialog is visible.
        return window.clipboardData.setData("Text", text);

    }
    else if (document.queryCommandSupported && document.queryCommandSupported("copy")) { alert(2);
        var textarea = document.createElement("textarea");
        textarea.textContent = text;
        textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in Microsoft Edge.
        document.body.appendChild(textarea);
        textarea.select();
        try {
            return document.execCommand("copy");  // Security exception may be thrown by some browsers.
        }
        catch (ex) {
            console.warn("Copy to clipboard failed.", ex);
            console.log('AAAAAAAAAAAAAAAAAA');
            return false;
        }
        finally {
            document.body.removeChild(textarea);
        }
    }
}*/

function copyToClipboard(text) {
	var $temp = $("<textarea>");
	$("#dialog").append($temp);
	$temp.val(text).select();
	document.execCommand("copy");
	$temp.remove();

	$('#download-hrefs').after('<div class="copied">Скопировано!</div>');
	setTimeout("$('.copied').remove()", 1000);

	return false;
}