(function($) {
	'use strict';

	var dt = false;

	$(document).on('click','#copy-to-bufer', function(){
		var id = $(this).data('post-id');

		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	id: id,
		}

		$.ajax({
			type: "POST",
			url: "/company/post-report-link",
			data: data,
			dataType: "html",
			success: function (html) {
				//$("#dialog .modal-text").html(html);
				$("#copy-to-bufer").text('http://rmo-report.top?id='+html);
				$("#copy-to-bufer").attr('href', 'http://rmo-report.top?id='+html);
				$("#copy-to-bufer").attr('id','0');
			},
			error: function(xhr, ajaxOptions, thrownError) {
	            
	        }
		})

		return false;
	})

	var datatableInit = function() {
		var checked_report = false;
		var $table = $('#company-posts-list');
		dt = $table.DataTable({
			bProcessing: true,
			ordering: false,
			sAjaxSource: $table.data('url'),
            language: {
            	"processing": "Подождите...",
				"search": "",
				"lengthMenu": "Показать _MENU_ записей",
				"info": "Записи с _START_ до _END_ из _TOTAL_ записей",
				"infoEmpty": "Записи с 0 до 0 из 0 записей",
				"infoFiltered": "(отфильтровано из _MAX_ записей)",
				"infoPostFix": "",
				"loadingRecords": "Загрузка записей...",
				"zeroRecords": "Записи отсутствуют.",
				"emptyTable": "В таблице отсутствуют данные",
				"paginate": {
					"first": "Первая",
					"previous": "Предыдущая",
					"next": "Следующая",
					"last": "Последняя"
				},
				"aria": {
					"sortAscending": ": активировать для сортировки столбца по возрастанию",
					"sortDescending": ": активировать для сортировки столбца по убыванию"
				}
            },
            "columnDefs": [
            	{	
	            	"targets": [0],
	            	"visible": false
	            },
            	{	
	            	"targets": [1],
	            	"className": "text-center"
	            },
	            {	
	            	"targets": [3],
	            	"className": "text-center"
	            },
	            {	
	            	"targets": [4],
	            	"className": "text-center"
	            },
	            {	
	            	"targets": [5],
	            	"className": "text-center"
	            },
	            {	
	            	"targets": [6],
	            	"className": "text-center"
	            }
	            ,
	            {	
	            	"targets": [7],
	            	"className": "text-center"
	            }
	        ]
		});
		$(document).on('click', 'a.company-post-edit', function(e) { 
			e.preventDefault();

			var id = $(this).data('id');
			var group_id = $(this).data('group-id');
			var viewed = $(this).data('viewed');
			var vk_link = $(this).data('vk-link');

			$("#form-post-edit input[name='vk_link']").val(vk_link);
			$("#form-post-edit input[name='viewed']").val(viewed);
			$("#form-post-edit input[name='company_post_id']").val(id);
			$("#form-post-edit input[name='company_post_group_id']").val(group_id);

			$.magnificPopup.open({
				items: {
					src: '#dialog-edit',
					type: 'inline'
				},
				preloader: false,
				modal: true,
				callbacks: {
					open: function() { 
						
					},
					close: function() {
						$('#dialog-edit #dialogEditConfirm').off('click');
					}
				}
			});
		});

		$('#dialog-edit #dialogEditCancel').on( 'click', function( e ) {
			e.preventDefault();
			$.magnificPopup.close();
			checked_report.trigger('click');
		});
		$('#dialog-edit #dialogEditConfirm').on( 'click', function( e ) {
			e.preventDefault();

			$.ajax({
				type: "POST",
				url: "/company/company-edit-post",
				data: $("#form-post-edit input"),
				dataType: "html",
				success: function (html) {
					//$("#dialog .modal-text").html(html);

					setTimeout(function(){
						$.magnificPopup.close();
						checked_report.trigger('click');
					}, 500)
					
				},
				error: function(xhr, ajaxOptions, thrownError) {
		            
		        }
			})

			
		});

		$table.on('click', 'a.company-post-report', function(e) { 
			e.preventDefault();

			checked_report = $(this);
			var $row = $(this).closest('tr');
			var id = dt.row($(this).closest('tr')).data()["0"];
			$("#report-post-id").text(id);

			var data = {
				_token: function() {
	            	return $('meta[name="csrf-token"]').attr('content');
	          	},
	          	id: id,
			}

			$.ajax({
				type: "POST",
				url: "/company/post-report",
				data: data,
				dataType: "html",
				success: function (html) {
					$("#dialog .modal-text").html(html);
				},
				error: function(xhr, ajaxOptions, thrownError) {
		            
		        }
			})

			$.magnificPopup.open({
				items: {
					src: '#dialog',
					type: 'inline'
				},
				preloader: false,
				modal: true,
				callbacks: {
					open: function() { 
						
					},
					close: function() {
						$('#dialog #dialogConfirm').off('click');
					}
				}
			});
		});

		$('#dialog #dialogCancel').on( 'click', function( e ) {
			e.preventDefault();
			$.magnificPopup.close();
		});

		$table.on('click', 'a.remove-row', function(e) { 
			e.preventDefault();

			var $row = $(this).closest('tr');
			var id = dt.row($(this).closest('tr')).data()["0"];

			$.magnificPopup.open({
				items: {
					src: '#dialog-remove',
					type: 'inline'
				},
				preloader: false,
				modal: true,
				callbacks: {
					open: function() { 
						$('#dialog-remove #dialogRemoveConfirm').on( 'click', function( e ) {
							e.preventDefault();

							$.magnificPopup.close();
							$row.hide('slow', function(){ 
								dt.row($row.get(0)).remove(); 
							});

							$('.panel .panel-body').prepend('<div class="alert alert-success success-absolute"><i class="fa fa-check-circle"></i> Пост <span>'+id+'</span> удален.</div>');

							setTimeout(function () {
								$('.success-absolute').hide('slow', function(){ 
									$(this).remove(); 
								});
							}, 2000);

							var data = {
								_token: function() {
					            	return $('meta[name="csrf-token"]').attr('content');
					          	},
					          	id: id
							}

							$.ajax({
								url: '/company/drop-post',
								type: 'post',
								data: data,
								dataType: 'json',
								success: function(json) {
									//$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
								},
						        error: function(xhr, ajaxOptions, thrownError) {
						            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						        }
							});

							
						});
					},
					close: function() {
						$('#dialog-remove #dialogRemoveConfirm').off('click');
					}
				}
			});
		});

		$('#dialog-remove #dialogRemoveCancel').on( 'click', function( e ) {
			e.preventDefault();
			$.magnificPopup.close();
		});

		$table.on('click', 'a.change-company-row', function(e) { 
			e.preventDefault();

			var $row = $(this).closest('tr');
			var id = dt.row($(this).closest('tr')).data()["0"];
			$("#company-post-id").text(id);
			$(".alert-danger, .alert-success").remove();
			$.magnificPopup.open({
				items: {
					src: '#modal-change-company-for-post',
					type: 'inline'
				},
				preloader: false,
				modal: true,
				callbacks: {
					open: function() { 
						
					},
					close: function() {
						//$('#dialog-remove #dialogRemoveConfirm').off('click');
					}
				}
			});
		});

		$table.on('click', 'a.copy-company-row', function(e) { 
			e.preventDefault();

			var $row = $(this).closest('tr');
			var id = dt.row($(this).closest('tr')).data()["0"];
			$("#copy-company-post-id").text(id);
			$(".alert-danger, .alert-success").remove();
			$.magnificPopup.open({
				items: {
					src: '#modal-copy-company-for-post',
					type: 'inline'
				},
				preloader: false,
				modal: true,
				callbacks: {
					open: function() { 
						
					},
					close: function() {
						//$('#dialog-remove #dialogRemoveConfirm').off('click');
					}
				}
			});
		});
	};

	$(function() {
		datatableInit();

		$(document).on('click', "#recalculate", function(){
			var post_id = $(this).data('post-id');

			var data = {
				_token: function() {
	            	return $('meta[name="csrf-token"]').attr('content');
	          	},
	          	id: post_id
			}

			$(this).button('loading');
			
			$.ajax({
				url: '/vk/post-single-stat',
				type: 'post',
				data: data,
				dataType: 'json',
				success: function(json) {
					$.ajax({
						type: "POST",
						url: "/company/post-report",
						data: data,
						dataType: "html",
						success: function (html) {
							$("#dialog .modal-text").html(html);
							$(this).button('reset');
						},
						error: function(xhr, ajaxOptions, thrownError) {
				            
				        }
					})
				},
		        error: function(xhr, ajaxOptions, thrownError) {
		            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		            $(this).button('reset');
		        }
			});
		});

		$(document).on('click', "#recalculate-story", function(){
			var post_id = $(this).data('post-id');

			var data = {
				_token: function() {
	            	return $('meta[name="csrf-token"]').attr('content');
	          	},
	          	id: post_id
			}

			$(this).button('loading');
			
			$.ajax({
				url: '/vk/post-single-stat-story',
				type: 'post',
				data: data,
				dataType: 'json',
				success: function(json) {
					$.ajax({
						type: "POST",
						url: "/company/post-report",
						data: data,
						dataType: "html",
						success: function (html) {
							$("#dialog .modal-text").html(html);
							$(this).button('reset');
						},
						error: function(xhr, ajaxOptions, thrownError) {
				            
				        }
					})
				},
		        error: function(xhr, ajaxOptions, thrownError) {
		            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		            $(this).button('reset');
		        }
			});
		})

		$("#do-transport").click(function(){
			$(".alert-danger, .alert-success").remove();
			var company_post_id = $("#company-post-id").text();
			var to_company_id = $("#to-change").val();

			var data = {
				_token: function() {
	            	return $('meta[name="csrf-token"]').attr('content');
	          	},
	          	company_post_id: company_post_id,
	          	to_company_id: to_company_id
			}

			var $this = $(this);
			$this.button('loading');

			$.ajax({
				url: '/company/move-company-post',
				type: 'post',
				data: data,
				dataType: 'json',
				success: function(json) {
					$this.button('reset');
					if (json == 1){
						$("#modal-change-company-for-post .modal-text").append(`<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<strong>Готово!</strong> Пост перенесен
						</div>`);

						setTimeout(function(){
							$.magnificPopup.close();
							dt.ajax.reload();
						}, 2000)
					}
					else{
						$("#modal-change-company-for-post .modal-text").append(`<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<strong>Ошибка!</strong> Такой рекламной компании не существует.
						</div>`);
					}
				},
		        error: function(xhr, ajaxOptions, thrownError) {
		            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		            $(this).button('reset');
		        }
			});
		});


		$("#do-copy").click(function(){
			$(".alert-danger, .alert-success").remove();
			var company_post_id = $("#copy-company-post-id").text();
			var to_company_id = $("#to-copy").val();

			var data = {
				_token: function() {
	            	return $('meta[name="csrf-token"]').attr('content');
	          	},
	          	company_post_id: company_post_id,
	          	to_company_id: to_company_id
			}

			var $this = $(this);
			$this.button('loading');

			$.ajax({
				url: '/company/copy-company-post',
				type: 'post',
				data: data,
				dataType: 'json',
				success: function(json) {
					$this.button('reset');
					if (json == 1){
						$("#modal-copy-company-for-post .modal-text").append(`<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<strong>Готово!</strong> Пост скопирован
						</div>`);

						setTimeout(function(){
							$.magnificPopup.close();
							dt.ajax.reload();
						}, 2000)
					}
					else{
						$("#modal-copy-company-for-post .modal-text").append(`<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<strong>Ошибка!</strong> Такой рекламной компании не существует.
						</div>`);
					}
				},
		        error: function(xhr, ajaxOptions, thrownError) {
		            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		            $(this).button('reset');
		        }
			});
		});

		$(document).on('change', '.post-tag', function(){
			var tag_id = $(this).val();
			var post_id = $(this).data('post-id');

			var $this = $(this);
			$(this).prop('disabled', true);

			var data = {
				_token: function() {
	            	return $('meta[name="csrf-token"]').attr('content');
	          	},
	          	tag_id: tag_id, 
	          	post_id: post_id
			}

			$.ajax({
				url: '/company/change-post-id',
				type: 'post',
				data: data,
				dataType: 'json',
				success: function(json) {
					$this.prop('disabled', false);
				},
		        error: function(xhr, ajaxOptions, thrownError) {
		            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		            $(this).button('reset');
		        }
			});
		});

		$(document).on('change', '.post-ttag', function(){
			var tag_id = $(this).val();
			var post_id = $(this).data('post-id');

			var $this = $(this);
			$(this).prop('disabled', true);

			var data = {
				_token: function() {
	            	return $('meta[name="csrf-token"]').attr('content');
	          	},
	          	tag_id: tag_id, 
	          	post_id: post_id
			}

			$.ajax({
				url: '/company/change-ppost-id',
				type: 'post',
				data: data,
				dataType: 'json',
				success: function(json) {
					$this.prop('disabled', false);
				},
		        error: function(xhr, ajaxOptions, thrownError) {
		            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		            $(this).button('reset');
		        }
			});
		});

		$(document).on('keyup', '.post-tag-quantity', function(){
			var quantity = $(this).val();
			if (!quantity)
				return;
			var post_id = $(this).data('post-id');

			var $this = $(this);
			$(this).prop('disabled', true);

			var data = {
				_token: function() {
	            	return $('meta[name="csrf-token"]').attr('content');
	          	},
	          	quantity: quantity, 
	          	post_id: post_id,
	          	type: 'tag_quantity'
			}

			$.ajax({
				url: '/company/change-post-quantity',
				type: 'post',
				data: data,
				dataType: 'json',
				success: function(json) {
					$this.prop('disabled', false);
				},
		        error: function(xhr, ajaxOptions, thrownError) {
		            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		            $(this).button('reset');
		        }
			});
		});

		$(document).on('keyup', '.post-ttag-quantity', function(){
			var quantity = $(this).val();
			if (!quantity)
				return;
			var post_id = $(this).data('post-id');

			var $this = $(this);
			$(this).prop('disabled', true);

			var data = {
				_token: function() {
	            	return $('meta[name="csrf-token"]').attr('content');
	          	},
	          	quantity: quantity, 
	          	post_id: post_id,
	          	type: 'ttag_quantity'
			}

			$.ajax({
				url: '/company/change-post-quantity',
				type: 'post',
				data: data,
				dataType: 'json',
				success: function(json) {
					$this.prop('disabled', false);
				},
		        error: function(xhr, ajaxOptions, thrownError) {
		            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		            $(this).button('reset');
		        }
			});
		});
	});

}).apply( this, [ jQuery ]);