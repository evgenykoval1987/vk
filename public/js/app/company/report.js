var token = document.head.querySelector('meta[name="csrf-token"]');
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

var dt = false;

var app = new Vue({
	el: '#template',
	data: {
		template: '',
		target: ''
	},
	methods: {
		update: function(){
			var vm = this;
			axios.get('/company/report-settings')
		    .then(function (response) {
		        vm.template = response.data.template;
		        vm.target = response.data.target;
		    });
		},
		save: function(type){ 
			vm = this;
			axios.post('/company/report-settings-save', {
				template: vm.template,
				target: vm.target
				
			}).then(function (response) {
		        vm.update();
		        dt.ajax.reload();
		    });
		}
	},
	created: function(){
		this.update();
	}
});



(function($) {
	'use strict';

	

	var datatableInit = function() {
		var $table = $('#report-list');
		dt = $table.DataTable({
			bProcessing: true,
			ordering: false,
			sAjaxSource: $table.data('url'),
            language: {
            	"processing": "Подождите...",
				"search": "",
				"lengthMenu": "Показать _MENU_ записей",
				"info": "Записи с _START_ до _END_ из _TOTAL_ записей",
				"infoEmpty": "Записи с 0 до 0 из 0 записей",
				"infoFiltered": "(отфильтровано из _MAX_ записей)",
				"infoPostFix": "",
				"loadingRecords": "Загрузка записей...",
				"zeroRecords": "Записи отсутствуют.",
				"emptyTable": "В таблице отсутствуют данные",
				"paginate": {
					"first": "Первая",
					"previous": "Предыдущая",
					"next": "Следующая",
					"last": "Последняя"
				},
				"aria": {
					"sortAscending": ": активировать для сортировки столбца по возрастанию",
					"sortDescending": ": активировать для сортировки столбца по убыванию"
				}
            },
            "columnDefs": [
	            {	
	            	"targets": [3],
	            	"className": "text-center"
	            },
	            {	
	            	"targets": [5],
	            	"className": "text-center"
	            }
	        ]
		});
		$table.on('click', 'a.remove-row', function(e) { 
			e.preventDefault();

			var $row = $(this).closest('tr');
			var id = dt.row($(this).closest('tr')).data()["0"];
			var company = dt.row($(this).closest('tr')).data()["1"]
			$.magnificPopup.open({
				items: {
					src: '#dialog',
					type: 'inline'
				},
				preloader: false,
				modal: true,
				callbacks: {
					open: function() { 
						$('#dialog #dialogConfirm').on( 'click', function( e ) {
							e.preventDefault();

							$.magnificPopup.close();
							$row.hide('slow', function(){ 
								dt.row($row.get(0)).remove(); 
							});

							$('#report-list').before('<div class="alert alert-success success-absolute"><i class="fa fa-check-circle"></i> Отчет по рекламной компании <span>'+company+'</span> удален.</div>');

							setTimeout(function () {
								$('.success-absolute').hide('slow', function(){ 
									$(this).remove(); 
								});
							}, 2000);

							var data = {
								_token: function() {
					            	return $('meta[name="csrf-token"]').attr('content');
					          	},
					          	id: id
							}

							$.ajax({
								url: '/company/report-drop',
								type: 'post',
								data: data,
								dataType: 'json',
								success: function(json) {
									
								},
						        error: function(xhr, ajaxOptions, thrownError) {
						            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						        }
							});

							
						});
					},
					close: function() {
						$('#dialog #dialogConfirm').off('click');
					}
				}
			});
		});

		$('#dialog #dialogCancel').on( 'click', function( e ) {
			e.preventDefault();
			$.magnificPopup.close();
		});

		$(document).on('click', '.gather-activity', function(){
			var $this = $(this);
			var url = $(this).attr('href');

			$.ajax({
				url: url,
				type: 'get',
				dataType: 'json',
				beforeSend: function() {
					$this.button('loading');
				},
				complete: function() {
					$this.button('reset');
				},
				success: function(json) {
					
				},
		        error: function(xhr, ajaxOptions, thrownError) {
		            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		        }
			});

			return false;
		})
	};

	$(document).on('click','.do-readed', function(){
		$.ajax({
			url: $(this).attr('href'),
			type: 'get',
			data: {},
			dataType: 'json',
			success: function(json) {
				dt.ajax.reload();
				updateCounters();
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});

		return false;
	})

	$(document).on('click', '.copy-clipboard', function(){
		var text = $(this).data('text');
		var $temp = $("<input>");
  		$("body").append($temp);
  		$temp.val(text).select();
  		document.execCommand("copy");
  		$temp.remove();

  		$(this).after('<div class="copied">Скопировано!</div>');
  		setTimeout("$('.copied').remove()", 1000);

  		return false;
	})

	$(document).on('click','.generate-report', function(){
		var $this = $(this);
		var id = $(this).data('id');

		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	id: id,
		}

		$.ajax({
			type: "POST",
			url: "/company/generate-client-report",
			data: data,
			dataType: "html",
			success: function (html) { 
				$this.replaceWith(html);
			},
			error: function(xhr, ajaxOptions, thrownError) {
	            
	        }
		})

		return false;
	})

	$(function() {
		datatableInit();
	});

}).apply( this, [ jQuery ]);