var token = document.head.querySelector('meta[name="csrf-token"]');
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

var dt = false;

(function($) {
	'use strict';

	

	var datatableInit = function() {
		var $table = $('#report-list');
		dt = $table.DataTable({
			bProcessing: true,
			ordering: false,
			sAjaxSource: $table.data('url'),
            language: {
            	"processing": "Подождите...",
				"search": "",
				"lengthMenu": "Показать _MENU_ записей",
				"info": "Записи с _START_ до _END_ из _TOTAL_ записей",
				"infoEmpty": "Записи с 0 до 0 из 0 записей",
				"infoFiltered": "(отфильтровано из _MAX_ записей)",
				"infoPostFix": "",
				"loadingRecords": "Загрузка записей...",
				"zeroRecords": "Записи отсутствуют.",
				"emptyTable": "В таблице отсутствуют данные",
				"paginate": {
					"first": "Первая",
					"previous": "Предыдущая",
					"next": "Следующая",
					"last": "Последняя"
				},
				"aria": {
					"sortAscending": ": активировать для сортировки столбца по возрастанию",
					"sortDescending": ": активировать для сортировки столбца по убыванию"
				}
            },
            "columnDefs": [
	            {	
	            	"targets": [3,2,4],
	            	"className": "text-center"
	            },

	        ]
		});
	};

	
	$(document).on('click','.generate-report', function(){
		var $this = $(this);
		var id = $(this).data('id');

		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	id: id,
		}

		$.ajax({
			type: "POST",
			url: "/companies/generate-client-report",
			data: data,
			dataType: "html",
			success: function (html) { 
				$this.replaceWith(html);
			},
			error: function(xhr, ajaxOptions, thrownError) {
	            
	        }
		})

		return false;
	})

	$(function() {
		datatableInit();
	});

}).apply( this, [ jQuery ]);