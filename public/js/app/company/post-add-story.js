(function($) {
	'use strict';

	var current_group_id = false;

	$(".more-text").click(function(){
		$(this).parent().before('<textarea class="form-control post-description" rows="5" id="description" name="description[]" placeholder="Текст"></textarea>');
	});


	$(".company_posts_table .group-checkbox").each(function(){
		if ($(this).attr('id') != 'all'){
			var group_id = $(this).data('group-id');
			console.log(group_id);

			var data = {
				_token: function() {
	            	return $('meta[name="csrf-token"]').attr('content');
	          	},
	          	group_id: group_id
			}

			$.ajax({
				type: "POST",
				url: "/company/init-group-shedule-story",
				data: data,
				dataType: "html",
				success: function (html) {
					$("[data-group-id="+group_id+"]").parent().parent().parent().find('.group-shedule').text(html);
					$("[data-group-id="+group_id+"]").parent().parent().parent().find('.group-shedule-val').val(html);
				},
				error: function(xhr, ajaxOptions, thrownError) {
		        }
			});
		}
	});

	$(".package").change(function(){
		var package_date = $(this).data('package-date');
		if ($(this).prop('checked') == true){
			$(this).parent().find('.pg').each(function(){
				var val = $(this).val();
				$("#group-"+val).prop('checked', true);
			})

			/*var datestring = package_date.split(' ')[0];
			var time = package_date.split(' ')[1];*/
			
			$(".company_posts_table .group-checkbox").each(function(){
				if ($(this).attr('id') != 'all' && $(this).prop('checked') == true){
					if (package_date[$(this).data('group-id')] !== undefined){
						var datestring = package_date[$(this).data('group-id')].split(' ')[0];
						var time = package_date[$(this).data('group-id')].split(' ')[1];

						check_posts($(this).data('group-id'), datestring, time, $(this).parent().parent().parent().find('.next_date'));
					}
					//check_posts($(this).data('group-id'), datestring, time, $(this).parent().parent().parent().find('.next_date'));*/
				}
			})
		}
		else{
			$(this).parent().find('.pg').each(function(){
				var val = $(this).val();
				$("#group-"+val).prop('checked', false);
			})
		}
	});

	$("#send-post").click(function(){
		$(this).attr('disabled',true);
		$(".validation-message").remove();
		$.ajax({
			type: "POST",
			url: "/company/check-group-date-count-story",
			data: $("#post-add-form input:checked, #post-add-form input[name=\'_token\']"),
			dataType: "json",
			success: function (json) {
				if (json.success == true){
					//alert();
					$("#post-add-form").submit();
				}
				else{
					var html = '<div class="validation-message"><ul style="display: block;">';
					for (var err in json.error){
						html += '<li><label class="error" style="display: inline;">'+json.error[err]+'</label></li>';
					}
					html += '</ul></div>';
					$("#post-add-form .panel-body").prepend(html);
					$("html, body").animate({ scrollTop: 0 }, "slow");
					$("#send-post").attr('disabled',false);
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
	           
	        }
		})

		return false;
	})

	$("#post-add-form").validate({
		highlight: function( label ) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function( label ) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function( error, element ) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		}
	});

	$(".group-checkbox").change(function(){
		var checked = $(this).prop('checked');
		$(this).parent().parent().parent().find('.group-shedule-val').prop('checked', checked);
	})

	$(".edit-row").click(function(){
		var group_id = $(this).data('group-id');
		current_group_id = group_id;

		$.magnificPopup.open({
			items: {
				src: '#dialog',
				type: 'inline'
			},
			preloader: false,
			modal: true,
			callbacks: {
				open: function(){
					var $shedule = $("[data-group-id="+group_id+"]").parent().parent().find('.group-shedule');
					var dt = $shedule.text();
					$("#group-turn").text(dt);
					if ($shedule.data('shedule') != 0){
						$("#shedule-next").show();
					}
					else{
						$("#shedule-next").hide();
					}
				}
			}
		});
	})
	/*$('.change_date').click(function(){
		$(this).parent().find('.group-date').datepicker('show');
	})*/
	/*$('.change_date').datepicker({
		format: 'yyyy-mm-dd',
		language: 'ru',
		startDate: new Date() ,
		autoclose: true
	}).on('changeDate', function(ev){
		var dt = ev.date;
		var datestring = dt.getDate()  + "." + ("0" + (dt.getMonth() + 1)).slice(-2) + "." + dt.getFullYear();
		$(ev.currentTarget).parent().find('.group-date').val(datestring);
	});*/

	$(".change_time").click(function(){
		$(this).parent().find('.group-time').timepicker('showWidget');
	})

	setTimeout(function(){
		$('[data-plugin-datepicker]').datepicker().on('changeDate', function(e) {
	        if($(this).parent().find('.change_time')){
	        	$(this).datepicker('hide');
	        	$(this).parent().find('.change_time').trigger('click');
	        }
	    })
	}, 1000);

	$("#not-time").change(function(){
		if ($(this).prop('checked'))
			$(this).parent().parent().find(".change_time").attr('disabled', true);
		else
			$(this).parent().parent().find(".change_time").attr('disabled', false);
	});

	$(".next_date_all").click(function(){ 
		var dt = $(this).parent().find('.change_date').datepicker('getDate');
		if (dt == 'Invalid Date'){
			alert('Дата не выбрана');
			return;
		}
		var datestring = dt.getFullYear()+"-"+("0" + (dt.getMonth() + 1)).slice(-2)+"-"+dt.getDate();
		var time = $(this).parent().find('.group-time').val();
		var datetime = datestring+' '+time;

		var not_time = $("#not-time").prop('checked');
		
		var btn = $(this);
		btn.button('loading');
		$(".company_posts_table .group-checkbox").each(function(){
			if ($(this).attr('id') != 'all' && $(this).prop('checked') == true){
				if (!not_time)
					check_posts($(this).data('group-id'), datestring, time, $(this).parent().parent().parent().find('.next_date'));
				else{
					set_posts($(this).data('group-id'), datestring, $(this).parent().parent().parent().find('.next_date'));
				}
			}
		})
		btn.button('reset');
		/*var group_id = btn.parent().parent().find('.group-checkbox').data('group-id');
		check_posts(group_id, datestring, time, btn);*/
	})

	function set_posts(group_id, date, btn){
		$(".alert-danger").remove();

		btn.button('loading');
		var current_shedule = date+' 00:00';
		
		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	group_id: group_id,
          	shedule: current_shedule
		}
		
		$.ajax({
			type: "POST",
			url: "/company/check-group-shedule-story",
			data: data,
			dataType: "html",
			success: function (html) {
				btn.button('reset');
				$("[data-group-id="+group_id+"]").parent().parent().parent().find('.group-shedule').text(html);
				$("[data-group-id="+group_id+"]").parent().parent().parent().find('.group-shedule-val').val(html);
			},
			error: function(xhr, ajaxOptions, thrownError) {
	            btn.button('reset');
				btn.before('<div class="alert alert-danger"><strong>Ошибка!</strong> Выбранное время занято. </div>');
	        }
		})

		return false;
	}
	

	$(".next_date").click(function(){ 

		var dt = $(this).parent().find('.change_date').datepicker('getDate');
		if (dt == 'Invalid Date'){
			alert('Дата не выбрана');
			return;
		}
		var datestring = dt.getFullYear()+"-"+("0" + (dt.getMonth() + 1)).slice(-2)+"-"+dt.getDate();
		var time = $(this).parent().find('.group-time').val();
		var datetime = datestring+' '+time;
		
		var btn = $(this);

		var group_id = btn.parent().parent().find('.group-checkbox').data('group-id');
		check_posts(group_id, datestring, time, btn);
	})

	function check_posts(group_id, date, time, btn){
		$(".alert-danger").remove();
		
		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	group_id: group_id,
          	date: date,
          	time: time
		}
		if (date == '' || time == ''){
			btn.before('<div class="alert alert-danger"><strong>Ошибка!</strong> Указаны не все параметры. </div>');
			return;
		}
		btn.button('loading');

		$.ajax({
			type: "POST",
			url: "/company/check-group-date-story",
			data: data,
			dataType: "json",
			success: function (json) {
				btn.button('reset');
				if (json.success == true)
					//btn.before('<div class="alert alert-danger"><strong>Ошибка!</strong> Выбранное время занято. </div>');
				alert('Ошибка! Выбранное время занято для группы '+group_id+'.');
				else{
					var arr = date.split('-');
					var dt = arr[2]+'.'+arr[1]+'.'+arr[0]+' '+time;

					$("[data-group-id="+group_id+"]").parent().parent().parent().find('.group-shedule').text(dt);
					$("[data-group-id="+group_id+"]").parent().parent().parent().find('.group-shedule-val').val(dt);
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
	            btn.button('reset');
				alert('Ошибка! Выбранное время занято.');
	        }
		})
	}

	$(".next_shedule").click(function(){
		$(".alert-danger").remove();
		var btn = $(this);
		btn.button('loading');
		var group_id = btn.parent().parent().find('.group-checkbox').data('group-id');
		var $shedule = $("[data-group-id="+group_id+"]").parent().parent().parent().find('.group-shedule');
		var dt = $shedule.text();
		var current_shedule = dt;

		var data = {
			_token: function() {
            	return $('meta[name="csrf-token"]').attr('content');
          	},
          	group_id: group_id,
          	shedule: current_shedule
		}
		
		$.ajax({
			type: "POST",
			url: "/company/check-group-shedule-story",
			data: data,
			dataType: "html",
			success: function (html) {
				btn.button('reset');
				$("[data-group-id="+group_id+"]").parent().parent().parent().find('.group-shedule').text(html);
				$("[data-group-id="+group_id+"]").parent().parent().parent().find('.group-shedule-val').val(html);
			},
			error: function(xhr, ajaxOptions, thrownError) {
	            btn.button('reset');
				btn.before('<div class="alert alert-danger"><strong>Ошибка!</strong> Выбранное время занято. </div>');
	        }
		})

		return false;
	})

	$("#all").click(function(){
		var prop = $(this).prop('checked');
		$(".checkbox-default .group-checkbox").prop('checked', prop).trigger('change');
	})

	$("#atach").vkeditor();

	$("#repost").change(function(){
		var flag = $(this).prop('checked');

		if(flag){
			$("#repost-link").parent().parent().show();
			$("#atach a").attr('disabled',true);
		}
		else{
			$("#repost-link").parent().parent().hide();
			$("#atach a").attr('disabled',false);
		}
	})

	$(".next_date_all_days_story_four").click(function(){ 
		var btn = $(this);
		var k = 0;
		$(".alert-danger").remove();

		var date_start = $(this).parent().find('.change_date').datepicker('getDate');
		date_start = date_start.getFullYear()+"-"+("0" + (date_start.getMonth() + 1)).slice(-2)+"-"+date_start.getDate();

		$(".company_posts_table .group-checkbox").each(function(){
			if ($(this).attr('id') != 'all' && $(this).prop('checked') == true){
				var group_id = $(this).data('group-id');

				var data = {
					_token: function() {
		            	return $('meta[name="csrf-token"]').attr('content');
		          	},
		          	group_id: group_id,
		          	k: k,
		          	date_start: date_start
				}

				btn.button('loading');

				$.ajax({
					type: "POST",
					url: "/company/check-group-date-days-story",
					data: data,
					dataType: "html",
					success: function (html) {
						btn.button('reset');
						$("[data-group-id="+group_id+"]").parent().parent().parent().find('.group-shedule').text(html);
						$("[data-group-id="+group_id+"]").parent().parent().parent().find('.group-shedule-val').val(html);
					},
					error: function(xhr, ajaxOptions, thrownError) {
			            btn.button('reset');
						alert('Ошибка! Выбранное время занято.');
			        }
				})

				k++;
				if (k == 14)
					k = 0
			}
		})
	})

	$(".next_date_all_days_story_seven").click(function(){ 
		var btn = $(this);
		var k = 0;
		$(".alert-danger").remove();

		var date_start = $(this).parent().find('.change_date').datepicker('getDate');
		date_start = date_start.getFullYear()+"-"+("0" + (date_start.getMonth() + 1)).slice(-2)+"-"+date_start.getDate();

		$(".company_posts_table .group-checkbox").each(function(){
			if ($(this).attr('id') != 'all' && $(this).prop('checked') == true){
				var group_id = $(this).data('group-id');

				var data = {
					_token: function() {
		            	return $('meta[name="csrf-token"]').attr('content');
		          	},
		          	group_id: group_id,
		          	k: k,
		          	date_start: date_start
				}

				btn.button('loading');

				$.ajax({
					type: "POST",
					url: "/company/check-group-date-days-story",
					data: data,
					dataType: "html",
					success: function (html) {
						btn.button('reset');
						$("[data-group-id="+group_id+"]").parent().parent().parent().find('.group-shedule').text(html);
						$("[data-group-id="+group_id+"]").parent().parent().parent().find('.group-shedule-val').val(html);
					},
					error: function(xhr, ajaxOptions, thrownError) {
			            btn.button('reset');
						alert('Ошибка! Выбранное время занято.');
			        }
				})

				k++;
				if (k == 7)
					k = 0
			}
		})
	})

	$(".next_date_all_days_story_twoseven").click(function(){ 
		var btn = $(this);
		var k = 0;
		$(".alert-danger").remove();

		var date_start = $(this).parent().find('.change_date').datepicker('getDate');
		date_start = date_start.getFullYear()+"-"+("0" + (date_start.getMonth() + 1)).slice(-2)+"-"+date_start.getDate();

		$(".company_posts_table .group-checkbox").each(function(){
			if ($(this).attr('id') != 'all' && $(this).prop('checked') == true){
				var group_id = $(this).data('group-id');

				var data = {
					_token: function() {
		            	return $('meta[name="csrf-token"]').attr('content');
		          	},
		          	group_id: group_id,
		          	k: k,
		          	date_start: date_start
				}

				btn.button('loading');

				$.ajax({
					type: "POST",
					url: "/company/check-group-date-days-story",
					data: data,
					dataType: "html",
					success: function (html) {
						btn.button('reset');
						$("[data-group-id="+group_id+"]").parent().parent().parent().find('.group-shedule').text(html);
						$("[data-group-id="+group_id+"]").parent().parent().parent().find('.group-shedule-val').val(html);
					},
					error: function(xhr, ajaxOptions, thrownError) {
			            btn.button('reset');
						alert('Ошибка! Выбранное время занято.');
			        }
				})

				k++;
				if (k == 27)
					k = 0
			}
		})
	})

}).apply( this, [ jQuery ]);