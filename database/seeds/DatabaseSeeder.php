<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
       /*$role_employee = new Role();
	    $role_employee->slug = 'admin';
	    $role_employee->name = 'Администратор';
	    $role_employee->save();

	    $role_manager = new Role();
	    $role_manager->slug = 'editor';
	    $role_manager->name = 'Редактор';
	    $role_manager->save();*/

	    $role_admin = Role::where('slug', 'admin')->first()->id;
	    $role_editor  = Role::where('slug', 'editor')->first()->id;

	    $admin = new User();
	    $admin->name = 'Артем Хоменко';
	    $admin->login = 'khomenko';
	    $admin->password = bcrypt('khomenko');
	    $admin->role_id = $role_admin;
	    $admin->save();

	    $admin = new User();
	    $admin->name = 'Евгений Коваль';
	    $admin->login = 'koval';
	    $admin->password = bcrypt('svetlana');
	    $admin->role_id = $role_admin;
	    $admin->save();

	    $editor = new User();
	    $editor->name = 'Артем Редактор';
	    $editor->login = 'khomenko-r';
	    $editor->password = bcrypt('khomenko-r');
	    $editor->role_id = $role_editor;
	    $editor->save();
    }
}
