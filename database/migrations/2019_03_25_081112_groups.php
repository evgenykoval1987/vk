<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Groups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url', 255);
            $table->text('name');
            $table->string('code', 255);
            $table->string('idd', 255);
            $table->string('token', 500)->nullable();
            $table->string('image', 500)->nullable();
            $table->bigInteger('users_current')->unsigned();
            $table->bigInteger('users_before')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
