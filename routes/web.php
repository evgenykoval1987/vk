<?php
/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'Auth\LoginController@showLoginForm')->middleware('auth')->name('auth');
Auth::routes();

Route::get('/home', 'Panel\HomeController@index');
Route::get('/panel', 'Panel\HomeController@index')->name('home');
Route::post('/add_user_token', 'Panel\HomeController@addUserToken')->name('add_user_token');
Route::post('/add_user_audio_token', 'Panel\HomeController@addUserAudioToken')->name('add_user_audio_token');
Route::post('/group/callback', 'Panel\GroupController@statistic')->name('groups-callback'); 
Route::get('/monitor', 'Panel\MonitoringController@spider');
Route::get('/unpin', 'Panel\CompanyController@unpin');
Route::get('/company-make-reports', 'Panel\CompanyController@makeReport');
Route::get('/company-dowload-report/{company_id}', 'Panel\CompanyController@downloadReport')->name('company-dowload-report');

Route::group([
		'prefix' => 'user',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\UserController@index')->name('users');
		Route::get('/ajax', 'Panel\UserController@ajax')->name('users-ajax');
		Route::get('/add', 'Panel\UserController@addForm')->name('users-add');
		Route::post('/add', 'Panel\UserController@addUser')->name('users-save');
		Route::get('/edit/{user_id}', 'Panel\UserController@editForm')->name('users-edit');
		Route::post('/edite/{user_id}', 'Panel\UserController@editUser')->name('users-save-edit');
		Route::post('/check', 'Panel\UserController@checkLogin');
		Route::post('/drop', 'Panel\UserController@dropUser');
		Route::get('/groups/{user_id}', 'Panel\UserController@groups');
		Route::get('/shedule-groups', 'Panel\UserController@groupList');
		Route::post('/remove-shedule', 'Panel\UserController@removeShedule');
		Route::post('/add-shedule', 'Panel\UserController@addShedule');
	}
);
Route::group([
		'prefix' => 'group',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\GroupController@index')->name('groups');
		Route::get('/ajax', 'Panel\GroupController@ajax')->name('groups-ajax');
		Route::get('/add', 'Panel\GroupController@addForm')->name('groups-add');
		Route::post('/add', 'Panel\GroupController@addGroup')->name('groups-save');
		Route::post('/edit/{group_id}', 'Panel\GroupController@editGroupConfirm');
		Route::post('/editUser/{group_id}', 'Panel\GroupController@editUser');
		/*Route::post('/check', 'Panel\UserController@checkLogin');*/
		Route::post('/drop', 'Panel\GroupController@drop');
		Route::get('/price/{group_id}', 'Panel\GroupController@prices')->name('groups-prices');
		Route::post('/price-save/{group_id}', 'Panel\GroupController@pricesSave')->name('groups-prices-save');
		Route::get('/packages', 'Panel\GroupController@packages')->name('packages');
		Route::get('/packages-add', 'Panel\GroupController@addPackagesForm')->name('packages-add');
		Route::get('/packages-ajax', 'Panel\GroupController@packagesAjax')->name('packages-ajax');
		Route::post('/packages-drop', 'Panel\GroupController@packageDrop');
		Route::post('/packages-add', 'Panel\GroupController@addPackage')->name('packages-save');
		Route::get('/packages-edit/{post_id}', 'Panel\GroupController@editPackageForm')->name('packages-edit');
		Route::post('/packages-edit/{post_id}', 'Panel\GroupController@editPackage')->name('packages-save-edit');
	}
);

Route::group([
		'prefix' => 'manage',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\ManageController@index')->name('manage');
	}
);

Route::group([
		'prefix' => 'post',
		'middleware' => ['auth']
	], function(){
		Route::get('/', 'Panel\PostController@index')->name('posts');
		Route::get('/ajax', 'Panel\PostController@ajax')->name('posts-ajax');
		Route::get('/add', 'Panel\PostController@addForm')->name('posts-add');
		Route::get('/edit/{post_id}', 'Panel\PostController@editForm')->name('posts-edit');
		Route::post('/add', 'Panel\PostController@addPost')->name('posts-save');
		Route::post('/edit', 'Panel\PostController@editPost')->name('posts-esave');
		Route::post('/edit-ajax', 'Panel\PostController@editPostAjax')->name('posts-save-ajax');
		Route::post('/drop', 'Panel\PostController@drop');
		Route::post('/strike', 'Panel\PostController@strike');
		Route::get('/pay-request', 'Panel\PostController@payRequest')->name('posts-pay-request');

		Route::get('/pendings', 'Panel\PostController@pendings')->name('pendings');
		Route::get('/pendings/ajax', 'Panel\PostController@pendingsAjax')->name('pendings-ajax');

		Route::get('/post-errors', 'Panel\PostController@errors')->name('post-errors');
		Route::get('/post-errors/ajax', 'Panel\PostController@errorsAjax')->name('post-errors-ajax');

		Route::post('/to-manage', 'Panel\PostController@toManage');
	}
);

Route::group([
		'prefix' => 'shedule',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\SheduleController@index')->name('shedule');
		Route::get('/groups', 'Panel\SheduleController@groups');
		Route::post('/add', 'Panel\SheduleController@add');
		Route::post('/remove', 'Panel\SheduleController@remove');
		Route::post('/getByGroup', 'Panel\SheduleController@getByGroup');
	}
);

Route::group([
		'prefix' => 'vk'
	], function(){
		Route::post('/publish', 'Panel\VkController@publish');
		Route::post('/publishVkTurn', 'Panel\VkController@publishVkTurn');
		Route::post('/publishTurn', 'Panel\VkController@publishTurn');
		Route::get('/groupStat', 'Panel\VkController@groupStat');
		Route::get('/cronPublish', 'Panel\VkController@cronPublish');
		Route::post('/editorAccessToken', 'Panel\VkController@editorAccessToken');
		Route::get('/postStat', 'Panel\VkController@postStat');
		Route::get('/check-audio-token', 'Panel\VkController@checkAudioToken');
		Route::post('/post-single-stat', 'Panel\VkController@postSingleStat');
		Route::get('/check-token', 'Panel\VkController@checkToken');
		Route::get('/rubbish', 'Panel\VkController@rubbish');

		Route::get('/storyStat', 'Panel\VkController@storyStat');
		Route::post('/post-single-stat-story', 'Panel\VkController@postSingleStatStory');
		Route::get('/story-posting', 'Panel\VkController@storyPosting');
		Route::get('/refresh-stat', 'Panel\VkController@refreshStat');
	}
);

Route::group([
		'prefix' => 'client',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\ClientController@index')->name('client');
		Route::get('/add', 'Panel\ClientController@addForm')->name('client-add');
		Route::post('/add', 'Panel\ClientController@addClient')->name('client-save');
		Route::get('/ajax', 'Panel\ClientController@ajax')->name('client-ajax');
		Route::post('/drop', 'Panel\ClientController@dropClient');
		Route::get('/edit/{client_id}', 'Panel\ClientController@editForm')->name('client-edit');
		Route::post('/edit', 'Panel\ClientController@editClient')->name('client-esave');

		Route::get('/last', 'Panel\ClientController@last')->name('client-last');
		Route::post('/last-save', 'Panel\ClientController@lastEdit')->name('client-last-save');
		Route::get('/last-drop-all', 'Panel\ClientController@lastDropAll')->name('client-last-remove-all');
		Route::get('/client-last-ajax', 'Panel\ClientController@lastAjax')->name('client-last-ajax');
		Route::get('/client-last-remove-one/{id}', 'Panel\ClientController@lastDropOne')->name('client-last-remove-one');
		Route::get('/unload-clients', 'Panel\ClientController@unloadClients')->name('client-unload');
		Route::get('/unload-groups', 'Panel\ClientController@unloadGroups')->name('group-unload');

		Route::post('/add-user', 'Panel\ClientController@addClientUser')->name('client-user-save');
	}
);


Route::group([
		'prefix' => 'manager',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\ManagerController@index')->name('manager');
		Route::get('/add', 'Panel\ManagerController@addForm')->name('manager-add');
		Route::post('/add', 'Panel\ManagerController@addManager')->name('manager-save');
		Route::get('/ajax', 'Panel\ManagerController@ajax')->name('manager-ajax');
		Route::post('/drop', 'Panel\ManagerController@dropManager');
		Route::get('/edit/{client_id}', 'Panel\ManagerController@editForm')->name('manager-edit');
		Route::post('/edit', 'Panel\ManagerController@editManager')->name('manager-esave');
	}
);

Route::group([
		'prefix' => 'reminder',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\ReminderController@index')->name('reminder');
		Route::get('/add', 'Panel\ReminderController@addForm')->name('reminder-add');
		Route::post('/add', 'Panel\ReminderController@add')->name('reminder-save');
		Route::get('/ajax', 'Panel\ReminderController@ajax')->name('reminder-ajax');
		Route::post('/drop', 'Panel\ReminderController@drop');
		Route::post('/close', 'Panel\ReminderController@close');
	}
);

Route::group([
		'prefix' => 'company',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\CompanyController@index')->name('companys');
		Route::get('/add', 'Panel\CompanyController@addForm')->name('company-add');
		Route::post('/add', 'Panel\CompanyController@addCompany')->name('company-save');
		Route::get('/ajax', 'Panel\CompanyController@ajax')->name('company-ajax');
		Route::get('/edit/{company_id}', 'Panel\CompanyController@editForm')->name('company-edit');
		Route::post('/drop', 'Panel\CompanyController@dropCompany');
		Route::post('/edit', 'Panel\CompanyController@editCompany')->name('company-esave');
		Route::get('/posts/{company_id}', 'Panel\CompanyController@posts')->name('company-posts');
		Route::get('/posts/{company_id}/add', 'Panel\CompanyController@addPostForm')->name('company-post-add');
		Route::get('/posts/{company_id}/addNative', 'Panel\CompanyController@addPostFormNative')->name('company-post-add-native');
		Route::post('/posts/{company_id}/add', 'Panel\CompanyController@addPost')->name('company-post-save');
		Route::get('/posts/ajax/{company_id}', 'Panel\CompanyController@ajaxPosts')->name('company-posts-ajax');
		Route::post('/check-group-date', 'Panel\CompanyController@checkGroupDate');
		Route::post('/check-group-date-native', 'Panel\CompanyController@checkGroupDateNative');
		Route::post('/check-group-shedule', 'Panel\CompanyController@checkGroupShedule');
		Route::post('/check-group-shedule-native', 'Panel\CompanyController@checkGroupSheduleNative');
		Route::post('/post-report', 'Panel\CompanyController@getPostReport');
		Route::post('/drop-post', 'Panel\CompanyController@dropPost');
		Route::post('/check-group-date-count', 'Panel\CompanyController@checkGroupDateCount');
		Route::post('/check-group-date-count-native', 'Panel\CompanyController@checkGroupDateCountNative');
		Route::post('/post-report-link', 'Panel\CompanyController@generatePostReport');
		Route::post('/company-edit-post', 'Panel\CompanyController@companyEditPost');
		Route::get('/companys-end', 'Panel\CompanyController@ended')->name('companys-end');
		Route::get('/companys-end-ajax', 'Panel\CompanyController@endedAjax')->name('companys-end-ajax');
		Route::get('/companys-ended-remove-one/{id}', 'Panel\CompanyController@endedDropOne')->name('companys-ended-remove-one');
		Route::get('/pin', 'Panel\CompanyController@pin')->name('company-pin');
		Route::post('/pin-add', 'Panel\CompanyController@pinAdd')->name('company-pin-add');
		Route::post('/check-group-date-days', 'Panel\CompanyController@checkGroupDateByDays');
		Route::get('/timable', 'Panel\CompanyController@timable')->name('company-timable');
		Route::post('/timable-table', 'Panel\CompanyController@timableTable')->name('company-timable-table');
		Route::post('/check-group-date-days-native', 'Panel\CompanyController@checkGroupDateByDaysNative');
		Route::get('/report', 'Panel\CompanyController@report')->name('company-report');
		Route::get('/report-ajax', 'Panel\CompanyController@reportAjax')->name('company-report-ajax');
		Route::get('/report-readed/{company_id}', 'Panel\CompanyController@reportReaded')->name('company-report-readed');
		Route::post('/report-drop', 'Panel\CompanyController@dropReport');
		Route::get('/report-settings', 'Panel\CompanyController@reportSettings');
		Route::post('/report-settings-save', 'Panel\CompanyController@reportSettingsSave');
		Route::get('/report-activity/{company_id}', 'Panel\CompanyController@reportActivity')->name('company-report-activity');
		Route::post('/generate-client-report', 'Panel\CompanyController@generateCompanyClientReport');
		Route::post('/move-company-post', 'Panel\CompanyController@moveCompanyPost');
		Route::post('/copy-company-post', 'Panel\CompanyController@copyCompanyPost');
		Route::get('/company-active', 'Panel\CompanyController@active')->name('company-active');
		Route::get('/company-ajax-active', 'Panel\CompanyController@ajaxActive')->name('company-ajax-active');

		Route::get('/posts/{company_id}/addStory', 'Panel\CompanyController@addPostFormStory')->name('company-post-add-story');
		Route::post('/check-group-date-count-story', 'Panel\CompanyController@checkGroupDateCountStory');
		Route::post('/check-group-date-days-story', 'Panel\CompanyController@checkGroupDateByDaysStory');
		Route::post('/check-group-shedule-story', 'Panel\CompanyController@checkGroupSheduleStory');
		Route::post('/check-group-date-story', 'Panel\CompanyController@checkGroupDateStory');

		Route::post('/change-post-id', 'Panel\CompanyController@changePostId')->name('change-post-id');
		Route::post('/change-ppost-id', 'Panel\CompanyController@changePPostId')->name('change-ppost-id');
		Route::post('/change-post-quantity', 'Panel\CompanyController@changePostQuantityTag')->name('change-post-quantity');

		Route::post('/init-group-shedule', 'Panel\CompanyController@initGroupShedule');
		Route::post('/init-group-shedule-native', 'Panel\CompanyController@initGroupSheduleNative');
		Route::post('/init-group-shedule-story', 'Panel\CompanyController@initGroupSheduleStory');

		Route::get('/posts/{company_id}/makeReportSingle', 'Panel\CompanyController@makeReportSingle')->name('company-make-single-report');
		Route::get('/posts/{company_id}/removeReportSingle', 'Panel\CompanyController@removeReportSingle')->name('company-remove-single-report');
	}

);



Route::group([
		'prefix' => 'vkattach',
		'middleware' => ['auth']
	], function(){
		Route::get('/picture/{op}', 'Panel\AttachController@picture')->name('attach-picture');
		Route::get('/picture/{op}/{account_id}', 'Panel\AttachController@picture')->name('attach-picture-get-albums');

		Route::get('/video/{op}', 'Panel\AttachController@video')->name('attach-video');
		Route::get('/video/{op}/{account_id}', 'Panel\AttachController@video')->name('attach-video-get-albums');

		Route::get('/audio/{op}', 'Panel\AttachController@audio')->name('attach-audio');
		Route::get('/audio/{op}/{account_id}', 'Panel\AttachController@audio')->name('attach-audio-get-albums');

		Route::get('/playlist/{op}', 'Panel\AttachController@playlist')->name('attach-playlist');

		Route::get('/doc/{op}', 'Panel\AttachController@doc')->name('attach-doc');
		Route::get('/doc/{op}/{account_id}', 'Panel\AttachController@doc')->name('attach-doc-get-albums');

		Route::post('/picture/{op}', 'Panel\AttachController@picture')->name('attach-picture-post');
		Route::put('/picture/{op}', 'Panel\AttachController@picture')->name('attach-picture-put');
		Route::post('/video/{op}', 'Panel\AttachController@video')->name('attach-video-post');
		Route::put('/video/{op}', 'Panel\AttachController@video')->name('attach-video-put');
		Route::post('/audio/{op}', 'Panel\AttachController@audio')->name('attach-audio-post');
		Route::put('/audio/{op}', 'Panel\AttachController@audio')->name('attach-audio-put');
		Route::post('/doc/{op}', 'Panel\AttachController@doc')->name('attach-doc-post');
		Route::put('/doc/{op}', 'Panel\AttachController@doc')->name('attach-doc-put');
	}
);

Route::group([
		'prefix' => 'messages',
		'middleware' => ['auth']
	], function(){
		Route::get('/', 'Panel\MessageController@index')->name('messages');
		Route::get('/ajax', 'Panel\MessageController@ajax')->name('messages-ajax');
		Route::post('/drop', 'Panel\MessageController@drop');
		Route::get('/drop-all', 'Panel\MessageController@dropAll')->name('messages-remove-all');
		Route::get('/answer/{message_id}', 'Panel\MessageController@answer')->name('messages-edit');
		Route::post('/send-answer', 'Panel\MessageController@sendAnswer')->name('messages-answer');
		Route::get('/get-count', 'Panel\MessageController@counters')->name('messages-counters');
	}
);


Route::group([
		'prefix' => 'answer',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\AnswerController@index')->name('answer');
		Route::get('/add', 'Panel\AnswerController@addForm')->name('answer-add');
		Route::post('/add', 'Panel\AnswerController@addAnswer')->name('answer-save');
		Route::get('/ajax', 'Panel\AnswerController@ajax')->name('answer-ajax');
		Route::post('/drop', 'Panel\AnswerController@dropAnswer');
		Route::get('/edit/{answer_id}', 'Panel\AnswerController@editForm')->name('answer-edit');
		Route::post('/edit', 'Panel\AnswerController@editAnswer')->name('answer-esave');
	}
);

Route::group([
		'prefix' => 'sentences',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\SentenceController@index')->name('sentences');
		Route::get('/ajax', 'Panel\SentenceController@ajax')->name('sentences-ajax');
		Route::get('/drop-all', 'Panel\SentenceController@dropAll')->name('sentences-remove-all');
		Route::get('/drop-one/{sentence_id}', 'Panel\SentenceController@dropOne')->name('sentences-remove-one');
	}
);

Route::group([
		'prefix' => 'monitoring',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\MonitoringController@index')->name('monitoring');
		Route::post('/add', 'Panel\MonitoringController@add')->name('monitoring-save');
		Route::get('/ajax', 'Panel\MonitoringController@ajax')->name('monitoring-ajax');
		Route::get('/drop-one/{id}', 'Panel\MonitoringController@dropOne')->name('monitoring-remove-one');
		Route::get('/drop-all', 'Panel\MonitoringController@dropAll')->name('monitoring-remove-all');
	}
);

Route::group([
		'prefix' => 'transactions',
		'middleware' => ['auth']
	], function(){
		Route::get('/', 'Panel\TransactionController@index')->name('transactions');
		/*Route::post('/add', 'Panel\MonitoringController@add')->name('monitoring-save');
		Route::get('/ajax', 'Panel\MonitoringController@ajax')->name('monitoring-ajax');*/
	}
);


Route::group([
		'prefix' => 'pays',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\PayController@index')->name('pays');
		Route::get('/table', 'Panel\PayController@table')->name('pays-table');
		Route::get('/ajax', 'Panel\PayController@ajax')->name('pays-ajax');
		Route::get('/info/{pay_id}', 'Panel\PayController@info')->name('pays-info');
		Route::get('/paid/{pay_id}', 'Panel\PayController@paid')->name('pays-paid');
		Route::get('/price', 'Panel\PayController@price')->name('pays-price');
		Route::post('/price-edit', 'Panel\PayController@priceEdit')->name('pays-price-edit');
		Route::post('/pays-save', 'Panel\PayController@saveMin')->name('pays-save');
	}
);

Route::group([
		'prefix' => 'stat',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\StatisticController@index')->name('statistic');
		Route::post('statistic-ajax', 'Panel\StatisticController@ajax')->name('statistic-ajax');
	}
);

Route::group([
		'prefix' => 'rating',
		'middleware' => ['auth']
	], function(){
		Route::get('/', 'Panel\RatingController@index')->name('rating');
		Route::get('/ajax', 'Panel\RatingController@ajax')->name('rating-ajax');
	}
);

Route::group([
		'prefix' => 'videos',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\VideoController@index')->name('video');
		Route::get('/add', 'Panel\VideoController@addForm')->name('video-add');
		Route::post('/add', 'Panel\VideoController@addVideo')->name('video-save');
		Route::get('/ajax', 'Panel\VideoController@ajax')->name('video-ajax');
		Route::post('/drop', 'Panel\VideoController@dropVideo');
		Route::get('/edit/{client_id}', 'Panel\VideoController@editForm')->name('video-edit');
		Route::post('/edit', 'Panel\VideoController@editVideo')->name('video-esave');
	}
);

Route::group([
		'prefix' => 'news',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\NewController@index')->name('news');
		Route::get('/add', 'Panel\NewController@addForm')->name('new-add');
		Route::post('/add', 'Panel\NewController@addNew')->name('new-save');
		Route::get('/ajax', 'Panel\NewController@ajax')->name('new-ajax');
		Route::post('/drop', 'Panel\NewController@dropNew');
		Route::get('/edit/{client_id}', 'Panel\NewController@editForm')->name('new-edit');
		Route::post('/edit', 'Panel\NewController@editNew')->name('new-esave');
	}
);

Route::group([
		'prefix' => 'chat',
		'middleware' => ['auth']
	], function(){
		Route::get('/', 'Panel\ChatController@index')->name('chat');
		Route::get('/data', 'Panel\ChatController@data')->name('data');
		Route::post('/send', 'Panel\ChatController@send')->name('send');
		Route::post('/unread', 'Panel\ChatController@unread')->name('unread');
		/*Route::get('/add', 'Panel\VideoController@addForm')->name('video-add');
		Route::post('/add', 'Panel\VideoController@addVideo')->name('video-save');
		Route::get('/ajax', 'Panel\VideoController@ajax')->name('video-ajax');
		Route::post('/drop', 'Panel\VideoController@dropVideo');
		Route::get('/edit/{client_id}', 'Panel\VideoController@editForm')->name('video-edit');
		Route::post('/edit', 'Panel\VideoController@editVideo')->name('video-esave');*/
	}
);

Route::group([
		'prefix' => 'analytic',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\AnalyticController@index')->name('analytic');
		Route::post('/add', 'Panel\AnalyticController@add');
		Route::get('/ajax', 'Panel\AnalyticController@ajax')->name('analytic-ajax');
		Route::post('/drop', 'Panel\AnalyticController@drop');
		Route::post('/edit', 'Panel\AnalyticController@edit');
		Route::post('/do', 'Panel\AnalyticController@do');
		Route::post('/topicture', 'Panel\AnalyticController@topicture');
	}
);

Route::group([
		'prefix' => 'companies',
		'middleware' => ['auth']
	], function(){
		Route::get('/my', 'Panel\CompanyController@clientIndex')->name('client-companies');
		Route::get('/report-client-ajax', 'Panel\CompanyController@reportAjaxClient')->name('company-client-report-ajax');
		Route::post('/generate-client-report', 'Panel\CompanyController@generateCompanyClientReport');
	}
);

Route::group([
		'prefix' => 'tag',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\TagController@index')->name('tag');
		Route::get('/add', 'Panel\TagController@addForm')->name('tag-add');
		Route::post('/add', 'Panel\TagController@addTag')->name('tag-save');
		Route::get('/ajax', 'Panel\TagController@ajax')->name('tag-ajax');
		Route::post('/drop', 'Panel\TagController@dropTag');
		Route::get('/edit/{tag_id}', 'Panel\TagController@editForm')->name('tag-edit');
		Route::post('/edit', 'Panel\TagController@editTag')->name('tag-esave');
	}
);

Route::group([
		'prefix' => 'tagpack',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\TagpackController@index')->name('tagpack');
		Route::get('/add', 'Panel\TagpackController@addForm')->name('tagpack-add');
		Route::post('/add', 'Panel\TagpackController@addTagpack')->name('tagpack-save');
		Route::get('/ajax', 'Panel\TagpackController@ajax')->name('tagpack-ajax');
		Route::post('/drop', 'Panel\TagpackController@dropTagpack');
		Route::get('/edit/{tagpack_id}', 'Panel\TagpackController@editForm')->name('tagpack-edit');
		Route::post('/edit', 'Panel\TagpackController@editTagpack')->name('tagpack-esave');
	}
);

Route::group([
		'prefix' => 'taggroup',
		'middleware' => ['onlyadmin','auth']
	], function(){
		Route::get('/', 'Panel\TaggroupController@index')->name('taggroup');
		Route::get('/add', 'Panel\TaggroupController@addForm')->name('taggroup-add');
		Route::post('/add', 'Panel\TaggroupController@addTaggroup')->name('taggroup-save');
		Route::get('/ajax', 'Panel\TaggroupController@ajax')->name('taggroup-ajax');
		Route::post('/drop', 'Panel\TaggroupController@dropTaggroup');
		Route::get('/edit/{taggroup_id}', 'Panel\TaggroupController@editForm')->name('taggroup-edit');
		Route::post('/edit', 'Panel\TaggroupController@editTaggroup')->name('taggroup-esave');
	}
);