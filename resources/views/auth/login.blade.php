@extends('layouts.auth')

@section('content')
    <section class="body-sign">
        <div class="center-sign">
            <a href="{{ route('auth') }}" class="logo pull-left">
                <img src="{{ asset('images/logon.png') }}" height="54" alt="Porto Admin" />
            </a>
            <div class="panel panel-sign">
                <div class="panel-title-sign mt-xl text-right">
                    <h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Авторизация</h2>
                </div>

                <div class="panel-body">
                    <form method="POST" action="{{ route('login') }}">
                        {!! csrf_field() !!}

                        @if ($errors->has('login'))
                            <div class="alert alert-danger" role="alert">
                                <strong>Ошибка!</strong> {{ $errors->first('login') }}
                            </div>
                        @endif
                        @if ($errors->has('password'))
                            <div class="alert alert-danger" role="alert">
                                <strong>Ошибка!</strong> {{ $errors->first('password') }}
                            </div>
                        @endif

                        <div class="form-group mb-lg">
                            <label>Логин</label>
                            <div class="input-group input-group-icon">
                                <input name="login" type="text" class="form-control input-lg" />
                                <span class="input-group-addon">
                                    <span class="icon icon-lg">
                                        <i class="fa fa-user"></i>
                                    </span>
                                </span>
                            </div>
                        </div>

                        <div class="form-group mb-lg">
                            <div class="clearfix">
                                <label class="pull-left">Пароль</label>
                            </div>
                            <div class="input-group input-group-icon">
                                <input name="password" type="password" class="form-control input-lg" />
                                <span class="input-group-addon">
                                    <span class="icon icon-lg">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                </span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 text-right">
                                <button type="submit" class="btn btn-primary hidden-xs">Войти</button>
                                <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Войти</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <p class="text-center text-muted mt-md mb-md">&copy; Copyright 2019. All rights reserved. v. 0.0.1</p>
        </div>
    </section>
@endsection
