<aside id="sidebar-left" class="sidebar-left">
    <div class="sidebar-header">
        <div class="sidebar-title">
            Navigation
        </div>
        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                    @foreach ($menu as $key => $item)
                        @if ($item['children'])
                            <li class="nav-parent {{ ($item['active']) ? 'nav-expanded' : '' }}">
                                <a>
                                    @if(isset($item['counter']))
                                        <span class="pull-right label label-danger" id="counter-{{$item['counter']}}"></span>
                                    @endif
                                    <i class="{{ $item['icon'] }}" aria-hidden="true"></i>
                                    <span>{{ $item['title'] }}</span>
                                </a>
                                <ul class="nav nav-children">
                                    @foreach ($item['children'] as $k => $child)
                                        <li {{ ($child['active']) ? 'class=nav-active' : '' }}>
                                            <a href="{{ $child['link'] }}">
                                                @if(isset($child['counter']))
                                                    <span class="pull-right label label-danger" id="counter-{{$child['counter']}}"></span>
                                                @endif
                                                <span>{{ $child['title'] }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @else
                            <li {{ ($item['active']) ? 'class=nav-active' : '' }}>
                                <a href="{{ $item['link'] }}">
                                    @if(isset($item['counter']))
                                        <span class="pull-right label label-danger" id="counter-{{$item['counter']}}">{{$item['count']}}</span>
                                    @endif
                                    <i class="{{ $item['icon'] }}" aria-hidden="true"></i>
                                    <span>{{ $item['title'] }}</span>
                                </a>
                            </li>
                        @endif
                    @endforeach
                    
                </ul>
            </nav>
        </div>
    </div>
</aside>