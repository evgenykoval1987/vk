<!-- Vendor -->
<script src="{{ asset('vendor/jquery/jquery.js') }}"></script>
<script src="{{ asset('vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ asset('vendor/nanoscroller/nanoscroller.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-datepicker/js/locales/bootstrap-datepicker.ru.js') }}"></script>
<script src="{{ asset('vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ asset('vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>
<script src="https://vk.com/js/api/openapi.js?160" type="text/javascript"></script>
<script type="text/javascript">
	console.log(window.VK );
	if (window.VK !== undefined) {
		VK.init({
		    apiId: {{ $app_id }}
		});

		VK.Auth.getLoginStatus(function(r) {
			console.log(r);
			if (r.status == 'not_authorized' || r.status == 'unknown'){
				VK.Auth.login();
			}
		});
		
	}
</script>

@stack('scripts')
<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
<script src="{{ asset('js/forms/jquery.form.js') }}"></script>
<!-- Theme Base, Components and Settings -->
<script src="{{ asset('js/theme.js') }}"></script>

<!-- Theme Custom -->
<script src="{{ asset('js/theme.custom.js') }}"></script>

<!-- Theme Initialization Files -->
<script src="{{ asset('js/theme.init.js') }}"></script>

{{--<script src="{{ asset('js/ui-elements/examples.modals.js') }}"></script>--}}

<script type="text/javascript" src="https://code.createjs.com/1.0.0/soundjs.min.js"></script>
<!-- Examples -->
<script src="{{ asset('js/panel.js') }}?v=24"></script>
