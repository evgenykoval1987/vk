<div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
    	<li><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
    	@foreach ($breadcrumbs as $key => $breadcrumb)
    		@if ($key != count($breadcrumbs)-1)
				<li><a href="{{$breadcrumb['link']}}">{{$breadcrumb['title']}}</a></li>
			@else
				<li><span class="current">{{$breadcrumb['title']}}</span></li>
			@endif
		@endforeach
    </ol>
</div>