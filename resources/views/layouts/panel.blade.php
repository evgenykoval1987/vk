<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    @include('partials.styles')

    <!-- Head Libs -->
    <script src="{{ asset('vendor/modernizr/modernizr.js') }}"></script>
</head>
<body>
    <section class="body">
        <header class="header">
            <div class="logo-container">
                <a href="{{ route('home') }}" class="logo">
                    <img src="{{ asset('images/logon.png') }}" height="35" alt="JSOFT Admin" />
                </a>
                <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
                    <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                </div>
            </div>
            <div class="header-right">
                <div id="userbox" class="userbox" style="margin-top: 10px">
                    <a href="#" data-toggle="dropdown">
                        <figure class="profile-picture">
                            <img src="{{ asset('images/!logged-user.jpg') }}" alt="Joseph Doe" class="img-circle" data-lock-picture="{{ asset('images/!logged-user.jpg') }}" />
                        </figure>
                        <div class="profile-info" data-lock-name="{{ Auth::user()->name }}">
                            <span class="name">{{ Auth::user()->name }}</span>
                            <span class="role">{{ Auth::user()->role->name }}</span>
                        </div>
        
                        <i class="fa custom-caret"></i>
                    </a>
        
                    <div class="dropdown-menu">
                        <ul class="list-unstyled">
                            <li class="divider"></li>
                            <li>
                                <a role="menuitem" tabindex="-1" href="#" id="logout"><i class="fa fa-power-off"></i> Выход</a>
                                <form action="{{ route('logout') }}" method="POST" style="display: none;" id="logout-form">
                                    {!! csrf_field() !!}
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <div class="inner-wrapper">
            @include('partials.sidebar')
            <section role="main" class="content-body">
                <header class="page-header">
                    <h2>@yield('h1')</h2>
                    
                    @yield('breadcrumbs')
                </header>
                @yield('content')
                
            </section>
        </div>
    </section>
    {{--@if($access == 'admin-new' || $access == 'admin-audio')--}}
        <a class="mb-xs mt-xs mr-xs token-button btn btn-default" href="#modalToken" style="display: none;">Basic</a>
        <div id="modalToken" class="modal-block mfp-hide">
            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Получение права на постинг в сообществах</h2>
                </header>
                <div class="panel-body">
                    <div class="modal-wrapper">
                        <div class="modal-text">
                            @if($type_of_token == 'client')
                                <p>1. Получить токен перейдя по <a href="#" id="get-client-token">ссылке</a></p>
                            @else
                                <p>1. Получить токен перейдя по <a href="#" id="get-admin-token">ссылке</a></p>
                            @endif 
                            <p>2. Скопировать URL из адресной строки окна, открытого по ссылке п.1, после подтверждения прав доступа</p>
                            <p>3. Вставить URL в текстовое поле ниже.</p>
                            <p>4. Нажать кнопку "Подтвердить"</p>
                        </div>
                    </div>
                    <form class="form-horizontal mb-lg" id="admin-token-form" action="{{ route('add_user_token') }}" method="POST">
                        {!! csrf_field() !!}
                        <div class="form-group mt-lg">
                            <div class="col-sm-12">
                                <input type="text" name="user_token" class="form-control" placeholder="URL скопированный из окна" required/>
                            </div>
                        </div>
                    </form>
                </div>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-primary modal-confirm" onClick="$('#admin-token-form').submit();">Подтвердить</button>
                        </div>
                    </div>
                </footer>
            </section>
        </div>
        <a class="mb-xs mt-xs mr-xs audio-token-button btn btn-default" href="#modalAudioToken" style="display: none;">Basic</a>
        <div id="modalAudioToken" class="modal-block mfp-hide">
            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Получение токена для работы с аудиозаписями</h2>
                    
                </header>
                <div class="panel-body">
                    <div class="modal-wrapper">
                        <div class="modal-text">
                            <p>Введите свой логин и пароль vk.com</p>
                        </div>
                    </div>
                    <form class="form-horizontal mb-lg" id="admin-audio-token-form" action="{{ route('add_user_audio_token') }}" method="POST">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="login">Логин</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="login" name="login">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="password">Пароль</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" id="password" name="password">
                            </div>
                        </div>
                    </form>
                </div>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-primary modal-confirm" onClick="$('#admin-audio-token-form').submit();">Получить токен</button>
                        </div>
                    </div>
                </footer>
            </section>
        </div>
    {{--@endif--}}

    <div id="audio-success" class="modal-block mfp-hide">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Проверка Аудио токена</h2>
            </header>
            <div class="panel-body">
                <div class="modal-wrapper">
                    <div class="modal-text">
                        <div class="alert alert-success" id="audio-success-text">
                            Токен <strong>работает</strong> и получает аудиозаписи.
                        </div>
                        <div class="alert alert-danger" id="audio-error-text">
                            Токен <strong>не работает!!!!</strong> Обновите токен.
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div id="token-success" class="modal-block mfp-hide">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Проверка токена</h2>
            </header>
            <div class="panel-body">
                <div class="modal-wrapper">
                    <div class="modal-text">
                        <div class="alert alert-success" id="token-success-text">
                            Токен <strong>работает</strong>.
                        </div>
                        <div class="alert alert-danger" id="token-error-text">
                            Токен <strong>не работает!!!!</strong> Обновите токен.
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    @push('scripts')
        <script type="text/javascript">
            var app_id = '{{ $app_id }}';
            var access = '{{ $access }}';
        </script>
    @endpush

    @if($access == 'editor-new')
        @push('scripts')
            <script type="text/javascript">
                if(window.location.hash) {
                    var hash = window.location.hash.substring(1); 
                    var access_token = hash.substr(hash.search(/(?<=^|&)access_token=/))
                                  .split('&')[0]
                                  .split('=')[1];
                    if (access_token) {
                        var data = {
                            _token: function() {
                                return $('meta[name="csrf-token"]').attr('content');
                            },
                            vk_token: access_token
                        }
                        $.ajax({
                            type: "POST",
                            url: "/vk/editorAccessToken",
                            data: data,
                            dataType: "json",
                            success: function (json) {
                               document.location.href('http://rmocrm.top/panel');
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                               //document.location.reload();
                            }
                        })
                    }
                }
                else{
                    if ({{$access}} == 'editor-new'){
                        var app_id = '{{ $app_id }}';
                        var access = '{{ $access }}';
                        document.location.href = "https://oauth.vk.com/authorize?client_id="+app_id+"&display=popup&redirect_uri={{route('home')}}&scope=photos,video,audio,email,offline&response_type=token&v=5.92&revoke=1";
                    }
                }
            </script>
        @endpush
    @endif

    @include('partials.scripts') 
</body>
</html>
