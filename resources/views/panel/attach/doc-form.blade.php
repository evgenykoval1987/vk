<div id="ddialog" class="modal-block">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Документы</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<div class="tabs">
						<ul class="nav nav-tabs text-right tabs-primary">
							<li class="active">
								<a href="#vk" data-toggle="tab">Vk.com</a>
							</li>
						</ul>
						<div class="tab-content">
							<div id="vk" class="tab-pane active">
								<div class="form-group">
									<label class="col-md-3 control-label" for="group-id">Страница</label>
									<div class="col-md-9">
										<select class="form-control input-sm mb-md" id="group-id">
											<option value="">-</option>
											<option value="{{route('attach-doc-get-albums',['get-albums',$data['user_vk_id']])}}">Моя страница</option>
											@foreach($data['groups'] as $group)
												<option value="{{route('attach-doc-get-albums',['get-albums','-'.$group->id])}}">{{$group->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								
								<div class="form-group" id="doc_photos">
									<div class="vk-doc-area">
										
									</div>
									<div id="vk-doc-container">
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					{{--<button id="dialogConfirm" class="btn btn-primary">Удалить</button>--}}
					<div class="vk-doc-total">
						<b>Выбранных документов: <span id="vk-doc-total">0</span></b>
					</div>
					<button class="btn btn-primary" id="vk-add-doc">Применить</button>
					<button class="btn btn-default modal-close">Закрыть</button>
				</div>
			</div>
		</footer>
	</section>
</div>
<script type="text/javascript">
	$(function(){
		
	})
</script>