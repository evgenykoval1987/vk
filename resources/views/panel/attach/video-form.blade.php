<div id="vdialog" class="modal-block">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Видео</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<div class="tabs">
						<ul class="nav nav-tabs text-right tabs-primary">
							<li class="active">
								<a href="#local" data-toggle="tab">ПК</a>
							</li>
							@if (!$data['onlypc'])
								<li>
									<a href="#vk" data-toggle="tab">Vk.com</a>
								</li>
								<li>
									<a href="#youtube" data-toggle="tab">Youtube</a>
								</li>
							@endif
						</ul>
						<div class="tab-content">
							<div id="local" class="tab-pane active">
								<div id="vk-video-local-area">
									<div id="vk-picture-local-area">
										<div class="drop-title">Нажмите либо перетащите файлы</div>
									</div>
								</div>
							</div>
							@if (!$data['onlypc'])
								<div id="vk" class="tab-pane">
									<div class="form-group">
										<label class="col-md-3 control-label" for="group-id">Страница</label>
										<div class="col-md-9">
											<select class="form-control input-sm mb-md" id="group-id">
												<option value="">-</option>
												<option value="{{route('attach-video-get-albums',['get-albums',$data['user_vk_id']])}}">Моя страница</option>
												@foreach($data['groups'] as $group)
													<option value="{{route('attach-video-get-albums',['get-albums','-'.$group->id])}}">{{$group->name}}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" for="album-id">Альбом</label>
										<div class="col-md-9">
											<select class="form-control input-sm mb-md" id="album-id" disabled>
												<option>-</option>
											</select>
										</div>
									</div>
									<div class="form-group" id="video_videos">
										<div class="vk-video-area">
											
										</div>
										
									</div>
								</div>
								<div id="youtube" class="tab-pane">
									<b style="opacity: 0.5">Например</b>
									<ul style="margin-bottom: 40px; opacity: 0.5">
										<li>https://www.youtube.com/watch?v=pL-i4Lcgk2w</li>
									</ul>
									<div id="youtube-container" class="form-bordered">
										
									</div>
									<button type="button" class="btn btn-primary" id="video-youtube-add-item"><i class="fa fa-plus"></i></button>
								</div>
							@endif
							<div id="vk-video-container">
											
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					{{--<button id="dialogConfirm" class="btn btn-primary">Удалить</button>--}}
					<div class="vk-video-total">
						<b>Выбранных видеозаписей: <span id="vk-video-total">0</span></b>
					</div>
					<button class="btn btn-primary" id="vk-add-video">Применить</button>
					<button class="btn btn-default modal-close">Закрыть</button>
				</div>
			</div>
		</footer>
	</section>
</div>