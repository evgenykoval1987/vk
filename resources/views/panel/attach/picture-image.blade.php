@foreach($data['images'] as $image)
	<div class="vk-image">
		<input type="checkbox" name="vk_image[]" value="photo{{$image->owner_id}}_{{$image->id}}">
		<table>
			<tr>
				<td style="width: 30px">
					<img src="{{$image->thumb}}">
				</td>
				<td style="padding-left: 30px">
					<b><a href="https://vk.com/photo{{$image->owner_id}}_{{$image->id}}" target="_blank">{{$image->title}}</a></b><br>
					<b>{{$image->dt}}</b>
				</td>
			</tr>
		</table>
	</div>
@endforeach
@if($data['images'])
	<div class="vk-pagination">
		@if($data['prev'])
			<button class="btn btn-default" id="vk-prev-page"><i class="fa fa-arrow-left"></i></button>
		@else
			<button class="btn btn-default" id="vk-prev-page" disabled><i class="fa fa-arrow-left"></i></button>
		@endif
		&nbsp;
		@if($data['next'])
			<button class="btn btn-default" id="vk-next-page"><i class="fa fa-arrow-right"></i></button>
		@else
			<button class="btn btn-default" id="vk-next-page" disabled><i class="fa fa-arrow-right"></i></button>
		@endif
	</div>
@endif