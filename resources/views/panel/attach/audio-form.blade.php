<div id="adialog" class="modal-block">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Аудиозаписи</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<div class="tabs">
						<ul class="nav nav-tabs text-right tabs-primary">
							<li class="active">
								<a href="#search" data-toggle="tab">Поиск</a>
							</li>
							<li>
								<a href="#local" data-toggle="tab">ПК</a>
							</li>
							<li>
								<a href="#vk" data-toggle="tab">Vk.com</a>
							</li>
							<li>
								<a href="#manual" data-toggle="tab">Текст</a>
							</li>
						</ul>
						<div class="tab-content">
							<div id="search" class="tab-pane active">
								<div class="form-group">
									<label class="col-md-3 control-label" for="vk-audio-search">Найти</label>
									<div class="col-md-9">
										<div class="input-group mb-md">
											<input type="text" class="form-control" id="vk-audio-search">
											<span class="input-group-btn">
												<button class="btn btn-success" type="button" id="vk-audio-search-run" data-loading-text="Loading...">Найти</button>
											</span>
										</div>
									</div>
								</div>
								<p></p>
								<div class="form-group" id="audio_audios_s">
									<div class="vk-audio-area">
										
									</div>
									<div id="vk-audio-container-s">
										
									</div>
								</div>
							</div>

							<div id="local" class="tab-pane">
								<div id="vk-audio-local-area">
									<div class="drop-title">Нажмите либо перетащите файлы</div>
								</div>
							</div>
							<div id="vk" class="tab-pane">
								<div class="form-group">
									<label class="col-md-3 control-label" for="group-id">Страница</label>
									<div class="col-md-9">
										<select class="form-control input-sm mb-md" id="group-id">
											<option value="">-</option>
											<option value="{{route('attach-audio-get-albums',['get-albums',$data['user_vk_id']])}}">Моя страница</option>
											@foreach($data['groups'] as $group)
												<option value="{{route('attach-audio-get-albums',['get-albums','-'.$group->id])}}">{{$group->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="album-id">Альбом</label>
									<div class="col-md-9">
										<select class="form-control input-sm mb-md" id="album-id" disabled>
											<option>-</option>
										</select>
									</div>
								</div>
								<div class="form-group" id="audio_audios">
									<div class="vk-audio-area">
										
									</div>
									<div id="vk-audio-container">
										
									</div>
								</div>
							</div>

							<div id="manual" class="tab-pane">
								<div class="form-group">
									<textarea style="width: 100%; height: 100%" rows="10" id="manual-audios"></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<div class="vk-audio-total">
						<b>Выбранных фотографий: <span id="vk-audio-total">0</span></b>
					</div>
					<button class="btn btn-primary" id="vk-add-audio">Применить</button>
					<button class="btn btn-default modal-close">Закрыть</button>
				</div>
			</div>
		</footer>
	</section>
</div>
