<div id="dialog" class="modal-block">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Изображения</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<div class="tabs">
						<ul class="nav nav-tabs text-right tabs-primary">
							<li class="active">
								<a href="#local" data-toggle="tab">ПК</a>
							</li>
							@if (!$data['onlypc'])
								<li>
									<a href="#vk" data-toggle="tab">Vk.com</a>
								</li>
								<li>
									<a href="#url" data-toggle="tab">Ссылки</a>
								</li>
							@endif
						</ul>
						<div class="tab-content">
							<div id="local" class="tab-pane active">
								<div id="vk-picture-local-area">
									<div class="drop-title">Нажмите либо перетащите файлы</div>
								</div>
							</div>
							@if (!$data['onlypc'])
								<div id="vk" class="tab-pane">
									<div class="form-group">
										<label class="col-md-3 control-label" for="group-id">Страница</label>
										<div class="col-md-9">
											<select class="form-control input-sm mb-md" id="group-id">
												<option value="">-</option>
												<option value="{{route('attach-picture-get-albums',['get-albums',$data['user_vk_id']])}}">Моя страница</option>
												@foreach($data['groups'] as $group)
													<option value="{{route('attach-picture-get-albums',['get-albums','-'.$group->id])}}">{{$group->name}}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" for="album-id">Альбом</label>
										<div class="col-md-9">
											<select class="form-control input-sm mb-md" id="album-id" disabled>
												<option>-</option>
											</select>
										</div>
									</div>
									<div class="form-group" id="picture_photos">
										<div class="vk-picture-area">
											
										</div>
										
									</div>
								</div>
								<div id="url" class="tab-pane">
									<div id="url-picture-container" class="form-bordered">
										
									</div>
									<button type="button" class="btn btn-primary" id="picture-url-add-item"><i class="fa fa-plus"></i></button>
								</div>
							@endif
							<div id="vk-picture-container">
											
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					{{--<button id="dialogConfirm" class="btn btn-primary">Удалить</button>--}}
					<div class="vk-picture-total">
						<b>Выбранных фотографий: <span id="vk-picture-total">0</span></b>
					</div>
					<button class="btn btn-primary" id="vk-add-photo">Применить</button>
					<button class="btn btn-default modal-close">Закрыть</button>
				</div>
			</div>
		</footer>
	</section>
</div>
<script type="text/javascript">
	$(function(){
		
	})
</script>