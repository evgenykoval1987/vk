<div id="ldialog" class="modal-block">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Плейлист</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<b style="opacity: 0.5">Например</b>
					<ul style="margin-bottom: 40px; opacity: 0.5">
						<li>https://vk.com/music?z=audio_playlist-2000095183_4095183/6bd2d022d33c2e949f</li>
						<li>https://vk.com/audio?z=audio_playlist-33835321_1</li>
						<li>https://vk.com/artist/shtabelya?z=audio_playlist-2000299097_5299097%2F4689dae89534311150</li>
					</ul>
					<form class="form-horizontal form-bordered" method="POST">
						<div class="form-group">
							<label class="col-md-3 control-label" for="playlist-input">Ссылка</label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="playlist-input">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button class="btn btn-primary" id="vk-add-playlist">Применить</button>
					<button class="btn btn-default modal-close">Закрыть</button>
				</div>
			</div>
		</footer>
	</section>
</div>
