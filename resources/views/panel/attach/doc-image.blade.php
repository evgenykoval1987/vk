@foreach($data['docs'] as $doc)
	<div class="vk-doc">
		<input type="checkbox" name="vk_doc[]" value="doc{{$doc->owner_id}}_{{$doc->id}}">
		<table>
			<tr>
				<td style="width: 30px">
					<img src="{{$doc->thumb}}">
				</td>
				<td style="padding-left: 30px">
					<b><a href="https://vk.com/doc{{$doc->owner_id}}_{{$doc->id}}" target="_blank">{{$doc->title}}</a></b><br>
					<b>{{$doc->dt}}</b>
				</td>
			</tr>
		</table>
	</div>
@endforeach
@if($data['docs'])
	<div class="vk-pagination">
		@if($data['prev'])
			<button class="btn btn-default" id="vk-prev-page"><i class="fa fa-arrow-left"></i></button>
		@else
			<button class="btn btn-default" id="vk-prev-page" disabled><i class="fa fa-arrow-left"></i></button>
		@endif
		&nbsp;
		@if($data['next'])
			<button class="btn btn-default" id="vk-next-page"><i class="fa fa-arrow-right"></i></button>
		@else
			<button class="btn btn-default" id="vk-next-page" disabled><i class="fa fa-arrow-right"></i></button>
		@endif
	</div>
@endif