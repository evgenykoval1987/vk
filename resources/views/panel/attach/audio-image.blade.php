@foreach($data['audios'] as $audio)
	<div class="vk-audio">
		<input type="checkbox" name="vk_audio[]" value="audio{{$audio->owner_id}}_{{$audio->id}}">
		<table>
			<tr>
				<td style="width: 30px">
					<img src="{{$audio->thumb}}">
				</td>
				<td style="padding-left: 30px">
					<b><a href="https://vk.com/audio{{$audio->owner_id}}_{{$audio->id}}" target="_blank">{{$audio->title}}</a></b><br>
					<b>{{$audio->dt}}</b>
				</td>
			</tr>
		</table>
	</div>
@endforeach
@if($data['audios'])
	<div class="vk-pagination">
		@if($data['prev'])
			<button class="btn btn-default" id="vk-prev-page"><i class="fa fa-arrow-left"></i></button>
		@else
			<button class="btn btn-default" id="vk-prev-page" disabled><i class="fa fa-arrow-left"></i></button>
		@endif
		&nbsp;
		@if($data['next'])
			<button class="btn btn-default" id="vk-next-page"><i class="fa fa-arrow-right"></i></button>
		@else
			<button class="btn btn-default" id="vk-next-page" disabled><i class="fa fa-arrow-right"></i></button>
		@endif
	</div>
@endif