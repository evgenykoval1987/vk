@foreach($data['videos'] as $video)
	<div class="vk-video">
		<input type="checkbox" name="vk_video[]" value="video{{$video->owner_id}}_{{$video->id}}">
		<table>
			<tr>
				<td style="width: 30px">
					<img src="{{$video->thumb}}">
				</td>
				<td style="padding-left: 30px">
					<b><a href="https://vk.com/video{{$video->owner_id}}_{{$video->id}}" target="_blank">{{$video->title}}</a></b><br>
					<b>{{$video->dt}}</b>
				</td>
			</tr>
		</table>
	</div>
@endforeach
@if($data['videos'])
	<div class="vk-pagination">
		@if($data['prev'])
			<button class="btn btn-default" id="vk-prev-page"><i class="fa fa-arrow-left"></i></button>
		@else
			<button class="btn btn-default" id="vk-prev-page" disabled><i class="fa fa-arrow-left"></i></button>
		@endif
		&nbsp;
		@if($data['next'])
			<button class="btn btn-default" id="vk-next-page"><i class="fa fa-arrow-right"></i></button>
		@else
			<button class="btn btn-default" id="vk-next-page" disabled><i class="fa fa-arrow-right"></i></button>
		@endif
	</div>
@endif