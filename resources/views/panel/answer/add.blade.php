@extends('layouts.panel')

@section('title', 'VK.POSTER - Добавить заготовку ответа')

@section('h1', 'Добавить заготовку ответа')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<form class="form-horizontal form-bordered" method="POST" action="{{ $data['action'] }}" id="answer-add-form">
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">Добавить заготовку ответа</h2>
		</header>
		<div class="panel-body">
			<div class="form-group">
				<label class="col-md-3 control-label">Название</label>
				<div class="col-md-9">
					<div class="input-group input-group-icon">
						<input type="text" class="form-control" placeholder="Текст" name="text" value="{{ $data['text'] }}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Текст ответа</label>
				<div class="col-md-9">
					<div class="input-group input-group-icon">
						<textarea class="form-control" placeholder="Текст ответа" name="description">{{ $data['description'] }}</textarea>
					</div>
				</div>
			</div>
			<input type="hidden" name="answer_id" value="{{ $data['answer_id'] }}">
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Сохранить</button>
				</div>
			</div>
		</footer>
	</form>
</section>
@endsection

@push('scripts')
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
@endpush
