@extends('layouts.panel')

@section('title', 'VK.POSTER - Рассписание')

@section('h1', 'Рассписание постов')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Рассписание</h2>
	</header>
	<div class="panel-body" id="shedule-app">
		<div class="form-group">
			<label class="col-md-3 control-label" for="inputSuccess">Выберите группу</label>
			<div class="col-md-5">
				<select class="form-control input-sm mb-md" v-model="group">
			    	<option v-for="group in groups" :value="group.id">@{{group.name}}</option>
			    </select>
			</div>
		</div>
		<div v-show="group">
			<div class="form-group">
				<label class="col-md-3 control-label"><b>Расписание постов для группы</b></label>
				<div class="col-md-5">
					<b>@{{groupname}}</b>
				</div>
			</div>
			<div class="form-group">
				<div class="col-xs-12 col-sm-6 col-lg-3">
					<div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-clock-o"></i>
						</span>
						<input type="text" data-plugin-timepicker class="form-control" data-plugin-options='{ "showMeridian": false, "minuteStep": 1 }' id="time_to_add">
						<span class="input-group-btn">
							<button class="btn btn-primary" type="button" @click="addShedule('usual')">Добавить</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-sm-6" style="margin-top: 10px">
				<div class="row">
					<div class="col-xs-3 col-sm-4 col-md-2" v-for="item in items" style="margin-top: 10px;">
						<div class="shedule-item" @dblClick="removeShedule(item.id)">@{{item.value}}</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12" style="margin-top: 50px">
				<h4>Рекламное время</h4>
			</div>
			<div class="form-group">
				<div class="col-xs-12 col-sm-6 col-lg-3">
					<div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-clock-o"></i>
						</span>
						<input type="text" data-plugin-timepicker class="form-control" data-plugin-options='{ "showMeridian": false, "minuteStep": 1 }' id="time_to_add_adv">
						<span class="input-group-btn">
							<button class="btn btn-primary" type="button" @click="addShedule('adv')">Добавить</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-sm-6" style="margin-top: 10px">
				<div class="row">
					<div class="col-xs-3 col-sm-4 col-md-2" v-for="item in itemsa" style="margin-top: 10px;">
						<div class="shedule-item shedule-item-a" @dblClick="removeShedule(item.id)">@{{item.value}}</div>
					</div>
				</div>
			</div>

			<div class="col-sm-12" style="margin-top: 50px">
				<h4>Нативные посты</h4>
			</div>
			<div class="form-group">
				<div class="col-xs-12 col-sm-6 col-lg-3">
					<div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-clock-o"></i>
						</span>
						<input type="text" data-plugin-timepicker class="form-control" data-plugin-options='{ "showMeridian": false, "minuteStep": 1 }' id="time_to_add_adv_native">
						<span class="input-group-btn">
							<button class="btn btn-primary" type="button" @click="addShedule('native')">Добавить</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-sm-6" style="margin-top: 10px">
				<div class="row">
					<div class="col-xs-3 col-sm-4 col-md-2" v-for="item in itemsn" style="margin-top: 10px;">
						<div class="shedule-item shedule-item-native" @dblClick="removeShedule(item.id)">@{{item.value}}</div>
					</div>
				</div>
			</div>


			<div class="col-sm-12" style="margin-top: 50px">
				<h4>Stories</h4>
			</div>
			<div class="form-group">
				<div class="col-xs-12 col-sm-6 col-lg-3">
					<div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-clock-o"></i>
						</span>
						<input type="text" data-plugin-timepicker class="form-control" data-plugin-options='{ "showMeridian": false, "minuteStep": 1 }' id="time_to_add_adv_story">
						<span class="input-group-btn">
							<button class="btn btn-primary" type="button" @click="addShedule('story')">Добавить</button>
						</span>
					</div>
				</div>
			</div>
			<div class="col-sm-6" style="margin-top: 10px">
				<div class="row">
					<div class="col-xs-3 col-sm-4 col-md-2" v-for="item in itemss" style="margin-top: 10px;">
						<div class="shedule-item shedule-item-story" @dblClick="removeShedule(item.id)">@{{item.value}}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />
@endpush

@push('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios@0.12.0/dist/axios.min.js"></script>
	<script src="{{ asset('vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
	
	<script src="{{ asset('js/app/shedule/form.js') }}"></script>
@endpush
