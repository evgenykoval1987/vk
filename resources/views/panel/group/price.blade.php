@extends('layouts.panel')

@section('title', 'VK.POSTER - Цены группы '.$group_info->code)

@section('h1', 'Цены группы '.$group_info->code)

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($breadcrumbs)) ? $breadcrumbs : array()])
@endsection

@section('content')
<section class="panel">
	<form class="form-horizontal form-bordered" method="POST" action="{{route('groups-prices-save', $group_info->id)}}" >
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">Цены группы {{$group_info->code}}</h2>
		</header>
		<div class="panel-body">
			<div class="col-sm-6">
				<div class="table-responsive">
					<table class="table mb-none">
						<thead>
							<tr>
								<th>Цена</th>
								<th>Просмотры с</th>
								<th>Просмотры до</th>
							</tr>
						</thead>
						<tbody>
							@foreach($prices as $price)
								<tr>
									<td>{{$price->value}} р.</td>
									<td><input type="text" name="prices[{{$price->id}}][from]" class="form-control" size="3" value="{{(isset($group_prices[$price->id]['from']) ? $group_prices[$price->id]['from'] : 0)}}"></td>
									<td><input type="text" name="prices[{{$price->id}}][to]" class="form-control" size="3" value="{{(isset($group_prices[$price->id]['to']) ? $group_prices[$price->id]['to'] : 0)}}"></td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Сохранить</button>
				</div>
			</div>
		</footer>
	</form>
</section>

@endsection

@push('styles')
    
@endpush

@push('scripts')
	
@endpush
