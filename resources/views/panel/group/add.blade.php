@extends('layouts.panel')

@section('title', 'VK.POSTER - Добавить группу')

@section('h1', 'Добавить группу')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<form class="form-horizontal form-bordered" method="POST" action="{{ route('groups-save') }}" id="user-add-form">
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">Добавить группу</h2>
		</header>
		<div class="panel-body">
			<div class="form-group">
				<label class="col-md-3 control-label">URL</label>
				<div class="col-md-7">
					<div class="input-group mb-md">
						<input type="text" class="form-control" id="vk-group" name="group_url">
						<span class="input-group-btn">
							<button class="btn btn-success" type="button" id="run-token" data-loading-text="Loading...">Загрузить</button>
						</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Название</label>
				<div class="col-md-7">
					<div class="input-group input-group-icon">
						<span class="input-group-addon">
							<span class="icon"><i class="fa fa-user"></i></span>
						</span>
						<input type="text" class="form-control" placeholder="Название" name="name" required id="group-name" readonly>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Изображение</label>
				<div class="col-md-7">
					<div class="input-group input-group-icon">
						<span class="input-group-addon">
							<span class="icon"><i class="fa fa-file-image-o"></i></span>
						</span>
						<input type="text" class="form-control" placeholder="Изображение" name="thumb" required id="group-thumb" readonly>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Токен для сообщений</label>
				<div class="col-md-7">
					<input type="text" class="form-control" placeholder="Токен для сообщений" name="token_for_messages" id="group-token_for_messages">
				</div>
			</div>
			<input type="hidden" name="vktoken" value="" id="group-token">
			<input type="hidden" name="group_id" value="" id="group-id">
			<input type="hidden" name="group_code" value="" id="group-code">
			<input type="hidden" name="users" value="" id="group-users">
			<input type="hidden" name="count_post" value="" id="group-count-post">
			<input type="hidden" name="last_post_dt" value="" id="group-last-post-dt">
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit" disabled>Сохранить</button>
				</div>
			</div>
		</footer>
	</form>
</section>

<div id="dialog" class="modal-block mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Укажите ключ доступа сообщества</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="form-group">
					<div class="col-md-12">
						<div class="alert alert-warning">
							Не правильное указание <strong>ключа доступа</strong> приведет к невозможности получать статистику и добавлять записи в сообщество.
						</div>
						<div class="input-group input-group-icon">
							<span class="input-group-addon">
								<span class="icon"><i class="fa fa-key"></i></span>
							</span>
							<input type="password" class="form-control" placeholder="Ключ доступа" required id="enter-token">
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogConfirm" class="btn btn-primary">Продолжить</button>
					<button id="dialogCancel" class="btn btn-default">Отмена</button>
				</div>
			</div>
		</footer>
	</section>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/select2/select2.js') }}"></script>
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/app/group/add.js') }}"></script>
@endpush
