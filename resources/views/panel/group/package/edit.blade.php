@extends('layouts.panel')

@section('title', 'VK.POSTER - Редактировать пакет групп '.$data['package_info']->name)

@section('h1', 'Редактировать пакет групп '.$data['package_info']->name)

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<form class="form-horizontal form-bordered" method="POST" action="{{ route('packages-save-edit', $data['package_info']->id) }}" id="user-add-form">
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">Редактировать пакет групп {{$data['package_info']->name}}</h2>
		</header>
		<div class="panel-body">
			<div class="form-group">
				<label class="col-md-2 control-label">Название</label>
				<div class="col-md-10">
					<input type="text" class="form-control" placeholder="Название" name="name" required id="package-name" value="{{$data['package_info']->name}}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label">Группы</label>
				<div class="col-md-10">
					<table class="table table-hover">
					  	<thead>
						    <tr>
						      <th scope="col"></th>
						      <th scope="col">Группа</th>
						      <th scope="col">День недели</th>
						      <th scope="col">Время</th>
						    </tr>
					  	</thead>
					  	<tbody>
					  		@foreach($data['groups'] as $group)
							    <tr>
							      <th scope="row">
							      	@if(in_array($group->id, $data['in_groups']))
							      		<input class="form-check-input" type="checkbox" name="groups[{{$group->id}}][id]" value="{{$group->id}}" id="group{{$group->id}}" checked="checked">
							      	@else
							      		<input class="form-check-input" type="checkbox" name="groups[{{$group->id}}][id]" value="{{$group->id}}" id="group{{$group->id}}">
							      	@endif
							      </th>
							      <td>
							      	<label class="form-check-label" for="group{{$group->id}}">
								    	{{$group->url}}
								  	</label>
							      </td>
							      <td>
							      	@php
							      		$day = 100;
							      		$time = false;
							      		if (isset($data['selected'][$group->id])){
							      			$day = $data['selected'][$group->id]['day'];
							      			$time = $data['selected'][$group->id]['time'];
							      		}
							      	@endphp
							      	<select class="form-control" name="groups[{{$group->id}}][day]">
										<option></option>
										<option value="1" {{$day == 1 ? "selected" : ""}}>Понедельник</option>
										<option value="2" {{$day == 2 ? "selected" : ""}}>Вторник</option>
										<option value="3" {{$day == 3 ? "selected" : ""}}>Среда</option>
										<option value="4" {{$day == 4 ? "selected" : ""}}>Четверг</option>
										<option value="5" {{$day == 5 ? "selected" : ""}}>Пятница</option>
										<option value="6" {{$day == 6 ? "selected" : ""}}>Суббота</option>
										<option value="0" {{$day == '0' ? "selected" : ""}}>Воскресенье</option>
									</select>
							      </td>
							      <td>
							      	<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-clock-o"></i>
										</span>
										<input type="text" data-plugin-timepicker class="form-control" data-plugin-options='{ "showMeridian": false , "minuteStep": "1", "defaultTime": false}' name="groups[{{$group->id}}][time]" value="{{$time}}" size="4">
									</div>
							      </td>
							    </tr>
						    @endforeach
					  	</tbody>
					</table>
					{{--@foreach($data['groups'] as $group)
						<div class="form-check">
							@if(in_array($group->id, $data['in_groups']))
								<input class="form-check-input" type="checkbox" name="groups[]" value="{{$group->id}}" id="group{{$group->id}}" checked="checked">
							@else
								<input class="form-check-input" type="checkbox" name="groups[]" value="{{$group->id}}" id="group{{$group->id}}">
							@endif
						  	
						  	<label class="form-check-label" for="group{{$group->id}}">
						    	{{$group->url}}
						  	</label>
						</div>
					@endforeach--}}
				</div>
			</div>
			{{--<div class="form-group">
				<label class="col-md-3 control-label">День недели</label>
				<div class="col-md-7">
					<select class="form-control" name="day">
						<option></option>
						<option value="1" {{$data['package_info']->day == 1 ? "selected" : ""}}>Понедельник</option>
						<option value="2" {{$data['package_info']->day == 2 ? "selected" : ""}}>Вторник</option>
						<option value="3" {{$data['package_info']->day == 3 ? "selected" : ""}}>Среда</option>
						<option value="4" {{$data['package_info']->day == 4 ? "selected" : ""}}>Четверг</option>
						<option value="5" {{$data['package_info']->day == 5 ? "selected" : ""}}>Пятница</option>
						<option value="6" {{$data['package_info']->day == 6 ? "selected" : ""}}>Суббота</option>
						<option value="7" {{$data['package_info']->day == 0 ? "selected" : ""}}>Воскресенье</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">День недели</label>
				<div class="col-md-7">
					<div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-clock-o"></i>
						</span>
						<input type="text" data-plugin-timepicker class="form-control" data-plugin-options='{ "showMeridian": false , "minuteStep": "1", "defaultTime": false}' name="time" value="{{$data['package_info']->time}}">
					</div>
				</div>
			</div>--}}
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Сохранить</button>
				</div>
			</div>
		</footer>
	</form>
</section>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
	<script src="{{ asset('vendor/select2/select2.js') }}"></script>
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
    <!--<script src="{{ asset('js/app/group/package/add.js') }}"></script>-->
@endpush
