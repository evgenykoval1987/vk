@extends('layouts.panel')

@section('title', 'VK.POSTER - Добавить пакет групп')

@section('h1', 'Добавить пакет групп')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<form class="form-horizontal form-bordered" method="POST" action="{{ route('packages-save') }}" id="user-add-form">
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">Добавить пакет групп</h2>
		</header>
		<div class="panel-body">
			<div class="form-group">
				<label class="col-md-2 control-label">Название</label>
				<div class="col-md-10">
					<input type="text" class="form-control" placeholder="Название" name="name" required id="package-name">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label">Группы</label>
				<div class="col-md-10">
					<table class="table table-hover">
					  	<thead>
						    <tr>
						      <th scope="col"></th>
						      <th scope="col">Группа</th>
						      <th scope="col">День недели</th>
						      <th scope="col">Время</th>
						    </tr>
					  	</thead>
					  	<tbody>
					  		@foreach($data['groups'] as $group)
							    <tr>
							      <th scope="row">
							      	<input class="form-check-input" type="checkbox" name="groups[{{$group->id}}][id]" value="{{$group->id}}" id="group{{$group->id}}">
							      </th>
							      <td>
							      	<label class="form-check-label" for="group{{$group->id}}">
								    	{{$group->url}}
								  	</label>
							      </td>
							      <td>
							      	<select class="form-control" name="groups[{{$group->id}}][day]">
										<option></option>
										<option value="1">Понедельник</option>
										<option value="2">Вторник</option>
										<option value="3">Среда</option>
										<option value="4">Четверг</option>
										<option value="5">Пятница</option>
										<option value="6">Суббота</option>
										<option value="0">Воскресенье</option>
									</select>
							      </td>
							      <td>
							      	<div class="input-group">
										<span class="input-group-addon">
											<i class="fa fa-clock-o"></i>
										</span>
										<input type="text" data-plugin-timepicker class="form-control" data-plugin-options='{ "showMeridian": false , "minuteStep": "1", "defaultTime": false}' name="groups[{{$group->id}}][time]" value="" size="4">
									</div>
							      </td>
							    </tr>
						    @endforeach
					  	</tbody>
					</table>
					{{--@foreach($data['groups'] as $group)
						<div class="form-check">
						  	<input class="form-check-input" type="checkbox" name="groups[]" value="{{$group->id}}" id="group{{$group->id}}">
						  	<label class="form-check-label" for="group{{$group->id}}">
						    	{{$group->url}}
						  	</label>
						</div>
					@endforeach--}}
				</div>
			</div>
			{{--<div class="form-group">
				<label class="col-md-3 control-label">День недели</label>
				<div class="col-md-7">
					<select class="form-control" name="day">
						<option></option>
						<option value="1">Понедельник</option>
						<option value="2">Вторник</option>
						<option value="3">Среда</option>
						<option value="4">Четверг</option>
						<option value="5">Пятница</option>
						<option value="6">Суббота</option>
						<option value="0">Воскресенье</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">День недели</label>
				<div class="col-md-7">
					<div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-clock-o"></i>
						</span>
						<input type="text" data-plugin-timepicker class="form-control" data-plugin-options='{ "showMeridian": false , "minuteStep": "1", "defaultTime": false}' name="time" value="">
					</div>
				</div>
			</div>--}}
			
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Сохранить</button>
				</div>
			</div>
		</footer>
	</form>
</section>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
	<script src="{{ asset('vendor/select2/select2.js') }}"></script>
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
    <!--<script src="{{ asset('js/app/group/package/add.js') }}"></script>-->
@endpush
