@extends('layouts.panel')

@section('title', 'VK.POSTER - Добавить клиента')

@section('h1', 'Добавить клиента')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel col-md-8">
	<form class="form-horizontal form-bordered" method="POST" action="{{ $data['action'] }}" id="client-add-form">
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">Добавить клиента</h2>
		</header>
		<div class="panel-body">
			<div class="form-group">
				<label class="col-md-3 control-label">VK аккаунт</label>
				<div class="col-md-6">
					<div class="input-group mb-md">
						<input type="text" class="form-control" id="vk" name="client_profile" value="{{ $data['vk_profile'] }}">
						<span class="input-group-btn">
							<button class="btn btn-success" type="button" id="check-vk" data-loading-text="Loading...">Загрузить</button>
						</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Имя Фамилия</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<span class="input-group-addon">
							<span class="icon"><i class="fa fa-user"></i></span>
						</span>
						<input type="text" class="form-control" placeholder="Имя Фамилия" name="name" required id="client-name" value="{{ $data['name'] }}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Название сообщества</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<input type="text" class="form-control" placeholder="Название группы" name="group_name" id="client-group" value="{{ $data['group_name'] }}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Ссылка на сообщество</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<input type="text" class="form-control" placeholder="Ссылка на сообщество" name="group_link" id="client-group-link" value="{{ $data['group_link'] }}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Email клиента</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<span class="input-group-addon">
							<span class="icon"><i class="fa fa-envelope"></i></span>
						</span>
						<input type="text" class="form-control" placeholder="Email клиента" name="email" value="{{ $data['email'] }}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Менеджер клиента</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<select name="manager_id" class="form-control">
							<option></option>
							@foreach ($data['managers'] as $manager)
								@if($manager->id == $data['manager_id'])
									<option value="{{$manager->id}}" selected="selected">{{$manager->name}}</option>
								@else
									<option value="{{$manager->id}}">{{$manager->name}}</option>
								@endif
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Шаблоны комментариев</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<textarea class="form-control" placeholder="Шаблоны комментариев" name="comments">{{ $data['comments'] }}</textarea>
					</div>
				</div>
			</div>
			<input type="hidden" name="client_id" value="{{ $data['client_id'] }}">
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Сохранить</button>
				</div>
			</div>
		</footer>
	</form>
</section>
@if($data['client_id'])
	<section class="panel col-md-4">
		<form class="form-horizontal form-bordered" method="POST" action="{{ route('client-user-save') }}" id="client-add-form">
			{!! csrf_field() !!}
			<header class="panel-heading">
				<h2 class="panel-title">Пользователь клиента</h2>
			</header>
			<div class="panel-body">
				<div class="form-group">
					<label class="col-md-4 control-label">Логин</label>
					<div class="col-md-8">
						<div class="input-group input-group-icon">
							<span class="input-group-addon">
								<span class="icon"><i class="fa fa-user"></i></span>
							</span>
							<input type="text" class="form-control" placeholder="Логин" name="login" id="user-name" value="{{ ($data['system_info']) ? $data['system_info']->login : ''}}">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Пароль</label>
					<div class="col-md-8">
						<div class="input-group input-group-icon">
							<span class="input-group-addon">
								<span class="icon"><i class="fa fa-key"></i></span>
							</span>
							<input type="text" class="form-control" placeholder="Пароль" name="password" id="password" value="{{ ($data['system_info']) ? $data['system_info']->passwordd : ''}}">
						</div>
					</div>
				</div>
				@if($data['system_info'])
					<div>
						Адрес панели управления - <a href="http://rmocrm.top/">http://rmocrm.top/</a><br>
						Ваш логин - {{$data['system_info']->login}}<br>
						Ваш пароль - {{$data['system_info']->passwordd}}
					</div>

					@if($data['access'])
						<div class="alert alert-success" style="margin-top: 10px">
							<strong>Токен работает</strong> 
						</div>
					@else
						<div class="alert alert-warning" style="margin-top: 10px">
							<strong>Токен НЕ работает</strong>
						</div>
					@endif
				@endif
			</div>
			<footer class="panel-footer">
				<div class="row">
					<div class="col-sm-12 text-center">
						<div style="margin-bottom: 10px; color: #000; font-size: 12px">* Если пользователь уже создан то при сохранении изменится пароль</div>
						<button class="btn btn-primary" type="submit">Сохранить</button>
					</div>
				</div>
			</footer>

			<input type="hidden" name="client_id" value="{{ $data['client_id'] }}">

			
		</form>
	</section>
@endif
@endsection

@push('scripts')
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/app/client/add.js') }}?v=1"></script>
@endpush
