@extends('layouts.panel')

@section('title', 'VK.POSTER - Клиенты')

@section('h1', 'Клиенты')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Клиенты</h2>
	</header>
	<div class="panel-body">

		<div class="row">
			<div class="col-sm-12">
				<div class="mb-md">
					<a id="addToTable" class="btn btn-primary" href="{{ route('client-add') }}">Добавить <i class="fa fa-plus"></i></a>
					<a href="{{ route('client-unload') }}" class="btn btn-default"><i class="fa fa-upload"></i>&nbsp;Выгрузить клиентов</a>
					<a href="{{ route('group-unload') }}" class="btn btn-default"><i class="fa fa-upload"></i>&nbsp;Выгрузить группы</a>
				</div>
			</div>
		</div>
		@if ($data['success'])
			<div class="row">
				<div class="col-sm-12">
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						{!! $data['success'] !!}
					</div>
				</div>
			</div>
		@endif
		<table class="table table-bordered table-striped" id="client-list" data-url="{{ route('client-ajax') }}">
			<thead>
				<tr>
					<th width="0%">ID</th>
					<th width="30%">Имя Фамилия</th>
					<th width="40%">Группа</th>
					<th width="20%">Сумма заказов</th>
					<th width="20%">Менеджер</th>
					<th width="20%">Дата добавления</th>
					<th width="5%">T</th>
					<th width="5%">AOV</th>
					<th width="5%">ALT</th>
					<th width="5%">LTV</th>
					<th width="10%"></th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</section>

<div id="dialog" class="modal-block mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Вы уверены?</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<p>Вы уверены что хотите удалить клиента?</p>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogConfirm" class="btn btn-primary">Удалить</button>
					<button id="dialogCancel" class="btn btn-default">Отмена</button>
				</div>
			</div>
		</footer>
	</section>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('js/app/client/list.js') }}"></script>
@endpush
