@extends('layouts.panel')

@section('title', 'Закрепить запись')

@section('h1', 'Закрепить запись')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Закрепить запись</h2>
	</header>
	<div class="panel-body">
		@if ($data['success'])
			<div class="row">
				<div class="col-sm-12">
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						{!! $data['success'] !!}
					</div>
				</div>
			</div>
		@endif

		@if ($data['error'])
			<div class="row">
				<div class="col-sm-12">
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						{!! $data['error'] !!}
					</div>
				</div>
			</div>
		@endif
		<form class="form-inline" method="POST" action="{{ route('company-pin-add') }}">
			{!! csrf_field() !!}
			<div class="form-group">
				@if ($data['pinned'])
					<input type="text" class="form-control" placeholder="ID поста в рекламной компании" name="company_post_id" value="{{ $data['pinned']->id}}">
				@else
					<input type="text" class="form-control" placeholder="ID поста в рекламной компании" name="company_post_id">
				@endif
			</div>
			<div class="form-group">
				&nbsp;|&nbsp;
				<div class="input-group">
					<span class="input-group-addon">
						<i class="fa fa-calendar"></i>
					</span>
					<input type="text" data-plugin-datepicker class="form-control" name="pin_date">
				</div>
				<div class="input-group">
					<span class="input-group-addon">
						<i class="fa fa-clock-o"></i>
					</span>
					<input type="text" data-plugin-timepicker="" class="form-control" data-plugin-options='{ "showMeridian": false , "minuteStep": "1"}' name="pin_time">
				</div>
				<input type="checkbox" name="unpin" id="unpin" style="display: none;">
			</div>
			<button type="submit" class="btn btn-primary">Закрепить</button>&nbsp;
			<button type="submit" class="btn btn-danger" onClick="$('#unpin').prop('checked', true); $('.form-inline').submit(); return false;">Открепить</button>
		</form>
		@if ($data['pinned'])
			<div style="margin-top: 30px"><h4>Текущий закрепленный пост - {{$data['pinned']->id}}</h4></div>
			<div class="col-sm-6">
				<div class="table-responsive">
					<table class="table mb-none">
						<thead>
							<tr>
								<th>Група</th>
								<th>Дата публикации</th>
								<th>Статус</th>
								<th>Сообщение</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data['pinned']->post_groups as $post)
								<tr>
									<td>{{$post->group->url}}</td>
									<td><a href="{{$post->vk_link}}" target="_blank">{{$post->publish_date}}</a></td>
									<td>
										@if($post->pin == 1)
											Закреплен
										@else
											<span style="color: red">Ошибка</span>
										@endif
									</td>
									<td>{{$post->pin_message}}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		@endif
	</div>
</section>
@endsection

@push('styles')
	<link rel="stylesheet" href="{{ asset('vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />
	<link rel="stylesheet" href="{{ asset('vendor/select2/select2.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
	<script src="{{ asset('vendor/select2/select2.js') }}"></script>
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
@endpush
