@extends('layouts.panel')

@section('title', $data['title'])

@section('h1', $data['h1'])

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
	<form class="form-horizontal form-bordered" method="POST" action="{{route('company-post-save', $data['company_id'])}}" id="post-add-form">
		{!! csrf_field() !!}
		<input type="hidden" name="this_is_story" value="1">
		<header class="panel-heading">
			<h2 class="panel-title">{{ $data['h1'] }}</h2>
		</header>
		<div class="panel-body">
			<input type="hidden" name="post_type" value="story">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-bordered">
				<b>Текст</b>
				<div class="checkbox-custom checkbox-warning" style="float: right; display: none;">
					<input type="checkbox" id="repost" name="repost">
					<label for="repost">Репост</label>
				</div>
				<div class="form-group" style="margin-top: 10px; display: none;">
					<div class="col-md-12">
						<input class="form-control" id="repost-link" name="repost_link" placeholder="Ссылка на пост">
					</div>
				</div>
				<div class="form-group" style="margin-top: 10px">
					<div class="col-md-12">
						<select name="title" class="form-control" style="margin-bottom: 20px">
							<option value=""></option>
						    <option value="to_store">В магазин</option>
						    <option value="vote">Голосовать</option>
						    <option value="more">Ещё</option>
						    <option value="book">Забронировать</option>
						    <option value="order">Заказать</option>
						    <option value="enroll">Записаться</option>
						    <option value="fill">Заполнить</option>
						    <option value="signup">Зарегистрироваться</option>
						    <option value="buy">Купить</option>
						    <option value="ticket">Купить билет</option>
						    <option value="write">Написать</option>
						    <option value="open">Открыть</option>
						    <option value="learn_more">Подробнее (по умолчанию)</option>
						    <option value="view">Посмотреть</option>
						    <option value="go_to">Перейти</option>
						    <option value="contact">Связаться</option>
						    <option value="watch">Смотреть</option>
						    <option value="play">Слушать</option>
						    <option value="install">Установить</option>
						    <option value="read">Читать</option>
						</select>
						<input type="text" name="link" id="link" placeholder="Адрес ссылки для перехода из истории" value="{{ $data['link'] }}" class="form-control" style="margin-top: 20px">
					</div>
				</div>
				<div class="form-group" style="margin-top: 10px" style="display: none;">
					<div class="col-md-12">
						<!--<input type="text" name="title" id="title" placeholder="Название" value="{{ $data['ttitle'] }}" class="form-control" style="margin-bottom: 20px">-->
						<textarea style="display: none;" class="form-control post-description" rows="5" id="description" name="description[]" placeholder="Текст">{{ $data['description'] }}</textarea>
						<div style="display: none;" class="pull-right" style="margin-bottom: 10px"><a class="btn btn-primary more-text" ><i class="fa fa-plus"></i></a></div>
						<!--<input type="text" name="link" id="link" placeholder="Адрес ссылки для перехода из истории" value="{{ $data['link'] }}" class="form-control" style="margin-top: 20px">-->
						<input style="display: none;" type="text" name="copyright" id="copyright" placeholder="Источник" value="{{ $data['copyright'] }}" class="form-control" style="margin-top: 20px">
					</div>
				</div>
				<!--<b>Комментарии(оставить пустыми если не выполнять)</b>-->
				<div class="form-group" style="margin-top: 10px; display: none;">
					<div class="col-md-12">
						<textarea class="form-control" placeholder="Комментарии(оставить пустыми если не выполнять)" name="comments">{{$data['comments']}}</textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<div id="atach">
							<div class='vkeditor-panel'>
								<a href='{{route('attach-picture','get-form')}}?onlypc=1' class='btn btn-success' ajax-modal><i class='fa fa-file-image-o'></i></a>
								<a href="{{route('attach-video','get-form')}}?onlypc=1" class='btn btn-success' vajax-modal><i class='fa fa-file-video-o'></i></a>
		
							</div>
							<div id="attachments"></div>
						</div>
					</div>
				</div>
				@if ($data['post_id'])
					<input type="hidden" name="post_id" value="{{ $data['post_id'] }}">
				@endif
			</div>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-bordered">
				@if($data['packages'])
					<b style="margin-bottom: 10px">Пакеты групп</b>
					<div style="margin-bottom: 20px; margin-top: 10px">
						@foreach($data['packages'] as $package)
							@php
								$package_dates = [];

								$days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
								foreach($package->packagegroups as $pg){
									$package_date = false;
									if ($pg->day != '' && $pg->time != ''){
										$dayofweek = date('w', strtotime(date('d.m.Y')));
										if ($dayofweek != $pg->day){
											$package_date = date('Y-m-d', strtotime($days[$pg->day], strtotime(date('d.m.Y'))));
											$package_date = $package_date.' '.$pg->time;
										}
										else{ 
											if (strtotime($pg->time) < strtotime(date('H:i:s'))){ 
												$package_date = date('Y-m-d', strtotime($days[$pg->day].' + 1 week', strtotime(date('d.m.Y H:i:s'))));
												$package_date = $package_date.' '.$pg->time;
											}
											else{ 
												$package_date = date('Y-m-d', strtotime($days[$pg->day], strtotime(date('d.m.Y'))));
												$package_date = $package_date.' '.$pg->time;
											}
										}
										$package_dates[$pg->group_id] = $package_date;
									}
									
								}
								$package_dates = json_encode($package_dates);
								
							@endphp
							<div class="checkbox-custom checkbox-default">
								<input type="checkbox" class="group-checkbox package" data-package-date="{{$package_dates}}">
								<label>{{$package->name}}</label>
								@foreach($package->packagegroups as $pg)
									<input type="hidden" class="pg" value="{{$pg->group_id}}">
								@endforeach
							</div>
						@endforeach
					</div>
				@endif
				

				<b>Группы</b>
				<div>
					<a class="btn btn-default change_date" data-plugin-datepicker><i class="fa fa-calendar"></i></a>
					
					<a class="btn btn-warning next_date_all_days_story_seven">Разбить по 7 дней</a>
					<a class="btn btn-warning next_date_all_days_story_four">Разбить по 14 дня</a>
					<a class="btn btn-warning next_date_all_days_story_twoseven">Разбить по 27 дней</a>
				</div>
				<table class="company_posts_table">
					<tr>
						<td>
							<div class="checkbox-custom checkbox-warning">
								<input type="checkbox" id="all" class="group-checkbox">
								<label for="all">Выбрать все</label>
							</div>
						</td>
						<td>
							
						</td>
						<td>
							<div>
								<input type="checkbox" name="not_time" id="not-time"> Без времени
							</div>
							<a class="btn btn-default change_date" data-plugin-datepicker><i class="fa fa-calendar"></i></a>
							<input type="text" class="group-time" data-plugin-timepicker data-plugin-options='{ "showMeridian": false , "minuteStep": "1"}' style="width: 0px; border: 0px">
							<a class="btn btn-default change_time"><i class="fa fa-clock-o"></i></a>
							<a class="btn btn-primary next_date_all"><i class="fa fa-save"></i></a>
							<!--<a class="btn btn-warning next_date_all_days">Разбить по дням</a>-->
						</td>
					</tr>
					@foreach($data['groups'] as $group)
						<tr>
							<td>
								<div class="checkbox-custom checkbox-default">
									<input type="checkbox" id="group-{{$group['id']}}" value="{{$group['id']}}" name="group[{{$group['id']}}][name]" class="group-checkbox" data-group-id="{{$group['id']}}">
									<label for="group-{{$group['id']}}">{{$group['url']}}</label>
								</div>
							</td>	
							<td>
								<span class="group-shedule" data-shedule="{{$group['data-shedule']}}">{{$group['shedule']}}</span>
								<input class="group-shedule-val" type="checkbox" name="group[{{$group['id']}}][dt]" value="{{$group['shedule']}}" style="display: none;">
							</td>
							<td>
								
									<a class="btn btn-primary next_shedule">След</a>&nbsp; | &nbsp;
								
								<a class="btn btn-default change_date" data-plugin-datepicker><i class="fa fa-calendar"></i></a>
								<input type="text" class="group-time" data-plugin-timepicker data-plugin-options='{ "showMeridian": false , "minuteStep": "1"}' style="width: 0px; border: 0px">
								<a class="btn btn-default change_time"><i class="fa fa-clock-o"></i></a>
								<a class="btn btn-primary next_date"><i class="fa fa-save"></i></a>

							</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>

		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="button" id="send-post">Сохранить</button>
					<a class="btn btn-default" href="{{ route('company-posts', $data['company_id']) }}">Отмена</a> 
				</div>
			</div>
		</footer>
	</form>
	<div id="dialog" class="modal-block mfp-hide">
		<section class="panel">
			<header class="panel-heading">
				<h2 class="panel-title">Изминение даты публикации</h2>
			</header>
			<div class="panel-body">
				<div class="modal-wrapper">
					<div class="modal-text">

						<div class="tabs">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#shedule" data-toggle="tab" aria-expanded="true"><i class="fa fa-clock-o"></i>Расписание</a>
								</li>
								<li>
									<a href="#choose" data-toggle="tab" aria-expanded="false"><i class="fa fa-calendar"></i>&nbsp;Выбрать</a>
								</li>
							</ul>
							<div class="tab-content">
								<div id="shedule" class="tab-pane active">
									<p>
										Добавить пост в текущее рассписание 
										<span id="group-turn" style="font-weight: bold"></span>
										<button class="btn btn-primary" id="shedule-next">След</button>
									</p>
									<p class="text-center" style="margin-top: 20px">
										<button class="btn btn-primary" id="change-shedule">Изменить</button>
									</p>
								</div>
								<div id="choose" class="tab-pane">
									<p>Выбрать дату и время для публикации</p>
									<div class="row">
										<div class="col-md-6">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</span>
												<input type="text" data-plugin-datepicker class="form-control" id="publish-date" data-plugin-options='{"format": "yyyy-mm-dd", autoclose:"true" }'>
											</div>
										</div>
										<div class="col-md-6">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-clock-o"></i>
												</span>
												<input type="text" data-plugin-timepicker class="form-control" data-plugin-options='{ "showMeridian": false , "minuteStep": "1"}' id="publish-time">
											</div>
										</div>
									</div>
									<p class="text-center" style="margin-top: 20px">
										<button class="btn btn-primary" id="change-date">Изменить</button>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="panel-footer">
				<div class="row">
					<div class="col-md-12 text-right">
						<button id="dialogCancel" class="btn btn-default">Отмена</button>
					</div>
				</div>
			</footer>
		</section>
	</div>
@endsection


@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />
    <link rel="stylesheet" href="{{ asset('js/vk-editor/vk-editor.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/dropzone/css/dropzone.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
	<script src="{{ asset('vendor/dropzone/dropzone.js') }}"></script>
	<script src="{{ asset('js/vk-editor/vk-editor.js') }}"></script>
    <script src="{{ asset('js/app/company/post-add-story.js') }}?v=2"></script>
@endpush