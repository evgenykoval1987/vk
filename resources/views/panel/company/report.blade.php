@extends('layouts.panel')

@section('title', 'VK.POSTER - Итоговый отчет')

@section('h1', 'Итоговый отчет')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel" id="template">
	<header class="panel-heading">
		<h2 class="panel-title">Шаблон</h2>
	</header>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<textarea class="form-control" rows="5" id="template" placeholder="Шаблон сообщения" v-model="template"></textarea>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<input type="text" class="form-control" id="target" placeholder="Цена таргета" v-model="target">
				</div>
				<div class="form-group">
					<button class="btn btn-primary" id="save-template" @click="save()">Сохранить</button>
				</div>
			</div>
			<div class="col-sm-6">
				<p>{client} - имя клиента</p>
				<p>{views} - количество просмотров</p>
				<p>{price} - цена за 1000 показов</p>
				<p>{odds} - разница с минимальной ценой таргета</p>
				<p>{activations} - количество активносетей</p>
			</div>
		</div>
	</div>
</section>
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Итоговые отчет</h2>
	</header>
	<div class="panel-body">

		<table class="table table-bordered table-striped" id="report-list" data-url="{{ route('company-report-ajax') }}">
			<thead>
				<tr>
					<th width="5%">ID</th>
					<th width="5%">Дата создания отчета</th>
					<th width="10%">Менеджер</th>
					<th width="10%">Название РК</th>
					<th width="20%">Клиент</th>
					<th width="30%">Сообщение</th>
					<th width="10%">Скопировать в буфер</th>
					<th width="15%">Общий CPM</th>
					<th width="15%">Просмотров</th>
					<th width="15%">Активности</th>
					<th width="15%">Отчет</th>
					<th width="15%">Отписка</th>
					<th width="10%">Действие</th>
					<!--<th width="15%">Сумма заказа</th>
					<th width="10%">СPM</th>
					<th width="20%">Дата добавления</th>
					<th width="10%"></th>-->
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</section>
<div id="dialog" class="modal-block mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Вы уверены?</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<p>Вы уверены что хотите удалить отчет по рекламной компании?</p>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogConfirm" class="btn btn-primary">Удалить</button>
					<button id="dialogCancel" class="btn btn-default">Отмена</button>
				</div>
			</div>
		</footer>
	</section>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endpush

@push('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios@0.12.0/dist/axios.min.js"></script>
	<script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('js/app/company/report.js') }}"></script>
@endpush
