@extends('layouts.panel')

@section('title', 'VK.POSTER - Активные рекламные компании')

@section('h1', 'Активные рекламные компании')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Активные рекламные компании</h2>
	</header>
	<div class="panel-body">

		<table class="table table-bordered table-striped" id="company-list" data-url="{{ route('company-ajax-active') }}">
			<thead>
				<tr>
					<th width="0%">ID</th>
					<th width="30%">Название</th>
					<th width="40%">Клиент</th>
					<th width="15%">Сумма заказа</th>
					<th width="10%">СPM</th>
					<th width="10%">Просмотров</th>
					<th width="10%">Плановое число просмотров</th>
					<th width="10%">Количество публикаций план</th>
					<th width="10%">Количество публикаций факт</th>
					<th width="20%">Дата добавления</th>
					<th width="10%"></th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</section>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('js/app/company/active.js') }}"></script>
@endpush
