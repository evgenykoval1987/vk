@extends('layouts.panel')

@section('title', 'VK.POSTER - Рекламные компании')

@section('h1', $data['h1'])

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">{{$data['h1']}}</h2>
	</header>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-12">
				<div class="mb-md">
					<a id="addToTable" class="btn btn-primary" href="{{ route('company-post-add', $data['company_id']) }}">Добавить рекламный пост <i class="fa fa-plus"></i></a>
					<a id="addToTable" class="btn btn-primary" href="{{ route('company-post-add-native', $data['company_id']) }}">Добавить Нативный пост <i class="fa fa-plus"></i></a>
					<a id="addToTable" class="btn btn-primary" href="{{ route('company-post-add-story', $data['company_id']) }}">Добавить Stories <i class="fa fa-plus"></i></a>
					@if(!$data['report'])
						<a class="btn btn-success" href="{{ route('company-make-single-report', $data['company_id']) }}">Сформировать итоговый отчет</a>
					@else
						<a class="btn btn-danger" href="{{ route('company-remove-single-report', $data['company_id']) }}">Убрать итоговый отчет</a>
					@endif
				</div>
			</div>
		</div>
		@if($data['show_tags'])
			<div class="row" style="margin-bottom: 10px">
				<div class="col-sm-12">
					<span style="color: orange; font-weight: bold">Метки которые не сделаны:</span>
				</div>
			</div>
			@foreach($data['show_tags'] as $group)
				<div class="row" style="margin-bottom: 10px">
					<div class="col-sm-12">
						<div><b>{{$group['name']}} : {{count($group['items'])}}</b></div>
						<div style="color: orange; font-weight: bold">{{implode(', ', $group['items'])}}</div>
					</div>
				</div>
			@endforeach
		@endif
		@if ($data['success'])
			<div class="row">
				<div class="col-sm-12">
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						{!! $data['success'] !!}
					</div>
				</div>
			</div>
		@endif

		<table class="table table-bordered table-striped" id="company-posts-list" data-url="{{ route('company-posts-ajax', $data['company_id']) }}">
			<thead>
				<tr>
					<th width="0%">ID</th>
					<th width="10%">ID</th>
					<th width="35%">Метка</th>
					<th width="15%">Кол-во в метке</th>
					<th width="60%">Расписание выхода</th>
					<th width="10%">Дата создания</th>
					<th width="5%">Отчет</th>
					<th width="5%">Действие</th>
					<th width="5%">Перенос</th>
					<th width="5%">Копирование</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</section>

<div id="dialog" class="modal-block mfp-hide modal-block-lg">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Отчет по посту №<span id="report-post-id"></span></h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogCancel" class="btn btn-default">Выход</button>
				</div>
			</div>
		</footer>
	</section>
</div>

<div id="dialog-edit" class="modal-block mfp-hide modal-block-lg">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Изминение данных поста</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<form id="form-post-edit">
						{!! csrf_field() !!}
						<input type="hidden" name="company_post_id">
						<input type="hidden" name="company_post_group_id">
						<div class="form-group">
							<label class="col-md-3 control-label">Ссылка</label>
							<div class="col-md-6">
								<input type="text" class="form-control" placeholder="Ссылка" name="vk_link">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Просмотров</label>
							<div class="col-md-6">
								<input type="text" class="form-control" placeholder="Просмотров" name="viewed">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogEditConfirm" class="btn btn-primary">Сохранить</button>
					<button id="dialogEditCancel" class="btn btn-default">Выход</button>
				</div>
			</div>
		</footer>
	</section>
</div>

<div id="dialog-remove" class="modal-block mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Вы уверены?</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<p>Вы уверены что хотите удалить сообщество?</p>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogRemoveConfirm" class="btn btn-primary">Удалить</button>
					<button id="dialogRemoveCancel" class="btn btn-default">Отмена</button>
				</div>
			</div>
		</footer>
	</section>
</div>

<div id="modal-change-company-for-post" class="modal-block modal-header-color modal-block-warning mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Перемещение поста рекламной компании</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-icon">
					<i class="fa fa-warning"></i>
				</div>
				<div class="modal-text">
					<p>Если вы хотите переместить пост рекламной компании № <span id="copy-company-post-id">111</span> - введите ниже, ID рекламной компании куда хотите его перенести. И нажмите кнопку "Перенести"</p>
					<div class="form-group">
						<input type="text" name="to-change" id="to-change" placeholder="ID рекламной компании куда переносить пост" class="form-control">
					</div>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button class="btn btn-warning modal-confirm" id="do-transport">Переместить</button>
					<button class="btn btn-default modal-dismiss">Отмена</button>
				</div>
			</div>
		</footer>
	</section>
</div>


<div id="modal-copy-company-for-post" class="modal-block modal-header-color modal-block-primary mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Копирование поста рекламной компании</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-icon">
					<i class="fa fa-warning"></i>
				</div>
				<div class="modal-text">
					<p>Если вы хотите скопировать пост рекламной компании № <span id="company-post-id">111</span> - введите ниже, ID рекламной компании куда хотите его скопировать. И нажмите кнопку "Перенести"</p>
					<div class="form-group">
						<input type="text" name="to-copy" id="to-copy" placeholder="ID рекламной компании куда скопировать пост" class="form-control">
					</div>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button class="btn btn-primary modal-confirm" id="do-copy">Скопировать</button>
					<button class="btn btn-default modal-dismiss">Отмена</button>
				</div>
			</div>
		</footer>
	</section>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('js/app/company/post-list.js?v=13') }}"></script>
@endpush
