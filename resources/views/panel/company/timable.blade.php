@extends('layouts.panel')

@section('title', 'VK.POSTER - Расписание рекламы')

@section('h1', 'Расписание рекламы')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Расписание рекламы</h2>
	</header>
	<div class="panel-body">
		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
			<select class="form-control" id="month">
				@foreach($data['months'] as $key => $month)
					@if($month == $data['current_month'])
						<option value="{{$key}}" selected="selected">{{$month}}</option>
					@else
						<option value="{{$key}}">{{$month}}</option>
					@endif
				@endforeach
			</select>
		</div>
		<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
			<select class="form-control" id="year">
				@foreach($data['years'] as $year)
					@if($year == $data['current_year'])
						<option value="{{$year}}" selected="selected">{{$year}}</option>
					@else
						<option value="{{$year}}">{{$year}}</option>
					@endif
				@endforeach
			</select>
		</div>
		<div id="result" style="margin-top: 50px;width: 1300px; border-right: 1px solid #ccc" ></div>
	</div>
</section>

@endsection

@push('styles')
	<link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="https://cdn.datatables.net/fixedcolumns/3.3.0/js/dataTables.fixedColumns.min.js"></script>
	<script src="{{ asset('js/app/company/timable.js') }}"></script>
@endpush
