@if($data['post_info']->report == '')
	<div style="margin-bottom: 20px; text-align: right;"><a href="#" id="copy-to-bufer" data-post-id="{{$data['post_info']->id}}">Сгенерировать ссылку</a></div>
@else
	<div style="margin-bottom: 20px; text-align: right;"><a href="http://rmo-report.top?id={{$data['post_info']->report}}" target="_blank">http://rmo-report.top?id={{$data['post_info']->report}}</a></div>
@endif
<input type="text" id="hid" style="position: absolute; left: -999px">

<div class="table-responsive">
	<table class="table mb-none">
		<thead>
			<tr>
				<th>Группа</th>
				<th>Просмотры</th>
				<th>Поделились</th>
				<th>Подписались</th>
				<th>Скрыли из новостей</th>
				<th>Перешли по ссылке</th>
				<th>Лайков</th>
				<th>Статус</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data['posts'] as $post)
				<tr>
					<td>{{$post['group']}}</td>
					<td>{{$post['story_views']}}</td>
					<td>{{$post['story_shares']}}</td>
					<td>{{$post['story_subscribers']}}</td>
					<td>{{$post['story_bans']}}</td>
					<td>{{$post['story_opens']}}</td>
					<td>{{$post['story_likes']}}</td>
					<td>{{$post['status']}}</td>
					
				</tr>
			@endforeach
			<tr>
				<td colspan="2">
					<div class="text-left">
						<a href="#" class="btn btn-primary" id="recalculate-story" data-post-id="{{$data['post_info']->id}}" data-loading-text="Loading...">Пересчитать просмотры</a>
					</div>
				</td>
				<td colspan="5" class="text-right"><b>Общее количество просмотров</b></td>
				<td class="text-center"><b>{{ $data['total'] }}</b></td>
			</tr>
		</tbody>
	</table>
</div>
