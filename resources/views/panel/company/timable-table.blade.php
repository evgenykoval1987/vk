<style type="text/css">
	.my-table th, .my-table td{
		/*padding: 5px;
		border: 1px solid #ccc;*/
		vertical-align: top;
	}
	.my-table .btn-sm{
		padding: 2px 5px;
		margin-bottom: 5px;
		min-width: 150px;
		white-space: nowrap!important;
		text-align: left;
	}
	/*.my-table{
		table-layout:fixed;
    	width:100%;
	}*/
</style>
<table class="my-table table table-bordered table-striped table-responsive">
	<thead>
		<tr>
			<th style="text-align: center;">Группа</th>
			@foreach($data['dates'] as $date)
				<th style="text-align: center;">{{$date['dt']}}</th>
			@endforeach
		</tr>
		<tr>
			<th ></th>
			@foreach($data['dates'] as $date)
				<th style="text-align: center;">{{$date['day']}}</th>
			@endforeach
		</tr>
	</thead>
	<tbody>
		@foreach($data['groups'] as $group)
			<tr>
				<td style="text-align: center;" ><a href="{{$group['href']}}" target="_blank">{{$group['name']}}</a></td>
				@foreach($group['posts'] as $posts)
					@if(!$posts)
						<td></td>
					@else
						<td>
							@foreach($posts as $post)
								<a class="btn btn-success btn-sm" href="{{$post->post->company->client->vk_profile}}" target="_blank">
									{{date('H:i',strtotime($post->publish_date))}}
									-
									{{$post->post->company->client->name}}
								</a>
							@endforeach
						</td>
					@endif
					
				@endforeach
			</tr>
		@endforeach
	</tbody>
</table>




{{--<table class="table table-bordered table-condensed mb-none my-table">
	<thead>
		<tr>
			<th style="text-align: center;">Группа</th>
			@foreach($data['dates'] as $date)
				<th style="text-align: center;">{{$date['dt']}}</th>
			@endforeach
		</tr>
		<tr>
			<th></th>
			@foreach($data['dates'] as $date)
				<th style="text-align: center;">{{$date['day']}}</th>
			@endforeach
		</tr>
	</thead>
	<tbody>
		@foreach($data['groups'] as $group)
			<tr>
				<td style="text-align: center;">{{$group['name']}}</td>
				@foreach($group['posts'] as $posts)
					@if(!$posts)
						<td></td>
					@else
						<td>
							@foreach($posts as $post)
								<a class="mt-sm mb-sm btn btn-success btn-sm" href="{{$post->post->company->client->vk_profile}}" target="_blank">
									{{date('H:i',strtotime($post->publish_date))}}
									-
									{{$post->post->company->client->name}}
								</a>
							@endforeach
						</td>
					@endif
					
				@endforeach
			</tr>
		@endforeach
	</tbody>
</table>
--}}