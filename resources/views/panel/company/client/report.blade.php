@extends('layouts.panel')

@section('title', 'VK.POSTER - Мои рекламные кампании')

@section('h1', 'Мои рекламные кампании')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	
	<div class="panel-body">

		<table class="table table-bordered table-striped" id="report-list" data-url="{{ route('company-client-report-ajax') }}">
			<thead>
				<tr>
					<th width="5%">ID</th>
					<th width="10%">Название РК</th>
					<th width="15%">Общий CPM</th>
					<th width="15%">Просмотров</th>
					<th width="15%">Отчет</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</section>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endpush

@push('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios@0.12.0/dist/axios.min.js"></script>
	<script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('js/app/company/client/report.js') }}"></script>
@endpush
