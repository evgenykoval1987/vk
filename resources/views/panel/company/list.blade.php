@extends('layouts.panel')

@section('title', 'VK.POSTER - Рекламные компании')

@section('h1', 'Рекламные компании')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Рекламные компании</h2>
	</header>
	<div class="panel-body">
		
		@if ($data['success'])
			<div class="row">
				<div class="col-sm-12">
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						{!! $data['success'] !!}
					</div>
				</div>
			</div>
		@endif

		<table class="table table-bordered table-striped" id="company-list" data-url="{{ route('company-ajax') }}">
			<thead>
				<tr>
					<th width="0%">ID</th>
					<th width="30%">Название</th>
					<th width="40%">Клиент</th>
					<th width="15%">Сумма заказа</th>
					<th width="10%">СPM</th>
					<th width="10%">Просмотров</th>
					<th width="10%">Плановое число просмотров</th>
					<th width="10%">Количество публикаций план</th>
					<th width="10%">Количество публикаций факт</th>
					<th width="20%">Дата добавления</th>
					<th width="10%"></th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</section>
<div id="dialog" class="modal-block mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Вы уверены?</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<p>Вы уверены что хотите удалить рекламную компанию?</p>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogConfirm" class="btn btn-primary">Удалить</button>
					<button id="dialogCancel" class="btn btn-default">Отмена</button>
				</div>
			</div>
		</footer>
	</section>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('js/app/company/list.js?v=1') }}"></script>
@endpush
