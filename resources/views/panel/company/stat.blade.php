<div class="row">
	<div class="col-sm-8" style="text-align: center;">
		<a href="#" id="download-hrefs" onClick="copyToClipboard('{{$data['str']}}'); return false;">Скачать ссылки</a>
	</div>
	<div class="col-sm-4">
		@if($data['post_info']->report == '')
			<div style="margin-bottom: 20px; text-align: right;"><a href="#" id="copy-to-bufer" data-post-id="{{$data['post_info']->id}}">Сгенерировать ссылку</a></div>
		@else
			<div style="margin-bottom: 20px; text-align: right;"><a href="http://rmo-report.top?id={{$data['post_info']->report}}" target="_blank">http://rmo-report.top?id={{$data['post_info']->report}}</a></div>
		@endif
	</div>
</div>
 
<input type="text" id="hid" style="position: absolute; left: -999px">

<div class="table-responsive">
	<table class="table mb-none">
		<thead>
			<tr>
				<th>Группа</th>
				<th>Дата публикации</th>
				<th>Ссылка на пост</th>
				<th>Статус</th>
				<th>Просмотров</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data['posts'] as $post)
				<tr>
					<td>{{$post['group']}}</td>
					<td><a href="{{$post['url']}}" target="_blank">{{$post['publish_date']}}</a></td>
					<td>
						@if($post['showlink'])
							@if($post['sstatus'] == 0 && $post['url'] == '')
								<span style="color: red">Ошибка публикации</span>
							@else
								<a href="{{$post['url']}}" target="_blank">{{$post['url']}}</a>
							@endif
						@else
							@if($post['sstatus'] == '0')
								<span style="color: red">Ошибка публикации</span>
							@else
								Поставлен в очередь
							@endif
						@endif
					</td>
					<td>{{$post['comment']}}</td>
					<td style="text-align: center;">{{$post['viewed']}}&nbsp;<a href="#" data-id="{{$post['company_post_id']}}" data-viewed="{{$post['viewed']}}" data-vk-link="{{$post['url']}}" data-group-id="{{$post['group_id']}}" class="company-post-edit" style="margin-left: 10px"><i class="fa fa-pencil"></i></a></td>
				</tr>
			@endforeach
			<tr>
				<td colspan="2">
					<div class="text-left">
						<a href="#" class="btn btn-primary" id="recalculate" data-post-id="{{$data['post_info']->id}}" data-loading-text="Loading...">Пересчитать просмотры</a>
					</div>
				</td>
				<td colspan="2" class="text-right">
					<b>Общее количество просмотров</b>
				</td>
				<td class="text-center"><b>{{ $data['total'] }}</b></td>
			</tr>

			<tr>
				<td colspan="2"></td>
				<td colspan="2" class="text-right">
					<b>Общее количество лайков</b>
				</td>
				<td class="text-center"><b>{{ $data['likes'] }}</b></td>
			</tr>

			<tr>
				<td colspan="2"></td>
				<td colspan="2" class="text-right">
					<b>Общее количество репостов</b>
				</td>
				<td class="text-center"><b>{{ $data['reposts'] }}</b></td>
			</tr>

			<tr>
				<td colspan="2"></td>
				<td colspan="2" class="text-right">
					<b>Общее количество комментариев</b>
				</td>
				<td class="text-center"><b>{{ $data['comments'] }}</b></td>
			</tr>
		</tbody>
	</table>
</div>
