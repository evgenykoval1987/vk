@extends('layouts.panel')

@section('title', $data['title'])

@section('h1', $data['h1'])

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<form class="form-horizontal form-bordered" method="POST" action="{{ $data['action'] }}" id="company-add-form">
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">{{$data['h1']}}</h2>
		</header>
		<div class="panel-body">
			<div class="form-group">
				<label class="col-md-3 control-label">Название</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<input type="text" class="form-control" placeholder="Название" name="title" id="company-title" value="{{$data['ctitle']}}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Клиент</label>
				<div class="col-md-6">
					<select class="form-control" data-plugin-selectTwo name="client_id">
						<option></option>
						@foreach ($data['clients'] as $client)
							@if($client->id == $data['client_id'])
								<option value="{{ $client->id }}" selected="selected">{{ $client->name }}</option>
							@else
								<option value="{{ $client->id }}">{{ $client->name }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Сумма заказа</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<input type="text" class="form-control" placeholder="Сумма заказа" name="total" value="{{$data['total']}}">
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Дата заказа</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon"> 
						<input type="text" class="form-control" placeholder="Дата заказа" name="dt" value="{{$data['dt']}}" data-plugin-datepickerr autocomplete="off">
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Дата завершения рекламной кампании</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon"> 
						<input type="text" class="form-control" placeholder="Дата завершения рекламной кампании" name="dt_end" value="{{$data['dt_end']}}" data-plugin-datepickerr autocomplete="off">
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Количество просмотров</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<input type="text" class="form-control" placeholder="Количество просмотров" name="views" value="{{ $data['views'] }}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Количество постов</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<input type="text" class="form-control" placeholder="Количество постов" name="quantity" value="{{ $data['quantity'] }}">
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Пакет меток</label>
				<div class="col-md-6">
					<select class="form-control" name="tagpack_id">
						<option></option>
						@foreach ($data['tagpacks'] as $tagpack)
							@if($tagpack->id == $data['tagpack_id'])
								<option value="{{ $tagpack->id }}" selected="selected" data-quantity="{{$tagpack->quantity}}" data-views="{{$tagpack->views}}">{{ $tagpack->name }}</option>
							@else
								<option value="{{ $tagpack->id }}" data-quantity="{{$tagpack->quantity}}" data-views="{{$tagpack->views}}">{{ $tagpack->name }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label">Метки</label>
				<div class="col-md-6">
					<div class="input-group">
						<select class="form-control" name="tag" id="tag">
							@foreach($data['tags'] as $tag)
								<option value="{{$tag->id}}" data-quantity="{{$tag->quantity}}">{{$tag->name}}</option>
							@endforeach
						</select>
						<span class="input-group-btn">
							<button class="btn btn-primary" type="button" id="addTag">Добавить</button>
						</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Выбранные метки</label>
				<div class="col-md-6">
					<div class="table-responsive">
						<table class="table mb-none" id="total-tags">
							<thead>
								<tr>
									<th>Метка</th>
									<th>Удалить</th>
								</tr>
							</thead>
							<tbody>
								@if ($data['selected'])
									@foreach($data['selected'] as $item)
										<tr>
											<td>
												{{$item->tag->name}}
												<input type="hidden" value="{{$item->tag_id}}" name="tags[]">
												<input type="hidden" value="{{$item->tag->quantity}}" class="quantity">
											</td>
											<td class="actions">
												<a href="" class="delete-row"><i class="fa fa-trash-o"></i></a>
											</td>
										</tr>
									@endforeach
								@endif 
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<input type="hidden" name="company_id" value="{{$data['company_id']}}">
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Сохранить</button>
				</div>
			</div>
		</footer>
	</form>
</section>
@endsection

@push('styles')
	<link rel="stylesheet" href="{{ asset('vendor/select2/select2.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/select2/select2.js') }}"></script>
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/app/company/add.js?v=4') }}"></script>
@endpush
