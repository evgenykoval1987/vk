@extends('layouts.panel')

@section('title', 'VK.POSTER - Завершенные посты в рекламных компаниях')

@section('h1', 'Завершенные посты в рекламных компаниях')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Завершенные посты в рекламных компаниях</h2>
	</header>
	<div class="panel-body">

		<table class="table table-bordered table-striped" id="company-ended-list" data-url="{{ route('companys-end-ajax') }}">
			<thead>
				<tr>
					<th width="10%">ID</th>
					<th width="30%">Клиент</th>
					<th width="30%">Менеджер клиента</th>
					<th width="20%">Отчет</th>
					<th width="10%">Действие</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</section>

<div id="dialog" class="modal-block mfp-hide modal-block-lg">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Отчет по посту №<span id="report-post-id"></span></h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogCancel" class="btn btn-default">Выход</button>
				</div>
			</div>
		</footer>
	</section>
</div>

<div id="dialog-edit" class="modal-block mfp-hide modal-block-lg">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Изминение данных поста</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<form id="form-post-edit">
						{!! csrf_field() !!}
						<input type="hidden" name="company_post_id">
						<input type="hidden" name="company_post_group_id">
						<div class="form-group">
							<label class="col-md-3 control-label">Ссылка</label>
							<div class="col-md-6">
								<input type="text" class="form-control" placeholder="Ссылка" name="vk_link">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Просмотров</label>
							<div class="col-md-6">
								<input type="text" class="form-control" placeholder="Просмотров" name="viewed">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogEditConfirm" class="btn btn-primary">Сохранить</button>
					<button id="dialogEditCancel" class="btn btn-default">Выход</button>
				</div>
			</div>
		</footer>
	</section>
</div>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('js/app/company/company-end.js?v=11') }}"></script>
@endpush
