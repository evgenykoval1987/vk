@extends('layouts.panel')

@section('title', $data['title'])

@section('h1', $data['h1'])

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
	<form class="form-horizontal form-bordered" method="POST" action="{{ $data['action'] }}" id="post-add-form" enctype="multipart/form-data"> 
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">{{ $data['h1'] }}</h2>
		</header>
		<div class="panel-body">
			<div class="form-group">
				<label class="col-md-2 control-label">Текст</label>
				<div class="col-md-10">
					<textarea class="form-control" rows="5" id="description" name="description">{{ $data['description'] }}</textarea>
					@if ($data['slug'] == 'admin')
						<input type="text" name="link" placeholder="Ссылка под постом" value="{{ $data['link'] }}" class="form-control" style="margin-top: 20px">
					@else
						<input type="text" name="link" placeholder="Ссылка под постом" value="{{ $data['link'] }}" class="form-control" style="margin-top: 20px; display: none;">
					@endif
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<div id="atach">
						<div class='vkeditor-panel'>
							<a href='{{route('attach-picture','get-form')}}' class='btn btn-success' ajax-modal><i class='fa fa-file-image-o'></i></a> 
							<a href="{{route('attach-video','get-form')}}" class='btn btn-success' vajax-modal><i class='fa fa-file-video-o'></i></a> 
							<a href="{{route('attach-audio','get-form')}}" class='btn btn-success' aajax-modal><i class='fa fa-file-audio-o'></i></a>
							<a href="{{route('attach-playlist','get-form')}}" class='btn btn-success' lajax-modal title="Плейлист с аудио"><i class='fa fa-tasks'></i></a>
							<a href='{{route('attach-doc','get-form')}}' class='btn btn-success' dajax-modal><i class='fa fa-file-text-o'></i></a> 
						</div>
						<div id="attachments">
							@if($data['attachments'])
								@foreach($data['attachments'] as $attachment)
									<div class="image {{ ($attachment->type == 'audio-local' || $attachment->type == 'audio-vk' || $attachment->type == 'playlist-vk') ? 'image-audio' : '' }}">
										<a href="#" class="remove-item btn btn-danger"><i class="fa fa-trash-o"></i></a>
										<a href="{{$attachment->content}}" target="_blank">
											@if($attachment->type == 'video-local' || $attachment->type == 'video-youtube')
												<img src="/images/you.png">
											@elseif($attachment->type == 'audio-local' || $attachment->type == 'audio-vk' || $attachment->type == 'playlist-vk')
												<a href="#" target="_blank">{{$attachment->title}}</a>
											@else
												<img src="{{$attachment->content}}">
											@endif
											<input type="hidden" name="attachments[]" value="{{$attachment->attachment}}:::{{$attachment->content}}:::{{$attachment->type}}:::{{$attachment->title}}">
										</a>
									</div>
								@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
			
			@if ($data['post_id'])
				<input type="hidden" name="post_id" value="{{ $data['post_id'] }}">
			@endif
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Сохранить</button>
					<a class="btn btn-default" href="{{ route('posts') }}">Отмена</a>
				</div>
			</div>
		</footer>
	</form>
@endsection


@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('js/vk-editor/vk-editor.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/dropzone/css/basic.css') }}" />
	<link rel="stylesheet" href="{{ asset('vendor/dropzone/css/dropzone.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/select2/select2.js') }}"></script>
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
	<script src="{{ asset('vendor/dropzone/dropzone.js') }}"></script>
	<script src="{{ asset('js/vk-editor/vk-editor.js') }}"></script>
    <script src="{{ asset('js/app/post/add.js') }}"></script>
@endpush