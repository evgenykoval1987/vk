@extends('layouts.panel')

@section('title', 'VK.POSTER - Посты')

@section('h1', 'Посты')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Посты</h2>количество постов в ожидании : <b>{{ $data['count_created_posts'] }}</b>
	</header>
	<div class="panel-body">

		<div class="row">
			<div class="col-sm-3">
				<div class="mb-md">
					<a id="addToTable" class="btn btn-primary" href="{{ route('posts-add') }}">Добавить <i class="fa fa-plus"></i></a>
				</div>
			</div>
			@if($data['total'])
				<div class="col-sm-9">
					<div class="mb-md" style="text-align: right; font-size: 16px; color: green">
						Прибыль: {{$data['total']}} р.
						@if($data['total'] != 0 && $data['total'] > $data['min_pay'])
							<a class="btn btn-success" href="{{route('posts-pay-request')}}"><i class="fa fa-money"></i></a>
						@endif
					</div>
				</div>
			@endif
		</div>
		@if ($data['success'])
			<div class="row">
				<div class="col-sm-12">
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						{!! $data['success'] !!}
					</div>
				</div>
			</div>
		@endif
		<table class="table table-bordered table-striped" id="post-list" data-url="{{ route('posts-ajax') }}">
			<thead>
				<tr>
					<th width="0%">ID</th>
					<th width="20%">Группа</th>
					<th width="20%">Описание</th>
					<th width="10%">Статус</th>
					<th width="10%">Пользователь</th>
					<th width="15%">Дата публикации</th>
					<th width="10%">Просмотров</th>
					<th width="10%">Цена</th>
					<th width="10%">Страйк</th>
					<th width="10%">Действие</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</section>

<div id="dialog" class="modal-block mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Вы уверены?</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<p>Вы уверены что хотите удалить пост?</p>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogConfirm" class="btn btn-primary">Удалить</button>
					<button id="dialogCancel" class="btn btn-default">Отмена</button>
				</div>
			</div>
		</footer>
	</section>
</div>

<div id="strike-set" class="modal-block mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Страйк</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<div class="form-group">
						<label class="col-md-2 control-label">Страйк</label>
						<div class="col-md-10">
							<input type="checkbox" name="strike" id="strike">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">Комментарий</label>
						<div class="col-md-10">
							<textarea class="form-control" rows="2" id="strike_comment" name="strike_comment"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="strikeConfirm" class="btn btn-primary">Сохранить</button>
					<button id="strikeCancel" class="btn btn-default">Отмена</button>
				</div>
			</div>
		</footer>
	</section>
</div>

<div id="strike-info" class="modal-block mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Страйк</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text" id="strike_info_comment" style="text-align: center;">
					
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="strikeInfoCancel" class="btn btn-default">Закрыть</button>
				</div>
			</div>
		</footer>
	</section>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endpush

@push('scripts')
    <script src="{{ asset('vendor/select2/select2.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('js/app/post/list.js') }}"></script>
@endpush
