@extends('layouts.panel')

@section('title', 'VK.POSTER - Ошибки публикации')

@section('h1', 'Ошибки публикации')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-2" style="border-right: 1px solid #ccc">
				<div id="group-tree">
					<ul>
						@foreach($data['groups'] as $group)
							<li data-group="{{ $group->name }}">
								@if($group->post()->error()->count() != 0)
									<span class="pull-right label label-danger">{{$group->post()->error()->count()}}</span> 
								@endif
								{{ $group->name }}
							</li>
						@endforeach 
					</ul>
				</div>
			</div>
			<div class="col-sm-10">
				<table class="table table-bordered table-striped" id="pendings-list" data-url="{{ route('post-errors-ajax') }}">
					<thead>
						<tr>
							<th width="0%">ID</th>
							<th width="30%">Группа</th>
							<th width="30%">Описание</th>
							<th width="20%">Редактор</th>
							<th width="20%">Дата выхода</th>
							<th width="10%">Действие</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>

<div id="dialog" class="modal-block mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Вы уверены?</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<p>Вы уверены что хотите удалить пост?</p>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogConfirm" class="btn btn-primary">Удалить</button>
					<button id="dialogCancel" class="btn btn-default">Отмена</button>
				</div>
			</div>
		</footer>
	</section>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
    <style type="text/css">
    	#group-tree ul{
    		list-style: none;
    		margin-left: 0px!important;
    		padding-left: 0px!important;
    	}
    	#group-tree li{
    		padding: 2px;
    		border: 1px solid #ccc;
    		margin-bottom: 5px;
    		border-radius: 4px;
    		cursor: pointer;
    	}
    	#group-tree li.active{
    		background-color: #0088cc;
    		border-color: #0088cc;
    		color: #fff;
    	}
    	#group-tree li:hover{
    		background-color: #0088cc;
    		border-color: #0088cc;
    		color: #fff;
    	}
    </style>
@endpush

@push('scripts')
	<script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('js/app/post/pendings.js') }}"></script>
@endpush
