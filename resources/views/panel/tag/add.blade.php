@extends('layouts.panel')

@section('title', 'VK.POSTER - Добавить метку')

@section('h1', 'Добавить метку')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<form class="form-horizontal form-bordered" method="POST" action="{{ $data['action'] }}" id="tag-add-form">
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">Добавить метку</h2>
		</header>
		<div class="panel-body">
			<div class="form-group">
				<label class="col-md-3 control-label">Название</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<span class="input-group-addon">
							<span class="icon"><i class="fa fa-user"></i></span>
						</span>
						<input type="text" class="form-control" placeholder="Название" name="name" required value="{{ $data['name'] }}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Количество постов</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<span class="input-group-addon">
							<span class="icon"><i class="fa fa-user"></i></span>
						</span>
						<input type="text" class="form-control" placeholder="Количество постов" name="quantity" value="{{ $data['quantity'] }}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Группа меток</label>
				<div class="col-md-6">
					<select class="form-control" name="tag_group_id">
						<option value="0"></option>
						@foreach($data['taggroups'] as $taggroup)
							@if($taggroup->id == $data['tag_group_id'])
								<option value="{{$taggroup->id}}" selected>{{$taggroup->name}}</option>
							@else
								<option value="{{$taggroup->id}}">{{$taggroup->name}}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			<input type="hidden" name="tag_id" value="{{ $data['tag_id'] }}">
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Сохранить</button>
				</div>
			</div>
		</footer>
	</form>
</section>
@endsection

@push('scripts')
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/app/tag/add.js') }}"></script>
@endpush
