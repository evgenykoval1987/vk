@extends('layouts.panel')

@section('title', 'VK.POSTER - Новости')

@section('h1', 'Новости')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($breadcrumbs)) ? $breadcrumbs : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Новости</h2>
	</header>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-3">
				<div class="mb-md">
					<a id="addToTable" class="btn btn-primary" href="{{ route('new-add') }}">Добавить <i class="fa fa-plus"></i></a>
				</div>
			</div>
		</div>
		@if ($success)
			<div class="row">
				<div class="col-sm-12">
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						{!! $success !!}
					</div>
				</div>
			</div>
		@endif
		<table class="table table-bordered table-striped" id="news-list" data-url="{{ route('new-ajax') }}">
			<thead>
				<tr>
					<th width="10%">ID</th>
					<th width="40%">Название</th>
					<th width="40%">Текст</th>
					<th width="10%">Действие</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</section>

<div id="dialog" class="modal-block mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Вы уверены?</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<p>Вы уверены что хотите удалить новость?</p>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogConfirm" class="btn btn-primary">Удалить</button>
					<button id="dialogCancel" class="btn btn-default">Отмена</button>
				</div>
			</div>
		</footer>
	</section>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('js/app/news/list.js') }}"></script>
@endpush
