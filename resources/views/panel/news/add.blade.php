@extends('layouts.panel')

@section('title', 'VK.POSTER - Добавить новость')

@section('h1', 'Добавить новость')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($breadcrumbs)) ? $breadcrumbs : array()])
@endsection

@section('content')
<section class="panel">
	<form class="form-horizontal form-bordered" method="POST" action="{{ $action }}" id="client-add-form">
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">Добавить новость</h2>
		</header>
		<div class="panel-body">

			<div class="form-group">
				<label class="col-md-3 control-label">Название</label>
				<div class="col-md-9">
					<input type="text" class="form-control" placeholder="Название" name="title" required value="{{    $title }}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Текст</label>
				<div class="col-md-9">
					<textarea class="form-control" placeholder="Текст" name="description" required>{{ $description }}</textarea>
				</div>
			</div>
			<input type="hidden" name="new_id" value="{{ $new_id }}">
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Сохранить</button>
				</div>
			</div>
		</footer>
	</form>
</section>
@endsection

@push('scripts')
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
@endpush
