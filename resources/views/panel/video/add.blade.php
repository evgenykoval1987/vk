@extends('layouts.panel')

@section('title', 'VK.POSTER - Добавить видеоинструкцию')

@section('h1', 'Добавить видеоинструкцию')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($breadcrumbs)) ? $breadcrumbs : array()])
@endsection

@section('content')
<section class="panel">
	<form class="form-horizontal form-bordered" method="POST" action="{{ $action }}" id="client-add-form">
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">Добавить видеоинструкцию</h2>
		</header>
		<div class="panel-body">

			<div class="form-group">
				<label class="col-md-3 control-label">Название</label>
				<div class="col-md-9">
					<input type="text" class="form-control" placeholder="Название" name="title" required value="{{    $title }}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Ссылка на youtube</label>
				<div class="col-md-9">
					<input type="text" class="form-control" placeholder="Ссылка на youtube" name="href" required value="{{ $href }}">
					<div style="opacity: 0.5;">Формат ссылки: https://www.youtube.com/embed/P1CVv_JvWXU</div>
				</div>
			</div>
			<input type="hidden" name="video_id" value="{{ $video_id }}">
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Сохранить</button>
				</div>
			</div>
		</footer>
	</form>
</section>
@endsection

@push('scripts')
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/app/manager/add.js') }}"></script>
@endpush
