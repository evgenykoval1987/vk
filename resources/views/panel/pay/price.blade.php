@extends('layouts.panel')

@section('title', 'VK.POSTER - Цены')

@section('h1', 'Цены')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($breadcrumbs)) ? $breadcrumbs : array()])
@endsection

@section('content')
<section class="panel">
	<form class="form-horizontal form-bordered" method="POST" action="{{route('pays-price-edit')}}">
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">Цены</h2>
		</header>
		<div class="panel-body">
			@if ($success)
				<div class="row">
					<div class="col-sm-12">
						<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							{!! $success !!}
						</div>
					</div>
				</div>
			@endif
			<div id="prices">
				@foreach($prices as $price)
					<div class="form-group">
						<div class="col-md-3">
							<div class="input-group mb-md">
								<input type="text" class="form-control" name="prices[{{$price->id}}]" value="{{$price->value}}">
								<span class="input-group-btn"><a class="btn btn-danger remove"><i class="fa fa-minus-circle"></i></a></span>
							</div>
						</div>
					</div>
				@endforeach
			</div>
			<div class="form-group" style="margin-top: 25px">
				<div class="col-md-6">
					<a class="btn btn-primary" onClick="addPrice();"><i class="fa fa-plus-circle"></i></a>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Сохранить</button>
				</div>
			</div>
		</footer>
	</form>
</section>
	
@endsection

@push('styles')

@endpush

@push('scripts')
	<script type="text/javascript">
		function addPrice(){
			html = '<div class="form-group">';
				html += '<div class="col-md-3">';
					html += '<div class="input-group mb-md">';
						html += '<input type="text" class="form-control" name="prices[]"><span class="input-group-btn"><a class="btn btn-danger remove"><i class="fa fa-minus-circle"></i></a></span>';
					html += '</div>';
				html += '</div>';
			html += '</div>';
			$("#prices").append(html);
		}

		$(function(){
			$(document).on('click','.remove',function(){
				$(this).parent().parent().parent().parent().remove();
			})
		})
	</script>
@endpush
