@extends('layouts.panel')

@section('title', 'VK.POSTER - Выплаты')

@section('h1', 'Выплаты')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($breadcrumbs)) ? $breadcrumbs : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Выплаты</h2>
	</header>
	<div class="panel-body">
		@if ($success)
			<div class="row">
				<div class="col-sm-12">
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						{!! $success !!}
					</div>
				</div>
			</div>
		@endif
		<div class="row">
			<div class="col-sm-4">
				<form class="form-horizontal form-bordered" method="POST" action="{{route('pays-save')}}">
					{!! csrf_field() !!}
					<div class="form-group">
						
							<div class="col-sm-6"><b>Мин. сумма для выплаты</b></div>
							<div class="col-sm-6">
								<div class="input-group mb-md">
									<input type="text" class="form-control" id="setting-min-pay" value="{{$min_pay}}" name="setting_min_pay">
									<span class="input-group-btn">
										<button class="btn btn-success" type="submit" id="setting-min-pay-save" data-loading-text="Loading...">Сохранить</button>
									</span>
								</div>
							</div>
						
					</div>
				</form>
			</div>
			<div class="col-sm-8">
				<div class="mb-md" style="text-align: right">
					<a class="btn btn-primary" href="#" id="refresh"><i class="fa fa-refresh"></i>&nbsp;Обновить</a>
				</div>
			</div>
		</div>
		<table class="table table-bordered table-striped table-responsive" id="pays-list" data-url="{{ route('pays-ajax') }}">
			<thead>
				<tr>
					<th width="0%">ID</th>
					<th width="20%">Редактор</th>
					<th width="20%">Дата</th>
					<th width="15%">Количество постов</th>
					<th class="text-right" width="15%">Сумма</th>
					<th class="text-center" width="20%">Статус</th>
					<th class="text-center" width="10%">Действие</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</section>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endpush

@push('scripts')
    <script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('js/app/pay/list.js') }}"></script>
@endpush
