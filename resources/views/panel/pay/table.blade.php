@extends('layouts.panel')

@section('title', 'VK.POSTER - Сводная таблица')

@section('h1', 'Сводная таблица')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($breadcrumbs)) ? $breadcrumbs : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Сводная таблица</h2>
	</header>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table mb-none">
				<thead>
					<tr>
						<th>Группа</th>
						@foreach($prices as $price)
							<th>{{$price->value}} р.</th>
						@endforeach
					</tr>
				</thead>
				<tbody>
					@foreach($groups as $group)
						<tr>
							<td>{{$group['url']}}</td>
							@foreach($prices as $key => $price)
								@if($key == 0)
									<td>{{(isset($group['prices'][$price->id]['from']) ? $group['prices'][$price->id]['from'] : 0)}}</td>
								@else
									<td>{{(isset($group['prices'][$price->id]['from']) ? $group['prices'][$price->id]['from'] : 0)}}</td>
								@endif
							@endforeach
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>

@endsection

@push('styles')
    
@endpush

@push('scripts')
	
@endpush
