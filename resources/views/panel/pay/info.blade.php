@extends('layouts.panel')

@section('title', 'VK.POSTER - Счет №'.$pay_info->id)

@section('h1', 'Счет №'.$pay_info->id)

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($breadcrumbs)) ? $breadcrumbs : array()])
@endsection

@section('content')
<section class="panel">
	<div class="panel-body">
		<div class="invoice">
			<header class="clearfix">
				<div class="row">
					<div class="col-sm-6 mt-md">
						<h2 class="h2 mt-none mb-sm text-dark text-bold">Счет на выплату #{{$pay_info->id}}</h2>
					</div>
				</div>
			</header>
			<div class="bill-info">
				<div class="row">
					<div class="col-md-6">
						<div class="bill-to">
							<p class="h5 mb-xs text-dark text-semibold">
								Редактор: {{$pay_info->user->name}}<br>
								Кошелек: {{$pay_info->user->wallet}}
							</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="bill-data text-right">
							<p class="mb-none">
								<span class="text-dark">Дата:</span>
								<span class="value" style="width: 200px">{{$pay_info->dt}}</span>
							</p>
						</div>
					</div>
				</div>
			</div>
		
			<div class="table-responsive">
				<table class="table invoice-items">
					<thead>
						<tr class="h4 text-dark">
							<th id="cell-id" class="text-semibold">#</th>
							<th id="cell-desc" class="text-semibold">Ссылка</th>
							<th id="cell-price" class="text-center text-semibold">Дата</th>
							<th id="cell-qty" class="text-center text-semibold">Просмотров</th>
							<th id="cell-total" class="text-center text-semibold">Цена</th>
						</tr>
					</thead>
					<tbody>
						@foreach($pay_info->posts as $post)
							<tr>
								<td>{{$post->post_id}}</td>
								<td><a href="{{$post->post->vk_link}}" target="_blank">{{$post->post->vk_link}}</a></td>
								<td class="text-center">{{$post->post->publish_date}}</td>
								<td class="text-center">{{$post->post->viewed}}</td>
								<td class="text-center">{{$post->post->cost}} р.</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		
			<div class="invoice-summary">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-8">
						<table class="table h5 text-dark">
							<tbody>
								<tr class="h4">
									<td colspan="2">Сумма к оплате</td>
									<td class="text-left">{{$pay_info->total}} р.</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="text-right mr-lg">
			<a href="{{route('pays')}}" class="btn btn-default">Отмена</a>
			<a href="{{route('pays-paid',$pay_info->id)}}" class="btn btn-primary">Оплатить</a>
		</div>
	</div>
</section>
@endsection

@push('styles')

@endpush

@push('scripts')

@endpush
