@extends('layouts.panel')

@section('title', 'VK.POSTER - Мои выплаты')

@section('h1', 'Мои выплаты')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Мои выплаты</h2>
	</header>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-striped table-condensed mb-none">
				<thead>
					<tr>
						<th>Дата</th>
						<th>Количество постов</th>
						<th class="text-right">Сумма</th>
						<th class="text-center">Статус</th>
					</tr>
				</thead>
				<tbody>
					@foreach($pays as $pay)
						<tr>
							<td>{{$pay['dt']}}</td>
							<td>{{$pay['count']}}</td>
							<td class="text-right">{{$pay['total']}} р.</td>
							<td class="text-center">
								@if($pay['status'] == 1)
									<span>В процессе</span>
								@else
									<span style="color:green">Оплачен</span>
								@endif
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>

@endsection

@push('styles')
    
@endpush

@push('scripts')
    
@endpush
