@extends('layouts.panel')

@section('title', 'VK.POSTER - Напоминания')

@section('h1', 'Напоминания')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')

@if($data['reminders'])
	<div class="row">
		<div class="col-sm-12">
			@foreach($data['reminders'] as $reminder)
				<div class="alert alert-success">
					<button type="button" class="close close-reminder" data-dismiss="alert" aria-hidden="true" data-id="{{$reminder->id}}">×</button>
					<p>Дата - {{date('d.m.Y', strtotime($reminder->dt))}}</p>
					<p>Клиент - {{$reminder->client->name}}</p>
					@if($reminder->manager)
						<p>Менеджер - {{$reminder->manager->name}}</p>
					@endif
					<p>{{$reminder->text}}</p>
				</div>
			@endforeach
		</div>
	</div>
@endif
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Напоминания</h2>
	</header>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-3">
				<div class="mb-md">
					<a id="addToTable" class="btn btn-primary" href="{{ route('reminder-add') }}">Добавить <i class="fa fa-plus"></i></a>
				</div>
			</div>
		</div>
		@if ($data['success'])
			<div class="row">
				<div class="col-sm-12">
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						{!! $data['success'] !!}
					</div>
				</div>
			</div>
		@endif
		<table class="table table-bordered table-striped" id="reminder-list" data-url="{{ route('reminder-ajax') }}">
			<thead>
				<tr>
					<th width="0%">ID</th>
					<th width="20%">Напоминание</th>
					<th width="25%">Клиент</th>
					<th width="15%">Менеджер</th>
					<th width="15%">Дата</th>
					<th width="15%">Статус</th>
					<th width="5%"></th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</section>

<div id="dialog" class="modal-block mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Вы уверены?</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<p>Вы уверены что хотите удалить напоминание?</p>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogConfirm" class="btn btn-primary">Удалить</button>
					<button id="dialogCancel" class="btn btn-default">Отмена</button>
				</div>
			</div>
		</footer>
	</section>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endpush

@push('scripts')
    <script src="{{ asset('vendor/select2/select2.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('js/app/reminder/list.js') }}"></script>
@endpush
