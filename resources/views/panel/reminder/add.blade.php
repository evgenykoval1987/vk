@extends('layouts.panel')

@section('title', 'VK.POSTER - Добавить напоминание')

@section('h1', 'Добавить напоминание')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<form class="form-horizontal form-bordered" method="POST" action="{{ route('reminder-save') }}" id="user-add-form">
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">Добавить напоминание</h2>
		</header>
		<div class="panel-body">
			<div class="form-group">
				<label class="col-md-3 control-label">Клиент</label>
				<div class="col-md-6">
					<select data-plugin-selectTwo class="form-control populate" name="client_id" required>
						@foreach ($data['clients'] as $client)
							<option value="{{ $client->id }}">{{ $client->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Менеджер</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<select name="manager_id" class="form-control">
							<option></option>
							@foreach ($data['managers'] as $manager)
								<option value="{{$manager->id}}">{{$manager->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Сообщение</label>
				<div class="col-md-6">
					<textarea class="form-control" placeholder="Сообщение" name="text" required></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Дата</label>
				<div class="col-md-6">
					<div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</span>
						<input type="text" data-plugin-datepicker class="form-control" id="publish-date" data-plugin-options='{ "format": "yyyy-mm-dd", language: "ru", autoclose:"true", locale: "ru" }' name="dt">
					</div>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Сохранить</button>
				</div>
			</div>
		</footer>
	</form>
</section>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/select2/select2.js') }}"></script>
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/app/user/add.js') }}"></script>
@endpush
