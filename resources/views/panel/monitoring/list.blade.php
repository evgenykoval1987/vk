@extends('layouts.panel')

@section('title', 'VK.POSTER - Мониторинг групп')

@section('h1', 'Мониторинг групп')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
@if ($success)
	<div class="row">
		<div class="col-sm-12">
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				{!! $success !!}
			</div>
		</div>
	</div>
@endif
<div class="tabs">
	<ul class="nav nav-tabs tabs-primary">
		<li class="active">
			<a href="#list" data-toggle="tab" aria-expanded="true">Список постов</a>
		</li>
		<li class="">
			<a href="#edit" data-toggle="tab" aria-expanded="false">Настройки</a>
		</li>
	</ul>
	<div class="tab-content">
		<div id="list" class="tab-pane active">
			<div class="row">
				<div class="col-sm-12">
					<div class="mb-md" style="text-align: right">
						<a class="btn btn-primary" href="#" id="refresh"><i class="fa fa-refresh"></i>&nbsp;Обновить</a>
						<a class="btn btn-danger" href="{{ route('monitoring-remove-all') }}"><i class="fa fa-trash"></i>&nbsp;Очистить</a>
					</div>
				</div>
			</div>
			<table class="table table-bordered table-striped table-responsive" id="monitoring-list" data-url="{{ route('monitoring-ajax') }}">
				<thead>
					<tr>
						<th width="0%">ID</th>
						<th width="25%">Группа</th>
						<th width="30%">Ссылка</th>
						<th width="10%">Запрос</th>
						<th width="30%">Текст</th>
						<th width="15%">Дата</th>
						<th width="0%">Статус</th>
						<th width="10%">Действие</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
		</div>
		<div id="edit" class="tab-pane">
			<form class="form-horizontal form-bordered" method="POST" action="{{route('monitoring-save')}}">
				{!! csrf_field() !!}
				<div class="form-group">
					<label for="tags-inputtags-input" class="col-md-3 control-label">Ключевые слова для поиска</label>
					<div class="col-md-9">
						<input name="config[monitoring_keywords]" id="tags-input" data-role="tagsinput" data-tag-class="label label-primary" class="form-control" value="{{$monitoring_keywords}}" style="display: none;">
						<p>Введите ключевое слово и нажмите <code>Enter</code></p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="groups">Группы для мониторинга</label>
					<div class="col-md-9">
						<textarea class="form-control" rows="6" name="config[monitoring_groups]">{{$monitoring_groups}}</textarea>
						<p>Каждая группа с новой строки</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="groups">Не учитывать группы</label>
					<div class="col-md-9">
						<textarea class="form-control" rows="6" name="config[monitoring_not_groups]">{{$monitoring_not_groups}}</textarea>
						<p>Каждая группа с новой строки</p>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-3"></div>
					<div class="col-md-9 text-center">
						<button class="btn btn-primary">Сохранить</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" />
@endpush

@push('scripts')
    <script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
	<script src="{{ asset('js/app/monitoring/list.js') }}"></script>
@endpush
