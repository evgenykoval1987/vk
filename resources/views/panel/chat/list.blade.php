@extends('layouts.panel')

@section('title', 'VK.POSTER - Чаты')

@section('h1', 'Чаты')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Чаты</h2>
	</header>
	<div class="panel-body">
		@if ($data['role_id'] == 2)
			<div style="text-align: center;">
				<div class="alert alert-success">
					По вопросам подборок и оплаты и другим вопросам писать: <strong>Артёму Хоменко</strong> или <strong>Андрею Пиорунски</strong>.
				</div>
			</div>
		@endif
		<div class="messaging" id="vue-app">
      		<div class="inbox_msg">
        		<div class="inbox_people">
          			<div class="inbox_chat">
          				<div v-for="user in users">
				            <div class="chat_list" :class="{active_chat:user.id == active_user.id}" @click="active_user = user; active_user_id = user.id; scroll(); unreaded();">
				              	<div class="chat_people">
					                <div class="chat_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
					                <div class="chat_ib">
					                  	<h5>@{{user.name}} <span class="chat_date label label-success" v-if="user.unreaded != 0">@{{user.unreaded}}</span></h5>
					                  	<p>@{{user.last_message}}</p>
					                </div>
				              	</div>
				            </div>
			        	</div>
          			</div>
        		</div>
        		<div class="mesgs" v-show="active_user">
          			<div class="msg_history" id="history">
          				<div v-for="message in getmessages.messages">
          					<div class="incoming_msg" v-if="message.class == 'incoming_msg'">
				              	<div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
				              	<div class="received_msg">
				                	<div class="received_withd_msg">
				                  		<p>@{{message.text}}</p>
				                  		<span class="time_date">@{{message.date}}</span>
				                  	</div>
				              	</div>
				            </div>
				            <div class="outgoing_msg" v-if="message.class == 'outgoing_msg'">
				              	<div class="sent_msg">
				                	<p>@{{message.text}}</p>
				                  	<span class="time_date">@{{message.date}}</span>
				                </div>
				            </div>
          				</div>
          			</div>
          			<div class="type_msg">
            			<div class="input_msg_write">
			              	<input type="text" class="write_msg" placeholder="Введите текст сообщения" v-model="message" @keyup.enter="send"/>
			              	<button class="msg_send_btn" type="button" @click="send"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
            			</div>
          			</div>
        		</div>
      		</div>
      	</div>
	</div>
</section>
@endsection

@push('styles')
	<link rel="stylesheet" href="{{ asset('css/app/chat/chat.css') }}" />
@endpush
@push('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vue"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios@0.12.0/dist/axios.min.js"></script>
	<script src="{{ asset('js/app/chat/list.js') }}"></script>
@endpush
