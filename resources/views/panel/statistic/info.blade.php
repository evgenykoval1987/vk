@extends('layouts.panel')

@section('title', 'VK.POSTER - Статистика')

@section('h1', 'Статистика')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Статистика</h2>
	</header>
	<div class="panel-body">
		<form class="form-inline" id="refresh-form" action="{{route('statistic-ajax')}}">
			{!! csrf_field() !!}
	  		<div class="form-group mb-2">
	    		<label for="date_from">Дата с</label>&nbsp;
	    		<input type="text" class="form-control" id="date_from" value="{{$date_from}}" datepicker-from autocomplete="off" name="date_from">
	  		</div>
	  		<div class="form-group mx-sm-3 mb-2">
	    		<label for="date_to">Дата по</label>&nbsp;
	    		<input type="text" class="form-control" id="date_to" value="{{$date_to}}"  datepicker-to autocomplete="off" name="date_to">
	  		</div>
	  		<a class="btn btn-primary mb-2" id="refresh"><i class="fa fa-refresh"></i>&nbsp;Обновить</a>
		</form>
		<table class="table table-striped" style="margin-top: 50px">
		  	<tbody>
		    	<tr>
		    		<th scope="row">1</th>
		      		<th scope="row">Новые клиенты (Сумма заказов)</th>
		      		<td id="p1"></td>
		    	</tr>
		    	<tr>
		    		<th scope="row">2</th>
		      		<th scope="row">Старые клиенты (Сумма заказов)</th>
		      		<td id="p2"></td>
		    	</tr>
		    	<tr>
		    		<th scope="row">3</th>
		      		<th scope="row">Количество отписок из сообщений в группе</th>
		      		<td id="p3"></td>
		    	</tr>
		    	<tr>
		    		<th scope="row">4</th>
		      		<th scope="row">Количество отписок из предложки</th>
		      		<td id="p4"></td>
		    	</tr>
		    	<tr>
		    		<th scope="row">5</th>
		      		<th scope="row">Количество отписок из мониторинга</th>
		      		<td id="p5"></td>
		    	</tr>
		    	<tr>
		    		<th scope="row">6</th>
		      		<th scope="row">Количество сброшенных отчетов клиенту</th>
		      		<td id="p6"></td>
		    	</tr>
		    	<tr>
		    		<th scope="row">7</th>
		      		<th scope="row">Общий средний СPM</th>
		      		<td id="p7"></td>
		    	</tr>
		    	<tr>
		    		<th scope="row">8</th>
		      		<th scope="row">Средний чек</th>
		      		<td id="p8"></td>
		    	</tr>
		    	<tr>
		    		<th scope="row">9</th>
		      		<th scope="row">Количество заказов</th>
		      		<td id="p9"></td>
		    	</tr>
		  	</tbody>
		</table>
	</div>

</section>
@endsection

@push('scripts')
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/app/statistic/info.js') }}"></script>
    <script type="text/javascript">
    	$(function(){
    		$('[datepicker-from]').datepicker({
				format: "yyyy-mm-dd", 
				language: "ru", 
				autoclose:"true", 
				locale: "ru"
			});
			$('[datepicker-to]').datepicker({
				format: "yyyy-mm-dd", 
				language: "ru", 
				autoclose:"true", 
				locale: "ru"
			});
    	})
    </script>
@endpush
