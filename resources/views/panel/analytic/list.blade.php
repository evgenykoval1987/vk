@extends('layouts.panel')

@section('title', 'VK.POSTER - Анализ рекламных записей')

@section('h1', 'Анализ рекламных записей')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Анализ рекламных записей</h2>
	</header>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-3">
				<div class="mb-md">
					<a class="btn btn-primary" href="#" id="add-analytic">Добавить <i class="fa fa-plus"></i></a>
				</div>
			</div>
		</div>

		<table class="table table-bordered table-striped" id="analytic-list" data-url="{{ route('analytic-ajax') }}">
			<thead>
				<tr>
					<th width="5%">ID</th>
					<th width="30%">Группа</th>
					<th width="10%">Начальная запись</th>
					<th width="10%">Конечная запись</th>
					<th width="10%">Текущая запись</th>
					<th width="10%">Количество постов</th>
					<th width="10%">VK ID</th>
					<th width="20%"></th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</section>

<div id="dialog" class="modal-block mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Вы уверены?</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<p>Вы уверены что хотите удалить группу со всей ее статистикой?</p>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogConfirm" class="btn btn-primary">Удалить</button>
					<button id="dialogCancel" class="btn btn-default">Отмена</button>
				</div>
			</div>
		</footer>
	</section>
</div>


<div id="dialog-add" class="modal-block mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Добавить</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<form id="add-form">
						{!! csrf_field() !!}
						<div class="form-group">
							<label class="col-md-3 control-label">Группа</label>
							<div class="col-md-9">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" placeholder="Группа" name="name">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">ID Группы в VK</label>
							<div class="col-md-9">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" placeholder="ID Группы в VK" name="vk_id">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Начальный пост</label>
							<div class="col-md-9">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" placeholder="Начальный пост" name="start" value="1">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Конечный пост</label>
							<div class="col-md-9">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" placeholder="Конечный пост" name="end" value="1">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Текущий пост</label>
							<div class="col-md-9">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" placeholder="Конечный пост" name="current" value="1">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogAddConfirm" class="btn btn-primary">Добавить</button>
					<button id="dialogAddCancel" class="btn btn-default">Отмена</button>
				</div>
			</div>
		</footer>
	</section>
</div>

<div id="dialog-edit" class="modal-block mfp-hide">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Редактирование</h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<form id="edit-form">
						{!! csrf_field() !!}
						<input type="hidden" name="id">
						<div class="form-group">
							<label class="col-md-3 control-label">Группа</label>
							<div class="col-md-9">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" placeholder="Группа" name="name">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">ID Группы в VK</label>
							<div class="col-md-9">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" placeholder="ID Группы в VK" name="vk_id">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Начальный пост</label>
							<div class="col-md-9">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" placeholder="Начальный пост" name="start">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Конечный пост</label>
							<div class="col-md-9">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" placeholder="Конечный пост" name="end">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Текущий пост</label>
							<div class="col-md-9">
								<div class="input-group input-group-icon">
									<input type="text" class="form-control" placeholder="Конечный пост" name="current" >
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogEditConfirm" class="btn btn-primary">Сохранить</button>
					<button id="dialogEditCancel" class="btn btn-default">Отмена</button>
				</div>
			</div>
		</footer>
	</section>
</div>


<style type="text/css">
	#dialog-play .col-sm-3{
		text-align: center;
	}
	#dialog-play .col-sm-3 .col-title{
		font-size: 16px;
	}
	#dialog-play .col-sm-3 .col-text{
		font-size: 18px;
		font-weight: bold;
	}
</style>
<div id="dialog-play" class="modal-block mfp-hide modal-block-lg modal-header-color modal-block-primary">
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title">Анализатор группы <span id="play-name"></span></h2>
		</header>
		<div class="panel-body">
			<div class="modal-wrapper">
				<div class="modal-text">
					<div class="col-sm-3">
						<div class="col-title">Начальная запись</div>
						<div class="col-text" id="start" style="color: #0088cc"></div>
					</div>
					<div class="col-sm-3">
						<div class="col-title">Конечная запись</div>
						<div class="col-text" id="end" style="color: #0088cc"></div>
					</div>
					<div class="col-sm-3">
						<div class="col-title">Текущая запись</div>
						<div class="col-text" id="current" style="color: #51b451"></div>
					</div>
					<div class="col-sm-3">
						<div class="col-title">Количество постов</div>
						<div class="col-text" id="quantity" style="color: #d2322d"></div>
					</div>
					<div class="col-sm-12">
						<div style="margin-top: 50px; text-align: center;">
							<button class="btn btn-success" id="begin"><i class="fa fa-play" style="color: #fff"></i>&nbsp; Запустить</button>
						</div>
					</div>
					<input type="hidden" id="date_parser" value="">
					<input type="hidden" id="analytic-id" value="">

					<div class="col-sm-12">
						<div class="progress progress-striped light active m-md" id="progress" style="display: none;">
							<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
								<span>60%</span>
							</div>
						</div>
						<div style="margin-top: 50px; height: 400px; overflow-y: scroll; border: 1px solid #ccc" id="play-result"></div>
					</div>
				</div>
			</div>
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button id="dialogPlayCancel" class="btn btn-default">Закрыть</button>
				</div>
			</div>
		</footer>
	</section>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('js/app/analytic/list.js') }}"></script>
@endpush
