@foreach($results as $key => $result)
	<div>
		<div><b>[{{$key}}]<b></div>
		@foreach($result as $item)
			<div><a href="{{$item->link}}" target="_blank">{{$item->link}}</a></div>
		@endforeach
	</div>
@endforeach