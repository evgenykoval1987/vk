@extends('layouts.panel')

@section('title', 'VK.POSTER - Добавить пакет меток')

@section('h1', 'Добавить пакет меток')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<form class="form-horizontal form-bordered" method="POST" action="{{ $data['action'] }}" id="taggroup-add-form">
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">Добавить группу меток</h2>
		</header>
		<div class="panel-body">
			<div class="form-group">
				<label class="col-md-3 control-label">Название</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<span class="input-group-addon">
							<span class="icon"><i class="fa fa-user"></i></span>
						</span>
						<input type="text" class="form-control" placeholder="Название" name="name" required value="{{ $data['name'] }}">
					</div>
				</div>
			</div>
			
			<input type="hidden" name="taggroup_id" value="{{ $data['taggroup_id'] }}">
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Сохранить</button>
				</div>
			</div>
		</footer>
	</form>
</section>
@endsection

@push('scripts')
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/app/taggroup/add.js') }}"></script>
@endpush
