@extends('layouts.panel')

@section('title', 'VK.POSTER - Предложка')

@section('h1', 'Предложка')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Предложка</h2>
	</header>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-12">
				<div class="mb-md" style="text-align: right">
					<a class="btn btn-primary" href="#" id="refresh"><i class="fa fa-refresh"></i>&nbsp;Обновить</a>
					<a class="btn btn-danger" href="{{ route('sentences-remove-all') }}"><i class="fa fa-trash-o"></i>&nbsp;Очистить все</a>
				</div>
			</div>
		</div>
		<table class="table table-bordered table-striped table-responsive" id="sentences-list" data-url="{{ route('sentences-ajax') }}">
			<thead>
				<tr>
					<th width="0%">ID</th>
					<th width="20%">Пользователь</th>
					<th width="20%">Группа</th>
					<th width="25%">Сообщение</th>
					<th width="15%">Дата</th>
					<th width="30%">Действие</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</section>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endpush

@push('scripts')
    <script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('js/app/sentence/list.js') }}"></script>
@endpush
