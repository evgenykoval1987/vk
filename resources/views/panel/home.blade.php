
@extends('layouts.panel')

@section('title', 'VK.POSTER - Главная')

@section('h1', 'Главная')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
	<div style="text-align: right;">
		@if($items)
			<a id="check-audio-token" class="btn btn-danger" href="#"><i class="fa fa-refresh"></i> Проверить аудио токен</a>
			<a id="refresh-audio-token" class="btn btn-primary" href="#"><i class="fa fa-refresh"></i> Обновить аудио токен</a>
		@endif

		<a id="check-token" class="btn btn-danger" href="#"><i class="fa fa-refresh"></i> Проверить токен</a>
		<a id="refresh-token" class="btn btn-primary" href="#"><i class="fa fa-refresh"></i> Обновить токен</a>
		<a id="refresh-stat" class="btn btn-success" href="#"><i class="fa fa-refresh"></i> Обновить отчет</a></a>
	</div>
	@if($leftposts)
		<h4 style="color: red; text-align: right;">{{$leftposts}}</h4>
	@endif
	
	@if($news)
		<div class="row" style="margin-top: 50px;">
			<header class="panel-heading">
				<h2 class="panel-title">Важные новости по ранжированию записей</h2>
			</header>
			<div class="panel-body">
				<ul class="simple-post-list">
					@foreach($news as $new)
						<li>
							<div class="post-info">
								<a href="#">{{$new->title}}</a>
								<div class="post-meta">
									{{ date('d.m.Y H:i', strtotime($new->created_at)) }}
								</div>
								<div class="post-meta" style="font-size: 16px;">
									{!! nl2br($new->description)!!}
								</div>
							</div>
						</li>
					@endforeach
				</ul>
				
			</div>
		</div>
	@endif
	@if($videos)
		<div class="row" style="margin-top: 50px;">

			<header class="panel-heading">
				<h2 class="panel-title">Обучающие видео по работе с сервисом</h2>
			</header>
			<div class="panel-body">
				@foreach($videos as $video)
					<div class="col-sm-4">
						<h3 style="text-align: center">{{$video->title}}</h3>
						<iframe width="100%" height="300px" src="{{$video->href}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				@endforeach
			</div>
		</div>
	@endif
	@if($items)
	<section class="panel" style="margin-top: 20px">
		<header class="panel-heading">
			<h2 class="panel-title">Статистика</h2>
		</header>
		<div class="panel-body">
			<table class="table table-striped" style="margin-top: 50px">
				<thead>
					<tr>
						<th>Месяца</th>
						<th>Новые клиенты (Сумма заказов)</th>
						<th>Старые клиенты (Сумма заказов)</th>
						<th>Средний чек</th>
						<th>Количество заказов</th>
						<th>Общий средний СPM</th>
						<th>Количество отписок из сообщений в группе</th>
						<th>Количество отписок из предложки</th>
						<th>Количество отписок из мониторинга</th>
						<th>Количество сброшенных отчетов клиенту</th>
						<th>Количество отписок итогового отчета</th>
					</tr>
				</thead>
			  	<tbody>
			    	@foreach ($items as $item)
			    		<tr>
			    			<td>{{$item['month']}}</td>
			    			<td>{{$item['p1']}}</td>
			    			<td>{{$item['p2']}}</td>
			    			<td>{{$item['p8']}}</td>
			    			<td>{{$item['p9']}}</td>
			    			<td>{{$item['p7']}}</td>
			    			<td>{{$item['p3']}}</td>
			    			<td>{{$item['p4']}}</td>
			    			<td>{{$item['p5']}}</td>
			    			<td>{{$item['p6']}}</td>
			    			<td>{{$item['p10']}}</td>
			    		</tr>
			    	@endforeach
			  	</tbody>
			</table>

			<span>Для запуска скрипта по просчету итогового отчету - <a href="http://rmocrm.top/company-make-reports" target="_blank">http://rmocrm.top/company-make-reports</span>
		</div>
	</section>
	@endif
@endsection
