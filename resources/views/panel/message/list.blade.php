@extends('layouts.panel')

@section('title', 'VK.POSTER - Сообщения')

@section('h1', 'Сообщения')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Сообщения</h2>
	</header>
	<div class="panel-body">
		@if ($success)
			<div class="row">
				<div class="col-sm-12">
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						{!! $success !!}
					</div>
				</div>
			</div>
		@endif
		@if ($error)
			<div class="row">
				<div class="col-sm-12">
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						{!! $error !!}
					</div>
				</div>
			</div>
		@endif
		<div class="row">
			<div class="col-sm-12">
				<div class="mb-md" style="text-align: right">
					<a class="btn btn-warning" href="{{ route('answer') }}"><i class="fa fa-share"></i>&nbsp;Заготовки ответов</a>
					<a class="btn btn-primary" href="#" id="refresh"><i class="fa fa-refresh"></i>&nbsp;Обновить</a>
					<a class="btn btn-danger" href="{{ route('messages-remove-all') }}"><i class="fa fa-trash-o"></i>&nbsp;Очистить все</a>
				</div>
			</div>
		</div>
		<table class="table table-bordered table-striped table-responsive" id="messages-list" data-url="{{ route('messages-ajax') }}">
			<thead>
				<tr>
					<th width="0%">ID</th>
					<th width="10%">Группа</th>
					<th width="20%">Пользователь</th>
					<th width="50%">Сообщение</th>
					<th width="15%">Дата</th>
					<th width="10%">Действие</th>
					<th width="0%">Статус</th>
					<th width="0%">Ответ</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</section>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endpush

@push('scripts')
    <script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('js/app/message/list.js') }}"></script>
@endpush
