@extends('layouts.panel')

@section('title', 'VK.POSTER - Ответ на сообщение')

@section('h1', 'Ответ на сообщение')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<form class="form-horizontal form-bordered" method="POST" action="{{ route('messages-answer') }}" id="user-add-form">
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">Ответ на сообщение</h2>
		</header>
		<div class="panel-body">
			<b>Текст сообщения</b>

			<input type="hidden" name="message_id" value="{{$message_id}}">
			<div class="form-group">
				<div class="col-md-12">
					<textarea class="form-control" rows="5" id="answer" name="answer" placeholder="Введите ответ на сообщение"></textarea>
				</div>
			</div>
			<div class="answers">
				@if($answers)
					<b>Заготовки ответов</b>
					@foreach ($answers as $answer)
						<div class="form-check">
						  	<input class="form-check-input" type="checkbox" value="{{$answer->description}}">
						  	<label class="form-check-label">
						    	{{$answer->text}}
						  	</label>
						</div>
					@endforeach
				@endif
			</div>
			
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Отправить</button>
				</div>
			</div>
		</footer>
		<div class="panel-body">
			<div class="timeline timeline-simple mt-xlg mb-md">
				<div class="tm-body">
					<div class="tm-title">
						<h3 class="h5 text-uppercase">История сообщений</h3>
					</div>
					<ol class="tm-items">
						@foreach($messages as $message)
							<li>
								<div class="tm-box">
									<p class="text-muted mb-none">
										<a href="https://vk.com/id{{$message['user_id']}}" class="im-mess-stack--lnk" title="" target="_blank">{{$message['name']}}</a>
										<a href="https://vk.com/gim{{$message['group_id']}}?sel={{$message['user_id']}}" class="_im_mess_link">{{$message['dt']}}</a>
									</p>
									<p>
										{{$message['text']}}
									</p>
								</div>
							</li>
						@endforeach
					</ol>
				</div>
			</div>
		</div>
	</form>
</section>
@endsection

@push('styles')
@endpush

@push('scripts')
<script type="text/javascript">
	$(function(){
		$(".answers input").change(function(){
			var val = $("#answer").val();
			
			if ($(this).prop('checked') == true){
				var r = '';
				if (val != '')
					r = '\r';
				$("#answer").val(val+r+$(this).val());
			}
			else{
				$("#answer").val(val.replace($(this).val(), ''));
			}
		})
	})
</script>
@endpush
