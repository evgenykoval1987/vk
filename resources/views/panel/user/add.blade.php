@extends('layouts.panel')

@section('title', $data['title'])

@section('h1', $data['h1'])

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<form class="form-horizontal form-bordered" method="POST" action="{{ $data['action'] }}" id="user-add-form">
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">{{$data['h1']}}</h2>
		</header>
		<div class="panel-body">
			
			<div class="tabs tabs-primary">
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#data" data-toggle="tab">Данные</a>
					</li>
					@if($data['id'] != 0)
						<li>
							<a href="#shedule" data-toggle="tab">Рассписание по группам</a>
						</li>
					@endif
				</ul>
				<div class="tab-content">
					<div id="data" class="tab-pane active">
						@if($data['role_id'])
							<div class="form-group" style="text-align: center;">
								<b>Если не хотите менять пароль - не заполняйте их.</b>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-3 control-label">VK аккаунт</label>
							<div class="col-md-6">
								<div class="input-group mb-md">
									<input type="text" class="form-control" id="vk" value="{{$data['vk_account']}}">
									<span class="input-group-btn">
										<button class="btn btn-success" type="button" id="check-vk" data-loading-text="Loading...">Загрузить</button>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Имя</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-user"></i></span>
									</span>
									<input type="text" class="form-control" placeholder="Имя" name="name" required id="user-name" value="{{$data['name']}}">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Логин</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-user"></i></span>
									</span>
									<input type="text" class="form-control" placeholder="Логин" name="login" required id="user-login" value="{{$data['login']}}">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Пароль</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-key"></i></span>
									</span>
									<input type="password" class="form-control" placeholder="Пароль" name="password" id="password">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Пароль еще раз</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-key"></i></span>
									</span>
									<input type="password" class="form-control" placeholder="Пароль еще раз" name="confirm">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Роль</label>
							<div class="col-md-6">
								<select data-plugin-selectTwo class="form-control populate" name="role_id">
									@foreach ($data['roles'] as $role)
										@if($role->id == $data['role_id'])
											<option value="{{ $role->id }}" selected="selected">{{ $role->name }}</option>
										@else
											<option value="{{ $role->id }}">{{ $role->name }}</option>
										@endif
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Кошелек</label>
							<div class="col-md-6">
								<div class="input-group input-group-icon">
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-money"></i></span>
									</span>
									<input type="text" class="form-control" placeholder="Кошелек" name="wallet" id="wallet" " value="{{$data['wallet']}}">
								</div>
							</div>
						</div>
						<input type="hidden" name="vk_id" id="user-vk-id" value="{{$data['vk_id']}}">

					</div>
					@if($data['id'] != 0)
					<div id="shedule" class="tab-pane">
						<div style="font-size: 10px; text-align: right; margin-bottom: 30px">* - добавление и удаление рассписания происходит в реальном времени. Нажимать кнопку сохранить не нужно.</div>
						<input type="hidden" v-model="user_id" value="{{$data['id']}}" id="user_id">
						<div class="form-group">
							<label class="col-md-1 control-label" for="inputSuccess">Группа</label>
							<div class="col-md-3">
								<select class="form-control input-sm mb-md" v-model="group">
							    	<option v-for="group in selects" :value="group.id">@{{group.name}}</option>
							    </select>
							</div>

							<div class="col-md-2">
								<select class="form-control input-sm mb-md" v-model="time">
									<option v-for="shedule in times" :value="shedule.id">@{{shedule.value}}</option>
								</select>
							</div>
							<div class="col-md-1">
								<button type="button" class="btn btn-primary" @click="addShedule()">Добавить</button>
							</div>
						</div>
						<div class="row" v-if="items">
							<div v-for="item in items" style="margin-bottom: 20px;" class="user-shedule-container col-sm-12">
								<div class="user-shedule-group-title">@{{item.name}}</div>
								<div style="margin-top: 5px">
									<div class="col-xs-1 col-sm-1 col-md-1" v-for="shedule in item.shedule" style="margin-bottom: 5px;">
										<div class="shedule-item" @dblClick="removeShedule(shedule.id)">@{{shedule.value}}</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row" v-if="items.length <= 0">
							<div class="col-sm-12">Данному пользователю не назначено рассписания.</div>
						</div>
					</div>
					@endif
				</div>
			</div>
			
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Сохранить</button>
				</div>
			</div>
		</footer>
	</form>
</section>
@endsection


@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />
@endpush


@push('scripts')
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/axios@0.12.0/dist/axios.min.js"></script>
	<script src="{{ asset('vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>

	<script src="{{ asset('vendor/select2/select2.js') }}"></script>
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
	@if($data['role_id'])
		<script src="{{ asset('js/app/user/edit.js') }}"></script>
	@else
    	<script src="{{ asset('js/app/user/add.js') }}"></script>
    @endif

    <script src="{{ asset('js/app/user/vue.js') }}"></script>
@endpush
