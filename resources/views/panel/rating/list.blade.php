@extends('layouts.panel')

@section('title', 'VK.POSTER - Статистика редакторов')

@section('h1', 'Статистика редакторов')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Статистика редакторов</h2>
	</header>
	<div class="panel-body">
		<table class="table table-striped" id="post-list" data-url="{{ route('rating-ajax') }}">
			<thead>
				<tr>
					<th width="20%">Редактор</th>
					<th width="20%" style="text-align: center;">Созданные посты</th>
					<th width="20%" style="text-align: center;">Опубликованные посты</th>
					<th width="20%" style="text-align: center;">Заработок</th>
					<th width="25%" style="text-align: center;">Страйки</th>
				</tr>
			</thead>
			<tbody>
				@foreach($editors as $editor)
					<tr>
						<td width="20%">{{$editor['name']}}</th>
						<td width="20%" style="text-align: center;">{{$editor['count']}}</th>
						<td width="20%" style="text-align: center;">{{$editor['publish']}}</th>
						<td width="20%" style="text-align: center;">{{$editor['cost']}}</th>
						<td width="25%" style="text-align: center;">{{$editor['strike']}}</th>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
	<script src="{{ asset('js/app/rating/list.js') }}"></script>
@endpush
