@extends('layouts.panel')

@section('title', 'VK.POSTER - Добавить пакет меток')

@section('h1', 'Добавить пакет меток')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : array()])
@endsection

@section('content')
<section class="panel">
	<form class="form-horizontal form-bordered" method="POST" action="{{ $data['action'] }}" id="tagpack-add-form">
		{!! csrf_field() !!}
		<header class="panel-heading">
			<h2 class="panel-title">Добавить пакет меток</h2>
		</header>
		<div class="panel-body">
			<div class="form-group">
				<label class="col-md-3 control-label">Название</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<span class="input-group-addon">
							<span class="icon"><i class="fa fa-user"></i></span>
						</span>
						<input type="text" class="form-control" placeholder="Название" name="name" required value="{{ $data['name'] }}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Количество просмотров</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<input type="text" class="form-control" placeholder="Количество просмотров" name="views" value="{{ $data['views'] }}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Количество постов</label>
				<div class="col-md-6">
					<div class="input-group input-group-icon">
						<input type="text" class="form-control" placeholder="Количество постов" name="quantity" value="{{ $data['quantity'] }}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Метки</label>
				<div class="col-md-6">
					<div class="input-group">
						<select class="form-control" name="tag" id="tag">
							@foreach($data['tags'] as $tag)
								<option value="{{$tag->id}}" data-quantity="{{$tag->quantity}}">{{$tag->name}}</option>
							@endforeach
						</select>
						<span class="input-group-btn">
							<button class="btn btn-primary" type="button" id="addTag">Добавить</button>
						</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Выбранные метки</label>
				<div class="col-md-6">
					<div class="table-responsive">
						<table class="table mb-none" id="total-tags">
							<thead>
								<tr>
									<th>Метка</th>
									<th>Удалить</th>
								</tr>
							</thead>
							<tbody>
								@if ($data['selected'])
									@foreach($data['selected'] as $item)
										<tr>
											<td>
												{{$item->tag->name}}
												<input type="hidden" value="{{$item->tag_id}}" name="tags[]">
												<input type="hidden" value="{{$item->tag->quantity}}" class="quantity">
											</td>
											<td class="actions">
												<a href="" class="delete-row"><i class="fa fa-trash-o"></i></a>
											</td>
										</tr>
									@endforeach
								@endif 
								<!--<tr>
									<td>1</td>
									<td class="actions">
										<a href="" class="delete-row"><i class="fa fa-trash-o"></i></a>
									</td>
								</tr>
								<tr>
									<td>2</td>
									<td class="actions">
										<a href="" class="delete-row"><i class="fa fa-trash-o"></i></a>
									</td>
								</tr>
								<tr>
									<td>3</td>
									<td class="actions">
										<a href="" class="delete-row"><i class="fa fa-trash-o"></i></a>
									</td>
								</tr>-->
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<input type="hidden" name="tagpack_id" value="{{ $data['tagpack_id'] }}">
		</div>
		<footer class="panel-footer">
			<div class="row">
				<div class="col-sm-12 text-center">
					<button class="btn btn-primary" type="submit">Сохранить</button>
				</div>
			</div>
		</footer>
	</form>
</section>
@endsection

@push('scripts')
	<script src="{{ asset('vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/app/tagpack/add.js') }}"></script>
@endpush
