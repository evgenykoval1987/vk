@extends('layouts.panel')

@section('title', 'VK.POSTER - Посты')

@section('h1', 'Посты')

@section('breadcrumbs')
    @include('partials.breadcrumbs',['breadcrumbs' => (isset($breadcrumbs)) ? $breadcrumbs : array()])
@endsection

@section('content')
<section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title">Посты</h2>количество постов в ожидании : <b>{{ $count_created_posts }}</b>
	</header>
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<form class="form-bordered ">
					<div class="form-group" style="text-align: center;">
						<select class="form-control" style="width: 250px; display: unset;" onchange="document.location.href = this.value">
							<option value="{{route('manage')}}?user_id=0">-- Выберите редактора --</option>
							@foreach($users as $user)
								@if ($user->id == $user_id)
									<option value="{{route('manage')}}?user_id={{$user->id}}" selected="selected">{{$user->name}}</option>
								@else
									<option value="{{route('manage')}}?user_id={{$user->id}}">{{$user->name}}</option>
								@endif
							@endforeach
						</select>
					</div>
					<div class="form-group">
					</div>
				</form>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"></div>
		</div>
		@if ($post)
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-bordered">
					<form id="post-body" method="post" action="{{ route('posts-save-ajax') }}">
						{!! csrf_field() !!}
						<div class="form-group">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									# {{ $post->id }}
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<i class="fa fa-user" aria-hidden="true"></i>
									&nbsp; {{ $post->user->name }}
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<i class="fa fa-clock-o" aria-hidden="true"></i>
									&nbsp; {{ date('d-m-Y H:i', strtotime($post->created_at)) }}
								</div>
							</div>
						</div>
						<div class="form-group">
							<input type="hidden" name="post_id" id="post_id" value="{{ $post->id }}">
							<input type="hidden" name="group_id" id="group_id" value="{{ $post->group_id }}">
							<textarea class="form-control" rows="5" id="description" name="description" required>{{ $post->description }}</textarea>
							<input type="text" name="link" placeholder="Ссылка под постом" value="{{ $post->link }}" class="form-control" style="margin-top: 20px; display: none;">
						</div>
						<div class="form-group">
							<div class="">
								<div id="atach">
									<div class='vkeditor-panel'>
										<a href='{{route('attach-picture','get-form')}}' class='btn btn-success' ajax-modal><i class='fa fa-file-image-o'></i></a> 
										<a href="{{route('attach-video','get-form')}}" class='btn btn-success' vajax-modal><i class='fa fa-file-video-o'></i></a> 
										<a href="{{route('attach-audio','get-form')}}" class='btn btn-success' aajax-modal><i class='fa fa-file-audio-o'></i></a>
										<a href="{{route('attach-playlist','get-form')}}" class='btn btn-success' lajax-modal title="Плейлист с аудио"><i class='fa fa-tasks'></i></a>
									</div>
									<div id="attachments">
										@if($post->attachments)
											@foreach($post->attachments as $attachment)
												<div class="image {{ ($attachment->type == 'audio-local' || $attachment->type == 'audio-vk') ? 'image-audio' : 'image-manage' }}">
													<a href="#" class="remove-item btn btn-danger"><i class="fa fa-trash-o"></i></a>
													<a href="{{$attachment->content}}" target="_blank">
														@if($attachment->type == 'video-local' || $attachment->type == 'video-youtube')
															<img src="/images/you.png">
														@elseif($attachment->type == 'audio-local' || $attachment->type == 'audio-vk')
															
														@else
															<img src="{{$attachment->content}}">
														@endif
														{{$attachment->title}}
														<input type="hidden" name="attachments[]" value="{{$attachment->attachment}}:::{{$attachment->content}}:::{{$attachment->type}}:::{{$attachment->title}}">
													</a>
												</div>
											@endforeach
										@endif
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="form-group">
						<select class="form-control mb-md" id="post-group">
							<option></option>
							@foreach ($groups as $group)
								@if($group->id == $group_id)
									<option value="{{ $group->id }}" selected="selected">{{ $group->name }}</option>
								@else
									<option value="{{ $group->id }}">{{ $group->name }}</option>
								@endif
							@endforeach
						</select>
						@foreach ($groups as $group)
							@if ($group->post()->lastEnded()->count() != 0)
								<div class="group-stat" id="group-stat-{{ $group->id }}">Последний пост {{ $group->post()->lastEnded()->first()->publish_date }}<br></div>
							@else
								<div class="group-stat" id="group-stat-{{ $group->id }}">В данной группе еще нету опубликованных постов.</div>
							@endif
						@endforeach
					</div>
					<div class="form-group">
						<div class="tabs">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#shedule" data-toggle="tab" aria-expanded="true"><i class="fa fa-clock-o"></i>&nbsp;В расписание</a>
								</li>
								<li>
									<a href="#now" data-toggle="tab" aria-expanded="false"><i class="fa fa-share-alt"></i>&nbsp;Сейчас</a>
								</li>
								<li>
									<a href="#choose" data-toggle="tab" aria-expanded="false"><i class="fa fa-calendar"></i>&nbsp;Выбрать</a>
								</li>
							</ul>
							<div class="tab-content">
								<div id="shedule" class="tab-pane active">
									<p>Добавить пост в текущее рассписание <span id="group-turn" style="font-weight: bold"></span></p>
									<div class="checkbox-custom checkbox-default">
										<input type="checkbox" id="user_shedule" name="user_shedule" value="1" checked="checked">
										<label for="user_shedule">Добавить в рассписание редактора</label>
									</div>
									<input type="hidden" name="user_id" id="user_id" value="{{$user_id}}">
									<div class="checkbox-custom checkbox-default">
										<input type="checkbox" id="novk" name="novk" value="1">
										<label for="novk">Не отправлять в очередь VK</label>
									</div>
									<p class="text-center" style="margin-top: 20px">
										<button class="btn btn-primary" id="publish_later">Добавить</button>
									</p>
								</div>
								<div id="now" class="tab-pane">
									<p>Опубликовать пост прямо сейчас</p>
									<p class="text-center">
										<button class="btn btn-primary" id="publish">Опубликовать</button>
									</p>
								</div>
								<div id="choose" class="tab-pane">
									<p>Выбрать дату и время для публикации</p>
									<div class="row">
										<div class="col-md-6">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</span>
												<input type="text" data-plugin-datepicker class="form-control" id="publish-date" data-plugin-options='{ "format": "yyyy-mm-dd", language: "ru", autoclose:"true", locale: "ru" }'>
											</div>
										</div>
										<div class="col-md-6">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-clock-o"></i>
												</span>
												<input type="text" data-plugin-timepicker class="form-control" data-plugin-options='{ "showMeridian": false , "minuteStep": "1"}' id="publish-time">
											</div>
										</div>
									</div>
									<div class="checkbox-custom checkbox-default" style="margin-top: 10px">
										<input type="checkbox" id="novkd" name="novkd" value="1">
										<label for="novkd">Не отправлять в очередь VK</label>
									</div>
									<p class="text-center" style="margin-top: 20px">
										<button class="btn btn-primary" id="publish_vk_later">Опубликовать</button>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<p class="text-center">
							<a class="btn btn-warning" href="{{$skip}}">Пропустить</a>
						</p>
					</div>
				</div>
			</div>
		@else
			<p>Нет постов для публикации</p>
		@endif
	</div>
</section>
@endsection

@push('styles')
	<link rel="stylesheet" href="{{ asset('vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}" />
	<link rel="stylesheet" href="{{ asset('js/vk-editor/vk-editor.css') }}" />
	<link rel="stylesheet" href="{{ asset('vendor/dropzone/css/dropzone.css') }}" />
@endpush

@push('scripts')
	<script src="{{ asset('vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
	<script src="{{ asset('js/jquery.form.js') }}"></script>
	<script src="{{ asset('vendor/dropzone/dropzone.js') }}"></script>
	<script src="{{ asset('js/vk-editor/vk-editor.js') }}"></script>
	<script src="{{ asset('js/app/manage/main.js') }}"></script>
@endpush
